#include "MglResourceManager.h"

#include "MglGl.h"
#include "MglTexture.h"
#include "MglUtils.h"
#include "SOIL.h"

using namespace std;
using namespace foundation;
using namespace Mgl;

namespace Mgl
{

ResourceManager::ResourceManager()
{
}

ResourceManager::~ResourceManager()
{
}

ResourceManager& ResourceManager::Instance()
{
  static ResourceManager theInstance;
  return (theInstance);
}

bool ResourceManager::init()
{
  bool success = true;

  return (success);
}

void ResourceManager::close()
{
  for (map<wstring, Texture*>::iterator iter = mTextures.begin(); iter != mTextures.end(); ++iter)
  {
    Texture* texture = iter->second;

    if (texture)
    {
      glDeleteTextures(1, &texture->mTextureId);
      texture->mTextureId = 0;

      delete texture;
    }
  }

  if (mTextures.size() > 0)
    mTextures.clear();
  
  for (map<wstring, GeometryInfo>::iterator iter = mGeometryInfos.begin(); iter != mGeometryInfos.end(); ++iter)
  {
    GeometryInfo& info = iter->second;

    if (info.vao > 0)
    {
      glDeleteBuffers(GeometryInfo::MAX_VBOS, &info.vbos[0]);
      glDeleteBuffers(1, &info.vao);
    }
  }

  if (mGeometryInfos.size() > 0)
    mGeometryInfos.clear();
}

bool ResourceManager::addTexture(const wstring& _textureTag, const wstring& _fileName)
{
  bool success = false;

  if (mTextures.find(_fileName) == mTextures.end())
  {
    int width = 0;
    int height = 0;
    int channels = 0;

    unsigned char* imageData = SOIL_load_image(toString(_fileName).c_str(), &width, &height, &channels, SOIL_LOAD_AUTO);

    if (imageData)
    {
      wstring textureTag = _textureTag;
      Texture* texture = new Texture;
 
      mTextures[_textureTag] = texture;

      unsigned int textureId = 0;

      glGenTextures(1, &textureId);
        
      if (textureId > 0)
      {
        glBindTexture(GL_TEXTURE_2D, textureId);
      
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
        
        if (channels == 4)
          glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, imageData);
        else
          glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, imageData);

        glBindTexture(GL_TEXTURE_2D, 0);

        texture->mTextureId = textureId;
        texture->mWidth     = width;
        texture->mHeight    = height;

        success = true;
      }

      SOIL_free_image_data(imageData);
      imageData = NULL;
    }
  }

  return (success);
}

Texture* ResourceManager::getTexture(const wstring& _textureTag)
{
  Texture* texture = 0;

  if (mTextures.find(_textureTag) != mTextures.end())
  {
    texture = mTextures[_textureTag];
  }

  return (texture);
}

bool ResourceManager::addGeometry(const wstring& _tag, int _dataSize, Vertex* _vertexData, int _indexSize, unsigned int* _indexData)
{
  bool success = false;
  
  if (mGeometryInfos.find(_tag) == mGeometryInfos.end())
  {
    GeometryInfo& info = mGeometryInfos[_tag];

    if (info.vao == 0)
    {
      glGenBuffers(1, &info.vao);
      glGenBuffers(2, &info.vbos[0]);
      glGenBuffers(1, &info.ibo);
    }
  
    if (info.vao > 0)
    {
      /////////////////////////////////////////////////////////////////////////

      //glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, info.ibo);
      //glBufferData(GL_ELEMENT_ARRAY_BUFFER, _dataSize * sizeof(GLubyte), _vertexData, GL_STATIC_DRAW);

      /////////////////////////////////////////////////////////////////////////
      // Assign and describe the data.
      glBindVertexArray(info.vao);

        // Allocate the space on the GPU.
        glBindBuffer(GL_ARRAY_BUFFER, info.vbos[0]);
        glBufferData(GL_ARRAY_BUFFER, _dataSize * sizeof(Vertex), _vertexData, GL_STATIC_DRAW);

        // 'Define' the data layout
        glBindBuffer(GL_ARRAY_BUFFER, info.vbos[0]);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *)offsetof(Vertex, v));

        //glBindBuffer(GL_ARRAY_BUFFER, info.vbos[0]);
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *)offsetof(Vertex, t));
        //glEnableVertexAttribArray(1);
        //glBindBuffer(GL_ARRAY_BUFFER, info.vbos[1]);
        //glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Point), NULL);
        //glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, info.ibo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, _indexSize * sizeof(GLuint), _indexData, GL_STATIC_DRAW);

        //glEnableClientState(GL_TEXTURE_COORD_ARRAY);
      glBindVertexArray(0);
      /////////////////////////////////////////////////////////////////////////

      //glEnableClientState(GL_TEXTURE_COORD_ARRAY);
      //glEnableClientState(GL_COLOR_ARRAY);
    }
    success = true;
  }

  return (success);
}

unsigned ResourceManager::getGeometry(const wstring& _tag)
{
  unsigned vao = 0;

  if (mGeometryInfos.find(_tag) != mGeometryInfos.end())
  {
    GeometryInfo& info = mGeometryInfos[_tag];
    vao = info.vao;
  }

  return (vao);
}

}