#include "MglTexture.h"

#include "MglGl.h"

using namespace std;
using namespace foundation;
using namespace Mgl;

Texture::Texture() :
  mTextureId(0),
  mWidth(0),
  mHeight(0)
{
}

Texture::~Texture()
{
}

void Texture::bind()
{
  glBindTexture(GL_TEXTURE_2D, mTextureId);
}

void Texture::unbind()
{
  glBindTexture(GL_TEXTURE_2D, 0);
}