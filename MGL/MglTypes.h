#pragma once

#include <iostream>
#include <string>
#include <windows.h>

#include "collection_types.h"
#include "math_types.h"

#define ID_INVALID USHRT_MAX

typedef unsigned ID;

namespace Mgl
{

enum
{
  EVENT_INIT,
  EVENT_CLOSE,
  
  EVENT_SHOW,
  EVENT_HIDE,

  EVENT_KEY_INPUT,
  EVENT_MOUSE_INPUT,

  EVENT_MOVE, // handleEvent(EVENT_MOVE, EventMoveData);
  EVENT_SIZE, // handleEvent(EVENT_SIZE, EventSizeData);
  EVENT_WINDOW_REFRESH, // handleEvent(EVENT_WINDOW_REFRESH, 0);

  EVENT_COUNT
};

struct EventHeader
{
  int      type;
  unsigned length;
  BYTE*    data;
};

struct EventSizeData
{
  int x;
  int y;
  int width;
  int height;
};

struct FileInfo
{
  FileInfo() : fileSize(0), directory(false) { memset(fileName, 0, sizeof(wchar_t) * MAX_PATH); }
  FileInfo(const std::wstring& _fileName, int64_t _fileSize, bool _directory) : fileSize(_fileSize), directory(_directory) { wcscpy_s(fileName, MAX_PATH, _fileName.c_str()); }
  ~FileInfo() {}

  wchar_t fileName[MAX_PATH];
  int64_t fileSize;
  bool    directory;
};

struct EventMoveData
{
  int x;
  int y;
};

struct Event
{
  ID id;
};

struct GuiEvent
{
  ID  id;
  int event;
};

struct CameraInfo
{
  CameraInfo() { x = y = width = height = 0.0f; }
  float x, y;
  float width, height;
};

enum
{
  MOUSE_BUTTON1 = 0x0001,
  MOUSE_BUTTON2 = 0x0002,
  MOUSE_BUTTON3 = 0x0004,
  MOUSE_BUTTON4 = 0x0008,
  MOUSE_BUTTON5 = 0x0010,
};

enum
{
  MOUSE_EVENT_BUTTON_DOWN,
  MOUSE_EVENT_BUTTON_UP,
  MOUSE_EVENT_MOVE,
  MOUSE_EVENT_COUNT
};

enum
{
  SPECIAL_KEY_SHIFT    = 0x0001, // VK_SHIFT
  SPECIAL_KEY_CONTROL  = 0x0002, // VK_CONTROL
  SPECIAL_KEY_ALT      = 0x0004, // VK_MENU
  SPECIAL_KEY_NUMLOCK  = 0x0008, // VK_NUMLOCK
  SPECIAL_KEY_CAPSLOCK = 0x0010, // VK_CAPITAL
};

struct EventDataKeyInput
{
  int  key;
  int  specialKey;
  bool down;
};

struct EventDataMouseInput
{
  int  event;
  int  button;
  int  x;
  int  y;
  int  wheel;
  bool down;
  bool initialDown;
};

}
