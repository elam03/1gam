#pragma once

class MglApplication;

#include "MglEventSystem.h"

class AppState
{
public:
  AppState();
  virtual ~AppState();

  bool pushEvent(int _event, void* _eventData = 0, unsigned _eventDataSize = 0);

  virtual bool handleEvent(int _event, void* _eventData = 0);
  virtual bool processEvents();
  virtual void render();
  virtual void update(double _deltaTime);

  MglApplication* mApplication;
  EventSystem mEventSystem;
  foundation::Array<Mgl::EventHeader*> mEvents;
};