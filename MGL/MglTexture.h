#pragma once

#include "MglTypes.h"

namespace Mgl
{

class Texture
{
public:
  Texture();
  virtual ~Texture();

  void bind();
  void unbind();

  unsigned mTextureId;
  int      mWidth;
  int      mHeight;
};

};