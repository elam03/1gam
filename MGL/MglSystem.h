#pragma once

#include "MglTypes.h"

#include <vector>

typedef unsigned ID;

//#define MAX_OBJECTS 16
//#define INDEX_MASK 0xf
//#define NEW_OBJECT_ID_ADD 0x10

#define MAX_OBJECTS  (64 * 1024)
#define INDEX_MASK 0xffff
#define NEW_OBJECT_ID_ADD 0x10000

struct Index {
  ID id;
  unsigned short index;
  unsigned short next;
};

template<typename T>
struct System
{
  unsigned _num_objects;
  T _objects[MAX_OBJECTS];
  Index _indices[MAX_OBJECTS];
  unsigned short _freelist_enqueue;
  unsigned short _freelist_dequeue;

  System() {
    reset();
  }
  
  void reset() {
    _num_objects = 0;
    for (unsigned i=0; i<MAX_OBJECTS; ++i) {
      _indices[i].id = i;
      _indices[i].next = i+1;
    }
    _freelist_dequeue = 0;
    _freelist_enqueue = MAX_OBJECTS-1;
  }

  inline unsigned getNumObjects() const {
    return (_num_objects);
  }

  inline bool has(ID id) {
    if (id == ID_INVALID) return false;
    Index &in = _indices[id & INDEX_MASK];
    return in.id == id && in.index != ID_INVALID;
  }

  inline T &lookup(ID id) {
    return _objects[_indices[id & INDEX_MASK].index];
  }

  inline ID add() {

    if (_num_objects < (MAX_OBJECTS - 1)) {
      Index &in = _indices[_freelist_dequeue];
      _freelist_dequeue = in.next;
      in.id += NEW_OBJECT_ID_ADD;
      in.index = _num_objects++;
      T &o = _objects[in.index];
      o.id = in.id;
      return o.id;
    }
    else {
      return ID_INVALID;
    }
  }

  inline void remove(ID id) {
    if (_num_objects > 0) {
      Index &in = _indices[id & INDEX_MASK];

      T &o = _objects[in.index];
      o = _objects[--_num_objects];
      _indices[o.id & INDEX_MASK].index = in.index;

      in.index = ID_INVALID;
      _indices[_freelist_enqueue].next = id & INDEX_MASK;
      _freelist_enqueue = id & INDEX_MASK;

      //if (_freelist_dequeue == MAX_OBJECTS)
      //  _freelist_dequeue = _freelist_enqueue;
    }
  }

  inline void removeAll() {
    reset();
  }
};