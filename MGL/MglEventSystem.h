#ifndef __MGL_EVENT_SYSTEM_H__
#define __MGL_EVENT_SYSTEM_H__

#include "MglTypes.h"

#include "collection_types.h"

class EventSystem
{
public:
  EventSystem();
  virtual ~EventSystem();

  bool init(unsigned _maxStreamSize = 1024 * 1024);
  void close();

  bool queueEvent(int _eventType, const void* _data, unsigned _dataSize);

  bool getEvents(foundation::Array<Mgl::EventHeader*>& _packets);
  bool flushEvents();

private:
  unsigned mEventStreamSize;
  unsigned mEventStreamMaxSize;
  BYTE*    mEventStream;
  
  foundation::Array<Mgl::EventHeader*> mEventStreamHeaderPackets;
};

#endif