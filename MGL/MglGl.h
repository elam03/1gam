#pragma once

#define GL_GLEXT_PROTOTYPES
#define GLEW_STATIC

#include "gl/glew.h"

#include <gl/gl.h>
#include <gl/glu.h>
#include "gl/glut.h"

