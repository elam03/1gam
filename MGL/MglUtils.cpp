#include "MglUtils.h"

#include "MglApplication.h"

#include "MglGl.h"

using namespace std;
using namespace foundation;

Vector3 Mgl::operator+(const Vector3& _lhs, const Vector3& _rhs)
{
  Vector3 v = _lhs;

  v.x += _rhs.x;
  v.y += _rhs.y;
  v.z += _rhs.z;

  return (v);
}

Vector3 Mgl::operator-(const Vector3& _lhs, const Vector3& _rhs)
{
  Vector3 v = _lhs;

  v.x -= _rhs.x;
  v.y -= _rhs.y;
  v.z -= _rhs.z;

  return (v);
}

Vector3 Mgl::operator*(const Vector3& _lhs, float _scalar)
{
  Vector3 v = _lhs;

  v.x *= _scalar;
  v.y *= _scalar;
  v.z *= _scalar;

  return (v);
}

Vector3 Mgl::operator*(const Vector3& _lhs, double _scalar)
{
  Vector3 v = _lhs;

  v.x *= (float)_scalar;
  v.y *= (float)_scalar;
  v.z *= (float)_scalar;

  return (v);
}

Vector3 Mgl::operator/(const Vector3& _lhs, float _scalar)
{
  Vector3 v = _lhs;

  v.x /= _scalar;
  v.y /= _scalar;
  v.z /= _scalar;

  return (v);
}

Vector3 Mgl::operator/(const Vector3& _lhs, double _scalar)
{
  Vector3 v = _lhs;

  v.x /= (float)_scalar;
  v.y /= (float)_scalar;
  v.z /= (float)_scalar;

  return (v);
}

Vector3 Mgl::operator+=(Vector3& _lhs, const Vector3& _rhs)
{
  _lhs.x += _rhs.x;
  _lhs.y += _rhs.y;
  _lhs.z += _rhs.z;
  return _lhs;
}

Vector3 Mgl::operator-=(Vector3& _lhs, const Vector3& _rhs)
{
  _lhs.x -= _rhs.x;
  _lhs.y -= _rhs.y;
  _lhs.z -= _rhs.z;
  return _lhs;
}

Vector3 Mgl::makeVector(float _angle, float _magnitude)
{
  Vector3 v;

  v.x = cos(_angle * DEG) * _magnitude;
  v.y = sin(_angle * DEG) * _magnitude;
  v.z = 0.0f;

  return (v);
}

float Mgl::angleOf(const Vector3& _origin)
{
  float dx = (float)(_origin.x);
  float dy = (float)(_origin.y);

  float result = atan2(dy, dx) * RAD;

  if (result < 0.0f)
    result += 360.0f;

  return (result);
}

float Mgl::magnitudeOf(const Vector3& _origin)
{
  float dx = _origin.x;
  float dy = _origin.y;
  float dz = _origin.z;

  float distance = sqrt((dx * dx) + (dy * dy) + (dz * dz));

  return (distance);
}

float Mgl::magnitudeSquaredOf(const Vector3& _origin)
{
  float dx = _origin.x;
  float dy = _origin.y;
  float dz = _origin.z;

  float distance = (dx * dx) + (dy * dy) + (dz * dz);

  return (distance);
}

float Mgl::angleTo(const Vector3& _origin, const Vector3& _target)
{
  float dx = (float)(_target.x - _origin.x);
  float dy = (float)(_target.y - _origin.y);

  float result = atan2(dy, dx) * RAD;

  if (result < 0.0f)
    result += 360.0f;

  return (result);
}

float Mgl::distanceTo(const Vector3& _origin, const Vector3& _target)
{
  float dx = (float)(_origin.x - _target.x);
  float dy = (float)(_origin.y - _target.y);
  float dz = (float)(_origin.z - _target.z);

  float distance = sqrt((dx * dx) + (dy * dy) + (dz * dz));

  return (distance);
}

float Mgl::distanceSquaredTo(const Vector3& _origin, const Vector3& _target)
{
  float dx = (float)(_origin.x - _target.x);
  float dy = (float)(_origin.y - _target.y);
  float dz = (float)(_origin.z - _target.z);

  float distanceSquared = ((dx * dx) + (dy * dy) + (dz * dz));

  return (distanceSquared);
}

float Mgl::dotProductOf(const Vector3& _a, const Vector3& _b)
{
  return (_a.x * _b.x + _a.y * _b.y + _a.z * _b.z);
}

Vector3 Mgl::crossProductOf(const Vector3& _a, const Vector3& _b)
{
  //vec vector; 
  //vector.x = Ay*Bz - By*Az; 
  //vector.y = Bx*Az - Ax*Bz; 
  //vector.z = Ax*By - Ay*Bx; 

  Vector3 result;

  result.x = (_a.y * _b.z) - (_b.y * _a.z);
  result.y = (_b.x * _a.z) - (_a.x * _b.z);
  result.z = (_a.x * _b.y) - (_a.y * _b.x);

  return (result);

  //float ma = magnitudeOf(_a);
  //float mb = magnitudeOf(_b);
  //float a = acos(dotProductOf(_a, _b) / (ma * mb));
  //return (ma * mb * sin(a));
}

Vector2 Mgl::normalize(const Vector2& _v)
{
  Vector2 result = _v;

  float mag = magnitudeOf(result);

  result = result / mag;

  return (result);
}

Vector3 Mgl::normalize(const Vector3& _v)
{
  Vector3 result = _v;

  float mag = magnitudeOf(result);

  result = result / mag;

  return (result);
}

void Mgl::reset(Vector3& _p)
{
  _p.x = _p.y = _p.z = 0.0f;
}

///////////////////////////////////////////////////////////////////////////////

void glColor3f(const Vector3& _color)
{
  glColor3f(_color.x, _color.y, _color.z);
}

void glColor4f(const Vector3& _color, float _alpha)
{
  glColor4f(_color.x, _color.y, _color.z, _alpha);
}

void glVertex2f(const Vector2& _origin)
{
  glVertex2f(_origin.x, _origin.y);
}

void glVertex3f(const Vector3& _origin)
{
  glVertex3f(_origin.x, _origin.y, _origin.z);
}

void glTranslatef(const Vector3& _origin)
{
  glTranslatef(_origin.x, _origin.y, _origin.z);
}

void glLight4f(unsigned int _light, unsigned int _pname, float _p1, float _p2, float _p3, float _p4)
{
  GLfloat params[4] = { _p1, _p2, _p3, _p4 };
  glLightfv(_light, _pname, params);
}

void Mgl::glCircle(const Vector3& _center, float _radius, unsigned numSegments)
{
  glBegin(GL_LINE_LOOP);
    float angleInc = 360.0f / numSegments;

    for (unsigned i = 0; i < numSegments; i++)
    {
      Vector3 p = _center;

      p.x += cos(angleInc * i * DEG) * (_radius);
      p.y += sin(angleInc * i * DEG) * (_radius);
      p.z += 0.0f;

      glVertex3f(p);
    }
  glEnd();
}

void Mgl::glCircle(const Vector2& _center, float _radius, unsigned numSegments)
{
  glBegin(GL_LINE_LOOP);
    float angleInc = 360.0f / numSegments;

    for (unsigned i = 0; i < numSegments; i++)
    {
      Vector2 p = _center;

      p.x += cos(angleInc * i * DEG) * (_radius);
      p.y += sin(angleInc * i * DEG) * (_radius);

      glVertex2f(p);
    }
  glEnd();
}

void Mgl::glText(const char *_text, ...)
{
  if (gFontDisplayId != 0 && _text && strlen(_text) > 0)
  {
    char    buf[256];
    va_list args;

    va_start(args, _text);
    vsprintf_s(buf, 256, _text, args);
    va_end(args);

    glPushAttrib(GL_LIST_BIT);
      glListBase(gFontDisplayId - 32);
      glCallLists((GLsizei)strlen(buf), GL_UNSIGNED_BYTE, buf);
    glPopAttrib();
  }
}

void Mgl::glText(const wchar_t *_text, ...)
{
  if (gFontDisplayId != 0 && _text && wcslen(_text) > 0)
  {
    wchar_t buf[256];
    va_list args;

    va_start(args, _text);
    vswprintf_s(buf, 256, _text, args);
    va_end(args);

    glPushAttrib(GL_LIST_BIT);
      glListBase(gFontDisplayId - 32);
      glCallLists((GLsizei)wcslen(buf), GL_UNSIGNED_SHORT, buf);
    glPopAttrib();
  }
}

void Mgl::glCross(float _x, float _y, float _size)
{
  glBegin(GL_LINES);
    glVertex2f(_x - _size, _y);
    glVertex2f(_x + _size, _y);
    glVertex2f(_x, _y - _size);
    glVertex2f(_x, _y + _size);
  glEnd();
}

void Mgl::glCross(const foundation::Vector2& _center, float _size)
{
  glCross(_center.x, _center.y, _size);
}

void Mgl::glQuad(float _x, float _y, float _w, float _h)
{
  glBegin(GL_QUADS);
    glVertex2f(_x, _y);
    glVertex2f(_x + _w, _y);
    glVertex2f(_x + _w, _y + _h);
    glVertex2f(_x, _y + _h);
  glEnd();
}

void Mgl::glQuadTexture(unsigned int _textureId, float _x, float _y, float _w, float _h)
{
  glBindTexture(GL_TEXTURE_2D, _textureId);

  glBegin(GL_QUADS);
    glTexCoord2f(0.0f, 0.0f); glVertex2f(_x, _y + _h);
    glTexCoord2f(1.0f, 0.0f); glVertex2f(_x + _w, _y + _h);
    glTexCoord2f(1.0f, 1.0f); glVertex2f(_x + _w, _y);
    glTexCoord2f(0.0f, 1.0f); glVertex2f(_x, _y);
  glEnd();

  glBindTexture(GL_TEXTURE_2D, 0);
}

Vector2 Mgl::operator+(const Vector2& _lhs, const Vector2& _rhs)
{
  Vector2 v = _lhs;

  v.x += _rhs.x;
  v.y += _rhs.y;

  return (v);
}

Vector2 Mgl::operator-(const Vector2& _lhs, const Vector2& _rhs)
{
  Vector2 v = _lhs;

  v.x -= _rhs.x;
  v.y -= _rhs.y;

  return (v);
}

Vector2 Mgl::operator*(const Vector2& _lhs, float _scalar)
{
  Vector2 v = _lhs;

  v.x *= _scalar;
  v.y *= _scalar;

  return (v);
}

Vector2 Mgl::operator*(const Vector2& _lhs, double _scalar)
{
  Vector2 v = _lhs;

  v.x *= (float)_scalar;
  v.y *= (float)_scalar;

  return (v);
}

Vector2 Mgl::operator/(const Vector2& _lhs, float _scalar)
{
  Vector2 v = _lhs;

  v.x /= _scalar;
  v.y /= _scalar;

  return (v);
}

Vector2 Mgl::operator/(const Vector2& _lhs, double _scalar)
{
  Vector2 v = _lhs;

  v.x /= (float)_scalar;
  v.y /= (float)_scalar;

  return (v);
}

Vector2 Mgl::operator+=(Vector2& _lhs, const Vector2& _rhs)
{
  _lhs.x += _rhs.x;
  _lhs.y += _rhs.y;
  return _lhs;
}

Vector2 Mgl::operator-=(Vector2& _lhs, const Vector2& _rhs)
{
  _lhs.x -= _rhs.x;
  _lhs.y -= _rhs.y;
  return _lhs;
}

Vector2 Mgl::makeVector2(const Vector3& _v)
{
  Vector2 v;

  v.x = _v.x;
  v.y = _v.y;

  return (v);
}

Vector3 Mgl::makeVector3(const Vector2& _v)
{
  Vector3 v;

  v.x = _v.x;
  v.y = _v.y;
  v.z = 0.0f;

  return (v);
}

Vector2 Mgl::makeVector2(float _angle, float _magnitude)
{
  Vector2 v;

  v.x = cos(_angle * DEG) * _magnitude;
  v.y = sin(_angle * DEG) * _magnitude;

  return (v);
}

float Mgl::angleOf(const Vector2& _origin)
{
  float dx = (float)(_origin.x);
  float dy = (float)(_origin.y);

  float result = atan2(dy, dx) * RAD;

  if (result < 0.0f)
    result += 360.0f;

  return (result);
}

float Mgl::magnitudeOf(const Vector2& _origin)
{
  float dx = _origin.x;
  float dy = _origin.y;

  float distance = sqrt((dx * dx) + (dy * dy));

  return (distance);
}

float Mgl::magnitudeSquaredOf(const Vector2& _origin)
{
  float dx = _origin.x;
  float dy = _origin.y;

  float distance = (dx * dx) + (dy * dy);

  return (distance);
}

float Mgl::angleTo(const Vector2& _origin, const Vector2& _target)
{
  float dx = (float)(_target.x - _origin.x);
  float dy = (float)(_target.y - _origin.y);

  float result = atan2(dy, dx) * RAD;

  if (result < 0.0f)
    result += 360.0f;

  return (result);
}

float Mgl::distanceTo(const Vector2& _origin, const Vector2& _target)
{
  float dx = (float)(_origin.x - _target.x);
  float dy = (float)(_origin.y - _target.y);

  float distance = sqrt((dx * dx) + (dy * dy));

  return (distance);
}

float Mgl::distanceSquaredTo(const Vector2& _origin, const Vector2& _target)
{
  float dx = (float)(_origin.x - _target.x);
  float dy = (float)(_origin.y - _target.y);

  float distanceSquared = ((dx * dx) + (dy * dy));

  return (distanceSquared);
}

float Mgl::dotProductOf(const Vector2& _a, const Vector2& _b)
{
  return (_a.x * _b.x + _a.y * _b.y);
}

bool Mgl::contains(const CameraInfo& _cameraInfo, const Vector2 _p)
{
  return (_p.x >= _cameraInfo.x && _p.x <= (_cameraInfo.x + _cameraInfo.width) && _p.y >= _cameraInfo.y && _p.y <= (_cameraInfo.y + _cameraInfo.height));
}

void Mgl::glQuadFilled(const Rect& _rect)
{
  glBegin(GL_QUADS);
    glVertex2f(_rect.x, _rect.y);
    glVertex2f(_rect.x + _rect.w, _rect.y);
    glVertex2f(_rect.x + _rect.w, _rect.y + _rect.h);
    glVertex2f(_rect.x, _rect.y + _rect.h);
  glEnd();
}

void Mgl::glQuad(const Rect& _rect)
{
  glBegin(GL_LINE_LOOP);
    glVertex2f(_rect.x, _rect.y);
    glVertex2f(_rect.x + _rect.w, _rect.y);
    glVertex2f(_rect.x + _rect.w, _rect.y + _rect.h);
    glVertex2f(_rect.x, _rect.y + _rect.h);
  glEnd();
}

void Mgl::setFontDisplayId(int _displayId)
{
  gFontDisplayId = _displayId;
}

bool Mgl::lineSegmentLineSegmentCheck(const Vector2& _a, const Vector2& _b, const Vector2& _c, const Vector2& _d)
{
    float denominator = ((_b.x - _a.x) * (_d.y - _c.y)) - ((_b.y - _a.y) * (_d.x - _c.x));
    float numerator1  = ((_a.y - _c.y) * (_d.x - _c.x)) - ((_a.x - _c.x) * (_d.y - _c.y));
    float numerator2  = ((_a.y - _c.y) * (_b.x - _a.x)) - ((_a.x - _c.x) * (_b.y - _a.y));

    // Detect coincident lines (has a problem, read below)
    if (denominator == 0)
      return (numerator1 == 0 && numerator2 == 0);

    float r = numerator1 / denominator;
    float s = numerator2 / denominator;

    return (r >= 0 && r <= 1) && (s >= 0 && s <= 1);
}

bool Mgl::circleLineSegmentCheck(const Vector3& _center, float _radius, const Vector3& _p1, const Vector3& _p2)
{
  return (circleLineSegmentCheck(makeVector2(_center), _radius, makeVector2(_p1), makeVector2(_p2)));
}

bool Mgl::circleLineSegmentCheck(const Vector2& _center, float _radius, const Vector2& _p1, const Vector2& _p2)
{
  Vector2 dir = _p2 - _p1;
  Vector2 diff = _center - _p1;

  float t = dotProductOf(diff, dir) / dotProductOf(dir, dir);

  if (t < 0.0f)
    t = 0.0f;
  if (t > 1.0f)
    t = 1.0f;

  Vector2 closestPoint = _p1 + dir * t;
  Vector2 d = _center - closestPoint;
  float distanceSquared = dotProductOf(d, d);

  return (distanceSquared <= (_radius * _radius));

}

bool Mgl::circleLineSegmentCheck(const Vector3& _center, float _radius, const Vector3& _p1, const Vector3& _p2, Vector3& _where)
{
  Vector3 dir = _p2 - _p1;
  Vector3 diff = _center - _p1;

  float t = dotProductOf(diff, dir) / dotProductOf(dir, dir);

  if (t < 0.0f)
    t = 0.0f;
  if (t > 1.0f)
    t = 1.0f;

  Vector3 closestPoint = _p1 + dir * t;
  Vector3 d = _center - closestPoint;
  float distanceSquared = dotProductOf(d, d);

  _where = closestPoint;

  return (distanceSquared <= (_radius * _radius));
}

bool Mgl::circleCircleCheck(const Vector3& _center1, float _radius1, const Vector3& _center2, float _radius2, Vector3& _where)
{
  Vector3 diff = _center2 - _center1;

  float distanceSquared = ((diff.x * diff.x) + (diff.y * diff.y));
  float distanceSquaredThreshold = (_radius1 + _radius2) * (_radius1 + _radius2);

  _where = _center2;

  return (distanceSquared < distanceSquaredThreshold);
}

///////////////////////////////////////////////////////////////////////////////

Mgl::Light::Light()
{
  reset();
}

Mgl::Light::~Light()
{
}

void Mgl::Light::apply()
{
  glEnable(GL_LIGHT0 + mIndex);
  glLight4f(GL_LIGHT0 + mIndex, GL_POSITION, mPosition.x, mPosition.y, mPosition.z, 0.0f);
  glLightfv(GL_LIGHT0 + mIndex, GL_AMBIENT, mAmbient);
  glLightfv(GL_LIGHT0 + mIndex, GL_DIFFUSE, mDiffuse);
  glLightfv(GL_LIGHT0 + mIndex, GL_SPECULAR, mSpecular);
}

void Mgl::Light::reset()
{
  mIndex = 0;
  mPosition.x = mPosition.y = mPosition.z = 0.0f;
  mAmbient[0]  = mAmbient[1]  = mAmbient[2]  = 0.0f; mAmbient[3]  = 1.0f;
  mDiffuse[0]  = mDiffuse[1]  = mDiffuse[2]  = 1.0f; mDiffuse[3]  = 1.0f;
  mSpecular[0] = mSpecular[1] = mSpecular[2] = 1.0f; mSpecular[3] = 1.0f;
}

bool Mgl::getFileList(const wstring& _path, Mgl::List<FileInfo>& _fileList)
{
  bool success = false;

  if (_path.length() > 1)
  {
    wstring path = _path;
    if (path.find_last_of(L"\\/") != (path.length() - 1))
      path += L"\\*";
    else
      path += L"*";
      //path = path.substr(0, path.length() - 1);

    WIN32_FIND_DATA ffd;
    HANDLE handle = nullptr;

    handle = FindFirstFile(path.c_str(), &ffd);
    if (handle != INVALID_HANDLE_VALUE)
    {
      _fileList.clear();

      do
      {
        wstring s = ffd.cFileName;

        LARGE_INTEGER fileSize;
        fileSize.LowPart = ffd.nFileSizeLow;
        fileSize.HighPart = ffd.nFileSizeHigh;

        if (s != L"." && s != L"..")
        {
          if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
          {
            _fileList.push_back(FileInfo(ffd.cFileName, fileSize.QuadPart, true));
          }
          else
          {
            _fileList.push_back(FileInfo(ffd.cFileName, fileSize.QuadPart, false));
          }
        }
      } while (FindNextFile(handle, &ffd) != 0);

      FindClose(handle);

      success = true;
    }
  }

  return (success);
}
