#ifndef __GL_CONTEXT_H__
#define __GL_CONTEXT_H__

#include <windows.h>

namespace Mgl
{

class GlContext
{
public:
  GlContext()
  {
    reset();
  }

  virtual ~GlContext()
  {
    purge();
  }

  bool init(HWND _hwnd)
  {
    bool initialized = false;

    // remember the window handle (HWND)
    mHwnd = _hwnd;

    // get the device context (DC)
    mHDC = GetDC(mHwnd);

    // set the pixel format for the DC
    PIXELFORMATDESCRIPTOR pfd;
    ZeroMemory(&pfd, sizeof(pfd));
    pfd.nSize = sizeof(pfd);
    pfd.nVersion = 1;
    pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    pfd.iPixelType = PFD_TYPE_RGBA;
    pfd.cColorBits = 24;
    pfd.cDepthBits = 16;
    pfd.iLayerType = PFD_MAIN_PLANE;

    //int pixelFormat = 1;  
    //DescribePixelFormat(mHDC, pixelFormat, sizeof(PIXELFORMATDESCRIPTOR), &pfd);

    int format = ChoosePixelFormat(mHDC, &pfd);

    if (SetPixelFormat(mHDC, format, &pfd))
    {
      // create the render context (RC)
      mHGLRC = wglCreateContext(mHDC);

      if (mHGLRC)
      {
        // make it the current render context
        if (wglMakeCurrent(mHDC, mHGLRC))
        {
          initialized = true;
        }
      }
    }

    return initialized;
  }

  void purge()
  {
    if (mHGLRC)
    {
      wglMakeCurrent(NULL, NULL);
      wglDeleteContext(mHGLRC);
    }
    if (mHwnd && mHDC)
    {
      ReleaseDC(mHwnd, mHDC);
    }
    reset();
  }

  HDC getHDC()
  {
    return mHDC;
  }

private:

  void reset()
  {
    mHwnd  = NULL;
    mHDC   = NULL;
    mHGLRC = NULL;
  }

  HWND  mHwnd;
  HDC   mHDC;
  HGLRC mHGLRC;

};

}

#endif