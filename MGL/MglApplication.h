#pragma once

#include <string>
#include <deque>
#include <vector>

#include "MglEventSystem.h"
#include "MglGlContext.h"
#include "MglTypes.h"

#include "collection_types.h"

class AppState;
class ApplicationEvent;

class MglApplication
{
public:
  class Event
  {
  public:
    Event() {}
    virtual ~Event() {}
    virtual void execute() = 0;
  };

  class InitParams
  {
  public:
    InitParams()
    {
      mHInstance = NULL;
      mName = L"MglApplication";
      mWindowTitle = L"MglApplication";
      mWindowX = mWindowY = 5;
      mWindowWidth = mWindowHeight = 500;
      mConsoleX = 510; mConsoleY = 5;
      mConsoleWidth = 500; mConsoleHeight = 300;
      mConsoleEnabled = false;
    }
    virtual ~InitParams() {}

    HINSTANCE    mHInstance;
    std::wstring mName;
    std::wstring mWindowTitle;
    int          mWindowX;
    int          mWindowY;
    int          mWindowWidth;
    int          mWindowHeight;
    int          mConsoleX;
    int          mConsoleY;
    int          mConsoleWidth;
    int          mConsoleHeight;
    bool         mConsoleEnabled;
  };

public:
  virtual ~MglApplication();

  /////////////////////////////////////////////////////////////////////////////
  bool init(InitParams& _initParams);
  int  run();
  void close();
  /////////////////////////////////////////////////////////////////////////////
  
  bool isKeyPressed(int _key);
  void printText(const char *_text, ...);
  void renderBorder();
  void setAppState(int _appState);

  int mWindowX;
  int mWindowY;
  int mWindowWidth;
  int mWindowHeight;

  int mConsoleX;
  int mConsoleY;
  int mConsoleWidth;
  int mConsoleHeight;

protected:
  MglApplication();

  HWND createWindow(InitParams& _initParams);

  virtual bool appInit() = 0;
  virtual void appClose() = 0;
  virtual void appWindowRefresh();
  virtual void appShow();

  static LRESULT CALLBACK InitialWndProc(HWND _hwnd, UINT _message, WPARAM _wParam, LPARAM _lParam);
  static LRESULT CALLBACK StaticWndProc(HWND _hwnd, UINT _message, WPARAM _wParam, LPARAM _lParam);
  LRESULT wndProc(HWND _hwnd, UINT _message, WPARAM _wParam, LPARAM _lParam);

  bool initFonts();
  void closeFonts();
  
  void idle();
  bool addAppState(AppState* _appState);

  Mgl::GlContext mGlContext;
  HWND           mHwnd;
  HDC            mDeviceContext;
  
  unsigned int mFontDisplayId;

  bool mWindowSizeChanged;

  double mLastTime;

  int mUpdatesPerSecond;

  int mAppStatePrev;
  int mAppState;
  std::vector<AppState*> mAppStates;

  //friend class ApplicationEvent;
  //std::deque<Event*> mEvents;

  void processEvents();
  foundation::Array<Mgl::EventHeader*> mEvents;
  EventSystem mInputEvents;
  EventSystem mOutputEvents;

  void setKeyPressed(int _key, bool _down = true);

  int mKeyboardSpecialKeys;
  foundation::Hash<bool>* mKeyboardInfo;

  /////////////////////////////////////////////////////////////////////////////
public:
  virtual bool defaultHandleEvent(int _event, void* _eventData = 0);
  virtual void defaultRender();
  virtual void defaultUpdate(double _deltaTime);
  /////////////////////////////////////////////////////////////////////////////
};