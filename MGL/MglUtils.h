#pragma once

#define GL_GLEXT_PROTOTYPES

#include <fstream>
#include <iostream>
#include <memory>    // for std::unique_ptr
#include <stdarg.h>  // for va_start, etc
#include <windows.h>
#include <windowsx.h>

#include "math_types.h"

#include "MglTypes.h"

void glColor3f(const foundation::Vector3& _color);
void glColor4f(const foundation::Vector3& _color, float _alpha);
void glVertex2f(const foundation::Vector2& _origin);
void glVertex3f(const foundation::Vector3& _origin);
void glTranslatef(const foundation::Vector3& _origin);
void glLight4f(unsigned int _light, unsigned int _pname, float _p1, float _p2, float _p3, float _p4);

namespace Mgl
{
  template<typename T>
  class List
  {
  public:
    List() : mDataSize(0), mDataMaxSize(0), mData(nullptr)
    {
      resize(16);
    }
    List(unsigned _size) : mDataSize(0), mDataMaxSize(0), mData(nullptr) { resize(_size); }
    virtual ~List()
    {
      if (mData)
      {
        delete[] mData;
        mData = nullptr;
      }
      
      mDataSize = 0;
      mDataMaxSize = 0;
    }

    void push_back(const T& _data)
    {
      if (mDataSize >= mDataMaxSize)
        resize(mDataMaxSize * 2);

      mData[mDataSize++] = _data;
    }

    void push_front(const T& _data)
    {
      if (mDataSize >= mDataMaxSize)
        resize(mDataMaxSize * 2);

      if (mDataSize == 0)
        mData[mDataSize++] = _data;
      else
      {
        memcpy(&mData[0], &mData[1], sizeof(T) * (mDataSize++));
        mData[0] = _data;
      }
    }

    void pop_front()
    {
      if (mDataSize > 0)
      {
        if (mDataSize == 1)
        {
          mDataSize = 0;
        }
        else  
        {
          mDataSize--;
          memcpy(mData, mData[1], sizeof(T) * (mDataSize));
        }
      }
    }

    void pop_back()
    {
      if (mDataSize > 0)
        mDataSize--;
    }

    inline void clear() { mDataSize = 0; }
    inline unsigned size() const { return mDataSize; }
    inline T& operator[](int _index) { return mData[_index]; }
    inline const T& operator[](int _index) const { return mData[_index]; }

  private:
    bool resize(unsigned _newSize)
    {
      if (_newSize > mDataMaxSize)
      {
        T* newData = new T[_newSize];
      
        if (mDataSize > 0)
          memcpy(newData, mData, sizeof(T) * mDataSize);
        
        T* dataToBeDeleted = mData;

        mData = newData;
        mDataMaxSize = _newSize;

        delete[] dataToBeDeleted;

        return true;
      }
      return false;
    }

    unsigned mDataSize;
    unsigned mDataMaxSize;
    T*       mData;
  };

  ///////////////////////////////////////////////////////////////////////////////
  // Vector3 math functions
  
  foundation::Vector3 operator+(const foundation::Vector3& _lhs, const foundation::Vector3& _rhs);
  foundation::Vector3 operator-(const foundation::Vector3& _lhs, const foundation::Vector3& _rhs);
  foundation::Vector3 operator*(const foundation::Vector3& _lhs, float _scalar);
  foundation::Vector3 operator*(const foundation::Vector3& _lhs, double _scalar);
  foundation::Vector3 operator/(const foundation::Vector3& _lhs, float _scalar);
  foundation::Vector3 operator/(const foundation::Vector3& _lhs, double _scalar);
  foundation::Vector3 operator+=(foundation::Vector3& _lhs, const foundation::Vector3& _rhs);
  foundation::Vector3 operator-=(foundation::Vector3& _lhs, const foundation::Vector3& _rhs);

  foundation::Vector3 makeVector(float _angle, float _magnitude);
  float angleOf(const foundation::Vector3& _origin);
  float magnitudeOf(const foundation::Vector3& _origin);
  float magnitudeSquaredOf(const foundation::Vector3& _origin);
  float angleTo(const foundation::Vector3& _origin, const foundation::Vector3& _target);
  float distanceTo(const foundation::Vector3& _origin, const foundation::Vector3& _target);
  float distanceSquaredTo(const foundation::Vector3& _origin, const foundation::Vector3& _target);
  float dotProductOf(const foundation::Vector3& _a, const foundation::Vector3& _b);
  foundation::Vector3 crossProductOf(const foundation::Vector3& _a, const foundation::Vector3& _b);
  foundation::Vector3 normalize(const foundation::Vector3& _v);
  void reset(foundation::Vector3& _p);

  void glCircle(const foundation::Vector2& _center, float _radius, unsigned numSegments = 16);
  void glCircle(const foundation::Vector3& _center, float _radius, unsigned numSegments = 16);
  void glText(const char *_text, ...);
  void glText(const wchar_t *_text, ...);
  void glCross(float _x, float _y, float _size);
  void glCross(const foundation::Vector2& _center, float _size);
  void glQuad(float _x, float _y, float _w, float _h);
  void glQuadTexture(unsigned int _textureId, float _x, float _y, float _w, float _h);

  foundation::Vector2 operator+(const foundation::Vector2& _lhs, const foundation::Vector2& _rhs);
  foundation::Vector2 operator-(const foundation::Vector2& _lhs, const foundation::Vector2& _rhs);
  foundation::Vector2 operator*(const foundation::Vector2& _lhs, float _scalar);
  foundation::Vector2 operator*(const foundation::Vector2& _lhs, double _scalar);
  foundation::Vector2 operator/(const foundation::Vector2& _lhs, float _scalar);
  foundation::Vector2 operator/(const foundation::Vector2& _lhs, double _scalar);
  foundation::Vector2 operator+=(foundation::Vector2& _lhs, const foundation::Vector2& _rhs);
  foundation::Vector2 operator-=(foundation::Vector2& _lhs, const foundation::Vector2& _rhs);

  foundation::Vector2 makeVector2(const foundation::Vector3& _v);
  foundation::Vector3 makeVector3(const foundation::Vector2& _v);

  foundation::Vector2 makeVector2(float _angle, float _magnitude);
  float angleOf(const foundation::Vector2& _origin);
  float magnitudeOf(const foundation::Vector2& _origin);
  float magnitudeSquaredOf(const foundation::Vector2& _origin);
  float angleTo(const foundation::Vector2& _origin, const foundation::Vector2& _target);
  float distanceTo(const foundation::Vector2& _origin, const foundation::Vector2& _target);
  float distanceSquaredTo(const foundation::Vector2& _origin, const foundation::Vector2& _target);
  float dotProductOf(const foundation::Vector2& _a, const foundation::Vector2& _b);
  foundation::Vector2 normalize(const foundation::Vector2& _v);

  inline bool isNil(const foundation::Vector2& _v) { return (_v.x == 0.0f && _v.y == 0.0f) ? true : false; }
  inline bool isNil(const foundation::Vector3& _v) { return (_v.x == 0.0f && _v.y == 0.0f && _v.z == 0.0f) ? true : false; }

  bool contains(const CameraInfo& _cameraInfo, const foundation::Vector2 _p);

  ///////////////////////////////////////////////////////////////////////////////

  struct Rect
  {
    Rect() { x = y = w = h = 0.0f; }
    Rect(float _x, float _y, float _w, float _h) : x(_x), y(_y), w(_w), h(_h) {}
    virtual ~Rect() {}
    bool contains(const foundation::Vector2& _point) const { return (_point.x >= x && _point.x <= (x + w) && _point.y >= y && _point.y <= (y + h)); }

    float x, y, w, h;
  }; 

  void glQuadFilled(const Rect& _rect);
  void glQuad(const Rect& _rect);
  
  ///////////////////////////////////////////////////////////////////////////////

  class Light
  {
  public:
    Light();
    virtual ~Light();

    void apply();
    void reset();

    unsigned mIndex;
    foundation::Vector3 mPosition;
    float mAmbient[4];
    float mDiffuse[4];
    float mSpecular[4];
  };

  ///////////////////////////////////////////////////////////////////////////////

  /* SOURCE: http://stackoverflow.com/a/8098080 */

  static std::string string_format(const std::string fmt_str, ...) {
    int final_n, n = ((int)fmt_str.size()) * 6; /* reserve 2 times as much as the length of the fmt_str */
    std::string str;
    std::unique_ptr<char[]> formatted;
    va_list ap;
    while(1) {
      formatted.reset(new char[n]); /* wrap the plain char array into the unique_ptr */
      strcpy_s(&formatted[0], n, fmt_str.c_str());
      va_start(ap, fmt_str);
      final_n = vsnprintf_s(&formatted[0], n, n, fmt_str.c_str(), ap);
      va_end(ap);
      if (final_n < 0 || final_n >= n)
        n += abs(final_n - n + 1);
      else
        break;
    }
    return std::string(formatted.get());
  }

  static std::wstring string_format(const std::wstring fmt_str, ...) {
    static const int n = 260;
    static wchar_t buf[n];

    va_list ap;
    va_start(ap, fmt_str);
    _vsnwprintf_s(buf, n, n, fmt_str.c_str(), ap);
    va_end(ap);

    return std::wstring(buf);

    //int final_n, n = ((int)fmt_str.size()) * 6; /* reserve 2 times as much as the length of the fmt_str */
    //std::wstring str;
    //std::unique_ptr<wchar_t[]> formatted;
    //va_list ap;
    //while(1) {
    //  formatted.reset(new wchar_t[n]); /* wrap the plain char array into the unique_ptr */
    //  wcscpy_s(&formatted[0], n, fmt_str.c_str());
    //  va_start(ap, fmt_str);
    //  final_n = _vsnwprintf_s(&formatted[0], n, n, fmt_str.c_str(), ap);
    //  va_end(ap);
    //  if (final_n < 0 || final_n >= n)
    //    n += abs(final_n - n + 1);
    //  else
    //    break;
    //}
    //return std::wstring(formatted.get());
  }
  ///////////////////////////////////////////////////////////////////////////////

  bool getFileList(const std::wstring& _path, List<FileInfo>& _fileList);

  ///////////////////////////////////////////////////////////////////////////////

  static const float PI   = 3.1415f;
  static const float DEG = (PI / 180.0f);
  static const float RAD = (180.0f / PI);
  static const int   RANDOM_NUMBER_PRECISION = 1000;

  static void debugPrint(unsigned int _flags, const char* _format, ...)
  {
    va_list argList;
    va_start(argList, _format);

    char buf[512];
    vsnprintf_s(buf, 512, _TRUNCATE, _format, argList);

    strcat_s(buf, 512, "\n");

    va_end(argList);

    std::cout << buf;
  }

  static void debugPrint(unsigned int _flags, const wchar_t* _format, ...)
  {
    va_list argList;
    va_start(argList, _format);

    wchar_t buf[512];
    _vsnwprintf_s(buf, 512, _TRUNCATE, _format, argList);

    wcscat_s(buf, 512, L"\n");

    va_end(argList);

    std::wcout << buf;
  }

  static void debugMessageBox(unsigned int _flags, const char* _title, const char* _format, ...)
  {
    va_list argList;
    va_start(argList, _format);

    char buf[512];
    vsnprintf_s(buf, 512, _TRUNCATE, _format, argList);

    strcat_s(buf, 512, "\n");

    va_end(argList);

    MessageBoxA(NULL, buf, _title, MB_OK);
  }

  static void debugMessageBox(unsigned int _flags, const wchar_t* _title, const wchar_t* _format, ...)
  {
    va_list argList;
    va_start(argList, _format);

    wchar_t buf[512];
    _vsnwprintf_s(buf, 512, _TRUNCATE, _format, argList);

    wcscat_s(buf, 512, L"\n");

    va_end(argList);

    MessageBox(NULL, buf, _title, MB_OK);
  }

  static FILE* gFileStdOut = NULL;
  static FILE* gFileStdIn = NULL;

  static bool initConsole()
  {
    //std::locale::global(std::locale("Japan"));
    //std::wcout.imbue( std::locale("") );

    //  AllocConsole();
    //freopen("CONOUT$", "w", stdout);
    //std::cout << "This works" << std::endl;

    AllocConsole();

    AttachConsole(GetProcessId(GetModuleHandle(NULL)));

    freopen_s(&gFileStdOut, "CONOUT$", "w", stdout);
    freopen_s(&gFileStdIn, "CONIN$", "r", stdin);

    return (true);
  }

  static void closeConsole()
  {
    FreeConsole();

    if (gFileStdIn)  fclose(gFileStdIn);
    if (gFileStdOut) fclose(gFileStdOut);
  } 

  static bool          mTimerInitialized = false;
  static LARGE_INTEGER mTimerFrequency;
  static LARGE_INTEGER mTimerStart;
  static LARGE_INTEGER mTimerEnd;
  static double        mTimerElapsedTime;

  static int gFontDisplayId = 0;

  void setFontDisplayId(int _displayId);
  
  static void timerInit()
  {
    if (!mTimerInitialized)
    {
      QueryPerformanceFrequency(&mTimerFrequency);
    }
  }

  inline static unsigned random(unsigned _min, unsigned _max)
  {
    if (_max < _min) { int temp = _max; _max = _min; _min = temp; }
    return (_min + rand() % (_max - _min));
  }

  inline static int random(int _min, int _max)
  {
    if (_max < _min) { int temp = _max; _max = _min; _min = temp; }
    return (_min + rand() % (_max - _min));
  }

  inline static float randomf(float _min, float _max)
  {
    float randonNumber = float(rand() % RANDOM_NUMBER_PRECISION) / (RANDOM_NUMBER_PRECISION);
    float diff = fabs(_max - _min);

    return (_min + (diff * randonNumber));
  }

  template<typename T>
  inline static T clamp(T _value, T _min, T _max)
  {
    if (_min > _max) swap(_min, _max);
    return (min(max(_value, _min), _max));
  }

  static void timerStart()
  {
    QueryPerformanceCounter(&mTimerStart);
  }

  static void timerStop()
  {
    QueryPerformanceCounter(&mTimerEnd);
    mTimerElapsedTime = (float)(mTimerEnd.QuadPart - mTimerStart.QuadPart) / mTimerFrequency.QuadPart;
  }

  static double getTickTime()
  {
    if (!mTimerInitialized)
    {
      QueryPerformanceFrequency(&mTimerFrequency);
      mTimerInitialized = true;
    }

    QueryPerformanceCounter(&mTimerEnd);

    return double(mTimerEnd.QuadPart) / mTimerFrequency.QuadPart;
  }

  // Quake's magically fast inverse square root. inverseSqrt(x) = (1 / sqrt(x))
  static float inverseSqrt(float x)
  {
    float xhalf = 0.5f * x;
    int i = *(int*)&x; // store floating-point bits in integer
    i = 0x5f3759d5 - (i >> 1); // initial guess for Newton's method
    x = *(float*)&i; // convert new bits into float
    x = x * (1.5f - xhalf * x * x); // One round of Newton's method
    return x;
  }

  static std::wstring toWstring(const std::string &_stringToBeConverted)
  {
    std::wstring convertedResult(_stringToBeConverted.begin(), _stringToBeConverted.end());

    return (convertedResult);
  }

  static std::string toString(const std::wstring &_stringToBeConverted)
  {
    std::string convertedResult(_stringToBeConverted.begin(), _stringToBeConverted.end());

    return (convertedResult);
  }

  bool lineSegmentLineSegmentCheck(const foundation::Vector2& _a, const foundation::Vector2& _b, const foundation::Vector2& _c, const foundation::Vector2& _d);
  bool circleLineSegmentCheck(const foundation::Vector2& _center, float _radius, const foundation::Vector2& _p1, const foundation::Vector2& _p2);
  bool circleLineSegmentCheck(const foundation::Vector3& _center, float _radius, const foundation::Vector3& _p1, const foundation::Vector3& _p2);
  bool circleLineSegmentCheck(const foundation::Vector3& _center, float _radius, const foundation::Vector3& _p1, const foundation::Vector3& _p2, foundation::Vector3& _where);
  
  bool circleCircleCheck(const foundation::Vector3& _center1, float _radius1, const foundation::Vector3& _center2, float _radius2, foundation::Vector3& _where);
}

///////////////////////////////////////////////////////////////////////////////
