#include "MglApplication.h"

#include "MglAppState.h"
#include "MglUtils.h"

#include "hash.h"
#include "murmur_hash.h"
#include "memory.h"

#include <time.h>
#include "MglGl.h"

using namespace std;
using namespace foundation;
using namespace Mgl;

///////////////////////////////////////////////////////////////////////////////

class AppStateDefault : public AppState
{
public:
  AppStateDefault() : AppState() {}
  virtual ~AppStateDefault() {}

  virtual bool handleEvent(int _event, void* _eventData = 0)
  {
    return (mApplication->defaultHandleEvent(_event, _eventData));
  }

  virtual bool processEvents()
  {
    bool handled = false;

    if (mEventSystem.getEvents(mEvents))
    {
      for (unsigned i = 0; i < array::size(mEvents); i++)
      {
        EventHeader& eventHeader = *mEvents[i];

        handleEvent(eventHeader.type, eventHeader.data);
      }

      mEventSystem.flushEvents();

      handled = true;
    }

    return (handled);
  }

  virtual void render()
  {
    mApplication->defaultRender();
  }

  virtual void update(double _deltaTime)
  {
    mApplication->defaultUpdate(_deltaTime);
  }
};

bool MglApplication::defaultHandleEvent(int _event, void* _eventData)
{
  return (false);
}

void MglApplication::defaultRender()
{
}

void MglApplication::defaultUpdate(double _deltaTime)
{
}

///////////////////////////////////////////////////////////////////////////////

HWND MglApplication::createWindow(InitParams& _initParams)
{
  wstring className = _initParams.mName.c_str();
  wstring title = _initParams.mWindowTitle.c_str();

  HWND hwnd = NULL;
  
  WNDCLASSEX wc;
  ZeroMemory(&wc, sizeof(WNDCLASSEX));

  wc.cbSize = sizeof(WNDCLASSEX);
  wc.style = CS_OWNDC;
  wc.hInstance = _initParams.mHInstance;
  wc.lpszClassName = className.c_str();
  wc.lpfnWndProc = InitialWndProc;
  wc.hbrBackground = (HBRUSH)COLOR_BACKGROUND;
  wc.hCursor = LoadCursor(NULL, IDC_ARROW);

  if (!RegisterClassEx(&wc))
  {
    MessageBox(NULL, TEXT("Call to RegisterClassEx failed!"), NULL, MB_OK);
  }
  else
  {
    hwnd = CreateWindowEx(0,
                          className.c_str(),
                          title.c_str(),
                          WS_OVERLAPPEDWINDOW,
                          _initParams.mWindowX,
                          _initParams.mWindowY,
                          _initParams.mWindowWidth,
                          _initParams.mWindowHeight,
                          NULL,
                          NULL,
                          _initParams.mHInstance,
                          this);
  }

  if (hwnd)
  {
    ShowWindow(hwnd, SW_SHOW);
  }
  else
  {
    DWORD error = GetLastError();
  }

  return (hwnd);
}

MglApplication::MglApplication() :
  mGlContext(),
  mHwnd(NULL),
  mDeviceContext(NULL),

  mFontDisplayId(0),

  mWindowSizeChanged(false),
  
  mUpdatesPerSecond(60),

  mAppStatePrev(0),
  mAppState(0),
  mAppStates(),

  mEvents(memory_globals::default_allocator()),
  mInputEvents(),
  mOutputEvents(),

  mKeyboardSpecialKeys(0),
  mKeyboardInfo(NULL)
{
}

MglApplication::~MglApplication()
{
}

bool MglApplication::init(InitParams& _initParams)
{
  bool success = false;

  HWND hwnd = createWindow(_initParams);

  if (mGlContext.init(hwnd))
  {
    glewInit();

    Mgl::timerInit();
		
    srand((unsigned int)time(NULL));

    mHwnd = hwnd;
    mDeviceContext = GetDC(mHwnd);

    if (initFonts())
    {
      char* version = (char *)glGetString(GL_VERSION);

      if (version)
      {
        Mgl::debugPrint(0, "GL_VERSION: \"%s\"", version);
      }
      else
      {
        Mgl::debugPrint(0, "GL_VERSION: \"\"");
      }

      Mgl::setFontDisplayId(mFontDisplayId);

      mInputEvents.init();
      mOutputEvents.init();

      mKeyboardInfo = new foundation::Hash<bool>(memory_globals::default_allocator());
      RECT clientArea;
      GetClientRect(mHwnd, &clientArea);

      mWindowX = clientArea.left;
      mWindowY = clientArea.top;
      mWindowWidth = clientArea.right;
      mWindowHeight = clientArea.bottom;

      mConsoleX      = _initParams.mConsoleX;
      mConsoleY      = _initParams.mConsoleY;
      mConsoleWidth  = _initParams.mConsoleWidth;
      mConsoleHeight = _initParams.mConsoleHeight;
      
      SetWindowPos(mHwnd, NULL, mWindowX, mWindowY, mWindowWidth, mWindowHeight, SWP_SHOWWINDOW);
      SetWindowPos(GetConsoleWindow(), NULL, mConsoleX, mConsoleY, mConsoleWidth, mConsoleHeight, SWP_SHOWWINDOW);
      
      mLastTime = Mgl::getTickTime();

      mWindowSizeChanged = true;

      success = appInit();

      if (mAppStates.size() <= 0)
      {
        Mgl::debugPrint(0, "No app state created! Using AppStateDefaultHome instead...");
        addAppState(new AppStateDefault);
      }
     
      for (unsigned i = 0; i < mAppStates.size(); i++)
        mAppStates[i]->pushEvent(EVENT_INIT);
    }
  }

  return (success);
}

int MglApplication::run()
{
  bool done = false;
  int  success = 0;
  MSG  message;

  appShow();

  double updateRate = double(1.0 / mUpdatesPerSecond); // 1.0 seconds / 60 frames

  double renderDueTime = getTickTime() + updateRate;

  while (!done)
  {
    if (PeekMessage(&message, NULL, 0, 0, PM_REMOVE) > 0)
    {
      if (message.message == WM_QUIT)
      {
        done = true;
      }
      else
      {
        TranslateMessage(&message);
        DispatchMessage(&message);
      }
     
      renderDueTime = getTickTime() + updateRate;
    }
    else
    {
      if (getTickTime() >= renderDueTime)
      {
        idle();

        renderDueTime = getTickTime() + updateRate;
      }
    }
  }

  return (success);
}

void MglApplication::close()
{
  mInputEvents.close();
  mOutputEvents.close();

  closeFonts();

  for (unsigned i = 0; i < mAppStates.size(); i++)
    mAppStates[i]->pushEvent(EVENT_CLOSE);

  for (unsigned i = 0; i < mAppStates.size(); i++)
    mAppStates[i]->processEvents();

  for (unsigned i = 0; i < mAppStates.size(); i++)
    delete mAppStates[i];

  mAppStates.clear();

  mGlContext.purge();

  if (mKeyboardInfo)
  {
    delete mKeyboardInfo;
    mKeyboardInfo = NULL;
  }

  DestroyWindow(mHwnd);
}

void MglApplication::appWindowRefresh()
{
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(0.0, mWindowWidth, 0.0, mWindowHeight, -20.0, 100.0);
  glViewport(0, 0, mWindowWidth, mWindowHeight);
  glMatrixMode(GL_MODELVIEW);
}

void MglApplication::appShow()
{
  mAppState = 0;
  mAppStates[mAppState]->pushEvent(EVENT_SHOW);
}

LRESULT CALLBACK MglApplication::InitialWndProc(HWND _hwnd, UINT _message, WPARAM _wParam, LPARAM _lParam)
{
  if (_message == WM_NCCREATE)
  {
    LPCREATESTRUCT create_struct = reinterpret_cast<LPCREATESTRUCT>(_lParam);
    void* lpCreateParam = create_struct->lpCreateParams;
    MglApplication* this_window = reinterpret_cast<MglApplication *>(lpCreateParam);
    SetWindowLongPtr(_hwnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(this_window));
    SetWindowLongPtr(_hwnd, GWLP_WNDPROC, reinterpret_cast<LONG_PTR>(&MglApplication::StaticWndProc));
    return this_window->wndProc(_hwnd, _message, _wParam, _lParam);
  }
  return DefWindowProc(_hwnd, _message, _wParam, _lParam);
}

LRESULT CALLBACK MglApplication::StaticWndProc(HWND _hwnd, UINT _message, WPARAM _wParam, LPARAM _lParam)
{
  LONG_PTR user_data = GetWindowLongPtr(_hwnd, GWLP_USERDATA);
  MglApplication* this_window = reinterpret_cast<MglApplication *>(user_data);
  return this_window->wndProc(_hwnd, _message, _wParam, _lParam);
}

LRESULT MglApplication::wndProc(HWND _hwnd, UINT _message, WPARAM _wParam, LPARAM _lParam)
{
  LRESULT result = 0;
  bool processed = false;
  
  //debugPrint(0, "WM_SOMETHING");

  switch (_message)
  {
    case WM_CLOSE:
    {
      PostQuitMessage(0);
      processed = true;
    } break;

    case WM_KEYDOWN:
    { 
      //debugPrint(0, "WM_KEYDOWN");

      switch (_wParam)
      {
        case VK_ESCAPE:
        {
          PostQuitMessage(0);
          processed = true;
        } break;
      }

      if (!processed)
      {
        int virtualKey = LOWORD(_wParam);
      
        EventDataKeyInput eventData;
        
        switch (virtualKey)
        {
          case (VK_SHIFT):   mKeyboardSpecialKeys |= SPECIAL_KEY_SHIFT; break;
          case (VK_CONTROL): mKeyboardSpecialKeys |= SPECIAL_KEY_CONTROL; break;
          case (VK_MENU):    mKeyboardSpecialKeys |= SPECIAL_KEY_ALT; break;
          case (VK_NUMLOCK): mKeyboardSpecialKeys |= SPECIAL_KEY_NUMLOCK; break;
          case (VK_CAPITAL): mKeyboardSpecialKeys |= SPECIAL_KEY_CAPSLOCK; break;
        }

        eventData.key = virtualKey;
        eventData.specialKey = mKeyboardSpecialKeys;
        eventData.down = true;

        setKeyPressed(virtualKey);

        processed = mAppStates[mAppState]->pushEvent(EVENT_KEY_INPUT, &eventData, sizeof(EventDataKeyInput));
      }
    } break;

    case WM_KEYUP:
    {
      //debugPrint(0, "WM_KEYUP");

      int virtualKey = LOWORD(_wParam);

      EventDataKeyInput eventData;

      switch (virtualKey)
      {
        case (VK_SHIFT):   mKeyboardSpecialKeys &= ~SPECIAL_KEY_SHIFT; break;
        case (VK_CONTROL): mKeyboardSpecialKeys &= ~SPECIAL_KEY_CONTROL; break;
        case (VK_MENU):    mKeyboardSpecialKeys &= ~SPECIAL_KEY_ALT; break;
        case (VK_NUMLOCK): mKeyboardSpecialKeys &= ~SPECIAL_KEY_NUMLOCK; break;
        case (VK_CAPITAL): mKeyboardSpecialKeys &= ~SPECIAL_KEY_CAPSLOCK; break;
      }

      eventData.key = virtualKey;
      eventData.specialKey = mKeyboardSpecialKeys;
      eventData.down = false;
      
      setKeyPressed(virtualKey, false);

      processed = mAppStates[mAppState]->pushEvent(EVENT_KEY_INPUT, &eventData, sizeof(EventDataKeyInput));
    } break;

    case WM_CHAR:
    {
      //debugPrint(0, "WM_CHAR");
      processed = true;
    } break;

    case WM_CREATE:
    {
    } break;

    case WM_NCCREATE:
    {
    } break;

    case WM_WINDOWPOSCHANGED:
    {
    } break;

    case WM_MOVING:
    {
    } break;

    case WM_SHOWWINDOW:
    {
      EventSizeData data;
      data.width  = mWindowWidth;
      data.height = mWindowHeight;

      mInputEvents.queueEvent(EVENT_SIZE, &data, sizeof(EventSizeData));
    } break;

    case WM_SIZE:
    {
      EventSizeData data;
      data.width  = LOWORD(_lParam);
      data.height = HIWORD(_lParam);

      // This can be used alternatively.
      //RECT rect;
      //GetClientRect(_hwnd, &rect);

      mInputEvents.queueEvent(EVENT_SIZE, &data, sizeof(EventSizeData));
    } break;

    case WM_MOVE:
    {
      EventMoveData data;
      data.x = LOWORD(_lParam);
      data.y = HIWORD(_lParam);

      mInputEvents.queueEvent(EVENT_MOVE, &data, sizeof(EventMoveData));
    } break;

    case WM_LBUTTONDOWN:
    {
      //debugPrint(0, "WM_LBUTTONDOWN");

      EventDataMouseInput eventData;
      eventData.event = MOUSE_EVENT_BUTTON_DOWN;
      eventData.button = MOUSE_BUTTON1;
      eventData.x = GET_X_LPARAM(_lParam); 
      eventData.y = GET_Y_LPARAM(_lParam); 
      eventData.wheel = 0;
      eventData.down = true;
      eventData.initialDown = true;

      mAppStates[mAppState]->pushEvent(EVENT_MOUSE_INPUT, &eventData, sizeof(EventDataMouseInput));

      processed = true;
    } break;
    
    case WM_LBUTTONUP:
    {
      //debugPrint(0, "WM_LBUTTONUP");

      EventDataMouseInput eventData;
      eventData.event = MOUSE_EVENT_BUTTON_UP;
      eventData.button = 1;
      eventData.x = GET_X_LPARAM(_lParam); 
      eventData.y = GET_Y_LPARAM(_lParam); 
      eventData.wheel = 0;
      eventData.down = false;
      eventData.initialDown = false;

      mAppStates[mAppState]->pushEvent(EVENT_MOUSE_INPUT, &eventData, sizeof(EventDataMouseInput));
      
      processed = true;
    } break;

    case WM_RBUTTONDOWN:
    {
      EventDataMouseInput eventData;
      eventData.event = MOUSE_EVENT_BUTTON_DOWN;
      eventData.button = MOUSE_BUTTON3;
      eventData.x = GET_X_LPARAM(_lParam); 
      eventData.y = GET_Y_LPARAM(_lParam); 
      eventData.wheel = 0;
      eventData.down = true;
      eventData.initialDown = true;

      mAppStates[mAppState]->pushEvent(EVENT_MOUSE_INPUT, &eventData, sizeof(EventDataMouseInput));
    } break;
    
    case WM_MOUSEMOVE:
    {
      EventDataMouseInput eventData;
      eventData.event = MOUSE_EVENT_MOVE;
      eventData.button = 0;
      if (_wParam & MK_LBUTTON)  eventData.button |= MOUSE_BUTTON1;
      if (_wParam & MK_MBUTTON)  eventData.button |= MOUSE_BUTTON2;
      if (_wParam & MK_RBUTTON)  eventData.button |= MOUSE_BUTTON3;
      if (_wParam & MK_XBUTTON1) eventData.button |= MOUSE_BUTTON4;
      if (_wParam & MK_XBUTTON2) eventData.button |= MOUSE_BUTTON5;
      eventData.x = GET_X_LPARAM(_lParam); 
      eventData.y = GET_Y_LPARAM(_lParam); 
      eventData.wheel = 0;
      eventData.down = (eventData.button > 0);
      eventData.initialDown = false;
      
      //debugPrint(0, "WM_MOUSEMOVE: %i", eventData.button);

      mAppStates[mAppState]->pushEvent(EVENT_MOUSE_INPUT, &eventData, sizeof(EventDataMouseInput));
    } break;
  }

  if (!processed)
  {
    result = DefWindowProc(_hwnd, _message, _wParam, _lParam);
  }

  return (result);
}

bool MglApplication::addAppState(AppState* _appState)
{
  bool success = false;

  if (_appState)
  {
    _appState->mApplication = this;

    mAppStates.push_back(_appState);
  }

  return (success);
}

void MglApplication::idle()
{
  double deltaTime = (Mgl::getTickTime() - mLastTime);

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  processEvents();

  mAppStates[mAppState]->update(deltaTime);

  mAppStates[mAppState]->render();
  
  mLastTime = Mgl::getTickTime();

  SwapBuffers(mGlContext.getHDC());
}

bool MglApplication::initFonts()
{
  bool fontsBuilt = false;

  if (mDeviceContext)
  {
    mFontDisplayId = glGenLists(96);

    HFONT  font;
    HFONT  oldFont;

    font = CreateFont(-16,                         // Height Of Font
                      0,                           // Width Of Font
                      0,                           // Angle Of Escapement
                      0,                           // Orientation Angle
                      FW_BOLD,                     // Font Weight
                      FALSE,                       // Italic
                      FALSE,                       // Underline
                      FALSE,                       // Strikeout
                      ANSI_CHARSET,                // Character Set Identifier
                      OUT_TT_PRECIS,               // Output Precision
                      CLIP_DEFAULT_PRECIS,         // Clipping Precision
                      ANTIALIASED_QUALITY,         // Output Quality
                      FF_DONTCARE | DEFAULT_PITCH, // Family And Pitch
                      TEXT("Courier New"));              // Font Name

    if (font)
    {
      oldFont = (HFONT)SelectObject(mDeviceContext, font);
    
      // Builds 96 Characters Starting At Character 32
      if (wglUseFontBitmaps(mDeviceContext, 32, 96, mFontDisplayId))
      {
        fontsBuilt = true;
      }
      SelectObject(mDeviceContext, oldFont);
      DeleteObject(font);
    }
  }

  return (fontsBuilt);
}

void MglApplication::closeFonts()
{
  if (mFontDisplayId != 0)
  {
    glDeleteLists(mFontDisplayId, 96);
    mFontDisplayId = 0;
  }
}

void MglApplication::printText(const char *_text, ...)
{
  if (mFontDisplayId != 0)
  {
    if (_text && strlen(_text) > 0)
    {
      char    buf[256];
      va_list args;

      va_start(args, _text);
      vsprintf_s(buf, 256, _text, args);
      va_end(args);

      glPushAttrib(GL_LIST_BIT);
        glListBase(mFontDisplayId - 32);
        glCallLists((GLsizei)strlen(buf), GL_UNSIGNED_BYTE, buf);
      glPopAttrib();
    }
  }
}

void MglApplication::setAppState(int _appState)
{
  if (mAppState != _appState)
  {
    mAppStates[mAppState]->pushEvent(EVENT_HIDE);
  
    mAppStatePrev = mAppState;
    mAppState = _appState;

    mAppStates[mAppState]->pushEvent(EVENT_SHOW);
  }
}

void MglApplication::renderBorder()
{
  static const int borderOffset = 5;

  glColor3f(0.0f, 1.0f, 0.0f);

  glBegin(GL_LINE_LOOP);
    glVertex2i(borderOffset, borderOffset);
    glVertex2i(mWindowWidth - borderOffset, borderOffset);
    glVertex2i(mWindowWidth - borderOffset, mWindowHeight - borderOffset);
    glVertex2i(borderOffset, mWindowHeight - borderOffset);
  glEnd();
}

void MglApplication::processEvents()
{
  if (mInputEvents.getEvents(mEvents))
  {
    for (unsigned i = 0; i < array::size(mEvents); i++)
    {
      mAppStates[mAppState]->pushEvent(mEvents[i]->type, mEvents[i]->data, mEvents[i]->length);

      switch (mEvents[i]->type)
      {
        case EVENT_SIZE:
        {
          EventSizeData& data = *((EventSizeData *)mEvents[i]->data);

          if (data.width > 0 && data.height > 0)
          {
            mWindowWidth  = data.width;
            mWindowHeight = data.height;
          }

          appWindowRefresh();
        } break;

        case EVENT_MOVE:
        {
          EventMoveData& data = *((EventMoveData *)mEvents[i]->data);

          mWindowX = data.x;
          mWindowY = data.y;
            
          appWindowRefresh();
        } break;
      }
    }
    mInputEvents.flushEvents();
  }

  mAppStates[mAppState]->processEvents();
}

bool MglApplication::isKeyPressed(int _key)
{
  return foundation::hash::get(*mKeyboardInfo, murmur_hash_64(&_key, sizeof(int), 0), false);
}

void MglApplication::setKeyPressed(int _key, bool _down)
{
  foundation::hash::set(*mKeyboardInfo, murmur_hash_64(&_key, sizeof(int), 0), _down);
}
