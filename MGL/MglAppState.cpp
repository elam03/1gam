#include "MglAppState.h"

#include "array.h"
#include "memory.h"

using namespace foundation;
using namespace Mgl;

AppState::AppState() :
  mApplication(0),
  mEventSystem(),
  mEvents(memory_globals::default_allocator())
{
  mEventSystem.init();
  array::clear(mEvents);
}

AppState::~AppState()
{
  mEventSystem.close();
  array::clear(mEvents);
}

bool AppState::pushEvent(int _event, void* _eventData, unsigned _eventDataSize)
{
  return (mEventSystem.queueEvent(_event, _eventData, _eventDataSize));
}

bool AppState::handleEvent(int _event, void* _eventData)
{
  return false;
}

bool AppState::processEvents()
{
  bool processed = false;

  if (mEventSystem.getEvents(mEvents))
  {
    for (unsigned i = 0; i < array::size(mEvents); i++)
    {
      EventHeader* eventHeader = mEvents[i];

      handleEvent(eventHeader->type, mEvents[i]->data);
      //switch (eventHeader->type)
      //{
      //  default:
      //  case EVENT_MOVE:
      //  case EVENT_SIZE:
      //  case EVENT_WINDOW_REFRESH:
      //  {
      //  } break;
      //}
    }

    mEventSystem.flushEvents();

    processed = true;
  }

  return (processed);
}

void AppState::render()
{
}

void AppState::update(double _deltaTime)
{
}