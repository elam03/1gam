#pragma once

#include "MglTexture.h"
#include "MglTypes.h"

#include <map>

namespace Mgl
{

struct Vertex
{
  float v[3];
  float t[2];
};

class ResourceManager
{
public:
  virtual ~ResourceManager();

  static ResourceManager& Instance();

  bool init();
  void close();

  bool     addTexture(const std::wstring& _textureTag, const std::wstring& _fileName);
  Texture* getTexture(const std::wstring& _textureTag);

  bool     addGeometry(const std::wstring& _tag, int _dataSize, Vertex* _vertexData, int _indexSize, unsigned int* _indexData);
  unsigned getGeometry(const std::wstring& _tag);

protected:
  class GeometryInfo
  {
  public:
    static const int MAX_VBOS = 4;

  public:
    GeometryInfo() : vao(0), ibo(0) { for (unsigned i = 0; i < MAX_VBOS; i++) vbos[i] = 0; }
    virtual ~GeometryInfo() {}

    unsigned vao; // vertex array object
    unsigned vbos[MAX_VBOS]; // vertex buffer object
    unsigned ibo; // index buffer object
  };

  ResourceManager();

  std::map<std::wstring, Texture*>     mTextures;
  std::map<std::wstring, GeometryInfo> mGeometryInfos;
};

};