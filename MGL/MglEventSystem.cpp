#include "MglEventSystem.h"

#include "array.h"
#include "memory.h"

using namespace std;
using namespace foundation;
using namespace Mgl;

EventSystem::EventSystem() :
  mEventStreamSize(0),
  mEventStreamMaxSize(0),
  mEventStream(NULL),
  mEventStreamHeaderPackets(memory_globals::default_allocator())
{
}

EventSystem::~EventSystem()
{
}

bool EventSystem::init(unsigned _maxStreamSize)
{
  bool initialized = false;
  
  array::clear(mEventStreamHeaderPackets);

  if (_maxStreamSize > 0)
  {
    mEventStreamSize = 0;
    mEventStreamMaxSize = _maxStreamSize;
    mEventStream = new BYTE[mEventStreamMaxSize];

    initialized = true;
  }

  return (initialized);
}

void EventSystem::close()
{
  array::clear(mEventStreamHeaderPackets);

  if (mEventStream)
  {
    delete[] mEventStream;
    mEventStream = NULL;
  }

  mEventStreamSize    = 0;
  mEventStreamMaxSize = 0;
}

bool EventSystem::queueEvent(int _eventType, const void* _data, unsigned _dataSize)
{
  bool queued = false;

  unsigned eventHeaderSize = sizeof(EventHeader);
  unsigned totalEventSize = eventHeaderSize + _dataSize;

  if ((totalEventSize + mEventStreamSize) < mEventStreamMaxSize)
  {
    EventHeader* eventHeader = (EventHeader *)(mEventStream + mEventStreamSize);
    eventHeader->type   = _eventType;
    eventHeader->length = _dataSize;
    eventHeader->data   = mEventStream + mEventStreamSize + eventHeaderSize;

    mEventStreamSize += eventHeaderSize;

    if (_data && _dataSize > 0)
    {
      memcpy(mEventStream + mEventStreamSize, _data, _dataSize);
      mEventStreamSize += _dataSize;
    }

    array::push_back(mEventStreamHeaderPackets, eventHeader);

    queued = true;
  }

  return (queued);
}

bool EventSystem::getEvents(Array<EventHeader*>& _packets)
{

  bool success = false;

  if (mEventStreamHeaderPackets._size > 0)
  {
    _packets = mEventStreamHeaderPackets;
    
    success = true;
  }

  return (success);
}

bool EventSystem::flushEvents()
{
  bool success = true;

  mEventStreamSize = 0;
  array::clear(mEventStreamHeaderPackets);

  return (success);
}

