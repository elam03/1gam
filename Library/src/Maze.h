#pragma once

#include "cinder/gl/Vbo.h"

#include <mutex>

class Maze
{
public:
  Maze();
  virtual ~Maze();

  bool init(ci::DataSourceRef _path);

  void render();
  void update(double _time_delta);

public:
  ci::gl::VboMesh m_vbo_mesh;
  std::mutex      m_vbo_mesh_mutex;

  double m_update_time_current;
  double m_update_time_threshold;

public:
  std::vector<ci::Vec3f> m_vertices;
  std::vector<uint32_t>  m_indices;
};
