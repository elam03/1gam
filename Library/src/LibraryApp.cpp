#include "cinder/app/AppNative.h"
#include "cinder/gl/gl.h"
#include "cinder/gl/Light.h"
#include "cinder/gl/GLee.h"
#include "cinder/gl/GlslProg.h"
#include "cinder/gl/Texture.h"
#include "cinder/gl/Vbo.h"
#include "cinder/Path2d.h"

#include "cinder/CinderMath.h"
#include "cinder/ImageIo.h"
#include "cinder/MayaCamUI.h"
#include "cinder/ObjLoader.h"

#include "Utils.h"
#include "Maze.h"

#include <thread>
#include <stdlib.h>
#include <tchar.h>

using namespace ci;
using namespace ci::app;
using namespace std;

///////////////////////////////////////////////////////////////////////////////

class LibraryApp : public AppNative
{
public:
  LibraryApp();
  virtual ~LibraryApp();

  void setup();
  void setupCamera();
  void keyDown(KeyEvent event);
  void keyUp(KeyEvent event);
  void mouseDown(MouseEvent event);
	void mouseDrag(MouseEvent event);
  void mouseUp(MouseEvent event);
	void mouseMove(MouseEvent event);
  void update();
  void draw();

  void renderScene();
  void renderGui();

public:
  Vec2f m_mouse_position;

  bool        m_camera_free_look_enabled;
  MayaCamUI   m_camera_free_look;
  CameraPersp m_camera;

  map<int, bool> m_key_states;
  double         m_last_update_time;
  bool           m_debug_enabled;
  string         m_debug_string;

public:
  Maze m_maze;
  
  bool loadShaderProgams();
  DataSourceRef   m_vertex_shader_prog;
  DataSourceRef   m_fragment_shader_prog;
  DataSourceRef   m_geometry_shader_prog;
  gl::GlslProgRef m_shader_program;
  gl::TextureRef  m_texture;
  float           m_time;
  bool            m_reload_shaders;

  thread m_shader_program_watcher;
  bool   m_shader_program_watcher_done;
  void shaderProgramWatcher();
};

void LibraryApp::shaderProgramWatcher()
{
  m_shader_program_watcher_done = false;

  DWORD dwWaitStatus; 
  HANDLE dwChangeHandles[2]; 
  TCHAR lpDrive[4];
  TCHAR lpDir[256];
  TCHAR lpFile[_MAX_FNAME];
  TCHAR lpExt[_MAX_EXT];

  //wcscpy_s(lpDir, 256, L"C:\\Stuff\\");
  wcscpy_s(lpDir, 256, L".\\assets\\");

  _tsplitpath_s(lpDir, lpDrive, 4, NULL, 0, lpFile, _MAX_FNAME, lpExt, _MAX_EXT);

  lpDrive[2] = (TCHAR)'\\';
  lpDrive[3] = (TCHAR)'\0';

  // Watch the directory for file creation and deletion. 

  dwChangeHandles[0] = FindFirstChangeNotification( 
    lpDir,                         // directory to watch 
    FALSE,                         // do not watch subtree 
    FILE_NOTIFY_CHANGE_LAST_WRITE);
    //FILE_NOTIFY_CHANGE_FILE_NAME); // watch file name changes 

  if (dwChangeHandles[0] == INVALID_HANDLE_VALUE) 
  {
    printf("\n ERROR: FindFirstChangeNotification function failed.\n");
    ExitProcess(GetLastError()); 
  }

  //// Watch the subtree for directory creation and deletion. 

  //dwChangeHandles[1] = FindFirstChangeNotification( 
  //  lpDrive,                       // directory to watch 
  //  TRUE,                          // watch the subtree 
  //  FILE_NOTIFY_CHANGE_DIR_NAME);  // watch dir name changes 

  //if (dwChangeHandles[1] == INVALID_HANDLE_VALUE) 
  //{
  //  printf("\n ERROR: FindFirstChangeNotification function failed.\n");
  //  ExitProcess(GetLastError()); 
  //}


  //// Make a final validation check on our handles.

  //if ((dwChangeHandles[0] == NULL) || (dwChangeHandles[1] == NULL))
  //{
  //  printf("\n ERROR: Unexpected NULL from FindFirstChangeNotification.\n");
  //  ExitProcess(GetLastError()); 
  //}

  // Change notification is set. Now wait on both notification 
  // handles and refresh accordingly. 

  while (!m_shader_program_watcher_done) 
  { 
    // Wait for notification.

    printf("\nWaiting for notification...\n");

    dwWaitStatus = WaitForMultipleObjects(1, dwChangeHandles, 
      FALSE, 500); 

    switch (dwWaitStatus) 
    { 
    case WAIT_OBJECT_0: 

      // A file was created, renamed, or deleted in the directory.
      // Refresh this directory and restart the notification.

      //RefreshDirectory(lpDir);
      m_reload_shaders = true;

      if ( FindNextChangeNotification(dwChangeHandles[0]) == FALSE )
      {
        printf("\n ERROR: FindNextChangeNotification function failed.\n");
        ExitProcess(GetLastError()); 
      }
      break; 

    case WAIT_OBJECT_0 + 1: 

      // A directory was created, renamed, or deleted.
      // Refresh the tree and restart the notification.

      //RefreshTree(lpDrive); 
      if (FindNextChangeNotification(dwChangeHandles[1]) == FALSE )
      {
        printf("\n ERROR: FindNextChangeNotification function failed.\n");
        ExitProcess(GetLastError()); 
      }
      break; 

    case WAIT_TIMEOUT:

      // A timeout occurred, this would happen if some value other 
      // than INFINITE is used in the Wait call and no changes occur.
      // In a single-threaded environment you might not want an
      // INFINITE wait.

      printf("\nNo changes in the timeout period.\n");
      break;

    default: 
      printf("\n ERROR: Unhandled dwWaitStatus.\n");
      ExitProcess(GetLastError());
      break;
    }
  }
}

bool LibraryApp::loadShaderProgams()
{
  bool success = false;
  string error;

  m_vertex_shader_prog = loadFile(".\\assets\\vertex_shader.glsl");
  m_fragment_shader_prog = loadFile(".\\assets\\fragment_shader.glsl");
  //m_geometry_shader_prog = loadFile(".\\assets\\geometry_shader.glsl");

  try {
    //gl::GlslProgRef new_shader_program = gl::GlslProg::create(m_vertex_shader_prog, m_fragment_shader_prog, m_geometry_shader_prog, GL_TRIANGLE_STRIP, GL_TRIANGLE_STRIP);
    gl::GlslProgRef new_shader_program = gl::GlslProg::create(m_vertex_shader_prog, m_fragment_shader_prog);

    //m_shader_program.swap(new_shader_program);

    m_shader_program = new_shader_program;
    
    success = true;
  } catch (gl::GlslProgCompileExc &exc) {
    error = exc.what();
  } catch (exception& e) {
    error = e.what();
  }

  error = error;

  return (success);
}

LibraryApp::LibraryApp() : AppNative(),
                           m_shader_program(),
                           m_texture(),
                           m_reload_shaders(false),
                           m_time(0.0f),
                           m_vertex_shader_prog(),
                           m_fragment_shader_prog(),
                           m_shader_program_watcher()
{
  
}

LibraryApp::~LibraryApp()
{
  m_shader_program_watcher_done = true;
  m_shader_program_watcher.join();
}

void LibraryApp::setup()
{
  m_shader_program_watcher = thread(&LibraryApp::shaderProgramWatcher, this);

  srand(static_cast<unsigned int>(time(nullptr)));

  m_mouse_position.set(static_cast<float>(getWindowSize().x / 2), static_cast<float>(getWindowSize().y / 2));

  //const boost::filesystem::path path = loadAsset("Wii - Adventure Island The Beginning - Snowman\\Snowman\\Snowman.obj");
  DataSourceRef path = loadAsset("Wii - Adventure Island The Beginning - Snowman\\Snowman\\Snowman.obj");
  if (!m_maze.init(path))
  {
    quit();
    return;
  }

  glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
  glClearDepth(1.0f);

  glEnable(GL_DEPTH_TEST);
  glEnable(GL_TEXTURE_2D);
  glDepthFunc(GL_LEQUAL);

  glShadeModel(GL_SMOOTH);
  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
  //glColorMaterial(GL_FRONT,GL_AMBIENT_AND_DIFFUSE);

  glEnable(GL_LIGHTING);
  glEnable(GL_COLOR_MATERIAL);

  m_last_update_time = getElapsedSeconds();

  setupCamera();

  ///////////

  m_time = 0.0f;

  m_texture = gl::Texture::create(loadImage(".\\assets\\colorful_watercolor_texture_by_connyduck-d6o409f.jpg"));

  if (!loadShaderProgams())
  {
    quit();
    return;
  }

  ///////////
}

void LibraryApp::setupCamera()
{
  m_camera = CameraPersp(getWindowWidth(), getWindowHeight(), 45.0f);
  m_camera.setEyePoint(Vec3f(0.0f, 10.0f, 10.0f));
  m_camera.setCenterOfInterestPoint(Vec3f::zero());
  m_camera.setPerspective(45.0f, getWindowAspectRatio(), 0.1f, 1000.0f);
  
  m_camera_free_look.setCurrentCam(m_camera);

  m_camera_free_look_enabled = true;
}

void LibraryApp::keyDown(KeyEvent event)
{
  switch (event.getCode())
  {
    case (KeyEvent::KEY_ESCAPE):
    {
      quit();
    } break;

    case (KeyEvent::KEY_c):
    {
      m_camera_free_look_enabled = !m_camera_free_look_enabled;

      if (m_camera_free_look_enabled)
      {
        m_camera_free_look.setCurrentCam(m_camera);
      }
      else
      {
        m_camera = m_camera_free_look.getCamera();
      }
    } break;

    case (KeyEvent::KEY_r):
    {
      if (!loadShaderProgams())
      {
        MessageBox(static_cast<HWND>(getWindow()->getNative()), L"loadShaderPrograms failed!", L"Error", MB_OK);
      }
    } break;

    case (KeyEvent::KEY_1):
    {
    } break;

    default:
    {
      m_key_states[event.getCode()] = true;
    } break;
  }
}

void LibraryApp::keyUp(KeyEvent event)
{
  switch (event.getCode())
  {
    case (KeyEvent::KEY_1):
    {
    } break;

    default:
    {
      m_key_states[event.getCode()] = false;
    } break;
  }
}

void LibraryApp::mouseDown(MouseEvent event)
{
  Vec2i& p = event.getPos();
  
  if (m_camera_free_look_enabled)
  {
    m_camera_free_look.mouseDown(p);
    m_camera = m_camera_free_look.getCamera();
  }

  m_mouse_position = p;

  if (event.isRightDown())
  {
  }
}

void LibraryApp::mouseDrag(MouseEvent event)
{
  Vec2i& p = event.getPos();
  
  if (m_camera_free_look_enabled)
  {
    m_camera_free_look.mouseDrag(p, event.isLeftDown(), event.isMiddleDown(), event.isRightDown());
    m_camera = m_camera_free_look.getCamera();
  }

  m_mouse_position = p;
  
  if (event.isShiftDown() && event.isRightDown())
  {
  }
}

void LibraryApp::mouseUp(MouseEvent event)
{
  if (m_camera_free_look_enabled)
  {
    m_camera_free_look.mouseDown(event.getPos());
    m_camera = m_camera_free_look.getCamera();
  }
}

void LibraryApp::mouseMove(MouseEvent event)
{
  Vec2i& p = event.getPos();

  m_mouse_position = p;
}

void LibraryApp::update()
{
  double time_delta = (getElapsedSeconds() - m_last_update_time);

  if (m_reload_shaders)
  {
    if (!loadShaderProgams())
    {
      //MessageBox(static_cast<HWND>(getWindow()->getNative()), L"loadShaderPrograms failed!", L"Error", MB_OK);
    }
    m_reload_shaders = false;
  }

  m_maze.update(time_delta);

  m_debug_enabled = true;
  m_debug_string = "debugging...";

	m_time += 0.05f;

  m_last_update_time = getElapsedSeconds();
}

void LibraryApp::draw()
{
  // clear out the window with black
  gl::clear(Color(0, 0, 0));
  gl::enableDepthWrite();
  gl::enableDepthRead();
  gl::enableAlphaBlending();

  glLoadIdentity();

  renderScene();
  renderGui();
}

void LibraryApp::renderScene()
{
  {
    m_texture->enableAndBind();
	    m_shader_program->bind();
	    //m_shader_program->uniform("_tex0", 0);
	    //m_shader_program->uniform("_sampleOffset", Vec2f(cos(m_time), sin(m_time)) * ( 3.0f / getWindowWidth()));
	    //gl::drawSolidRect(getWindowBounds());
	    //gl::drawSolidRect(Rectf(0.0f, 0.0f, 256.0f, 256.0f));
      
        m_shader_program->uniform("uMVP", &(m_camera.getProjectionMatrix() * m_camera.getModelViewMatrix()), 1);
        m_shader_program->uniform("uColor", Vec4f(1.0f, 0.0f, 1.0f, 1.0f));
        m_shader_program->uniform("uHasTexture", 0);
        m_shader_program->uniform("uAlpha", 0.0f);
        m_maze.render();

	    m_shader_program->unbind();
	  m_texture->unbind();
  }

  glPushAttrib(GL_LIGHTING);
    glEnable(GL_LIGHTING);

    glPushMatrix();
      gl::setMatrices(m_camera);

      //m_texture->enableAndBind();
      //  //m_shader_program->bind();
      //    //auto mat = m_camera.getProjectionMatrix() * m_camera.getModelViewMatrix();
      //    //m_shader_program->uniform("pvm", &(m_camera.getProjectionMatrix() * m_camera.getModelViewMatrix()), 1);
      //    //m_shader_program->uniform("_mvp", &(m_camera.getProjectionMatrix() * m_camera.getModelViewMatrix()), 1);
      //    //m_shader_program->uniform("_time", m_time);
      //    //m_shader_program->uniform("_model_view", &m_camera.getModelViewMatrix(), 1);
      //    //m_shader_program->uniform("_projection", &m_camera.getProjectionMatrix(), 1);

      //    //m_shader_program->uniform("alpha", mMeshAlpha);
      //    //m_shader_program->uniform("amp", mMeshWaveAmplitude);
      //    //m_shader_program->uniform("eyePoint", mEyePoint);
      //    //m_shader_program->uniform("lightAmbient", mLightAmbient);
      //    //m_shader_program->uniform("lightDiffuse", mLightDiffuse);
      //    //m_shader_program->uniform("lightPosition", mLightPosition);
      //    //m_shader_program->uniform("lightSpecular", mLightSpecular);
      //    //m_shader_program->uniform("mvp", gl::getProjection() * gl::getModelView());
      //    //m_shader_program->uniform("phase", mElapsedSeconds);
      //    //m_shader_program->uniform("rotation", mBoxRotationSpeed);
      //    //m_shader_program->uniform("scale", mMeshScale);
      //    //m_shader_program->uniform("dimensions", Vec4f(mBoxDimensions));
      //    //m_shader_program->uniform("shininess", mLightShininess);
      //    //m_shader_program->uniform("speed", mMeshWaveSpeed);
      //    //m_shader_program->uniform("transform", mBoxEnabled);
      //    //m_shader_program->uniform("uvmix", mMeshUvMix);
      //    //m_shader_program->uniform("width", mMeshWaveWidth);
      //    //m_shader_program->uniform("uMVP", &(m_camera.getProjectionMatrix() * m_camera.getModelViewMatrix()), 1);
      //    
      //    //m_shader_program->uniform("_tex0", 0);
      //    //m_shader_program->uniform("_texture_sampler", Vec2f(cos(m_time), sin(m_time)) * ( 3.0f / getWindowWidth()));
	     //   //m_shader_program->uniform("_sampleOffset", Vec2f(cos(m_time), sin(m_time)) * ( 3.0f / getWindowWidth()));
      //    
      //    m_maze.render();
      //  //m_shader_program->unbind();
      //m_texture->unbind();

      glEnable(GL_TEXTURE_2D);

      glDisable(GL_LIGHTING);

      glPushMatrix();
        gl::drawCoordinateFrame(1.0f);
      glPopMatrix();

      gl::drawStringCentered("blah?", Vec2f(50.0f, 50.0f));

    glPopMatrix();
  glPopAttrib();
}

void LibraryApp::renderGui()
{
  glPushAttrib(GL_LIGHTING);
    glDisable(GL_LIGHTING);
    gl::setMatricesWindow(getWindowSize(), true);

    gl::color(0.0f, 0.0f, 1.0f);
    gl::drawSolidCircle(m_mouse_position, 5.0f);

    if (m_debug_enabled)
    {
      glPushMatrix();
        gl::translate(150.0f, 150.0f, 0.0f);
        gl::scale(2.0f, 2.0f, 2.0f);
        gl::drawStringCentered(m_debug_string, Vec2f(0.0f, 0.0f));
      glPopMatrix();
    }
  glPopAttrib();
}

CINDER_APP_NATIVE(LibraryApp, RendererGl)
