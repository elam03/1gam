#include "Maze.h"

#include "cinder/gl/gl.h"
#include "cinder/gl/Texture.h"
#include "cinder/CinderMath.h"
#include "cinder/ObjLoader.h"
#include "cinder/TriMesh.h"


#include "Utils.h"

using namespace ci;
using namespace ci::gl;
using namespace std;

Maze::Maze() : m_update_time_current(0.0), m_update_time_threshold(0.05)
{
}

Maze::~Maze()
{
}

////////////////
//add_vertex(vertex)
//{
//   for (each triangle)
//   {
//      if (vertex is inside triangle's circumscribed circle)
//      {
//         store triangle's edges in edgebuffer
//         remove triangle
//      }
//   }
//   remove all double edges from edgebuffer,
//      keeping only unique ones
//   for (each edge in edgebuffer)
//   {
//      form a new triangle between edge and vertex
//   }
//}

//triangulate()
//{
//   create supertriangle and add it to the triangulation
//   for (each vertex)
//   {
//      add_vertex(vertex)
//   }
//   for (each triangle)
//   {
//      if (one or more vertices stem from supertriangle)
//      {
//         remove triangle
//      }
//   }
//}
////////////////

bool Maze::init(DataSourceRef _path)
{
  bool success = true;

  //{ ///////////////////////////////////////////////////////////////////////////
  //  ObjLoader obj_loader(_path);
  //  TriMesh tri_mesh;
  //  obj_loader.load(&tri_mesh, true, false, false);
  //
  //  m_vbo_mesh = gl::VboMesh(tri_mesh);
  //} ///////////////////////////////////////////////////////////////////////////
  
  { ///////////////////////////////////////////////////////////////////////////
    m_vbo_mesh = gl::VboMesh();

    //int segments = 10;
    //float increment = 1.0f / segments;
    //for (int i = 0; i < segments; i++)
    //{
    //  Vec2f p(randf(-15.0f, 15.0f), randf(-15.0f, 15.0f));
    //  
    //  m_vertices.push_back(Vec3f(p.x, 0.0f, p.y));
    //  m_indices.push_back(i);
    //}

    float angle = 0.0f;

    do
    {
      float r = randf(2.0f, 8.0f);
      
      float height = 0.0f;
      float x = cos(toRadians(angle));
      float y = sin(toRadians(angle));

      Vec3f p(x, 0.0f, y);

      m_vertices.push_back(p * r);
      m_vertices.push_back(p * (r * 2.0f));

      angle += randf(5.0f, 10.0f);
      //angle += 90.0f;
    } while (angle < 360.0f);
    
    for (int i = 0; i < m_vertices.size(); i++)
      m_indices.push_back(i);
    
    m_indices.push_back(0);
    m_indices.push_back(1);

    VboMesh::Layout layout;
    layout.setDynamicPositions();
    layout.setStaticIndices();
    //layout.setDynamicIndices();
    layout.setDynamicColorsRGB();

    m_vbo_mesh = VboMesh(m_vertices.size(), m_indices.size(), layout, GL_TRIANGLE_STRIP);

    for (auto iter = m_vbo_mesh.mapVertexBuffer(); !iter.isDone(); ++iter)
    {
      //iter.setPosition(Vec3f(p.x, 0.0f, p.y));
      int i = iter.getIndex();
      iter.setPosition(m_vertices[i]);

      float c = randf(.25f, 1.0f);
      iter.setColorRGB(Color(c, c, c));
    }

    m_vbo_mesh.bufferIndices(m_indices);
  } ///////////////////////////////////////////////////////////////////////////

  return (success);
}

void Maze::render()
{
  glPushAttrib(GL_LIGHTING);
    glDisable(GL_LIGHTING);
    m_vbo_mesh_mutex.lock();
      glPointSize(3.0f);
      m_vbo_mesh.bindAllData();
        draw(m_vbo_mesh);
      m_vbo_mesh.unbindBuffers();
    m_vbo_mesh_mutex.unlock();
  glPopAttrib();
}

void Maze::update(double _time_delta)
{
  static double t = 0.0;

  m_update_time_current += _time_delta;
  t += _time_delta;

  m_vbo_mesh_mutex.lock();
    if (m_update_time_current >= m_update_time_threshold)
    {
      //for (auto iter = m_vbo_mesh.mapVertexBuffer(); !iter.isDone(); ++iter)
      //{
      //  Vec3f* p = iter.getPositionPointer();
      //  int stride = iter.getStride();

      //  //p->y = sin(t / 100.0);
      //}

      m_update_time_current -= m_update_time_threshold;
    }
  m_vbo_mesh_mutex.unlock();
}