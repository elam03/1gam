/*
#version 440
layout(location = 0) uniform mat4 uMVP;

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 vertex_tex_coord;

layout(location = 0) out vec2 fragment_tex_coord;

void main() {
	fragment_tex_coord = vertex_tex_coord;
	gl_Position = uMVP * vec4(position, 1.0);
}
//*/

//out outVertex;
//in vec3 position;
//layout(location = 0) in vec3 vertexPosition_modelspace;
//layout(location = 1) in vec3 vertexColor;
//in mat4 _mvp;

//out vec3 fragmentColor;

//void main()
//{
	//gl_TexCoord[0] = gl_MultiTexCoord0;
  //gl_Position.xyz = vertexPosition_modelspace;
	//gl_Position = ftransform();
  //gl_Position.y += 3.05;
  //gl_Position = mvp * 
  //gl_Position = gl_ProjectionMatrix * gl_ModelViewMatrix * gl_Vertex
  //outVertex = ftransform();
  //fragmentColor = vertexColor;
//}

/*
in vec3 inVertex;
//layout(location = 1) in vec3 vertexColor;
//out vec3 fragmentColor;
uniform mat4 _model_view;
uniform mat4 _projection;
uniform mat4 _mvp;

void main()
{
  //mat4 mvp = _projection * _model_view;
  //gl_TexCoord[0] = gl_MultiTexCoord1;
  vec4 v = vec4(inVertex, 1);
  //gl_Position = _mvp * v;
  //gl_Position = _projection * _model_view * v;
  gl_Position = _mvp * v;
  //fragmentColor = vertexColor;
}
//*/

#version 330 core
in vec4 in_vertex;

uniform mat4 _model_view;
uniform mat4 _projection;
uniform mat4 uMVP;

void main()
{
  gl_Position = uMVP * in_vertex;
}
