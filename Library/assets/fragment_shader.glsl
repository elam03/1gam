/*
// Uniforms
uniform float alpha;
uniform vec3 eyePoint;
uniform vec4 lightAmbient;
uniform vec4 lightDiffuse;
uniform vec4 lightSpecular;
uniform vec3 lightPosition;
uniform float shininess;
uniform bool transform;
uniform float uvmix;

// in
in vec4 position;
in vec4 normal;
in vec2 uv;

// out
out vec4 color;

void main()
{
  // Initialize color
	color = vec4(0.0, 0.0, 0.0, 0.0);

	// Normalized eye position
	vec3 eye = normalize(-eyePoint);

  // Calculate light and reflection positions
	vec3 light = normalize(lightPosition.xyz - position.xyz);
	vec3 reflection = normalize(-reflect(light, normal.xyz));

  // Calculate ambient, diffuse, and specular values
	vec4 ambient = lightAmbient;
	vec4 diffuse = clamp(lightDiffuse * max(dot(normal.xyz, light), 0.0), 0.0, 1.0);
	vec4 specular = clamp(lightSpecular * pow(max(dot(reflection, eye), 0.0), 0.3 * shininess), 0.0, 1.0); 

  // Set color from light
	color = ambient + diffuse + specular;

	// Mix with UV map
	color = mix(color, vec4(uv.s, uv.t, 1.0, 1.0), uvmix);

	// Set alpha
	color.a = color.a * alpha;
}
//*/

/*
#version 440
layout(location = 1) uniform sampler2D uTexture;
layout(location = 2) uniform vec4 uColor;
layout(location = 3) uniform int uHasTexture;
layout(location = 4) uniform float uAlpha;

layout(location = 0) in vec2 tex_coord;

out vec4 outColor;

void main() {
	if (uHasTexture != 0) {	
		outColor = vec4(texture2D(uTexture, tex_coord).rgb, uAlpha);
	} else {
		//outColor = uColor;
    outColor = vec4(0.0, 1.0, 0.0, 1.0);
	}
}
*/

//*
#version 330 core

uniform sampler2D uTexture;
uniform vec4 uColor;
uniform int uHasTexture;
uniform float uAlpha;
//uniform sampler2D tex;

in vec3 fragmentColor;
in vec2 tex_coord;

out vec4 outColor;

void main() {
  //if (uHasTexture != 0) {	
  if (0) {
		outColor = vec4(texture2D(uTexture, tex_coord).rgb, uAlpha);
	} else {
		//outColor = uColor;
    outColor = vec4(0.0, 1.0, 0.0, 1.0);
	}
  //vec4 c = texture2D(tex, gl_TexCoord[0].st);
  //outColor = vec3(1.0, 0.0, 0.0);
  //outColor = fragmentColor;
  //outColor = c;
}
//*/

/*
#version 330 core
 
// Interpolated values from the vertex shaders
in vec2 _uv;
in vec3 fragmentColor;
 
// Ouput data
out vec3 color;
 
// Values that stay constant for the whole mesh.
uniform sampler2D _texture_sampler;
uniform float _time;
 
void main()
{
 
    // Output color = color of the texture at the specified UV
    //color = texture(_texture_sampler, _uv).rgb;
    //color = vec3(_texture_sampler, 1.0, 0.0);
    //color.rg = vec2(1.0, 0.0);
    //color.rgb = vec3(_time, _time, _time);
    //vec3 color = vec3(sin(_time + 1.8), cos(_time), sin(_time));
    //gl_FragColor.rgb = color;
    //color = vec3(1.0, 1.0, 1.0);
    //color.a = 1.0;
    color = fragmentColor;
}
*/

/*
#version 110

uniform sampler2D _tex0;
uniform vec2 _sampleOffset;
uniform float _time;

void main()
{
  vec3 color = vec3(sin(_time + 1.8), cos(_time), sin(_time));
  gl_FragColor.rgb = color;
  gl_FragColor.a = 0.5;
}

*/

/*
#version 110

uniform sampler2D tex0;
uniform vec2 sampleOffset;

float weights[21];

void main()
{
	weights[0] = 0.0091679276560113852;
	weights[1] = 0.014053461291849008;
	weights[2] = 0.020595286319257878;
	weights[3] = 0.028855245532226279;
	weights[4] = 0.038650411513543079;
	weights[5] = 0.049494378859311142;
	weights[6] = 0.060594058578763078;
	weights[7] = 0.070921288047096992;
	weights[8] = 0.079358891804948081;
	weights[9] = 0.084895951965930902;
	weights[10] = 0.086826196862124602;
	weights[11] = 0.084895951965930902;
	weights[12] = 0.079358891804948081;
	weights[13] = 0.070921288047096992;
	weights[14] = 0.060594058578763092;
	weights[15] = 0.049494378859311121;
	weights[16] = 0.0386504115135431;
	weights[17] = 0.028855245532226279;
	weights[18] = 0.020595286319257885;
	weights[19] = 0.014053461291849008;
	weights[20] = 0.00916792765601138;


	vec3 sum = vec3( 0.0, 0.0, 0.0 );
	vec2 baseOffset = -10.0 * sampleOffset;
	vec2 offset = vec2( 0.0, 0.0 );
	for( int s = 0; s < 21; ++s ) {
		sum += texture2D( tex0, gl_TexCoord[0].st + baseOffset + offset ).rgb * weights[s];
		offset += sampleOffset;
	}
	gl_FragColor.rgb = sum;
	gl_FragColor.a = 1.0;
}

*/