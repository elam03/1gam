@echo off

REM ===========================================================================
REM This is a useful piece of code to get absolute and relative paths.

REM set REL_PATH=.\
REM set ABS_PATH=

REM rem // save current directory and change to target directory
REM pushd %REL_PATH%
REM rem // save value of CD variable (current directory)
REM set ABS_PATH=%CD%
REM rem // restore original directory
REM popd

REM echo Relative path : %REL_PATH%
REM echo Maps to path  : %ABS_PATH%
REM ===========================================================================

IF %1.==. GOTO No1

SET "ProjectName=%1"
SET "BinaryFolder=bin32"

ECHO "Auto Building 7-zip file for '%ProjectName%'..."

IF EXIST ".\BuildTree\%BinaryFolder%\%ProjectName%.exe" (ECHO Found ".\BuildTree\%BinaryFolder%\%ProjectName%.exe") ELSE (GOTO NoProjectName)

del "1GAM - %ProjectName%.7z"
7z.exe a -r "1GAM - %ProjectName%.7z" "BuildTree" -x!*.exe -x!*.log -x!highscore.txt
7z.exe a -r "1GAM - %ProjectName%.7z" "BuildTree\%BinaryFolder%\%ProjectName%.exe"

GOTO End1

ELSE

:No1
  ECHO Need a 'ProjectName' as the first argument.
GOTO End1

:NoProjectName
  ECHO Failed to find ".\BuildTree\%BinaryFolder%\%ProjectName%.exe"
GOTO End1

:End1
