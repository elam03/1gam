#include "cinder/app/AppNative.h"
#include "cinder/ImageIo.h"
#include "cinder/gl/gl.h"
#include "cinder/gl/Texture.h"

#include "Resources.h"

#include <vector>

using namespace ci;
using namespace ci::app;
using namespace std;

struct Clock
{
  double time_start;
  double time_end;
  double duration;
};

struct Letter
{
  float x;
  float y;

  double angle;
  double angle_rate;
  double angle_offset;
  double angle_variance;

  double glow;
  double glow_rate;

  double scale;
  double scale_rate;

  float xv;
  float yv;

  bool frozen;

  char  value;
};

inline double random(double _min, double _max) { return (_min + fmod(rand(), (_max - _min))); }
inline float random(float _min, float _max) { return (_min + (float)fmod(rand(), (_max - _min))); }
inline int random(int _min, int _max) { return (_min + rand() % (_max - _min)); }

class DistortogramsApp : public AppNative
{
public:
  void setup();
  
  void mouseDown(MouseEvent event);
  void mouseMove(MouseEvent event);
	void keyDown(KeyEvent event);

  void update();
  void draw();

public:
  Vec3f m_cursor_position;
  float m_cursor_radius;

  float m_angle;

  gl::Texture m_texture_cube;
  gl::Texture m_texture_fonts;

  void draw(char _letter);
  void drawString(const string& _line);
  void drawAttempt();
  void draw(const Letter& _letter);

  void prepareAnswer(int _index);

  string m_attempt;
  int    m_answer_index;

  bool   m_done;
  bool   m_game_won;
  double m_transition_due_time;

  vector<Letter> m_letters;

  vector<string> m_answer_pool;
};

void DistortogramsApp::setup()
{
  string wordsPath = ".\\resources\\words.txt";

#ifdef _DEBUG
  wordsPath = "..\\resources\\words.txt";
#endif

  srand((unsigned)time(nullptr));

  ifstream is(wordsPath.c_str());

  if (is.is_open())
  {
    while (!is.eof())
    {
      string s;

      is >> s;

      if (s.length() > 0)
      {
        std::transform(s.begin(), s.end(), s.begin(), tolower);

        bool isWordValid = true;
        for (unsigned i = 0; i < s.size(); i++)
        {
          if (s[i] < 'a' || s[i] > 'z')
          {
            console() << "throwing out: \"" << s << "\"" << endl;
            isWordValid = false;
            break;
          }
        }

        if (isWordValid)
          m_answer_pool.push_back(s);
      }
    }

    is.close();

    if (m_answer_pool.size() > 0)
      prepareAnswer(rand() % m_answer_pool.size());
  }
  
  if (m_answer_pool.size() <= 0)
  {
    m_answer_pool.push_back("failure");
    prepareAnswer(0);
  }

  m_cursor_position.set(50.0f, 50.0f, 50.0f);
  m_cursor_radius = 5.0f;

  m_angle = 0.0f;

  m_attempt = "";

  m_done = false;
  m_game_won = false;
  m_transition_due_time = 0.0;

  m_texture_cube  = loadImage(loadAsset("img_8.png"));
  m_texture_fonts = loadImage(loadAsset("fonts.png"));
}

void DistortogramsApp::mouseDown(MouseEvent event)
{
  Vec2f p = event.getPos();
  m_cursor_position.set(p.x, p.y, 0.0f);
}

void DistortogramsApp::mouseMove(MouseEvent event)
{
  Vec2f p = event.getPos();
  m_cursor_position.set(p.x, p.y, 0.0f);
}

void DistortogramsApp::keyDown(KeyEvent event)
{
  if (event.getCode() == KeyEvent::KEY_ESCAPE)
  {
    quit();
  }

  char value = tolower(event.getChar());
  
  if (value >= 'a' && value <= 'z')
  {
    for (unsigned i = 0; i < m_letters.size(); i++)
    {
      if (!m_letters[i].frozen && m_letters[i].value == value)
      {
        m_letters[i].frozen = true;
        break;
      }
    }

    m_attempt += value;
  }
  else if (value == '\n' || value == '\r')
  {
    if (m_answer_pool.size() > 0)
    {
      if (m_attempt == m_answer_pool[m_answer_index])
      {
        m_done = true;
        m_transition_due_time = getElapsedSeconds() + 1.0;

        m_answer_pool.erase(m_answer_pool.begin() + m_answer_index);
      }
    }
    
    m_attempt = "";

    for (unsigned i = 0; i < m_letters.size(); i++)
      m_letters[i].frozen = false;
  }
  else if (value == '\b')
  {
    if (m_attempt.size() > 0)
    {
      for (int i = m_letters.size() - 1; i >= 0; i--)
      {
        if (m_letters[i].frozen && m_letters[i].value == m_attempt[m_attempt.size() - 1])
        {
          m_letters[i].frozen = false;
          break;
        }
      }

      m_attempt.erase(m_attempt.begin() + m_attempt.size() - 1);
    }
  }
}

void DistortogramsApp::update()
{
  m_cursor_radius += 1.0f;
  if (m_cursor_radius > 50.0f)
    m_cursor_radius = 5.0f;

  m_angle += .10f;
  if (m_angle >= 360.0f)
    m_angle -= 360.0f;

  for (unsigned i = 0; i < m_letters.size(); i++)
  {
    Letter& letter = m_letters[i];

    if (!letter.frozen)
    {
      letter.x += letter.xv;
      letter.y += letter.yv;
    
      if (letter.x > getWindowWidth())  letter.x -= getWindowWidth();
      if (letter.x < 0)                 letter.x += getWindowWidth();
      if (letter.y > getWindowHeight()) letter.y -= getWindowHeight();
      if (letter.y < 0)                 letter.y += getWindowHeight();

      letter.angle = letter.angle_offset + sin(getElapsedSeconds() * letter.angle_rate) * letter.angle_variance;
    
      // 0.5 - 1.0 -> mid: .75
      letter.glow = sin(getElapsedSeconds() * letter.glow_rate) * 0.25f + 0.75f;

      // 1.0 - 2.0 -> mid: 1.5
      letter.scale = sin(getElapsedSeconds() * letter.scale_rate) * 0.5f + 1.5f;
    }
  }

  if (m_done)
  {
    if (getElapsedSeconds() >= m_transition_due_time)
    {
      if (m_answer_pool.size() > 0)
      {
        prepareAnswer(rand() % m_answer_pool.size());
      }
      else
      {
        m_letters.clear();
        
        m_game_won = true;
      }
      
      m_done = false;
      m_transition_due_time = 0.0;
    }
  }
}

void DistortogramsApp::draw()
{
  // clear out the window with black
  gl::clear(Color(0, 0, 0));
  gl::enableAlphaTest();
  gl::enableDepthRead();

  gl::setMatricesWindow(getWindowSize());
  glLoadIdentity();

  Vec3f p = m_cursor_position;
  Vec3f r(m_cursor_radius, m_cursor_radius, m_cursor_radius);
  
  gl::enableAlphaBlending();
  for (unsigned i = 0; i < m_letters.size(); i++)
  {
    draw(m_letters[i]);
  }
  gl::disableAlphaBlending();

  drawAttempt();

  if (m_done)
  {
    gl::pushMatrices();
      gl::color(1.0f, 1.0f, 1.0f, 1.0f);
      gl::translate(100.0f, 150.0f, 0.0f);
      gl::scale(3.0f, 3.0f, 3.0f);

      drawString("Correct");
    gl::popMatrices();
  }

  if (m_game_won)
  {
    gl::pushMatrices();
      gl::color(1.0f, 1.0f, 1.0f, 1.0f);
      gl::translate(10.0f, 200.0f, 0.0f);
      gl::scale(1.0f, 1.0f, 1.0f);

      drawString("Winner. You been playing for a while.");
    gl::popMatrices();
  }
}

void DistortogramsApp::draw(char _letter)
{
  if (m_texture_fonts)
  {
    static const int LetterSize = 16;

    int offset = min(tolower(_letter) - 'a', 26);

    int x = 3 + offset * (LetterSize + 1);
    int y = 88;
    int w = LetterSize;
    int h = LetterSize;

    Area sourceRect(x, y, x + w, y + h);
    Area destRect(-LetterSize / 2, -LetterSize / 2, LetterSize / 2, LetterSize / 2);
  
    gl::draw(m_texture_fonts, sourceRect, destRect);
  }
}

void DistortogramsApp::drawString(const string& _line)
{
  for (unsigned i = 0; i < _line.size(); i++)
  {
    gl::pushMatrices();
      gl::translate(16.0f * i, 0.0f, 0.0f);
      draw(_line[i]);
    gl::popMatrices();
  }
}

void DistortogramsApp::drawAttempt()
{
  gl::pushMatrices();
    gl::color(1.0f, 1.0f, 1.0f, 1.0f);
    gl::translate(100.0f, 150.0f, 0.0f);

    drawString(m_attempt);
    //for (unsigned i = 0; i < m_attempt.size(); i++)
    //{
    //  gl::pushMatrices();
    //    gl::translate(100.0f + 16.0f * i, 150.0f, 0.0f);
    //    draw(m_attempt[i]);
    //  gl::popMatrices();
    //}
  gl::popMatrices();
}

void DistortogramsApp::draw(const Letter& _letter)
{
  if (m_texture_fonts)
  {
    static const int LetterSize = 16;

    int offset = min(tolower(_letter.value) - 'a', 26);

    int x = 3 + offset * (LetterSize + 1);
    int y = 88;
    int w = LetterSize;
    int h = LetterSize;

    Area sourceRect(x, y, x + w, y + h);
    Area destRect(-LetterSize / 2, -LetterSize / 2, LetterSize / 2, LetterSize / 2);
  
    gl::color((float)_letter.glow, (float)_letter.glow, (float)_letter.glow, 1.0f);
    //gl::color(1.0f, 1.0f, 1.0f, 1.0f);
    gl::pushMatrices();
      gl::translate(_letter.x, _letter.y, 0.0f);
      gl::rotate((float)_letter.angle);
      gl::scale((float)_letter.scale, (float)_letter.scale, (float)_letter.scale);
      gl::draw(m_texture_fonts, sourceRect, destRect);
    gl::popMatrices();
  }
}

void DistortogramsApp::prepareAnswer(int _index)
{
  if (_index < 0 || _index >= m_answer_pool.size())
    return;

  m_answer_index = _index;
  string& answer = m_answer_pool[m_answer_index];

  m_letters.clear();

  double base_angle_rate = 10.0;
  double base_angle_variance = 60.0;
  
  double base_glow_rate_min = 0.5;
  double base_glow_rate_max = 2.0;

  double base_scale_rate_min = 0.5;
  double base_scale_rate_max = 2.0;

  for (unsigned i = 0; i < answer.size(); i++)
  {
    Letter letter;
    letter.x = (float)(rand() % getWindowWidth());
    letter.y = (float)(rand() % getWindowHeight());

    letter.angle          = 0.0;
    letter.angle_rate     = random(0.0, base_angle_rate);
    letter.angle_offset   = random(0.0, 360.0);
    letter.angle_variance = random(0.0, base_angle_variance);
    
    letter.glow        = 1.0;
    letter.glow_rate   = random(base_glow_rate_min, base_glow_rate_max);
    
    letter.scale      = 1.0;
    letter.scale_rate = random(base_scale_rate_min, base_scale_rate_max);

    letter.xv = random(0.25f, 1.0f) * ((rand() % 2 == 0) ? -1.0f : 1.0f);
    letter.yv = random(0.25f, 1.0f) * ((rand() % 2 == 0) ? -1.0f : 1.0f);
    
    letter.value = answer[i];

    letter.frozen = false;

    m_letters.push_back(letter);
  }
}

CINDER_APP_NATIVE(DistortogramsApp, RendererGl)
