#include "cinder/app/AppNative.h"
#include "cinder/gl/gl.h"
#include "cinder/gl/Light.h"
#include "cinder/gl/GLee.h"
#include "cinder/gl/GlslProg.h"
#include "cinder/gl/Texture.h"
#include "cinder/gl/Vbo.h"
#include "cinder/Path2d.h"

#include "cinder/CinderMath.h"
#include "cinder/ImageIo.h"
#include "cinder/MayaCamUI.h"
#include "cinder/ObjLoader.h"

using namespace ci;
using namespace ci::app;
using namespace std;

template<typename T>
static inline T random(T _min, T _max) { return (_min + ((_max - _min) * ((float)(rand() % 1000) / 1000.0f))); }

template<typename T>
static inline T random(T _max) { return (random<T>(0, _max)); }

#define randf(max) random<float>(max)
#define randf(min, max) random<float>(min, max)

#define randd(max) random<double>(max)
#define randd(min, max) random<double>(min, max)

static float angleTo(const Vec3f& _origin, const Vec3f& _target)
{
  float dx =  (float)(_target.x - _origin.x);
  float dz = -(float)(_target.z - _origin.z);

  float result = toDegrees(atan2(dz, dx));

  if (result < 0.0f)
    result += 360.0f;

  return (result);
}

///////////////////////////////////////////////////////////////////////////////

struct SnowMan
{
  SnowMan() : m_position(), m_angle(0.0f), m_direction(), m_max_speed(1.0f), m_size(3.0f), m_color(0.4f, 0.4f, 0.4f), m_update_rate(0.0), m_last_update_time(0.0) { m_color.set(1.0f, 1.0f, 1.0f); }
  virtual ~SnowMan() {}

  Vec3f m_position;
  float m_angle;
  Vec3f m_direction;
  float m_max_speed;
  float m_size;
  Color m_color;

  double m_update_rate;
  double m_last_update_time;

  static gl::VboMesh vbo_mesh;
  static gl::Texture texture;
};

gl::VboMesh SnowMan::vbo_mesh;
gl::Texture SnowMan::texture;

void drawSnowMan(const SnowMan& _snow_man)
{
  glPushMatrix();
    gl::translate(_snow_man.m_position);
    gl::scale(Vec3f(_snow_man.m_size, _snow_man.m_size, _snow_man.m_size));
    gl::rotate(Vec3f(0.0f, _snow_man.m_angle, 0.0f));

    //stringstream ss;
    //ss << _snow_man.m_position;
    //gl::drawStringCentered(ss.str(), Vec2f(0.0f, 5.0f));

    glPushMatrix();
      gl::rotate(Vec3f(0.0f, 90.0f, 0.0f));
      SnowMan::texture.enableAndBind();
        SnowMan::vbo_mesh.bindAllData();
          gl::color(1.0f, 1.0f, 1.0f);
          gl::draw(SnowMan::vbo_mesh);
        SnowMan::vbo_mesh.unbindBuffers();
      SnowMan::texture.unbind();
    glPopMatrix();

    glPushAttrib(GL_LIGHTING);
      glDisable(GL_LIGHTING);
      glPushMatrix();
        gl::color(1.0f, 1.0f, 1.0f);
        gl::scale(5.0f, 5.0f, 5.0f);
        gl::drawVector(Vec3f::zero(), Vec3f(1.0f, 0.0f, 0.0f));
      glPopMatrix();
    glPopAttrib();
  glPopMatrix();
}

///////////////////////////////////////////////////////////////////////////////

struct Penguin
{
  Penguin() : m_position(), m_max_speed(1.0f), m_angle(0.0f), m_angle_turn_rate(5.0f), m_direction(1.0f, 0.0f, 0.0f), m_color(1.0f, 1.0f, 1.0f) {}
  virtual ~Penguin() {}

  Vec3f m_position;
  float m_max_speed;
  float m_angle;
  float m_angle_turn_rate;
  Vec3f m_direction;
  Color m_color;
  
  static gl::VboMesh vbo_mesh;
  static gl::Texture texture;
};

gl::VboMesh Penguin::vbo_mesh;
gl::Texture Penguin::texture;

void drawPenguin(const Penguin& _penguin)
{
  static const float size = 5.0f;
  
  glPushMatrix();
    gl::translate(_penguin.m_position);
    gl::translate(Vec3f(0.0f, 15.0f, 0.0f));
    gl::scale(size, size, size);
    gl::rotate(Vec3f(0.0f, _penguin.m_angle, 0.0f));

    glPushMatrix();
      gl::translate(Vec3f(0.0f, 3.75f, 0.0f));
      gl::rotate(Vec3f(0.0f, 90.0f, 0.0f));
      Penguin::texture.enableAndBind();
        Penguin::vbo_mesh.bindAllData();
          gl::color(1.0f, 1.0f, 1.0f);
          gl::draw(Penguin::vbo_mesh);
        Penguin::vbo_mesh.unbindBuffers();
      Penguin::texture.unbind();
    glPopMatrix();

    glPushAttrib(GL_LIGHTING);
      glDisable(GL_LIGHTING);
      glPushMatrix();
        gl::color(1.0f, 1.0f, 1.0f);
        gl::scale(5.0f, 5.0f, 5.0f);
        gl::drawVector(Vec3f::zero(), Vec3f(1.0f, 0.0f, 0.0f));
      glPopMatrix();
    glPopAttrib();
  glPopMatrix();
}

///////////////////////////////////////////////////////////////////////////////

class SnowApp : public AppNative
{
public:
  SnowApp();
  virtual ~SnowApp();

  void setup();
  void setupCamera();
  void keyDown(KeyEvent event);
  void keyUp(KeyEvent event);
  void mouseDown(MouseEvent event);
	void mouseDrag(MouseEvent event);
  void mouseUp(MouseEvent event);
	void mouseMove(MouseEvent event);
  void update();
  void draw();

  void setupLights();
  void updateLights(const Vec2i& _pos);

  void renderScene();
  void renderGui();

public:
  Vec2f m_mouse_position;

  bool        m_camera_free_look_enabled;
  MayaCamUI   m_camera_free_look;
  CameraPersp m_camera;

  gl::Light* m_main_light;
  Path2d     m_light_path;
  float      m_light_path_t;

  vector<SnowMan> m_snow_men;

  map<int, bool> m_key_states;

  Penguin m_penguin;

  float m_world_size;

  gl::Texture m_texture_snow;
  gl::Texture m_texture_snow_man;
  gl::Texture m_texture_penguin;

  gl::VboMesh m_ground_mesh;

  gl::VboMesh m_path_vbo_mesh;
  mutex       m_path_vbo_mesh_mutex;
  Path2d      m_path;
  float       m_path_t;

  bool   m_debug_enabled;
  string m_debug_string;
};

SnowApp::SnowApp() : AppNative(), m_main_light(nullptr), m_light_path(), m_light_path_t(0.0f)
{
}

SnowApp::~SnowApp()
{
  if (m_main_light)
    delete m_main_light;
}

void SnowApp::setup()
{
  srand(static_cast<unsigned int>(time(nullptr)));

  m_mouse_position.set(static_cast<float>(getWindowSize().x / 2), static_cast<float>(getWindowSize().y / 2));

  int count = 3 + rand() % 17;

  m_world_size = 600.0f;

  /////////////////////////////////////////////////////////////////////////////
  {
    gl::VboMesh::Layout vbo_layout;

    vbo_layout.setStaticPositions();
    vbo_layout.setDynamicColorsRGBA();
    //vbo_layout.setStaticColorsRGBA();
    vbo_layout.setStaticIndices();
    vbo_layout.setStaticNormals();
    vbo_layout.setStaticTexCoords2d();


    int vertCount = 4;
    int indicesCount = 4;
  
    vector<Vec3f> positions;
    positions.push_back(Vec3f(-m_world_size, -10.0f,  m_world_size));
    positions.push_back(Vec3f( m_world_size, -10.0f,  m_world_size));
    positions.push_back(Vec3f( m_world_size, -10.0f, -m_world_size));
    positions.push_back(Vec3f(-m_world_size, -10.0f, -m_world_size));

    vector<uint32_t> indices;
    for (unsigned i = 0; i < indicesCount; i++)
      indices.push_back(i);

    vector<Vec3f> normals;
    normals.push_back(Vec3f(0.0f, 1.0f, 0.0f));
    normals.push_back(Vec3f(0.0f, 1.0f, 0.0f));
    normals.push_back(Vec3f(0.0f, 1.0f, 0.0f));
    normals.push_back(Vec3f(0.0f, 1.0f, 0.0f));

    vector<Vec2f> texture_coords;
    texture_coords.push_back(Vec2f(0.0f, 0.0f));
    texture_coords.push_back(Vec2f(1.0f, 0.0f));
    texture_coords.push_back(Vec2f(1.0f, 1.0f));
    texture_coords.push_back(Vec2f(0.0f, 1.0f));

    m_ground_mesh = gl::VboMesh(vertCount, indicesCount, vbo_layout, GL_QUADS);

    m_ground_mesh.bufferPositions(positions);
    m_ground_mesh.bufferIndices(indices);
    m_ground_mesh.bufferNormals(normals);
    m_ground_mesh.bufferTexCoords2d(0, texture_coords);

    //float g = sin(getElapsedSeconds());
    //float b = cos(getElapsedSeconds());
    //for (auto iter = m_ground_mesh.mapVertexBuffer(); !iter.isDone(); ++iter)
    //{
    //  iter.setColorRGBA(ColorA(1 - (g + b / 3), g, b, 0.5));
    //}
    
    for (auto iter = m_ground_mesh.mapVertexBuffer(); !iter.isDone(); ++iter)
      iter.setColorRGBA(ColorA(1.0f, 1.0f, 1.0f, 1.0f));
  }
  /////////////////////////////////////////////////////////////////////////////
  // Texture Loading
  m_texture_snow     = loadImage(loadAsset("seamless_snow.jpg"));
  m_texture_snow_man = loadImage(loadAsset("Wii - Adventure Island The Beginning - Snowman\\Snowman\\Texture\\Snowman Texture.png"));
  m_texture_penguin  = loadImage(loadAsset("Nintendo 64 - Super Mario 64 - Penguin\\Penguin_grp.png"));

  SnowMan::texture = m_texture_snow_man;
  Penguin::texture = m_texture_penguin;

  /////////////////////////////////////////////////////////////////////////////
  {
    ObjLoader obj_loader(loadAsset("Wii - Adventure Island The Beginning - Snowman\\Snowman\\Snowman.obj"));
    TriMesh tri_mesh;
    obj_loader.load(&tri_mesh, true, false, false);
    SnowMan::vbo_mesh = gl::VboMesh(tri_mesh);
  }
  /////////////////////////////////////////////////////////////////////////////
  {
    ObjLoader obj_loader(loadAsset("Nintendo 64 - Super Mario 64 - Penguin\\Penguin.obj"));
    TriMesh tri_mesh;
    obj_loader.load(&tri_mesh, true, false, false);
    Penguin::vbo_mesh = gl::VboMesh(tri_mesh);
  }
  /////////////////////////////////////////////////////////////////////////////
  for (int i = 0; i < count; i++)
  {
    SnowMan snow_man;
    //snow_man.m_position.set(30.0f, 10.0f, 30.0f);
    snow_man.m_position.set(randf(-m_world_size, m_world_size), 10.0f, randf(-m_world_size, m_world_size));
    snow_man.m_size        = randf(5.0f, 20.0f);
    snow_man.m_angle       = randf(0.0f, 360.0f);
    //snow_man.m_angle       = 0.0f;
    snow_man.m_max_speed   = 1.0;
    snow_man.m_direction.set(1.0f, 0.0f, 0.0f);

    snow_man.m_update_rate      = 0.5;
    snow_man.m_last_update_time = randd(0.0, snow_man.m_update_rate);

    m_snow_men.push_back(snow_man);
  }
  /////////////////////////////////////////////////////////////////////////////
  {
    Path2d path;
    Vec2f  temp_path_pos(1.0f, 0.0f);

    path.moveTo(Vec2f::zero());
    temp_path_pos.set(1.0f, 0.0f); temp_path_pos.rotate(toRadians(randf(0.0f, 360.0f)));
    path.lineTo(temp_path_pos * randf(m_world_size * 0.0f, m_world_size * .5f));
    temp_path_pos.set(1.0f, 0.0f); temp_path_pos.rotate(toRadians(randf(0.0f, 360.0f)));
    path.lineTo(temp_path_pos * randf(m_world_size * 0.5f, m_world_size * .75f));
    temp_path_pos.set(1.0f, 0.0f); temp_path_pos.rotate(toRadians(randf(0.0f, 360.0f)));
    path.lineTo(temp_path_pos * randf(m_world_size * 0.75f, m_world_size * .85f));
    temp_path_pos.set(1.0f, 0.0f); temp_path_pos.rotate(toRadians(randf(0.0f, 360.0f)));
    path.lineTo(temp_path_pos * randf(m_world_size * 0.85f, m_world_size * 1.0f));
    //path.lineTo(Vec2f(randf(-m_world_size, m_world_size), randf(-m_world_size, m_world_size)));
    //path.lineTo(Vec2f(randf(-m_world_size, m_world_size), randf(-m_world_size, m_world_size)));
    //path.lineTo(Vec2f(randf(-m_world_size, m_world_size), randf(-m_world_size, m_world_size)));

    //path.getPosition();
    vector<Vec3f>    vertices;
    vector<uint32_t> indices;

    int segments = 10;
    float increment = 1.0f / segments;
    for (int i = 0; i < segments; i++)
    {
      Vec2f p = path.getPosition(increment * i);
      
      vertices.push_back(Vec3f(p.x, 0.0f, p.y));
      indices.push_back(i);
    }

    gl::VboMesh::Layout layout;
    //layout.setStaticPositions();
    layout.setDynamicPositions();
    layout.setStaticIndices();
    layout.setDynamicColorsRGB();

    gl::VboMesh vbo_mesh = gl::VboMesh(vertices.size(), indices.size(), layout, GL_LINE_STRIP);

    //vbo_mesh.bufferPositions(vertices);
    vbo_mesh.bufferIndices(indices);
    //vbo_mesh.bufferColorsRGB(colors);

    int i = 0;
    for (auto iter = vbo_mesh.mapVertexBuffer(); !iter.isDone(); ++iter)
    {
      Vec2f p = path.getPosition(increment * i++);

      iter.setPosition(Vec3f(p.x, 0.0f, p.y));
      iter.setColorRGB(Color(1.0f, 0.0f, 0.0f));
    }

    m_path_vbo_mesh = vbo_mesh;
    m_path = path;
    m_path_t = 0.0f;

    //gl::VboMesh m_path_vbo_mesh;
    //Path2d      m_path;
    //float       m_path_t;
  }
  /////////////////////////////////////////////////////////////////////////////
  m_penguin.m_position.set(0.0f, 0.0f, 0.0f);
  m_penguin.m_max_speed = 1.0f;
  /////////////////////////////////////////////////////////////////////////////

  glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
  glClearDepth(1.0f);

  glEnable(GL_DEPTH_TEST);
  glEnable(GL_TEXTURE_2D);
  glDepthFunc(GL_LEQUAL);

  glShadeModel(GL_SMOOTH);
  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
  //glColorMaterial(GL_FRONT,GL_AMBIENT_AND_DIFFUSE);
  
  setupCamera();
  setupLights();
}

void SnowApp::setupCamera()
{
  m_camera = CameraPersp(getWindowWidth(), getWindowHeight(), 45.0f);
  m_camera.setEyePoint(Vec3f(0.0f, 170.0f, m_world_size * 1.0f));
  m_camera.setCenterOfInterestPoint(m_penguin.m_position);
  m_camera.setPerspective(45.0f, getWindowAspectRatio(), 0.1f, 1000.0f);
  
  m_camera_free_look.setCurrentCam(m_camera);

  m_camera_free_look_enabled = false;
}

void SnowApp::setupLights()
{
  m_main_light = new gl::Light(gl::Light::POINT, 0);
  //m_main_light->setAmbient(Color::gray(.5f));
  //m_main_light->setDiffuse(Color::gray(.15f));
  //m_main_light->setSpecular(Color::gray(.05f));

  m_light_path_t = 0.0f;

  m_light_path.moveTo(Vec2f(0.0f, 0.0f));

  int segments = 10 + rand() % 15;
  for (int i = 0; i < segments; i++)
  {
    float x = -m_world_size + (m_world_size * 2.0f) * (static_cast<float>(i) / segments);
    float y = randf(-m_world_size, m_world_size);
    
    m_light_path.lineTo(Vec2f(x, y));
  }

  Vec2f p = m_light_path.getPosition(m_light_path_t);
  m_main_light->setPosition(Vec3f(p.x, 70.0f, p.y));

  glEnable(GL_LIGHTING);
  glEnable(GL_COLOR_MATERIAL);
}

void SnowApp::updateLights(const Vec2i& _pos)
{
  Vec3f p = m_main_light->getPosition();

  Vec2f mouse_position_normalized = Vec2f(static_cast<float>(_pos.x), static_cast<float>(_pos.y)) / getWindowSize();

  p.set(-m_world_size + (m_world_size * 2.0f) * mouse_position_normalized.x, p.y, -m_world_size + (m_world_size * 2.0f) * mouse_position_normalized.y);
  //p.set(static_cast<float>(_pos.x), p.y, static_cast<float>(_pos.y));

  //m_main_light->setPosition(p);
}

void SnowApp::keyDown(KeyEvent event)
{
  switch (event.getCode())
  {
    case (KeyEvent::KEY_ESCAPE):
    {
      quit();
    } break;

    case (KeyEvent::KEY_c):
    {
      m_camera_free_look_enabled = !m_camera_free_look_enabled;

      if (m_camera_free_look_enabled)
      {
        m_camera_free_look.setCurrentCam(m_camera);
      }
      else
      {
        m_camera = m_camera_free_look.getCamera();
      }
    } break;

    case (KeyEvent::KEY_l):
    {
      updateLights(Vec2i(static_cast<float>(m_mouse_position.x), static_cast<float>(m_mouse_position.y)));
    } break;

    default:
    {
      m_key_states[event.getCode()] = true;
    } break;
  }
}

void SnowApp::keyUp(KeyEvent event)
{
  switch (event.getCode())
  {
    default:
    {
      m_key_states[event.getCode()] = false;
    } break;
  }
}

void SnowApp::mouseDown(MouseEvent event)
{
  Vec2i& p = event.getPos();
  
  if (m_camera_free_look_enabled)
  {
    m_camera_free_look.mouseDown(p);
    m_camera = m_camera_free_look.getCamera();
  }

  m_mouse_position = p;

  if (event.isRightDown())
  {
    //updateLights(p);
  }
}

void SnowApp::mouseDrag(MouseEvent event)
{
  Vec2i& p = event.getPos();
  
  if (m_camera_free_look_enabled)
  {
    m_camera_free_look.mouseDrag(p, event.isLeftDown(), event.isMiddleDown(), event.isRightDown());
    m_camera = m_camera_free_look.getCamera();
  }

  m_mouse_position = p;
  
  if (event.isShiftDown() && event.isRightDown())
  {
    //updateLights(p);
  }
}

void SnowApp::mouseUp(MouseEvent event)
{
  if (m_camera_free_look_enabled)
  {
    m_camera_free_look.mouseDown(event.getPos());
    m_camera = m_camera_free_look.getCamera();
  }
}

void SnowApp::mouseMove(MouseEvent event)
{
  Vec2i& p = event.getPos();

  m_mouse_position = p;
}

void SnowApp::update()
{
  { ///////////////////////////////////////////////////////////////////////////
    bool moved = false;

    // Handle continuous inputs
    if (m_key_states[KeyEvent::KEY_a])
    {
      m_penguin.m_angle += m_penguin.m_angle_turn_rate;
      m_penguin.m_direction = Vec3f(1.0f, 0.0f, 0.0f);
      m_penguin.m_direction.rotateY(-toRadians(m_penguin.m_angle));
    }
    if (m_key_states[KeyEvent::KEY_d])
    {
      m_penguin.m_angle -= m_penguin.m_angle_turn_rate;
      m_penguin.m_direction = Vec3f(1.0f, 0.0f, 0.0f);
      m_penguin.m_direction.rotateY(-toRadians(m_penguin.m_angle));
    }
    if (m_key_states[KeyEvent::KEY_w])
    {
      m_penguin.m_position += m_penguin.m_direction * m_penguin.m_max_speed;
      moved = true;
    }
    if (m_key_states[KeyEvent::KEY_s])
    {
      m_penguin.m_position -= m_penguin.m_direction * m_penguin.m_max_speed;
      moved = true;
    }

    if (moved)
    {
      if (!m_camera_free_look_enabled)
      {
        static const float camera_distance_threshold = 300.0f;

        float dist = m_camera.getEyePoint().distanceSquared(m_penguin.m_position);
        
        if (dist > (camera_distance_threshold * camera_distance_threshold))
        {
          Vec3f dir = m_camera.getViewDirection();
          dir.safeNormalize();
          dir = dir * (camera_distance_threshold * .05f);

          Vec3f p = m_camera.getEyePoint() + dir;
          p.y = m_camera.getEyePoint().y;
          m_camera.setEyePoint(p);
        }

        m_camera.lookAt(m_penguin.m_position);
      }

      //Vec3f p = m_penguin.m_position;

      m_path_vbo_mesh_mutex.lock();
      for (auto iter = m_path_vbo_mesh.mapVertexBuffer(); !iter.isDone(); ++iter)
      {
        static const float threshold = 20.0f;

        Vec3f p = *iter.getPositionPointer();
        float dist = p.distanceSquared(m_penguin.m_position);

        if (dist < (threshold * threshold))
        {
          iter.setColorRGB(Color(0.0f, 1.0f, 0.0f));
        }
      }
      m_path_vbo_mesh_mutex.unlock();
    }
  } ///////////////////////////////////////////////////////////////////////////

  static const float snow_man_angle_turn_rate = 5.0f;
  static const float snow_man_angle_threshold = 5.0f;
  for (auto& snow_man : m_snow_men)
  {
    if ((getElapsedSeconds() - snow_man.m_last_update_time) > snow_man.m_update_rate)
    {
      bool moved = false;
      bool facing_target = false;

      float angle_to_target = angleTo(snow_man.m_position, m_penguin.m_position) - snow_man.m_angle;

      if (angle_to_target > 180.0f)
        angle_to_target -= 360.0f;
      if (angle_to_target < -180.0f)
        angle_to_target += 360.0f;

      //m_debug_string.clear();

      //stringstream ss;
      //ss << setprecision(3) << snow_man.m_position << " " << m_penguin.m_position << " " << angle_to_target;

      //m_debug_string += ss.str();

      if (fabs(angle_to_target) < snow_man_angle_threshold)
      {
        snow_man.m_angle += angle_to_target;
        facing_target = true;
      }
      else if (angle_to_target >= snow_man_angle_threshold)
      {
        snow_man.m_angle += snow_man_angle_turn_rate;
        moved = true;
      }
      else if (angle_to_target < -snow_man_angle_threshold)
      {
        snow_man.m_angle -= snow_man_angle_turn_rate;
        moved = true;
      }

      if (snow_man.m_angle > 360.0f) snow_man.m_angle -= 360.0f;
      if (snow_man.m_angle <   0.0f) snow_man.m_angle += 360.0f;

      if (facing_target)
      {
        snow_man.m_direction = Vec3f(1.0f, 0.0f, 0.0f);
        snow_man.m_direction.rotateY(-toRadians(snow_man.m_angle));

        snow_man.m_position += snow_man.m_direction * snow_man.m_max_speed;

        moved = true;
      }

      snow_man.m_last_update_time = getElapsedSeconds();
    }
  }

  { ///////////////////////////////////////////////////////////////////////////
    Vec2f p = m_light_path.getPosition(m_light_path_t);
    m_main_light->setPosition(Vec3f(p.x, 70.0f, p.y));

    static double last_time = getElapsedSeconds();
    float time_delta = static_cast<float>(getElapsedSeconds() - last_time) / 1000.0f;

    m_light_path_t += time_delta * 5.0f;
    m_light_path_t = fmod(m_light_path_t, 1.0f);

    last_time = getElapsedSeconds();
  } ///////////////////////////////////////////////////////////////////////////

  //float g = sin(getElapsedSeconds());
  //float b = cos(getElapsedSeconds());
  //for (auto iter = m_ground_mesh.mapVertexBuffer(); !iter.isDone(); ++iter)
  //{
  //  iter.setColorRGBA(ColorA(1 - (g + b / 3), g, b, 0.5));
  //}
}

void SnowApp::draw()
{
  // clear out the window with black
  gl::clear(Color(0, 0, 0));
  gl::enableDepthWrite();
  gl::enableDepthRead();
  gl::enableAlphaBlending();

  glLoadIdentity();

  renderScene();
  renderGui();
}

void SnowApp::renderScene()
{
  glPushAttrib(GL_LIGHTING);
    glEnable(GL_LIGHTING);

    glPushMatrix();
      //gl::setMatrices(m_camera_free_look.getCamera());
      gl::setMatrices(m_camera);

      glEnable(GL_LIGHT0);
      float pos[4] = {m_main_light->getPosition().x, m_main_light->getPosition().y, m_main_light->getPosition().z, 0.0f};
      glLightfv(GL_LIGHT0, GL_POSITION, pos);

      glEnable(GL_TEXTURE_2D);

      for (auto& snow_man : m_snow_men)
      {
        drawSnowMan(snow_man);
      }

      drawPenguin(m_penguin);

      { ///////////////////////////////////////////////////////////////////////
        //gl::VboMesh& mesh = SnowMan::vbo_mesh;
        m_texture_snow.enableAndBind();
          glPushMatrix();
            m_ground_mesh.bindAllData();
              //gl::scale(m_world_size, 1.0f, m_world_size);
              //gl::color(1.0f, 1.0f, 1.0f, 1.0f);
              gl::draw(m_ground_mesh);
            m_ground_mesh.unbindBuffers();
          glPopMatrix();
        m_texture_snow.unbind();
      } ///////////////////////////////////////////////////////////////////////

      glDisable(GL_LIGHTING);

      { ///////////////////////////////////////////////////////////////////////
        m_path_vbo_mesh_mutex.lock();
        glPushMatrix();
          m_path_vbo_mesh.bindAllData();
            gl::draw(m_path_vbo_mesh);
          m_path_vbo_mesh.unbindBuffers();
        glPopMatrix();
        m_path_vbo_mesh_mutex.unlock();
      } ///////////////////////////////////////////////////////////////////////

      glPushMatrix();
        gl::color(0.0f, 1.0f, 1.0f);
        glTranslatef(m_main_light->getPosition());
        gl::drawSphere(Vec3f::zero(), 5.0f);
      glPopMatrix();

      //glPushMatrix();
      //  gl::drawCoordinateFrame(m_world_size);
      //glPopMatrix();

      gl::drawStringCentered("blah?", Vec2f(50.0f, 50.0f));

    glPopMatrix();
  glPopAttrib();
}

void SnowApp::renderGui()
{
  glPushAttrib(GL_LIGHTING);
    glDisable(GL_LIGHTING);
    gl::setMatricesWindow(getWindowSize(), true);

    gl::color(0.0f, 0.0f, 1.0f);
    gl::drawSolidCircle(m_mouse_position, 5.0f);

    if (m_debug_enabled)
    {
      glPushMatrix();
        gl::translate(150.0f, 150.0f, 0.0f);
        gl::scale(2.0f, 2.0f, 2.0f);
        gl::drawStringCentered(m_debug_string, Vec2f(0.0f, 0.0f));
      glPopMatrix();
    }
  glPopAttrib();
}

CINDER_APP_NATIVE(SnowApp, RendererGl)
