#include "cinder/app/AppNative.h"
#include "cinder/Camera.h"
#include "cinder/gl/gl.h"
#include "cinder/gl/Texture.h"
#include "cinder/ImageIo.h"

#include "Config.h"
#include "Ground.h"
#include "RockTypes.h"
#include "ProximitySystem.h"
#include "System.h"
#include "Utils.h"

#include <chrono>
#include <functional>

using namespace ci;
using namespace ci::app;
using namespace std;
using namespace Mgl;

///////////////////////////////////////////////////////////////////////////////
// Utils
static float angleTo(const ci::Vec2f& _origin, const ci::Vec2f& _target)
{
  float dx = (float)(_target.x - _origin.x);
  float dy = (float)(_target.y - _origin.y);

  float result = ci::toDegrees(atan2(dy, dx));

  if (result < 0.0f)
    result += 360.0f;

  return (result);
}

template<typename T>
float getAngleTo(T& _o, const Vec2f& _target_position)
{
  float angle_to_target = _o.angle - angleTo(_o.position, _target_position);

  if (angle_to_target > 180.0f)
    angle_to_target -= 360.0f;
  if (angle_to_target < -180.0f)
    angle_to_target += 360.0f;

  if (angle_to_target > _o.turn_max)
    _o.turn = -1.0f;
  else if (angle_to_target < -_o.turn_max)
    _o.turn = 1.0f;
  else
    _o.turn = -(angle_to_target / _o.turn_max);

  return angle_to_target;
}

template<typename T>
float getAngleTo(T& _o)
{
  return getAngleTo<T>(_o, _o.ai_track_position);
}

///////////////////////////////////////////////////////////////////////////////

class Rocks2App : public AppNative
{
public:
  Rocks2App();
  virtual ~Rocks2App();

  void setup();
  void resize();
  void mouseDown(MouseEvent _event);
  void mouseMove(MouseEvent _event);
  void mouseDrag(MouseEvent _event);
  void mouseUp(MouseEvent _event);
  void keyDown(KeyEvent _event);
  void keyUp(KeyEvent _event);
  void update();
  void draw();

  virtual void quit();

  System<Rock>     m_rocks;
  System<Vessel>   m_vessels;
  System<Shot>     m_shots;
  System<Building> m_buildings;
  Ground           m_ground;

  ProximitySystem m_proximity_system;

  Vec2f m_mouse_pos;

  unsigned m_vessel_selected_id;
  Vec2f    m_vessel_selected_target;

  gl::Texture m_texture_vessel;

  CameraOrtho m_camera_ortho;
  CameraPersp m_camera_perspective;
  Vec2f       m_universe_scroll;
  Vec2f       m_universe_zoom;
  Vec2f       m_universe_scale;

  Universe m_universe;

  /////////////////////////////////////////////////////////////////////////////
  // Debug
  void renderDebugInfo();
  
  string m_debug_string;
  Vec2f  m_debug_position;
  /////////////////////////////////////////////////////////////////////////////
  // Config
  Config m_config;
  
  float m_text_spacing;

  float  m_ai_vessel_turn;
  float  m_ai_vessel_thrust;
  float  m_ai_vessel_size;

  float  m_shot_speed;
  double m_shot_time;

  float m_physics_friction;
  float m_physics_rebound_multiplier;
  /////////////////////////////////////////////////////////////////////////////
  // Gameplay
  template<typename T>
  void processAi(T& _o, int _ai_state, const Vec2f& _ai_position);

  template<typename T>
  void prepareShots(T& _o);

  void prepareShot(int _type, unsigned _origin_id, Vec2f& _origin_position, float _origin_angle);

  void addVessels();
  void addBuildings();
  void addVessel(const Vec2f& _position);
  void addBuilding(const Vec2f& _position);

  Vec2f toUniverseSpace(const Vec2f& _screen_position) const;
  Vec2f toScreenSpace(const Vec2f& _universe_position) const;
  void  handleLeftDown();
  void  handleRightDown();
  /////////////////////////////////////////////////////////////////////////////
  // Events
  void onCollisionVesselToShot(ProximitySystem::Event& _event)
  {
    if (m_vessels.has(_event.ids[0]) && m_shots.has(_event.ids[1]))
    {
      auto& vessel = m_vessels.lookup(_event.ids[0]);
      auto& shot = m_shots.lookup(_event.ids[1]);

      if (vessel.destructible.takeDamage(shot.attacker))
        m_vessels.remove(vessel.id);
      m_shots.remove(shot.id);
    }
  }

  void onCollisionVesselToVessel(ProximitySystem::Event& _event)
  {
    if (m_vessels.has(_event.ids[0]) && m_vessels.has(_event.ids[1]))
    {
      auto& vessel = m_vessels.lookup(_event.ids[0]);
      auto& other_vessel = m_vessels.lookup(_event.ids[1]);

      auto p1 = vessel.position;
      auto p2 = other_vessel.position;

      auto mass_ratio = other_vessel.size / vessel.size;
      auto rebound_vec = (p2 - p1);

      vessel.velocity -= (rebound_vec * (mass_ratio) * m_physics_rebound_multiplier);
      other_vessel.velocity += (rebound_vec * (1.0f / mass_ratio) * m_physics_rebound_multiplier);
    }
  }

  void onCollisionShotToBuilding(ProximitySystem::Event& _event)
  {
    if (m_shots.has(_event.ids[0]) && m_buildings.has(_event.ids[1]))
    {
      auto& shot = m_shots.lookup(_event.ids[0]);
      auto& building = m_buildings.lookup(_event.ids[1]);
    }
  }
};

class AppInitialization
{
public:
  AppInitialization()
  {
  }
  virtual ~AppInitialization()
  {
  }
};

static AppInitialization gAppInit;

Rocks2App::Rocks2App() :
  m_config(),
  
  m_proximity_system(m_vessels, m_shots, m_buildings, m_ground, m_universe),
  m_vessel_selected_id(ID_INVALID)
{
}

Rocks2App::~Rocks2App()
{
}

void Rocks2App::setup()
{
  /////////////////////////////////////////////////////////////////////////////
  if (!m_config.load("app.cfg"))
  {
    MessageBox(nullptr, L"Failed to load config!", L"Rocks2", MB_OK);
    console() << "CONFIG FAILED TO LOAD!" << endl;
  }
  /////////////////////////////////////////////////////////////////////////////
  m_text_spacing = m_config.as_float("setup.text_spacing", 3.0f);

  m_ai_vessel_size   = m_config.as_float("ai.size", 16.0f);
  m_ai_vessel_thrust = m_config.as_float("ai.thrust", 0.75f);
  m_ai_vessel_turn   = m_config.as_float("ai.turn", 3.5f);

  m_physics_friction           = m_config.as_float("physics.friction", .55f);
  m_physics_rebound_multiplier = m_config.as_float("physics.rebound_multiplier", 0.1f);

  m_shot_speed       = m_config.as_float("shot.speed", 6.0f);
  m_shot_time        = m_config.as_double("shot.time", 0.5);
  
  /////////////////////////////////////////////////////////////////////////////
  setWindowSize(1000, 500);

  m_mouse_pos = Vec2f::zero();

  m_vessel_selected_id = ID_INVALID;

  m_universe_scroll.set(0.0f, 0.0f);
  m_universe_zoom.set(0.0f, 0.0f);
  m_universe_scale.set(1.0f, 1.0f);

  m_universe.bounds.set(0.0f, 0.0f, static_cast<float>(getWindowWidth()), static_cast<float>(getWindowHeight()));
  m_universe.view = m_universe.bounds;

  /////////////////////////////////////////////////////////////////////////////
  m_texture_vessel = loadImage(loadAsset("img_99.png"));
  gl::enableAlphaBlending();
  /////////////////////////////////////////////////////////////////////////////
  // System setups.
  m_proximity_system.init();
  m_proximity_system.addListener(ProximitySystem::EVENT_VESSEL_TO_SHOT, bind(&Rocks2App::onCollisionVesselToShot, this, placeholders::_1));
  m_proximity_system.addListener(ProximitySystem::EVENT_VESSEL_TO_VESSEL, bind(&Rocks2App::onCollisionVesselToVessel, this, placeholders::_1));
  m_proximity_system.addListener(ProximitySystem::EVENT_SHOT_BUILDING, bind(&Rocks2App::onCollisionShotToBuilding, this, placeholders::_1));
  /////////////////////////////////////////////////////////////////////////////
  // Generate the ground
  m_ground.generate(static_cast<float>(getWindowWidth()), static_cast<float>(getWindowHeight()), static_cast<float>(getWindowHeight()) * (m_config.as_float("setup.ground_level")), 10);
  /////////////////////////////////////////////////////////////////////////////
  // Add Vessels, buildings, etc...
  addBuildings();
  addVessels();
  /////////////////////////////////////////////////////////////////////////////

  //setFrameRate(5.0f);
}

void Rocks2App::resize()
{
  gl::setMatricesWindow(getWindowSize(), true);
  m_universe_scroll.set(0.0f, 0.0f);
  m_universe_zoom.set(0.0f, 0.0f);
  m_universe_scale.set(1.0f, 1.0f);
  //m_universe.bounds.set(0.0f, 0.0f, static_cast<float>(getWindowWidth()), static_cast<float>(getWindowHeight()));
  m_universe.view.set(0.0f, 0.0f, static_cast<float>(getWindowWidth()), static_cast<float>(getWindowHeight()));
}

void Rocks2App::quit()
{
  AppNative::quit();
}

template<typename T>
void Rocks2App::processAi(T& _o, int _ai_state, const Vec2f& _ai_position)
{
  static const float buffer_distance = m_config.as_float("ai.buffer_distance", 100.0f) * m_config.as_float("ai.buffer_distance", 100.0f);

  switch (_ai_state)
  {
    default:
    {
      _o.thrust = 0.0f;
      _o.turn = 0.0f;
      _o.fire = false;
    } break;

    case AI_STATE_EVADE:
    {
      float angle_to_target = getAngleTo<Vessel>(_o, _ai_position);

      if (angle_to_target > -75.0f && angle_to_target < 75.0f)
        _o.thrust = -1.0f;
      else
        _o.thrust = 1.0f;

      if (angle_to_target > 0.0f && angle_to_target < 180.0f)
        _o.turn = 1.0f;
      else if (angle_to_target < 0.0f && angle_to_target < -180.0f)
        _o.turn = -1.0f;

      _o.fire = false;
    } break;

    case AI_STATE_MOVE:
    {
      float angle_to_target = getAngleTo<Vessel>(_o, _ai_position);

      if (fabs(_o.turn) < .25f)
      {
        float distance_squared = _o.position.distanceSquared(_ai_position);

        if (distance_squared > _o.size)
          _o.thrust = 1.0f;
        else
          _o.thrust = 0.0f;
      }
      else
      {
        _o.thrust = 0.0f;
      }

      _o.fire = false;
    } break;

    case AI_STATE_ATTACK:
    {
      float angle_to_target = getAngleTo<Vessel>(_o, _ai_position);

      if (fabs(_o.turn) < .25f)
      {
        float distance_squared = _o.position.distanceSquared(_ai_position);

        if (distance_squared > buffer_distance)
        {
          _o.thrust = 1.0f;
        }
        else if (distance_squared < buffer_distance)
        {
          _o.thrust = -1.0f;
          _o.fire = true;
        }
        else
        {
          _o.thrust = 0.0f;
          _o.fire = true;
        }
      }
      else
      {
        _o.thrust = 0.0f;
        _o.fire = false;
      }
    } break;

    case AI_STATE_SELECT:
    {
    } break;
  }
}

template<typename T>
void Rocks2App::prepareShots(T& _o)
{
  for (int i = 0; i < MAX_WEAPON_EMITTERS; i++)
  {
    auto& weapon_emitter = _o.weapon_emitters[i];

    if (weapon_emitter.fire(getElapsedSeconds()))
    {
      prepareShot(weapon_emitter.type, _o.id, _o.position, _o.angle + weapon_emitter.angle_offset);
    }
  }
}

void Rocks2App::prepareShot(int _type, unsigned _origin_id, Vec2f& _origin_position, float _origin_angle)
{
  if (_type)
  {
    float weapon_speed = 0.0f;
    float weapon_time = 0.0f;
    float weapon_power = 0.0f;

    switch (_type)
    {
      case WEAPON_TYPE_BULLET: weapon_speed =  6.0f; weapon_time = 0.5f; weapon_power = m_config.as_float("weapon.bullet.power", 5.0f); break;
      case WEAPON_TYPE_LASER:  weapon_speed = 12.0f; weapon_time = 0.5f; weapon_power = m_config.as_float("weapon.laser.power", 15.0f); break;
      case WEAPON_TYPE_MISSLE: weapon_speed =  5.0f; weapon_time = 5.5f; weapon_power = m_config.as_float("weapon.missle.power", 25.0f); break;
    }

    ID id = m_shots.add();

    if (m_shots.has(id))
    {
      Vec2f v(weapon_speed, 0.0f);
      v.rotate(toRadians(_origin_angle));
      Shot& shot = m_shots.lookup(id);
      shot.position = _origin_position;
      shot.velocity = v;
      shot.time_start = getElapsedSeconds();
      shot.time_end   = shot.time_start + weapon_time;
      shot.origin_id = _origin_id;

      shot.attacker.power = weapon_power;
    }
  }
}

void Rocks2App::addVessels()
{
  int num_vessels = m_config.as_int("setup.num_vessels", random(15, 30));
  
  for (int i = 0; i < num_vessels; ++i)
  {
    Vec2f p(randomf(0.0f, static_cast<float>(getWindowWidth())), randomf(0.0f, static_cast<float>(getWindowHeight())));

    addVessel(p);
  }
}

void Rocks2App::addBuildings()
{
  int num_buildings = m_config.as_int("setup.num_buildings", random(15, 30));
  
  float floor = static_cast<float>(getWindowHeight());
  float x_offset = (static_cast<float>(getWindowWidth()) / num_buildings);
  
  for (int i = 0; i < num_buildings; ++i)
  {
    Vec2f p = m_ground.getSurfacePoint(i * x_offset);

    addBuilding(p);
  }
}

void Rocks2App::addVessel(const Vec2f& _position)
{
  ID id = m_vessels.add();

  if (m_vessels.has(id))
  {
    Vessel& vessel = m_vessels.lookup(id);
    vessel.position.set(randomf(m_universe.bounds.getWidth()), randomf(m_universe.bounds.getHeight() * m_config.as_float("setup.ground_level", 0.75f)));
    vessel.velocity.set(randomf(-1.0f, 1.0f), randomf(-1.0f, 1.0f));
    vessel.angle = randomf(360.0f);

    vessel.turn_max = m_ai_vessel_turn;
    vessel.thrust_max = m_ai_vessel_thrust;

    vessel.turn = 0.0f;
    vessel.thrust = 0.0f;
    vessel.fire = false;

    vessel.size = randomf(8.0f, m_ai_vessel_size);

    vessel.ai_enabled = true;
    vessel.ai_state = AI_STATE_IDLE;
    vessel.ai_vessel_track_id = m_vessels.lookup(random(0, m_vessels.num_objects)).id;
    vessel.ai_projection_time = m_config.as_double("ai.projection_time", 0.0);

    vessel.weapon_emitters[0].set(WEAPON_TYPE_BULLET);

    vessel.destructible.set((vessel.size / m_ai_vessel_size) * 100.0f);
  }
}

void Rocks2App::addBuilding(const Vec2f& _position)
{
  Rectf new_building_placement;

  ID id = m_buildings.add();
    
  if (m_buildings.has(id))
  {
    auto& building = m_buildings.lookup(id);

    float building_width = randomf(15.0f, 25.0f);
    float building_height = randomf(15.0f, 100.0f);

    building.set(_position, building_width, building_height);
    building.angle = 270.0f;
      
    building.turn_max = 5.0f;
      
    building.ai_enabled = true;
    if (m_vessels.num_objects > 0)
      building.ai_vessel_track_id = m_vessels.lookup(random(0, m_vessels.num_objects)).id;
    else
      building.ai_vessel_track_id = ID_INVALID;

    building.weapon_emitters[0].set(WEAPON_TYPE_BULLET);
  }
}

Vec2f Rocks2App::toUniverseSpace(const Vec2f& _screen_position) const
{
  Vec2f u = m_universe.view.getUpperLeft();
  Vec2f n = _screen_position / getWindowSize();

  return (u + n * m_universe.view.getSize());
}

Vec2f Rocks2App::toScreenSpace(const Vec2f& _universe_position) const
{
  Vec2f u = m_universe.view.getUpperLeft();
  Vec2f n = (_universe_position - u) / (m_universe.view.getSize());
  
  return (n * Vec2f(getWindowSize()));
}

void Rocks2App::handleLeftDown()
{
  unsigned id = m_proximity_system.getVessel(m_mouse_pos);

  if (m_vessels.has(id))
    m_vessel_selected_id = m_vessels.lookup(id).id;
  else
    m_vessel_selected_id = ID_INVALID;
}

void Rocks2App::handleRightDown()
{
  unsigned id = m_proximity_system.getVessel(m_mouse_pos);
    
  if (m_vessels.has(m_vessel_selected_id))
  {
    if (m_vessels.has(id))
    {
      // Order the selected vessel to attack this target.
      auto& vessel = m_vessels.lookup(m_vessel_selected_id);
      auto& target = m_vessels.lookup(id);

      vessel.ai_vessel_track_id = target.id;
      vessel.ai_track_position = m_vessel_selected_target = m_mouse_pos;
      vessel.ai_state = AI_STATE_ATTACK;
      vessel.ai_state_due_time = getElapsedSeconds() + 5.0;
    }
    else
    {
      // Order the selected vessel to move to this position.
      if (m_vessels.has(m_vessel_selected_id))
      {
        auto& vessel = m_vessels.lookup(m_vessel_selected_id);
        vessel.ai_vessel_track_id = ID_INVALID;
        vessel.ai_state = AI_STATE_MOVE;
        vessel.ai_state_due_time = getElapsedSeconds() + 5.0;
        vessel.ai_track_position = m_vessel_selected_target = m_mouse_pos;
        // TODO(elam03): Do something about controlling a user-selected vessel...
        //vessel.ai_enabled = false;
      }
    }
  }
}

void Rocks2App::mouseDown(MouseEvent _event)
{
  m_mouse_pos = toUniverseSpace(_event.getPos());

  if (_event.isLeftDown())
    handleLeftDown();
  else if (_event.isRightDown())
    handleRightDown();
}

void Rocks2App::mouseMove(MouseEvent _event)
{
  m_mouse_pos = toUniverseSpace(_event.getPos());

  m_debug_position = m_ground.getSurfacePoint(m_mouse_pos.x);
}

void Rocks2App::mouseDrag(MouseEvent _event)
{
  m_mouse_pos = toUniverseSpace(_event.getPos());

  if (_event.isLeftDown())
    handleLeftDown();
  else if (_event.isRightDown())
    handleRightDown();
}
  
void Rocks2App::mouseUp(MouseEvent _event)
{
  m_mouse_pos = toUniverseSpace(_event.getPos());
}

void Rocks2App::keyDown(KeyEvent _event)
{
  if (_event.getCode() == KeyEvent::KEY_ESCAPE)
    quit();
  
  if (m_vessels.has(m_vessel_selected_id))
  {
    auto& o = m_vessels.lookup(m_vessel_selected_id);

    if (_event.getCode() == KeyEvent::KEY_1) o.ai_state = AI_STATE_MOVE;
    if (_event.getCode() == KeyEvent::KEY_2) o.ai_state = AI_STATE_ATTACK;
    if (_event.getCode() == KeyEvent::KEY_3) o.ai_state = AI_STATE_EVADE;
    if (_event.getCode() == KeyEvent::KEY_4) o.ai_state = AI_STATE_SELECT;
    if (_event.getCode() == KeyEvent::KEY_5) o.ai_state = AI_STATE_IDLE;
  }

  if (_event.getCode() == KeyEvent::KEY_SPACE)
  {
    if (m_rocks.num_objects > 0)
    {
      int index = random(m_rocks.num_objects);
      ID id = m_rocks.objects[index].id;

      m_rocks.remove(id);
    }
  }
  
  static const float scroll_rate = m_config.as_float("setup.scroll_rate", 5.0f);
  static const float zoom_rate   = m_config.as_float("setup.zoom_rate", 1.0f);

  if (_event.getCode() == KeyEvent::KEY_a) m_universe_scroll.x = -scroll_rate;
  if (_event.getCode() == KeyEvent::KEY_d) m_universe_scroll.x =  scroll_rate;
  if (_event.getCode() == KeyEvent::KEY_w) m_universe_scroll.y = -scroll_rate;
  if (_event.getCode() == KeyEvent::KEY_s) m_universe_scroll.y =  scroll_rate;
  if (_event.getCode() == KeyEvent::KEY_r) m_universe_zoom.set(zoom_rate, zoom_rate);
  if (_event.getCode() == KeyEvent::KEY_f) m_universe_zoom.set(-zoom_rate, -zoom_rate);
  
  if (_event.getCode() == KeyEvent::KEY_v)
  {
    Vec2f p(randomf(0.0f, static_cast<float>(getWindowWidth())), randomf(0.0f, static_cast<float>(getWindowHeight())));

    addVessel(p);
  }
  
  //if (_event.getCode() == KeyEvent::KEY_b)
  //  addBuilding();
  
  if (_event.getCode() == KeyEvent::KEY_z)
  {
    int count = random(5, 10);

    for (int i = 0; i < count; i++)
    {
      ID id = m_shots.add();

      if (m_shots.has(id))
      {
        auto& shot = m_shots.lookup(id);

        Vec2f v(randomf(m_shot_speed), 0.0f);
        v.rotate(toRadians(randomf(360)));

        shot.position = m_mouse_pos;
        shot.velocity = v;
        shot.origin_id = 0;
        shot.time_start = getElapsedSeconds();
        shot.time_end   = shot.time_start + m_shot_time;

        //ID id = m_rocks.add();

        //if (m_rocks.has(id))
          //m_rocks.lookup(id).position = m_mouse_pos + v;
      }
    }
  }
}

void Rocks2App::keyUp(KeyEvent _event)
{
  if (_event.getCode() == KeyEvent::KEY_a) m_universe_scroll.x = 0.0f;
  if (_event.getCode() == KeyEvent::KEY_d) m_universe_scroll.x = 0.0f;
  if (_event.getCode() == KeyEvent::KEY_w) m_universe_scroll.y = 0.0f;
  if (_event.getCode() == KeyEvent::KEY_s) m_universe_scroll.y = 0.0f;
  if (_event.getCode() == KeyEvent::KEY_r) m_universe_zoom.set(0.0f, 0.0f);
  if (_event.getCode() == KeyEvent::KEY_f) m_universe_zoom.set(0.0f, 0.0f);
}

void Rocks2App::update()
{
  static FpsCounter fpsCounter("updateRate");

  fpsCounter++;

  ScopedElapsedTime elapsedTime("update");
  double curr_time = getElapsedSeconds();

  /////////////////////////////////////////////////////////////////////////////
  // Update universe position if moving.
  if (m_universe_scroll != Vec2f::zero() || m_universe_zoom != Vec2f::zero())
  {
    Rectf& r = m_universe.view;

    //m_universe_scroll.yif (m_universe_scroll.y)
    m_universe_zoom.y = m_universe_zoom.y;

    r.offset(m_universe_scroll);
    Vec2f z = m_universe_zoom;
    r.inflate(z);

    //m_camera_ortho = CameraOrtho(r.x1, r.x2, r.y2, r.y1, -1.0f, 1.0f);
    m_camera_ortho = CameraOrtho(r.x1, r.x2, r.y2, r.y1, -1.0f, 1.0f);
    m_camera_perspective = CameraPersp(getWindowWidth(), getWindowHeight(), 60.0f);
    
    {
      static float zoom = 100.0f;

      if (m_universe_zoom.y != 0.0)
        zoom += m_universe_zoom.y;

      //Vec3f e = Vec3f(r.getCenter().x, r.getCenter().y, zoom);
      Vec3f e = Vec3f(getWindowWidth(), getWindowHeight(), zoom);
      Vec3f p = Vec3f(r.getCenter().x, r.getCenter().y, 0.0f);
      m_camera_perspective.setEyePoint(e);
      m_camera_perspective.setCenterOfInterestPoint(p);
      m_camera_perspective.setWorldUp(Vec3f(0.0f, -1.0f, 0.0f));
    }

    gl::setMatrices(m_camera_perspective);
  }

  /////////////////////////////////////////////////////////////////////////////
  // Update target positions

  for (unsigned i = 0; i < m_buildings.num_objects; i++)
  {
    auto& o = m_buildings[i];
    
    if (m_vessels.has(o.ai_vessel_track_id))
    {
      auto& target = m_vessels.lookup(o.ai_vessel_track_id);
      o.ai_track_position = target.position;
    }
  }

  /////////////////////////////////////////////////////////////////////////////

  for (unsigned i = 0; i < m_shots.num_objects; i++)
  {
    Shot& shot = m_shots[i];

    shot.position += shot.velocity;

    if (getElapsedSeconds() > shot.time_end)
    {
      m_shots.remove(shot.id);
    }
  }

  static const float dampening = m_config.as_float("ai.dampening_factor", 1.0f) * m_config.as_float("ai.dampening_factor", 1.0f);
  static const float m_proximity_distance = m_config.as_float("ai.proximity_distance", 50.0f) * m_config.as_float("ai.proximity_distance", 50.0f);

  static bool evasion_enabled = m_config.as_bool("debug.evasion_enabled", true);
  
  for (unsigned i = 0; i < m_vessels.num_objects; i++)
  {
    Vessel& vessel = m_vessels[i];

    if (vessel.ai_enabled)
    {
      bool evasion_engaged = false;
      /////////////////////////////////////////////////////////////////////////
      // General evasion
      if (evasion_enabled)
      {
        float dist = vessel.position.distanceSquared(vessel.ai_proximity_point);
        if (dist > 0.0f && dist < m_proximity_distance)
        {
          evasion_engaged = true;
        }
      }
      /////////////////////////////////////////////////////////////////////////

      //if (vessel.ai_state_due_time > curr_time)
      //{
      //  vessel.ai_state = AI_STATE_SELECT;
      //  vessel.ai_state_due_time = curr_time + 3.0f;
      //}

      if (evasion_engaged)
        processAi<Vessel>(vessel, AI_STATE_EVADE, vessel.ai_proximity_point);
      else
        processAi<Vessel>(vessel, vessel.ai_state, vessel.ai_track_position);
    }

    ///////////////////////////////////////////////////////////////////////////
    if (vessel.thrust != 0.0f)
    {
      vessel.thrust = clamp(vessel.thrust, -1.0f, 1.0f);

      Vec2f dir(vessel.thrust_max * vessel.thrust, 0.0f);
      dir.rotate(toRadians(vessel.angle));

      vessel.velocity += dir;
    }

    if (vessel.turn != 0.0f)
    {
      vessel.turn = clamp(vessel.turn, -1.0f, 1.0f);

      vessel.angle += vessel.turn * vessel.turn_max;

      if (vessel.angle <  0.0f)   vessel.angle += 360.0f;
      if (vessel.angle >= 360.0f) vessel.angle -= 360.0f;
    }

    if (vessel.fire)
    {
      prepareShots<Vessel>(vessel);
    }
    ///////////////////////////////////////////////////////////////////////////

    vessel.position += vessel.velocity;
    vessel.velocity *= m_physics_friction;
  }

  for (unsigned i = 0; i < m_buildings.num_objects; i++)
  {
    auto& building = m_buildings[i];

    if (building.ai_enabled)
    {
      Vec2f target_pos = building.position;

      if (m_vessels.has(building.ai_vessel_track_id))
      {
        auto& vessel = m_vessels.lookup(building.ai_vessel_track_id);

        float angle_to_target = getAngleTo<Building>(building);

        if (fabs(building.turn) < .25f)
          building.fire = true;
        else
          building.fire = false;

        if (building.turn != 0.0f)
        {
          building.turn = clamp(building.turn, -1.0f, 1.0f);

          building.angle += building.turn * building.turn_max;

          if (building.angle <  0.0f)   building.angle += 360.0f;
          if (building.angle >= 360.0f) building.angle -= 360.0f;
        } 
      }

      // Check for new targets. At random.
      if (m_vessels.has(building.ai_vessel_track_id))
      {
        prepareShots<Building>(building);
      }
      else
      {
        if (m_vessels.num_objects > 0)
        {
          unsigned id = m_vessels[random(m_vessels.num_objects)].id;
          if (m_vessels.has(id))
            building.ai_vessel_track_id = id;
        }
      }
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  m_proximity_system.update(getElapsedSeconds());

  m_proximity_system.handleEvents();
  /////////////////////////////////////////////////////////////////////////////
}

void Rocks2App::draw()
{
  ScopedElapsedTime elapsedTime("draw");

  // clear out the window with black
  gl::clear(Color(0, 0, 0));

  gl::color(1.0f, 1.0f, 1.0f);

  /////////////////////////////////////////////////////////////////////////////
  // Draw all buildings
  gl::lineWidth(3.0f);
  for (unsigned i = 0; i < m_buildings.num_objects; i++)
  {
    auto& building = m_buildings[i];
    float w = static_cast<float>(building.placement.getWidth() / 2.0f);
    Vec2f turret_heading(w * 2.0f, 0.0f);

    turret_heading.rotate(toRadians(building.angle));
    
    gl::color(.8f, .8f, .8f, 1.0f);
    gl::drawSolidCircle(building.position, w);
    gl::drawLine(building.position, building.position + turret_heading);
    
    gl::color(1.0f, 1.0f, 1.0f);
    gl::drawSolidRect(building.placement);
  }
  /////////////////////////////////////////////////////////////////////////////
  m_ground.render();
  /////////////////////////////////////////////////////////////////////////////
  m_proximity_system.render();
  /////////////////////////////////////////////////////////////////////////////
  for (unsigned i = 0; i < m_rocks.num_objects; i++)
  {
    Rock& rock = m_rocks.objects[i];

    gl::drawSolidCircle(rock.position, 5.0f);
  }
  /////////////////////////////////////////////////////////////////////////////
  gl::color(1.0f, 0.0f, 0.0f);
  for (unsigned i = 0; i < m_shots.num_objects; i++)
  {
    Shot& shot = m_shots.objects[i];

    gl::drawSolidCircle(shot.position, 3.0f);
  }
  /////////////////////////////////////////////////////////////////////////////
  gl::lineWidth(1.0f);
  gl::color(1.0f, 1.0f, 1.0f);
  for (unsigned i = 0; i < m_vessels.num_objects; i++)
  {
    Vessel& vessel = m_vessels[i];

    Rectf rect(-Vec2f(vessel.size, vessel.size), Vec2f(vessel.size, vessel.size));

    gl::pushMatrices();
      gl::translate(vessel.position);
      gl::rotate(vessel.angle);
      gl::draw(m_texture_vessel, rect);
      gl::drawLine(Vec2f::zero(), Vec2f(vessel.size, 0.0f));
    gl::popMatrices();
  }
  /////////////////////////////////////////////////////////////////////////////
  // Draw health on all vessels
  if (m_config.as_bool("debug.draw_health", true))
  {
    gl::lineWidth(3.0f);
    gl::color(0.0f, 1.0f, 0.0f);
    for (unsigned i = 0; i < m_vessels.num_objects; i++)
    {
      auto& o = m_vessels[i];
      float w = (o.destructible.hp / o.destructible.hp_max);

      Vec2f offset = Vec2f(-o.size, o.size);
      gl::drawLine(o.position + offset, o.position + offset + Vec2f(w * (o.size * 2.0f), 0.0f));
    }
  }
  /////////////////////////////////////////////////////////////////////////////
  // Draw AI State on all vessels
  if (m_config.as_bool("debug.draw_ai_state", true))
  {
    gl::lineWidth(3.0f);
    for (unsigned i = 0; i < m_vessels.num_objects; i++)
    {
      auto& o = m_vessels[i];
      static float inset = 3.0f;
      float size = o.size + inset;
      Rectf rect(-Vec2f(size, size), Vec2f(size, size));
      rect.offset(o.position);

      gl::color(toColor(o.ai_state));
      gl::drawStrokedRect(rect);
    }
  }
  /////////////////////////////////////////////////////////////////////////////
  // Draw closest target on all vessels
  if (m_config.as_bool("debug.draw_closest_target", true))
  {
    gl::lineWidth(2.0f);
    for (unsigned i = 0; i < m_vessels.num_objects; i++)
    {
      auto& o = m_vessels[i];

      gl::color(0.0f, 0.0f, 1.0f, 0.5f);
      gl::drawLine(o.position, o.ai_proximity_point);
    }
  }
  /////////////////////////////////////////////////////////////////////////////
  // Draw lines to targets
  if (m_config.as_bool("debug.target_paths_enabled", true))
  {
    gl::lineWidth(1.0f);
    gl::color(1.0f, 0.0f, 0.0f, .25f);

    for (unsigned i = 0; i < m_vessels.num_objects; i++)
    {
      auto& origin = m_vessels[i];

      if (m_vessels.has(origin.ai_vessel_track_id))
      {
        auto& target = m_vessels.lookup(origin.ai_vessel_track_id);

        gl::drawLine(origin.position, target.position);
      }
      else
      {
        gl::drawLine(origin.position, origin.ai_track_position);
      }
    }

    for (unsigned i = 0; i < m_buildings.num_objects; i++)
    {
      auto& origin = m_buildings[i];

      if (m_vessels.has(origin.ai_vessel_track_id))
      {
        auto& target = m_vessels.lookup(origin.ai_vessel_track_id);

        gl::drawLine(origin.position, target.position);
      }
    }
  }
  /////////////////////////////////////////////////////////////////////////////
  // Draw selected vessel + target
  if (m_vessels.has(m_vessel_selected_id))
  {
    auto& vessel = m_vessels.lookup(m_vessel_selected_id);

    gl::lineWidth(2.0f);
    Rectf rect(-Vec2f(vessel.size, vessel.size), Vec2f(vessel.size, vessel.size));
    rect.offset(vessel.position);

    gl::color(0.0f, 1.0f, 0.0f, .5f);
    gl::drawStrokedRect(rect);

    if (m_vessels.has(vessel.ai_vessel_track_id))
    {
      auto& target = m_vessels.lookup(vessel.ai_vessel_track_id);

      Rectf rect(-Vec2f(target.size, target.size), Vec2f(target.size, target.size));
      rect.offset(target.position);

      gl::color(1.0f, 0.0f, 0.0f, .75f);
      gl::drawStrokedRect(rect);
      
      gl::color(1.0f, 0.0f, 0.0f, .25f);
      gl::drawLine(vessel.position, target.position);
    }
    else
    {
      gl::color(1.0f, 0.0f, 0.0f, .25f);
      gl::drawLine(vessel.position, m_vessel_selected_target);
    }

    {
      stringstream ss;
      ss << " " << vessel.id
         << " -> " << vessel.ai_vessel_track_id
         << " " << vessel.destructible.hp << " / " << vessel.destructible.hp_max;
      m_debug_string = ss.str();
    }
  }
  /////////////////////////////////////////////////////////////////////////////

  renderDebugInfo();
}

void Rocks2App::renderDebugInfo()
{
  {
    static Font font = Font("Arial", 32.0f);
    Vec2f start, end;

    start.set(5.0f, 5.0f);
    end.set(5.0f, 10.0f);

    gl::color(1.0f, 1.0f, 1.0f);
    for (unsigned i = 0; i < m_rocks.num_objects; i++)
    {
      gl::drawLine(start, end);

      start.x += m_text_spacing;
      end.x += m_text_spacing;
    }

    start.set(5.0f, start.y + 20.0f);
    end.set(5.0f, end.y + 20.0f);
    Vec2f link_start;
    Vec2f link_end;
    
    #if (DEBUG_SYSTEM)
    for (int i = 0; i < MAX_OBJECTS; i++)
    {
      //if (i == m_rocks.freelist_dequeue)
      //  gl::color(1.0f, 1.0f, 1.0f);
      //else if (i == m_rocks.freelist_enqueue)
      //  gl::color(1.0f, 1.0f, 1.0f);
      //else
        gl::color(0.0f, 1.0f, 0.0f);
      gl::drawLine(start, end);

      if (m_rocks.has(m_rocks.indices[i].id))
      {
        link_start.set(5.0f + m_rocks.indices[i].index * m_text_spacing, 11.0f);
        link_end.set(start);
        link_end.y--;

        gl::color(1.0f, 0.0f, 0.0f);
        gl::drawLine(link_start, link_end);
      }

      start.x += m_text_spacing;
      end.x += m_text_spacing;
    }

    {
      start.set(5.0f + m_rocks.freelist_dequeue * m_text_spacing, 40.0f);
      end.set(5.0f + m_rocks.freelist_dequeue * m_text_spacing, 45.0f);
      gl::color(1.0f, 1.0f, 1.0f);
      gl::drawLine(start, end);

      start.set(5.0f + m_rocks.freelist_enqueue * m_text_spacing, 45.0f);
      end.set(5.0f + m_rocks.freelist_enqueue * m_text_spacing, 50.0f);
      gl::color(1.0f, 1.0f, 1.0f);
      gl::drawLine(start, end);
    }
    #endif // #if (DEBUG_SYSTEM)

    static const bool debug_draw_position = m_config.as_bool("debug.draw_position");
    if (debug_draw_position)
    {
      gl::color(0.0f, 1.0f, 0.0f);
      gl::drawSolidCircle(m_debug_position, 5.0f);
    }

    stringstream ss;
    string s;

    ss << "num: " << m_rocks.num_objects << " max: " << MAX_OBJECTS;

    ss << " freelist_dequeue: " << m_rocks.freelist_dequeue << " freelist_enqueue: " << m_rocks.freelist_enqueue;

    s = ss.str();
  
    gl::drawString(s, Vec2f(50.0f, 60.0f), ColorA(1.0f, 1.0f, 0.0f, 1.0f), font);
    
    gl::drawString(m_debug_string, Vec2f(50.0f, 80.0f), ColorA(1.0f, 1.0f, 0.0f, 1.0f), font);
  }
}

CINDER_APP_NATIVE(Rocks2App, RendererGl)
