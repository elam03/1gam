#include "ProximitySystem.h"

#include <cinder/gl/gl.h>

using namespace cinder;
using namespace std;
using namespace Mgl;

ProximitySystem::ProximitySystem(Mgl::System<Vessel>& _vessels, Mgl::System<Shot>& _shots, Mgl::System<Building>& _buildings, Ground& _ground, Universe& _universe) :
  m_events(16),
  
  m_vessels(_vessels),
  m_shots(_shots),
  m_buildings(_buildings),
  m_ground(_ground),
  m_universe(_universe),
  
  m_last_time_updated(0.0),
  m_update_rate(0.1),

  m_cell_size(128.0f, 128.0f),

  m_quad_tree_vessels(),
  m_quad_tree_shots()
{
}

ProximitySystem::~ProximitySystem()
{
}

void ProximitySystem::init()
{
  m_quad_tree_vessels.setBoundary(AABB(m_universe.bounds.getCenter(), m_universe.bounds.getSize() / 2.0f));
  m_quad_tree_shots.setBoundary(AABB(m_universe.bounds.getCenter(), m_universe.bounds.getSize() / 2.0f));
}

unsigned ProximitySystem::getVessel(const Vec2f& _pos)
{
  for (unsigned i = 0; i < m_vessels.num_objects; i++)
  {
    auto& vessel = m_vessels[i];
    float vessel_size_squared = vessel.size * vessel.size;

    if (_pos.distanceSquared(vessel.position) < vessel_size_squared)
    {
      return vessel.id;
    }
  }
  return ID_INVALID;
}

void ProximitySystem::render()
{
  gl::color(1.0f, 1.0f, 1.0f, .25f);
  gl::lineWidth(2.0f);

  //for (float y = 0.0f; y < m_universe.bounds.getHeight(); y += m_cell_size.y)
  //  gl::drawLine(Vec2f(0.0f, y), Vec2f(m_universe.bounds.getWidth(), y));

  //for (float x = 0.0f; x < m_universe.bounds.getWidth(); x += m_cell_size.x)
  //  gl::drawLine(Vec2f(x, 0.0f), Vec2f(x, m_universe.bounds.getHeight()));

  m_quad_tree_vessels.process([](const AABB& _parent, const ci::Vec2f& _child) {
    gl::drawSolidCircle(_parent.centre, 15.0f);
    gl::drawLine(_parent.centre, _child);

    gl::drawLine(_parent.centre + Vec2f(-_parent.halfSize.x, -_parent.halfSize.y), _parent.centre + Vec2f(+_parent.halfSize.x, -_parent.halfSize.y));
    gl::drawLine(_parent.centre + Vec2f(+_parent.halfSize.x, -_parent.halfSize.y), _parent.centre + Vec2f(+_parent.halfSize.x, +_parent.halfSize.y));
    gl::drawLine(_parent.centre + Vec2f(+_parent.halfSize.x, +_parent.halfSize.y), _parent.centre + Vec2f(-_parent.halfSize.x, +_parent.halfSize.y));
    gl::drawLine(_parent.centre + Vec2f(-_parent.halfSize.x, +_parent.halfSize.y), _parent.centre + Vec2f(-_parent.halfSize.x, -_parent.halfSize.y));
  });
}

void ProximitySystem::update(double _curr_time)
{
  buildQuadTrees();
  //if (_curr_time < (m_last_time_updated + m_update_rate))
  //{
  //  return;
  //}

  ///////////////////////////////////////////////////////////////////////////
  // m_vessels to m_shots
  for (unsigned i = 0; i < m_vessels.num_objects; i++)
  {
    auto& vessel = m_vessels[i];
    float vessel_size_squared = vessel.size * vessel.size;
    
    AABB range(vessel.position, Vec2f(vessel.size, vessel.size));

    auto list = m_quad_tree_shots.queryRange(range);

    for (auto& data : list)
    {
      auto& shot = *data.load;

      if (vessel.id != shot.origin_id)
      {
        if (vessel.position.distanceSquared(shot.position) < vessel_size_squared)
        {
          pushEvent(Event(EVENT_VESSEL_TO_SHOT, vessel.id, shot.id));
        }
      }
    }
  }
  ///////////////////////////////////////////////////////////////////////////
  // m_vessels to m_vessels
  // Does collision detection AND populates a vessel's ai_proximity_point
  // which tells a vessel a specific point to avoid.

  for (unsigned i = 0; i < m_vessels.num_objects; i++)
  {
    auto& vessel = m_vessels[i];
    float vessel_size_squared = vessel.size * vessel.size;

    unsigned closest_id = m_vessels[0].id;
    float    closest_dist = numeric_limits<float>::max();
    float    proximity_dist = numeric_limits<float>::max();
    
    AABB range(vessel.position, Vec2f(vessel.size * 2.0f, vessel.size * 2.0f));

    auto list = m_quad_tree_vessels.queryRange(range);

    for (auto& data : list)
    {
      auto& other_vessel = *data.load;
      float other_vessel_size_squared = other_vessel.size * other_vessel.size;

      if (vessel.id != other_vessel.id)
      {
        float dist = vessel.position.distanceSquared(other_vessel.position);
        dist -= (vessel_size_squared * 2);
        dist -= (other_vessel_size_squared * 2);

        if (dist < 0.0f)
          pushEvent(Event(EVENT_VESSEL_TO_VESSEL, vessel.id, other_vessel.id));
      }
    }
  }

  for (unsigned i = 0; i < m_vessels.num_objects; i++)
  {
    auto& vessel = m_vessels[i];
    float vessel_size_squared = vessel.size * vessel.size;

    unsigned closest_id = m_vessels[0].id;
    float    closest_dist = numeric_limits<float>::max();
    float    proximity_dist = numeric_limits<float>::max();

    if (m_vessels.num_objects > 1)
    {
      for (unsigned j = 0; j < m_vessels.num_objects; j++)
      {
        auto& other_vessel = m_vessels[j];
        float other_vessel_size_squared = other_vessel.size * other_vessel.size;

        if (i != j)
        {
          float dist = vessel.position.distanceSquared(other_vessel.position);
          dist -= (vessel_size_squared * 2);
          dist -= (other_vessel_size_squared * 2);
          
          if (dist < closest_dist)
          {
            closest_id = other_vessel.id;
            closest_dist = dist;
          }

          {
            // While we are here, calculate the other vessel's projected position and
            // flag it as a proximity point if needed.
            Vec2f projected_position = other_vessel.position + (other_vessel.velocity * static_cast<float>(vessel.ai_projection_time));

            float dist = vessel.position.distanceSquared(projected_position);
            if (dist < proximity_dist)
            {
              proximity_dist = dist;
              vessel.ai_proximity_point = projected_position;
            }
          }
        }
      }
    }

    //if (closest_dist < 0.0f)
    //  pushEvent(Event(EVENT_VESSEL_TO_VESSEL, vessel.id, closest_id));

    {
      // Do check how close we are to the surface and report it as a
      // threatening position or not.
      Vec2f surface_position = m_ground.getSurfacePoint(vessel.position.x);
        
      float dist = vessel.position.distanceSquared(surface_position);

      if (vessel.position.y < surface_position.y)
      {
        if (dist < proximity_dist)
        {
          proximity_dist = dist;
          vessel.ai_proximity_point = surface_position;
        }
      }
      else
      {
        vessel.position.y = surface_position.y - vessel.size - 2.0f;
      }
        
      if (dist <= (vessel.size * vessel.size))
      {
        // TODO(elam03): make this better...
        // The vessel is below the surface... I'll just apply a rebounding
        // force to make it pop up above...
        vessel.position.y = surface_position.y - vessel.size - 2.0f;
      }
    }
  }
  ///////////////////////////////////////////////////////////////////////////

  m_last_time_updated = _curr_time;
}

void ProximitySystem::handleEvents()
{
  while (m_events.getSize() > 0)
  {
    auto& e = m_events.remove();
    auto& listeners = m_listeners[e.type];

    for (auto j = 0; j < listeners.size(); j++)
      listeners[j](e);
  }
}

void ProximitySystem::buildQuadTrees()
{
  m_quad_tree_vessels.clear();
  m_quad_tree_shots.clear();

  for (unsigned i = 0; i < m_vessels.num_objects; ++i)
  {
    auto& o = m_vessels[i];

    Data<Vessel> data(o.position, &o);
    m_quad_tree_vessels.insert(data);
  }

  for (unsigned i = 0; i < m_shots.num_objects; ++i)
  {
    auto& o = m_shots[i];

    Data<Shot> data(o.position, &o);
    m_quad_tree_shots.insert(data);
  }
}
