#include "Ground.h"

#include "Utils.h"

#include "cinder/gl/gl.h"
#include "cinder/gl/vbo.h"

#include <vector>

using namespace ci;
using namespace std;
using namespace Mgl;

Ground::Ground() :
  m_ground_nodes(),
  m_vbo_mesh()
{
}

Ground::~Ground()
{
}

bool Ground::isBelow(const Vec2f& _pos) const
{
  bool result = false;

  return result;
}

Vec2f Ground::getSurfacePoint(float _x) const
{
  // TODO(elam03): optimize selection of the 2 nodes

  if (m_ground_nodes.num_objects > 2)
  {
    auto& n_begin = m_ground_nodes[0];
    auto& n_end   = m_ground_nodes[m_ground_nodes.num_objects - 1];

    if (_x <= n_begin.position.x)
    {
      return (n_begin.position);
    }
    else if (_x >= n_end.position.x)
    {
      return (n_end.position);
    }
    else
    {
      for (unsigned i = 1; i < m_ground_nodes.num_objects; ++i)
      {
        auto& n1 = m_ground_nodes[i - 1];
        auto& n2 = m_ground_nodes[i];

        Vec2f p1 = n1.position;
        Vec2f p2 = n2.position;

        if (_x >= p1.x && _x <= p2.x)
        {
          float t = (_x - p1.x) / (p2.x - p1.x);

          return ((p2 - p1) * t + p1);
        }
      } 
    }
  }
  
  return Vec2f::zero();
}

void Ground::generate(float _x_max, float _y_max, float _y_level, unsigned _num_nodes)
{
  static const float variance_max = 25.0f;

  // TODO(elam03): implement clear in System
  //m_ground_nodes.clear();

  Vec2f p(0.0f, _y_level);
  Vec2f p2(0.0f, _y_max);
  float inc = _x_max / _num_nodes;

  vector<Vec3f>    vertices;
  vector<unsigned> indices;
  vector<Color>    colors;
  unsigned         vertex_count = 0;

  for (unsigned i = 0; i <= _num_nodes; ++i)
  {
    unsigned id = m_ground_nodes.add();
    
    auto& ground_node = m_ground_nodes.lookup(id);

    ground_node.position = p;
    ground_node.color.set(randomf(0.65f, 0.75f), randomf(0.5f, 0.65f), randomf(0.5f, 0.6f));

    vertices.push_back(Vec3f(p2));
    colors.push_back(ground_node.color);
    indices.push_back(vertex_count++);

    vertices.push_back(Vec3f(p));
    colors.push_back(ground_node.color);
    indices.push_back(vertex_count++);

    p.x += inc;
    p.y += randomf(-variance_max, variance_max);
    p2.x += inc;
  }

  gl::VboMesh::Layout layout;
  layout.setStaticPositions();
  layout.setStaticIndices();
  layout.setStaticColorsRGB();

  m_vbo_mesh = gl::VboMesh(vertices.size(), vertices.size(), layout, GL_TRIANGLE_STRIP);
  
  m_vbo_mesh.bufferIndices(indices);
  m_vbo_mesh.bufferPositions(vertices);
  m_vbo_mesh.bufferColorsRGB(colors);
}

void Ground::render()
{
  if (m_ground_nodes.num_objects > 0)
    gl::draw(m_vbo_mesh);
}



