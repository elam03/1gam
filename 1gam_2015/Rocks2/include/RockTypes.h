#pragma once

#include "cinder/Color.h"
#include "cinder/Rect.h"
#include "cinder/Vector.h"

///////////////////////////////////////////////////////////////////////////////
// Constants
static const int MAX_WEAPON_EMITTERS = 8;
static const float DEFAULT_HP = 100.0f;

///////////////////////////////////////////////////////////////////////////////
// POD Structures

struct Rock
{
  unsigned  id;
  ci::Vec2f position;
};

struct WeaponEmitter
{
  bool      enabled;
  int       type;
  ci::Vec2f position_offset;
  float     angle_offset;

  bool   ready;
  double time_last_fired;
  double time_ready;
  double time_cooldown;

  WeaponEmitter() { reset(); }
  virtual ~WeaponEmitter() { reset(); }
  void reset()
  {
    enabled = false;
    type = 0;
    position_offset.set(0.0f, 0.0f);
    angle_offset = 0.0f;
    time_cooldown = 1.0;
    ready = true;
    time_last_fired = 0.0;
    time_ready = time_cooldown;
  }

  void set(int _type, ci::Vec2f& _position_offset = ci::Vec2f::zero(), float _angle_offset = 0.0f, float _time_cooldown = 1.0f)
  {
    enabled = true;
    type = _type;
    position_offset = _position_offset;
    angle_offset = _angle_offset;

    time_ready = _time_cooldown;
  }

  bool fire(double _curr_time)
  {
    if (enabled)
    {
      if (ready)
      {
        ready = false;
        time_last_fired = _curr_time;
        time_ready = _curr_time + time_cooldown;

        return true;
      }
      else
      {
        ready = _curr_time >= time_ready;
      }
    }

    return false;
  }
};

struct Attacker
{
  Attacker() { power = 1.0f; }

  inline void set(float _power) { power = _power; }

  float power;
};

struct Destructible
{
  Destructible() : hp(DEFAULT_HP), hp_max(DEFAULT_HP) {}

  inline bool takeDamage(const Attacker& _attacker) { hp -= _attacker.power; return (hp < 0.0f); }
  inline void set(float _hp) { hp_max = hp = _hp; }

  float hp_max;
  float hp;
};

struct Vessel
{
  unsigned  id;
  ci::Vec2f position;
  ci::Vec2f velocity;
  float     size;
  float     angle;

  float turn_max;
  float thrust_max;

  float turn;
  float thrust;
  bool  fire;

  bool      ai_enabled;
  int       ai_state;
  double    ai_state_due_time;
  ci::Vec2f ai_track_position;
  unsigned  ai_vessel_track_id;
  double    ai_projection_time;
  ci::Vec2f ai_proximity_point;

  Destructible  destructible;
  WeaponEmitter weapon_emitters[MAX_WEAPON_EMITTERS];
};

struct Shot
{
  unsigned  id;
  ci::Vec2f position;
  ci::Vec2f velocity;
  double    time_start;
  double    time_end;
  unsigned  origin_id;

  Attacker attacker;
};

struct Building
{
  unsigned  id;
  ci::Rectf placement;
  ci::Vec2f position;  // Turret position
  float     angle;     // Turret angle

  float turn_max;

  float turn;
  bool  fire;

  bool      ai_enabled;
  int       ai_state;
  double    ai_state_due_time;
  ci::Vec2f ai_track_position;
  unsigned  ai_vessel_track_id;

  Destructible  destructible;
  WeaponEmitter weapon_emitters[MAX_WEAPON_EMITTERS];

  void set(const ci::Vec2f& _pos, float _width, float _height)
  {
    placement.set(_pos.x - _width / 2.0f, _pos.y, _pos.x + _width / 2.0f, _pos.y - _height);
    position.set(_pos + ci::Vec2f(0.0f, -_height));
  }
};

struct GroundNode
{
  unsigned  id;
  ci::Vec2f position;
  ci::Color color;
};

struct Universe
{
  ci::Rectf scroll;
  ci::Rectf zoom;
  ci::Rectf scale;

  ci::Rectf bounds;
  ci::Rectf view;

  Universe() : scroll(), zoom(), scale(), bounds(), view() {}
  Universe(const ci::Rectf& _bounds) : scroll(), zoom(), scale(), bounds(_bounds), view(_bounds) {}
};

///////////////////////////////////////////////////////////////////////////////
// Enums
typedef enum WEAPON_TYPE_ENUM
{
  WEAPON_TYPE_NONE,
  WEAPON_TYPE_BULLET,
  WEAPON_TYPE_LASER,
  WEAPON_TYPE_MISSLE,
  WEAPON_TYPE_COUNT
} WEAPON_TYPE;

// Various AI states. Using bitwise-flag numbers for potentially combining and
// performing more complicated logic for various ai configurations
typedef enum AI_STATE_ENUM
{
  AI_STATE_NONE,
  AI_STATE_MOVE = 1,
  AI_STATE_ATTACK = 2,
  AI_STATE_EVADE = 4,
  AI_STATE_SELECT = 8,
  AI_STATE_IDLE = 16,
  AI_STATE_COUNT
} AI_STATE;

static ci::Color toColor(int _ai_state)
{
  switch (_ai_state)
  {
    case AI_STATE_MOVE:   return ci::Color(0.0f, 1.0f, 0.0f);
    case AI_STATE_ATTACK: return ci::Color(1.0f, 0.0f, 0.0f);
    case AI_STATE_EVADE:  return ci::Color(0.0f, 1.0f, 1.0f);
    case AI_STATE_SELECT: return ci::Color(0.0f, 0.0f, 1.0f);
    case AI_STATE_IDLE:   return ci::Color(1.0f, 0.0f, 1.0f);
    default:              return ci::Color(0.75f, 0.75f, 0.75f);
  }
}

///////////////////////////////////////////////////////////////////////////////