#pragma once

#include "Ground.h"
#include "RockTypes.h"
#include "System.h"
#include "QuadTree.h"
#include "Utils.h"

#include <chrono>
#include <functional>
#include <map>
#include <vector>
#include <unordered_map>

class ProximitySystem
{
public:
  enum EVENT_ENUM
  {
    EVENT_NONE,
    EVENT_VESSEL_TO_SHOT,
    EVENT_VESSEL_TO_VESSEL,
    EVENT_SHOT_BUILDING,
    EVENT_COUNT
  } EVENT;

  struct Event
  {
    unsigned id;
    int      type;
    unsigned ids[2];

    Event() : id(0), type(0) { ids[0] = ids[1] = 0; }
    Event(int _type, unsigned _id0, unsigned _id1) : id(0), type(_type) { ids[0] = _id0; ids[1] = _id1; }
  };

  ProximitySystem(Mgl::System<Vessel>& _vessels, Mgl::System<Shot>& _shots, Mgl::System<Building>& _buildings, Ground& _ground, Universe& _universe);
  virtual ~ProximitySystem();

  void init();

  unsigned getVessel(const ci::Vec2f& _pos);

  void render();
  void update(double _curr_time);

  Mgl::RingBuffer<Event> m_events;

  Mgl::System<Vessel>&   m_vessels;
  Mgl::System<Shot>&     m_shots;
  Mgl::System<Building>& m_buildings;
  Ground&                m_ground;
  Universe&              m_universe;

  double m_last_time_updated;
  double m_update_rate;

  ///////////////////////////////////////////////////////////////////////////
public:
  typedef std::function<void(Event&)> Listener;
  
  inline void addListener(int _event_type, Listener _listener) { m_listeners[_event_type].push_back(_listener); }
  void handleEvents();

private:
  std::map<int, std::vector<Listener> > m_listeners;

  inline void pushEvent(const Event& _event) { m_events.insert(_event); }

  ///////////////////////////////////////////////////////////////////////////

private:
  ci::Vec2f m_cell_size;

  Quadtree<Vessel> m_quad_tree_vessels;
  Quadtree<Shot>   m_quad_tree_shots;

  void buildQuadTrees();
  ///////////////////////////////////////////////////////////////////////////

};