#ifndef __GROUND_H__
#define __GROUND_H__

#include "RockTypes.h"
#include "System.h"

#include "cinder/gl/vbo.h"

class Ground
{
public:
  Ground();
  virtual ~Ground();
  
  bool      isBelow(const ci::Vec2f& _pos) const;
  ci::Vec2f getSurfacePoint(float _x) const;

  void generate(float _x_max, float _y_max, float _y_level, unsigned _num_nodes);
  void render();

  Mgl::System<GroundNode> m_ground_nodes;

  ci::gl::VboMesh m_vbo_mesh;
};

#endif