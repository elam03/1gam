#include "CiUtils.h"

#include "Utils.h"


namespace Mgl
{

  
bool circleLineSegmentCheck(const ci::Vec2f& _center, float _radius, const ci::Vec2f& _p1, const ci::Vec2f& _p2)
{
  ci::Vec2f dir = _p2 - _p1;
  ci::Vec2f diff = _center - _p1;

  float t = diff.dot(dir) / dir.dot(dir);

  if (t < 0.0f)
    t = 0.0f;
  if (t > 1.0f)
    t = 1.0f;

  ci::Vec2f closestPoint = _p1 + dir * t;
  ci::Vec2f d = _center - closestPoint;
  float distanceSquared = d.dot(d);

  return (distanceSquared <= (_radius * _radius));
}

bool circleCircleCheck(const ci::Vec2f& _center1, float _radius1, const ci::Vec2f& _center2, float _radius2, ci::Vec2f& _where)
{
  ci::Vec2f diff = _center2 - _center1;

  float distanceSquared = ((diff.x * diff.x) + (diff.y * diff.y));
  float distanceSquaredThreshold = (_radius1 + _radius2) * (_radius1 + _radius2);

  return (distanceSquared < distanceSquaredThreshold);
}

float angleTo(const ci::Vec2f& _origin, const ci::Vec2f& _target)
{
  float dx = (float)(_target.x - _origin.x);
  float dy = (float)(_target.y - _origin.y);

  float result = atan2(dy, dx) * RAD;

  if (result < 0.0f)
    result += 360.0f;

  return (result);
}


} // namespace Mgl