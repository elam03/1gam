#pragma once

#include <fstream>
#include <map>
#include <string>

#include <cinder/Json.h>

namespace Mgl
{

class Config
{
public:
  Config() : m_root(), m_settings_filepath("")
  {
    
  }

  virtual ~Config()
  {
    //if (m_settings_filepath.length() > 0)
    //  m_root.write(m_settings_filepath.c_str());
  }

  void write()
  {
    if (m_settings_filepath.length() > 0)
      m_root.write(m_settings_filepath.c_str());
  }

  bool load(const std::string& _path)
  {
    m_root.clear();
    m_settings_filepath = _path;

    try {
      m_root = cinder::JsonTree(cinder::loadFile(_path));
    } catch (std::exception& e) {
      e.what();
      return false;
    }

    return true;
  }

  bool as_bool(const std::string& _key, bool _def = false)
  {
    if (m_root.hasChild(_key))
      return m_root.getChild(_key).getValue<bool>();
    m_root.addChild(cinder::JsonTree(_key, _def));
    return _def;
  }

  int as_int(const std::string& _key, int _def = 0)
  {
    if (m_root.hasChild(_key))
      return m_root.getChild(_key).getValue<int>();
    m_root.addChild(cinder::JsonTree(_key, _def));
    return _def;
  }

  float as_float(const std::string& _key, float _def = 0.0f)
  {
    if (m_root.hasChild(_key))
      return m_root.getChild(_key).getValue<float>();
    m_root.addChild(cinder::JsonTree(_key, _def));
    return _def;
  }

  double as_double(const std::string& _key, double _def = 0.0)
  {
    if (m_root.hasChild(_key))
      return m_root.getChild(_key).getValue<double>();
    m_root.addChild(cinder::JsonTree(_key, _def));
    return _def;
  }

  void test()
  {
    cinder::JsonTree doc;
    cinder::JsonTree library = cinder::JsonTree::makeArray("library");//( "library" );
    cinder::JsonTree album = cinder::JsonTree::makeArray("albums");//( "albums" );
    album.pushBack( cinder::JsonTree( "musician", "Sufjan Stevens" ) );
    album.pushBack( cinder::JsonTree( "year", "2004" ) );
    album.pushBack( cinder::JsonTree( "title", "Seven Swans" ) );

    cinder::JsonTree tracks = cinder::JsonTree::makeArray("tracks");//( "tracks" );

    tracks.pushBack( cinder::JsonTree( "track", "All the Trees of the Field Will Clap Their Hands" ) );
    tracks.pushBack( cinder::JsonTree( "track", "The Dress Looks Nice on You" ) );
    tracks.pushBack( cinder::JsonTree( "track", "In the Devil's Territory" ) );
    tracks.pushBack( cinder::JsonTree( "track", "To Be Alone With You" ) );

    album.pushBack( tracks );
    library.pushBack( album );
    doc.pushBack( library );

    cinder::JsonTree node = doc.getChild("library.albums.title");
    std::string s = node.getValue();
    s = s;

    doc.write("testoutput.json");
  }

  cinder::JsonTree m_root;
  cinder::JsonTree m_doc;
  std::string      m_settings_filepath;
};

}