#pragma once

#define DEBUG_SYSTEM 0

namespace Mgl
{

typedef unsigned ID;

// TODO: Technically it only adds objects up to MAX_OBJECTS - 1. Want to fix
// this later since it's stable like this now.

#if (DEBUG_SYSTEM)
/////////////////////////////////////////////////////////////////////////////
// For debugging with a limited number of objects
#define MAX_OBJECTS 128
#define INDEX_MASK 0x00ff
#define INDEX_MAX  0x00ff
#define NEW_OBJECT_ID_ADD (INDEX_MASK + 1)
#define ID_INVALID INDEX_MASK
/////////////////////////////////////////////////////////////////////////////
#else
/////////////////////////////////////////////////////////////////////////////
// MAX_OBJECTS needs to be able to be represented with the bits
// in INDEX_MASK/INDEX_MAX
#define MAX_OBJECTS 65535
#define INDEX_MASK 0xffff
#define INDEX_MAX  0xffff
#define NEW_OBJECT_ID_ADD (INDEX_MASK + 1)
#define ID_INVALID INDEX_MASK
/////////////////////////////////////////////////////////////////////////////
#endif


struct Index
{
  ID id;
  unsigned short index;
  unsigned short next;
};

template<typename T>
struct System
{
  unsigned num_objects;
  T objects[MAX_OBJECTS];
  Index indices[MAX_OBJECTS];
  unsigned short freelist_enqueue;
  unsigned short freelist_dequeue;

  System() {
    num_objects = 0;
    for (unsigned i = 0; i < MAX_OBJECTS; i++) {
      indices[i].id = i;
      indices[i].index = INDEX_MAX;
      indices[i].next = i + 1;
    }
    freelist_dequeue = 0;
    freelist_enqueue = MAX_OBJECTS - 1;
  }

  virtual ~System() {}

  inline bool has(ID id) {
    Index &in = indices[id & INDEX_MASK];
    return in.id == id && in.index != INDEX_MAX;
  }

  inline T& lookup(ID id) {
    return objects[indices[id & INDEX_MASK].index];
  }

  inline T& operator[](unsigned _index) {
    return objects[_index];
  }

  inline const T& operator[](unsigned _index) const {
    return objects[_index];
  }

  inline ID add() {
    Index &in = indices[freelist_dequeue];
    if (freelist_dequeue == freelist_enqueue)
      return in.id;
    freelist_dequeue = in.next;
    in.id += NEW_OBJECT_ID_ADD;
    in.index = num_objects++;
    T &o = objects[in.index];
    o.id = in.id;
    return o.id;
  }

  inline void remove(ID id) {
    Index &in = indices[id & INDEX_MASK];

    T &o = objects[in.index];
    o = objects[--num_objects];
    indices[o.id & INDEX_MASK].index = in.index;

    in.index = INDEX_MAX;
    indices[freelist_enqueue].next = id & INDEX_MASK;
    freelist_enqueue = id & INDEX_MASK;
  }

  /////////////////////////////////////////////////////////////////////////////
  // Debug utilities
  void renderDebugInfo();
  /////////////////////////////////////////////////////////////////////////////
};

}