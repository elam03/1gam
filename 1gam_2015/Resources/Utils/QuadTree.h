#ifndef _QUADTREE_H_
#define _QUADTREE_H_

#include <vector>
#include <iostream>

#include <cinder/Vector.h>

struct AABB
{
  ci::Vec2f centre;
  ci::Vec2f halfSize;

  AABB(const ci::Vec2f& centre = ci::Vec2f(), const ci::Vec2f& halfSize = ci::Vec2f()) : centre(centre), halfSize(halfSize) {}

  bool contains(ci::Vec2f a)
  {
    if (a.x < centre.x + halfSize.x && a.x > centre.x - halfSize.x)
    {
      if (a.y < centre.y + halfSize.y && a.y > centre.y - halfSize.y)
      {
        return true;
      }
    }
    return false;
  }

  bool intersects(AABB other)
  {
    //this right > that left                                          this left <s that right
    if (centre.x + halfSize.x > other.centre.x - other.halfSize.x || centre.x - halfSize.x < other.centre.x + other.halfSize.x)
    {
      // This bottom > that top
      if (centre.y + halfSize.y > other.centre.y - other.halfSize.y || centre.y - halfSize.y < other.centre.y + other.halfSize.y)
      {
        return true;
      }
    }
    return false;
  }
};

template <typename T>
struct Data
{
  ci::Vec2f pos;
  T* load;

  Data(ci::Vec2f pos = ci::Vec2f(), T* data = nullptr): pos(pos), load(data){};
};


template <class T>
class Quadtree
{
private:
  //4 children
  Quadtree* nw;
  Quadtree* ne;
  Quadtree* sw;
  Quadtree* se;

  AABB boundary;

  std::vector< Data<T> > objects;

  int CAPACITY;       
public:
  Quadtree<T>();
  Quadtree<T>(AABB boundary);

  ~Quadtree();

  void setBoundary(AABB boundary);

  void clear();
  bool insert(Data<T> d);
  void subdivide();
  std::vector< Data<T> > queryRange(AABB range);

  void process(std::function<void(const AABB&, const ci::Vec2f&)> _process);
};

template <class T>
Quadtree<T>::Quadtree() : nw(nullptr), ne(nullptr), sw(nullptr), se(nullptr)
{
  CAPACITY = 1;
  setBoundary(AABB());
  objects = std::vector< Data<T> >();
}

template <class T>
Quadtree<T>::Quadtree(AABB boundary) : nw(nullptr), ne(nullptr), sw(nullptr), se(nullptr)
{
  objects = std::vector< Data<T> >();
  CAPACITY = 1;
  setBoundary(boundary);
}

template <class T>
Quadtree<T>::~Quadtree()
{
  clear();
}

template <class T>
void Quadtree<T>::setBoundary(AABB boundary)
{
  this->boundary = boundary;
}

template <class T>
void Quadtree<T>::clear()
{
  if (nw) { delete nw; nw = nullptr; }
  if (sw) { delete sw; sw = nullptr; }
  if (ne) { delete ne; ne = nullptr; }
  if (se) { delete se; se = nullptr; }

  objects.clear();
}

template <class T>
void Quadtree<T>::subdivide()
{
  ci::Vec2f qSize = ci::Vec2f(boundary.halfSize.x / 2.0f, boundary.halfSize.y / 2.0f);
  ci::Vec2f qCentre = ci::Vec2f(boundary.centre.x - qSize.x, boundary.centre.y - qSize.y);
  nw = new Quadtree(AABB(qCentre, qSize));

  qCentre = ci::Vec2f(boundary.centre.x + qSize.x, boundary.centre.y - qSize.y);
  ne = new Quadtree(AABB(qCentre, qSize));

  qCentre = ci::Vec2f(boundary.centre.x - qSize.x, boundary.centre.y + qSize.y);
  sw = new Quadtree(AABB(qCentre, qSize));

  qCentre = ci::Vec2f(boundary.centre.x + qSize.x, boundary.centre.y + qSize.y);
  se = new Quadtree(AABB(qCentre, qSize));
}

template <class T>
bool Quadtree<T>::insert(Data<T> d)
{
  if (!boundary.contains(d.pos))
  {
    return false;
  }

  if (objects.size() < CAPACITY)
  {
    objects.push_back(d);
    return true;
  }

  if (nw == nullptr)
  {
    subdivide();
  }

  if (nw->insert(d))
  {
    return true;
  }
  if (ne->insert(d))
  {
    return true;
  }
  if (sw->insert(d))
  {
    return true;
  }
  if (se->insert(d))
  {
    return true;
  }

  return false;   
}

template <class T>
std::vector< Data<T> > Quadtree<T>::queryRange(AABB range)
{
  std::vector< Data<T> > pInRange = std::vector< Data<T> >();

  if (!boundary.intersects(range))
  {
    return pInRange;
  }

  for(int i = 0; i < objects.size(); i++)
  {
    if (range.contains(objects.at(i).pos))
    {
      pInRange.push_back(objects.at(i));
    }
  }

  if (nw == nullptr)
  {
    return pInRange;
  }

  std::vector< Data<T> > temp = nw->queryRange(range);
  pInRange.insert(pInRange.end(), temp.begin(), temp.end());

  temp = ne->queryRange(range);
  pInRange.insert(pInRange.end(), temp.begin(), temp.end());

  temp = sw->queryRange(range);
  pInRange.insert(pInRange.end(), temp.begin(), temp.end());

  temp = se->queryRange(range);
  pInRange.insert(pInRange.end(), temp.begin(), temp.end());

  return pInRange;
}

template <class T>
void Quadtree<T>::process(std::function<void(const AABB&, const ci::Vec2f&)> _process)
{
  for (auto& list : objects)
  {
    _process(boundary, list.pos);
  }

  if (nw) nw->process(_process);
  if (ne) ne->process(_process);
  if (sw) sw->process(_process);
  if (se) se->process(_process);
}

#endif