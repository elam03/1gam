#pragma once

#include "Utils.h"

#include <cinder/Vector.h>

namespace Mgl
{

bool circleLineSegmentCheck(const ci::Vec2f& _center, float _radius, const ci::Vec2f& _p1, const ci::Vec2f& _p2);
bool circleCircleCheck(const ci::Vec2f& _center1, float _radius1, const ci::Vec2f& _center2, float _radius2, ci::Vec2f& _where);

} // namespace Mgl