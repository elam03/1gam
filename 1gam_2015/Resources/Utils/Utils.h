#pragma once

#include <chrono>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <unordered_map>

#include <math.h>

namespace Mgl
{

static const float PI   = 3.1415f;
static const float DEG = (PI / 180.0f);
static const float RAD = (180.0f / PI);
static const int   RANDOM_NUMBER_PRECISION = 1000;

inline static void swap(float& _v1, float& _v2)
{
  float temp = _v1;
  _v1 = _v2;
  _v2 = temp;
}

inline static float clamp(float _value, float _min, float _max)
{
  if (_min > _max) swap(_min, _max);
  return (_value > _max ? (_max) : (_value < _min) ? _max : _value);
}

inline static int random(int _min, int _max)
{
  if (_max < _min) { int temp = _max; _max = _min; _min = temp; }
  return (_min + rand() % (_max - _min));
}

inline static int random(int _max)
{
  return (random(0, _max));
}

inline static float randomf(float _min, float _max)
{
  float randonNumber = float(rand() % RANDOM_NUMBER_PRECISION) / (RANDOM_NUMBER_PRECISION);
  float diff = fabs(_max - _min);

  return (_min + (diff * randonNumber));
}

inline static float randomf(float _max)
{
  return (randomf(0.0f, _max));
}

// Borrowed from bitsquid foundation's implementation
uint64_t murmur_hash_64(const void * key, uint32_t len, uint64_t seed);

template<typename T>
struct RingBuffer
{
  RingBuffer(int _size) : m_size(0), m_data(nullptr), m_write_index(0), m_read_index(0) { memset(&m_data_null, 0, sizeof(T)); resize(_size); }
  virtual ~RingBuffer()
  {
    if (m_data)
      delete[] m_data;
    
    m_size = m_write_index = m_read_index = 0;
  }

  inline int getSize() const { return ((m_write_index >= m_read_index) ? (m_write_index - m_read_index) : (m_size - (m_read_index - m_write_index))); }
  inline bool isFull() const { return ((getSize()) == (m_size - 1)); }
  inline void resize(int _new_size)
  {
    if (m_data)
    {
      T* new_data = nullptr;
      if (_new_size >= m_size)
      {
        // r     
        //[012345]
        //      w
        //
        // size = 6
        // r    = 5   => size = 2
        // w    = 1
        new_data = new T[_new_size];
        memset(new_data, 0, sizeof(T) * _new_size);

        if (m_write_index >= m_read_index)
        {
          //   r    
          //[012345]
          //     w 
          //
          // size = 6
          // r    = 2   => size = 2
          // w    = 4
          memcpy(new_data, &m_data[m_read_index], sizeof(T) * (getSize()));
        }
        else
        {
          //     r  
          //[012345]
          //  w    
          //
          // size = 6
          // r    = 4   => size = 3
          // w    = 1
          int right_size = (m_size - m_read_index);
          memcpy(new_data, &m_data[m_read_index], sizeof(T) * right_size);
          memcpy(new_data + right_size, &m_data[0], sizeof(T) * m_write_index);
        }

      }
      else
      {
        // Not sure what I should do if we are reducing the size for now...
        new_data = new T[_new_size];
        memset(new_data, 0, sizeof(T) * _new_size);
      }

      delete[] m_data;
      m_data = new_data;
      m_size = _new_size;
    }
    else
    {
      m_data = new T[_new_size];
      memset(m_data, 0, sizeof(T) * _new_size);
      m_size = _new_size;
    }
  }

  inline T& operator[](int _index) { return m_data[_index]; }
  inline const T& operator[](int _index) const { return m_data[_index]; }

  inline T& remove() { T& o = m_data[m_read_index]; m_read_index = (m_read_index + 1) % m_size; return o; }
  
  inline void insert(const T& _data) { T& o = m_data[m_write_index]; m_write_index = (m_write_index + 1) % m_size; o = _data; }

  int m_size;
  T*  m_data;
  T   m_data_null;

  int m_read_index;
  int m_write_index;
};

///////////////////////////////////////////////////////////////////////////////

class Trace
{
public:
  Trace() : m_file() {}
  Trace(const std::string& _tag) : m_file()
  {
    std::string s(_tag);
    s += ".trace";

    m_file.open(s, std::ofstream::out);
  }
  virtual ~Trace()
  {
    m_file.close();
  }

  void trace(const std::string& _trace_entry)
  {
    m_file << _trace_entry;
  }

  std::ofstream m_file;
};

class Tracer
{
public:
  Tracer() {}
  virtual ~Tracer()
  {
    for (auto& trace : m_traces)
      delete trace.second;
  }

  void trace(const std::string& _tag, const std::string& _trace_entry)
  {
    if (m_traces.find(_tag) != m_traces.end())
      m_traces[_tag]->trace(_trace_entry);
    else
      m_traces.emplace(_tag, new Trace(_tag));
  }
  
  static Tracer& Instance() { static Tracer theInstance; return theInstance; }

  std::unordered_map<std::string, Trace*> m_traces;
};

class ScopedElapsedTime
{
public:
  ScopedElapsedTime(const std::string& _tag) : m_tag(_tag)
  {
    m_start_time = std::chrono::high_resolution_clock::now();
  }

  virtual ~ScopedElapsedTime()
  {
    m_stop_time = std::chrono::high_resolution_clock::now();
    std::stringstream ss;
    ss << std::chrono::duration_cast<std::chrono::nanoseconds>(m_stop_time - m_start_time).count() << std::endl;
    Tracer::Instance().trace(m_tag, ss.str());
  }

  std::string m_tag;
  std::chrono::system_clock::time_point m_start_time;
  std::chrono::system_clock::time_point m_stop_time;
};

class FpsCounter
{
public:
  //using std::chrono::system_clock::time_point = time_point;

  FpsCounter(const std::string& _tag) : m_tag(_tag), m_counter(0) { reset(); }
  virtual ~FpsCounter() {}

  void reset()
  {
    m_counter = 0;
    m_last_time = std::chrono::high_resolution_clock::now();
  }
  
  FpsCounter& operator++() // prefix
  {
    m_counter++;
    
    m_curr_time = std::chrono::high_resolution_clock::now();

    auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(m_curr_time - m_last_time).count();
    if (ms > 1000)
    {
      std::stringstream ss;
      ss << "m_counter: " << m_counter << std::endl;
      
      Tracer::Instance().trace(m_tag, ss.str());

      m_last_time = std::chrono::high_resolution_clock::now();
      m_counter = 0;
    }
    return (*this);
  }
  
  FpsCounter operator++(int _i) // postfix
  {
    return ++(*this);
  }

private:
  std::string m_tag;
  unsigned    m_counter;
  
  std::chrono::system_clock::time_point m_last_time;
  std::chrono::system_clock::time_point m_curr_time;
};

///////////////////////////////////////////////////////////////////////////////

}