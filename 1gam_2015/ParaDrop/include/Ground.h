#pragma once

#include "cinder/gl/gl.h"
#include "cinder/gl/GlslProg.h"
#include "cinder/gl/Texture.h"
#include "cinder/gl/Vbo.h"
#include "cinder/Perlin.h"
#include "cinder/TriMesh.h"

class Ground
{
public:
  struct DataVertex
  {
    ci::Vec3f p;
    ci::Vec3f n;
    ci::Color c;

    DataVertex(ci::Vec3f& _p, ci::Vec3f& _n, ci::Color& _c) : p(_p), n(_n), c(_c) {}
  };

public:
  Ground();
  virtual ~Ground();

  void init();
  void close();
  void render();
  void update();

  void generate(int _x_max, int _y_max);

  inline void toggleWireframe() { m_wire_frame_enabled = !m_wire_frame_enabled; }
  inline void toggleNormals() { m_render_normals_enabled = !m_render_normals_enabled; }
  inline void getBounds(ci::Vec3f& b1, ci::Vec3f& b2) { b1 = m_boundaries[0]; b2 = m_boundaries[1]; }

  ci::gl::VboMeshRef      m_mesh;
  bool                    m_wire_frame_enabled;
  bool                    m_render_normals_enabled;
  ci::Vec3f               m_boundaries[2];
  ci::Perlin              m_perlin_generator;
  int                     m_num_cells_x;
  int                     m_num_cells_y;
  std::vector<DataVertex> m_data_verticies;
};