#pragma once

#include <vector>

#include "cinder/Vector.h"


class CloudSystem
{
public:
  typedef struct Cloud
  {
    unsigned  id;
    ci::Vec3f position;
    float     size;
  };

public:
  CloudSystem();
  virtual ~CloudSystem();

  void generate(const ci::Vec3f& _min, const ci::Vec3f& _max);

  void render();
  void update();

  inline float getElevation() const { return m_elevation; }

  std::vector<Cloud> m_clouds;
  float              m_elevation;
};