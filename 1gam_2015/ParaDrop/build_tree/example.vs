//uniform mat4 mvp; // this is set for the entire shader, it doesn’t change for each point
//in vec4 vertexIn; // this is set for each vertex, i.e it changes for each point
//
//void main() {
//  //v.x += 1.;
//  gl_Position = mvp * vertexIn;
//}

//void main()
//{	
//	gl_TexCoord[0] = gl_MultiTexCoord0;	
//	//gl_Position = ftransform();
//}

//varying vec2 texCoord;

void main()
{
  //texCoord = gl_MultiTexCoord0;
  gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
}
