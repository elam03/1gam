varying vec3 n;
varying vec3 v;
varying vec2 texture_coord;
uniform sampler2D tex0;

void main()
{
  vec3 L = normalize(gl_LightSource[0].position.xyz - v);   

  //calculate Ambient Term:
  //vec4 Iamb = gl_FrontLightProduct[0].ambient;
  //calculate Diffuse Term:
  vec4 Idiff = gl_FrontLightProduct[0].diffuse * max(dot(n,L), 0.0);  

  // calculate Specular Term:
  //vec4 Ispec = gl_FrontLightProduct[0].specular * pow(max(dot(R,E),0.0),0.3*gl_FrontMaterial.shininess);

  //Idiff = clamp(Idiff, 0.0, 1.0); 

  gl_FragColor = Idiff * texture2D(tex0, texture_coord);

  // write Total Color:
  //gl_FragColor = texture2D(tex0, texture_coord) + Iamb + Idiff + Ispec;
}