varying vec3 n;
varying vec3 v;
varying vec2 texture_coord;

void main()
{
    v = vec3(gl_ModelViewMatrix * gl_Vertex);
    n = normalize(gl_NormalMatrix * gl_Normal);

    gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
    texture_coord = vec2(gl_MultiTexCoord0);
}