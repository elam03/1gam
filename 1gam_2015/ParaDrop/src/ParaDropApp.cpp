#include "cinder/app/AppNative.h"
#include "cinder/gl/gl.h"
#include "cinder/gl/Fbo.h"
#include "cinder/gl/GlslProg.h"
#include "cinder/gl/Light.h"
#include "cinder/gl/Texture.h"
#include "cinder/Camera.h"
#include "cinder/Capture.h"
#include "cinder/MayaCamUI.h"

#include "CiUtils.h"
#include "CloudSystem.h"
#include "Config.h"
#include "Ground.h"

using namespace ci;
using namespace ci::app;
using namespace std;
using namespace Mgl;

class ParaDropApp : public AppNative
{
public:
  ParaDropApp();
  virtual ~ParaDropApp();

  virtual void setup();
  virtual void mouseDown(MouseEvent event);
  virtual void mouseDrag(MouseEvent event);
  virtual void keyDown(KeyEvent event);
  virtual void keyUp(KeyEvent event);
  virtual void resize();
  virtual void update();
  virtual void draw();

  //void generateTerrain();

  bool           m_initialized;
  bool           m_keys[KeyEvent::KEY_LAST];
  Mgl::Config    m_config;

  MayaCamUI      m_camera_maya_ui;
  CameraPersp    m_camera;
  Vec3f          m_camera_position;
  Vec3f          m_camera_eye;
  Vec3f          m_camera_view_dir;
  Vec3f          m_camera_strafe_dir;

  Ground         m_ground;
  //gl::VboMeshRef m_ground_mesh;
  //bool           m_ground_mesh_wire_frame_enabled;
  //Vec3f          m_ground_boundaries[2];
  //Perlin         m_perlin_generator;

  CaptureRef     m_video_capture;
  gl::TextureRef m_video_capture_texture;

  gl::Fbo        m_main_fbo;

  CloudSystem    m_clouds;

  bool           m_sunlight_enabled;
  Vec3f          m_sunlight_position;
  Vec3f          m_sunlight_dir;
  gl::Light*     m_sunlight;

  gl::GlslProgRef m_blur_shader;
  gl::GlslProgRef m_sphere_shader;
};

ParaDropApp::ParaDropApp() :
  m_initialized(false),

  m_camera_maya_ui(),
  m_camera(),
  
  m_video_capture(nullptr),
  m_video_capture_texture(nullptr),

  m_sunlight_enabled(true),
  m_sunlight(nullptr)
{
  memset(&m_keys, 0, sizeof(m_keys));
}

ParaDropApp::~ParaDropApp()
{
  m_video_capture->stop();

  if (m_sunlight)
  {
    delete m_sunlight;
    m_sunlight = nullptr;
  }
}

void ParaDropApp::setup()
{
  if (!m_config.load("config.json"))
    quit();

  /////////////////////////////////////////////////////////////////////////////
  auto devices = Capture::getDevices();

  for (auto& device : devices)
  {
    string s = device->getName();

    if (device->checkAvailable())
    {
    }
  }

  m_video_capture = Capture::create(640, 480);

  m_video_capture->start();
  /////////////////////////////////////////////////////////////////////////////
  try {
    // Vertex Shader -> [Geometry Shader] -> Fragment Shader
    //m_blur_shader = gl::GlslProg::create(loadFile("example.vs"), loadFile("example.fs"));

    m_sphere_shader = gl::GlslProg::create(loadFile("sphere.vs"), loadFile("sphere.fs"));
  } catch (const exception& e) {
    console() << e.what() << endl;

    ofstream os("shader_debug.log");
    os << e.what();
    os.close();
    quit();
  }
  /////////////////////////////////////////////////////////////////////////////
  m_ground.generate(m_config.as_int("ground.x_max", 2), m_config.as_int("ground.y_max", 2));
  /////////////////////////////////////////////////////////////////////////////

  Vec3f bounds[2];
  m_ground.getBounds(bounds[0], bounds[1]);
  m_clouds.generate(bounds[0], bounds[1]);
  /////////////////////////////////////////////////////////////////////////////

  m_sunlight = new gl::Light(gl::Light::POINT, 0);
  m_sunlight->setAmbient(Color(0.5f, 0.5f, 0.5f));
  m_sunlight->setDiffuse(Color(1.5f, 1.5f, 1.5f));
  m_sunlight->setSpecular(Color(1.0f, 1.0f, 1.0f));
  m_sunlight_position = Vec3f(0.0f, m_clouds.getElevation() / 2.0f, 0.0f);
  m_sunlight_dir = Vec3f(0.0f, -1.0f, 0.0f);
  if (m_sunlight_enabled)
  {
    m_sunlight->enable();
    m_sunlight->lookAt(m_sunlight_position, m_sunlight_dir);
  }
  else
  {
    m_sunlight->disable();
  }
  /////////////////////////////////////////////////////////////////////////////

  m_camera_position = Vec3f(0.0f, 5.0f, 100.0f);
  m_camera.setEyePoint(m_camera_position);
  m_camera.setCenterOfInterestPoint(Vec3f::zero());
  m_camera.setFarClip(5000.0f);

  m_camera_maya_ui.setCurrentCam(m_camera);
  /////////////////////////////////////////////////////////////////////////////

  m_main_fbo = gl::Fbo(getWindowWidth(), getWindowHeight(), true);
  
  /////////////////////////////////////////////////////////////////////////////

  gl::enableDepthRead();
  gl::enableDepthWrite();
  //gl::enableAlphaBlending();

  m_initialized = true;
}

void ParaDropApp::mouseDown(MouseEvent event)
{
  auto ray = m_camera.generateRay(.5f, .5f, 1.0f);

  m_camera_maya_ui.mouseDown(event.getPos());

  m_camera_view_dir = m_camera.getViewDirection();
  m_camera_view_dir.safeNormalize();
  m_camera_strafe_dir = m_camera_view_dir.cross(m_camera.getWorldUp());
  m_camera_strafe_dir.safeNormalize();
}

void ParaDropApp::mouseDrag(MouseEvent event)
{
  m_camera_maya_ui.mouseDrag(event.getPos(), event.isLeftDown(), event.isMiddleDown(), event.isRightDown());

  m_camera_view_dir = m_camera.getViewDirection();
  m_camera_view_dir.safeNormalize();
  m_camera_strafe_dir = m_camera_view_dir.cross(m_camera.getWorldUp());
  m_camera_strafe_dir.safeNormalize();
}

void ParaDropApp::keyDown(KeyEvent event)
{
  m_keys[event.getCode()] = true;
}

void ParaDropApp::keyUp(KeyEvent event)
{
  // Non-continuous keybinds
  if (m_keys[KeyEvent::KEY_ESCAPE]) quit();
  if (m_keys[KeyEvent::KEY_1]) m_ground.toggleWireframe();
  if (m_keys[KeyEvent::KEY_2]) m_ground.toggleNormals();
  if (m_keys[KeyEvent::KEY_3])
  {
    m_sunlight_enabled = !m_sunlight_enabled;
    if (m_sunlight_enabled) m_sunlight->enable(); else m_sunlight->disable();
  }

  m_keys[event.getCode()] = false;
}

void ParaDropApp::resize()
{
  m_main_fbo = gl::Fbo(getWindowWidth(), getWindowHeight(), true);
  m_camera = m_camera_maya_ui.getCamera();
  float ratio = getWindowHeight() / getWindowWidth();
  m_camera.setAspectRatio(ratio);
}

void ParaDropApp::update()
{
  static const float offset = 1.0f;
  static const float MovementSpeed = 3.0f;

  // Continuous keybinds
  if (m_keys[KeyEvent::KEY_a]) m_camera_position -= m_camera_strafe_dir * MovementSpeed;
  if (m_keys[KeyEvent::KEY_d]) m_camera_position += m_camera_strafe_dir * MovementSpeed;
  if (m_keys[KeyEvent::KEY_s]) m_camera_position -= m_camera_view_dir * MovementSpeed;
  if (m_keys[KeyEvent::KEY_w]) m_camera_position += m_camera_view_dir * MovementSpeed;  

  m_camera = m_camera_maya_ui.getCamera();
  m_camera.setEyePoint(m_camera_position);
  m_camera_maya_ui.setCurrentCam(m_camera);

  m_ground.update();
  m_clouds.update();

  /////////////////////////////////////////////////////////////////////////////
  // Update video capture texture
  if (m_video_capture->isCapturing())
  {
    auto surface = m_video_capture->getSurface();
    if (!m_video_capture_texture)
    {
      m_video_capture_texture = gl::Texture::create(surface);
    }

    m_video_capture_texture->enableAndBind();
    m_video_capture_texture->update(surface);
    m_video_capture_texture->disable();
  }
  /////////////////////////////////////////////////////////////////////////////

}

void ParaDropApp::draw()
{
  if (!m_initialized)
    return;

  // clear out the window with black
  m_main_fbo.bindFramebuffer();
  
  gl::clear(Color(0, 0, 0));

  gl::setMatrices(m_camera_maya_ui.getCamera());

  /////////////////////////////////////////////////////////////////////////////
  // Draw video capture cube
  if (m_video_capture->isCapturing() && m_video_capture_texture)
  {
    float s = 5.0f;
    gl::color(1.0f, 1.0f, 1.0f);
    m_video_capture_texture->enableAndBind();
    gl::drawCube(Vec3f(25.0f, 10.0f, 0.0f), Vec3f(s, s, s));
    m_video_capture_texture->disable();
  }

  /////////////////////////////////////////////////////////////////////////////
  // Draw the ground
  if (m_sunlight_enabled)
  {
    gl::enable(GL_LIGHTING);
    m_sunlight->lookAt(m_sunlight_position, m_sunlight_dir);
  }
  m_ground.render();
  if (m_sunlight_enabled)
  {
    gl::disable(GL_LIGHTING);
  }
  /////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////
  // Draw the clouds
  m_clouds.render();
  /////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////
  // Draw the sun
  if (m_sunlight && m_sunlight_enabled)
  {
    gl::color(1.0f, 1.0f, 0.0, 1.0f);
    gl::drawSphere(m_sunlight->getPosition(), 15.0f);
  }
  /////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////
  // Shader test
  if (m_sunlight_enabled)
  {
    gl::enable(GL_LIGHTING);
    m_sunlight->lookAt(m_sunlight_position, m_sunlight_dir);
  }
  gl::color(1.0f, 1.0f, 1.0f);
  m_video_capture_texture->enableAndBind();
    m_sphere_shader->bind();
      gl::drawSphere(Vec3f(0.0f, 0.0f, 0.0f), 15.0f, 24);
    m_sphere_shader->unbind();
  m_video_capture_texture->disable();

  if (m_sunlight_enabled)
  {
    gl::disable(GL_LIGHTING);
  }
  /////////////////////////////////////////////////////////////////////////////
  
  m_main_fbo.unbindFramebuffer();

  /////////////////////////////////////////////////////////////////////////////

  gl::setMatrices(CameraOrtho(0.0f, getWindowWidth(), 0.0f, getWindowHeight(), -1.0f, 1.0f));
  
  gl::clear(Color(0, 0, 0));

  //m_blur_shader->bind();
  //m_blur_shader->uniform("tex0", 0);
	//m_blur_shader->uniform("sampleOffset", Vec2f(1.0f, 0.0f));
  gl::color(1.0f, 1.0f, 1.0f);
  gl::draw(m_main_fbo.getTexture());
  //m_blur_shader->unbind();
  
  /////////////////////////////////////////////////////////////////////////////
}

CINDER_APP_NATIVE(ParaDropApp, RendererGl)
