#include "CloudSystem.h"

#include <cinder/gl/gl.h>

#include "Utils.h"

using namespace ci;
using namespace Mgl;

CloudSystem::CloudSystem() :
  m_elevation(200.0f)
{
}

CloudSystem::~CloudSystem()
{
}

void CloudSystem::generate(const ci::Vec3f& _min, const ci::Vec3f& _max)
{
  int count = random(10, 100);

  Vec3f min  = _min; min.y = m_elevation;
  Vec3f max  = _max; max.y = m_elevation;
  Vec3f size = max - min;
  
  for (int i = 0; i < count; ++i)
  {
    Cloud cloud;

    cloud.position = min + Vec3f(randomf(size.x), randomf(size.y), randomf(size.z));
    cloud.size = randomf(5.0f, 15.0f);

    m_clouds.push_back(cloud);
  }
}

void CloudSystem::render()
{
  glPointSize(5.0f);
  gl::begin(GL_POINTS);
    gl::color(1.0f, 1.0f, 1.0f);
      for (auto& cloud : m_clouds)
        gl::vertex(cloud.position);
  gl::end();
}

void CloudSystem::update()
{
}