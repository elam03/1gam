#include "Ground.h"

#include "Utils.h"

using namespace std;
using namespace ci;
using namespace Mgl;

Ground::Ground() :
  m_mesh(nullptr),
  m_wire_frame_enabled(false),
  m_render_normals_enabled(false),
  m_perlin_generator()
{
}

Ground::~Ground()
{
  close();
}

void Ground::init()
{
}

void Ground::close()
{
}

void Ground::render()
{
  if (m_mesh)
  {
    gl::color(1.0f, 1.0f, 1.0f);
    if (m_wire_frame_enabled) gl::enableWireframe();
    gl::draw(m_mesh);
    if (m_wire_frame_enabled) gl::disableWireframe();

    if (m_render_normals_enabled)
    {
      static const float length = 5.0f;
      gl::color(1.0f, 1.0f, 1.0f);
      for (auto& v : m_data_verticies)
      {
        gl::drawVector(v.p, v.p + v.n * length, 2.0f, .5f);
      }
    }
  }
}

void Ground::update()
{
}

void Ground::generate(int _x_max, int _y_max)
{
  TriMesh          mesh;
  vector<Vec3f>    vertices;
  vector<uint32_t> indices;
  vector<ColorA>   colors;

  m_data_verticies.clear();

  /////////////////////////////////////////////////////////////////////////////
  static bool simple_generation = false;

  if (simple_generation)
  {
    static const float s = 30.0f;

    vertices.push_back(Vec3f(-s, 0.0f, -s));
    vertices.push_back(Vec3f(-s, 0.0f,  s));
    vertices.push_back(Vec3f( s, 0.0f,  s));
    vertices.push_back(Vec3f( s, 0.0f, -s));

    indices.push_back(0); indices.push_back(1); indices.push_back(2);
    indices.push_back(0); indices.push_back(2); indices.push_back(3);
  }
  else
  {
    static const float cell_size = 20.0f;
    m_num_cells_x = _x_max;
    m_num_cells_y = _y_max;

    Vec3f start = Vec3f(-(cell_size * _x_max) / 2, 0.0f, -(cell_size * _y_max) / 2);
    Vec3f end   = Vec3f((cell_size * _x_max) / 2, 0.0f, (cell_size * _y_max) / 2);

    float elevation_min =  999999.0f;
    float elevation_max = -999999.0f;

    for (int y = 0; y <= _y_max; ++y)
    {
      for (int x = 0; x <= _x_max; ++x)
      {
        float elevation = 0.0f;

        elevation = -50.0f + 1000.0f * m_perlin_generator.fBm(static_cast<float>(x * 0.005f), static_cast<float>(y * 0.005f), .1f);

        if (elevation < elevation_min) elevation_min = elevation;
        if (elevation > elevation_max) elevation_max = elevation;

        Vec3f p(start + Vec3f(cell_size * x, elevation, cell_size * y));
        Vec3f n(0.0f, 1.0f, 0.0f);
        Color c(1.0f, 1.0f, 1.0f);
        vertices.push_back(p);
      }
    }

    start.y = elevation_min;
    end.y   = elevation_max;

    m_boundaries[0] = start;
    m_boundaries[1] = end;

    uint32_t vertex_width = _x_max + 1;
    uint32_t vertex_height = _y_max + 1;
    
    for (uint32_t y = 0; y < static_cast<uint32_t>(_y_max); ++y)
    {
      for (uint32_t x = 0; x < static_cast<uint32_t>(_x_max); ++x)
      {
        uint32_t i = x + y * vertex_width;

        indices.push_back(i + 0); indices.push_back(i + vertex_width); indices.push_back(i + vertex_width + 1);
        indices.push_back(i + 0); indices.push_back(i + vertex_width + 1); indices.push_back(i + 1);
      }
    }
  }

  /////////////////////////////////////////////////////////////////////////////

  for (int i = 0; i < vertices.size(); ++i)
    colors.push_back(ColorA(randomf(0.25f, 0.75f), randomf(0.25f, 0.75f), randomf(0.25f, 0.75f), 1.0f));

  if (vertices.size() > 0)
  {
    mesh.appendVertices(&vertices[0], vertices.size());
    mesh.appendIndices(&indices[0], indices.size());
    mesh.appendColorsRgba(&colors[0], colors.size());
    mesh.recalculateNormals();

    auto mesh_verticies = mesh.getVertices();
    auto mesh_normals = mesh.getNormals();
    auto mesh_colors = mesh.getColorsRGBA();

    if (mesh_verticies.size() == mesh_normals.size() && mesh_verticies.size() == mesh_colors.size())
    {
      for (int i = 0; i < mesh_verticies.size(); ++i)
      {
        Vec3f p = mesh_verticies[i];
        Vec3f n = mesh_normals[i];
        Color c = mesh_colors[i];

        m_data_verticies.push_back(DataVertex(p, n, c));
      }
    }

    gl::VboMesh::Layout layout;
    layout.setStaticPositions();
    layout.setStaticIndices();
    layout.setStaticNormals();
    layout.setStaticColorsRGBA();
    m_mesh = gl::VboMesh::create(mesh, layout);
  }
}