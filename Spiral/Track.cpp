#include "Track.h"

#include "MglGl.h"
#include <vector>

using namespace foundation;
using namespace std;
using namespace Mgl;

Track::Track() :
  mTrackPoints(memory_globals::default_allocator()),
  mTrackSegments(memory_globals::default_allocator()),
  mWidth(1.0f),
  mTrackVaoId(0)
{
}

Track::~Track()
{
}

void Track::init()
{
  unsigned count = random(35, 45);
  
  array::clear(mTrackPoints);
  array::clear(mTrackSegments);

  //generateSimpleTrackPoints(count);
  generateSpiralTrackPoints(count);

  // Connect the points together...
  mTotalLength = 0.0f;
  for (unsigned i = 0; i < count; i++)
  {
    Vector3 p1 = mTrackPoints[(i % count)].vertex;
    Vector3 p2 = mTrackPoints[(i + 1) % count].vertex;
    TrackSegment segment;
    
    segment.points[0] = p1;
    segment.points[1] = p2;
    
    float segmentLength = magnitudeOf(p1 - p2);
    segment.length = segmentLength;
    segment.normalizedLength = segmentLength;
    segment.boostSection = (rand() % 4 == 0) ? true : false;

    mTotalLength += segmentLength;

    array::push_back(mTrackSegments, segment);
  }

  for (unsigned i = 0; i < array::size(mTrackSegments); i++)
  {
    mTrackSegments[i].normalizedLength = mTrackSegments[i].normalizedLength / mTotalLength;
  }

  if (mTrackVaoId == 0)
  {
    glGenBuffers(1, &mTrackVaoId);
    glGenBuffers(2, &mTrackVboId[0]);
  }

  if (mTrackVaoId > 0 && mTrackVboId[0] > 0 && mTrackVboId[1] > 0)
  {
    // Vertices
    glBindBuffer(GL_ARRAY_BUFFER, mTrackVboId[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Point) * array::size(mTrackPoints), &mTrackPoints[0], GL_STATIC_DRAW);

    // Colors
    glBindBuffer(GL_ARRAY_BUFFER, mTrackVboId[1]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Point) * array::size(mTrackPoints), &mTrackPoints[0], GL_STATIC_DRAW);
    
    glBindVertexArray(mTrackVaoId);
      glEnableVertexAttribArray(0);
      glBindBuffer(GL_ARRAY_BUFFER, mTrackVboId[0]);
      glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Point), NULL);
      glEnableVertexAttribArray(1);
      glBindBuffer(GL_ARRAY_BUFFER, mTrackVboId[1]);
      glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Point), NULL);
    
    glBindVertexArray(0);
  }
}

void Track::close()
{
  if (mTrackVaoId > 0)
  {
    glDeleteBuffers(1, &mTrackVaoId);
    glDeleteBuffers(2, &mTrackVboId[0]);
    mTrackVaoId = 0;
    mTrackVboId[0] = mTrackVboId[1] = 0;
  }
}

void Track::render()
{
  float oldLineWidth = 1.0f;
  glGetFloatv(GL_LINE_WIDTH, &oldLineWidth);

  /////////////////////////////////////////////////////////////////////////////
  // Draw the already uploaded vertex data
  glPushMatrix();
    glLineWidth(3.0f);
    
    glColor3f(0.0f, 0.0f, 1.0f);

    glBindVertexArray(mTrackVaoId);
      glEnableVertexAttribArray(0);
      glEnableVertexAttribArray(1);
        glDrawArrays(GL_LINE_LOOP, 0, array::size(mTrackPoints));
      glDisableVertexAttribArray(0);
      glDisableVertexAttribArray(1);
    glBindVertexArray(0);
  glPopMatrix();
  /////////////////////////////////////////////////////////////////////////////
  
  //// Draw the track without using the VAO, which means it's uploading vertex
  //// every time the track is rendered.
  //glColor3f(0.0f, 1.0f, 0.0f);
  //glLineWidth(3.0f);
  //glBegin(GL_LINES);
  //  for (unsigned i = 0; i < array::size(mTrackSegments); i++)
  //  {
  //    if (mTrackSegments[i].boostSection) glColor3f(0.0f, 1.0f, 0.0f);
  //    else                                glColor3f(1.0f, 0.0f, 0.0f);
  //    glVertex3f(mTrackSegments[i].points[0]);
  //    glVertex3f(mTrackSegments[i].points[1]);
  //  }
  //glEnd();

  glLineWidth(oldLineWidth);
}

Vector3 Track::getPosition(float _normalizedPos)
{
  Vector3 p;

  if (_normalizedPos >= 0.0f && _normalizedPos <= 1.0f)
  {
    float currLength = 0.0f;

    for (unsigned i = 0; i < array::size(mTrackSegments); i++)
    {
      if (_normalizedPos >= (currLength + mTrackSegments[i].normalizedLength))
      {
        currLength += mTrackSegments[i].normalizedLength;
      }
      else
      {
        float t = (_normalizedPos - currLength) / mTrackSegments[i].normalizedLength;

        p = mTrackSegments[i].points[0] + (mTrackSegments[i].points[1] - mTrackSegments[i].points[0]) * t;
        break;
      }
    }
  }

  return (p);
}

Vector3 Track::getCentralPosition()
{
  Vector3 p;

  reset(p);
  
  unsigned numTrackPoints = array::size(mTrackPoints);
  for (unsigned i = 0; i < numTrackPoints; i++)
  {
    p = p + mTrackPoints[i].vertex;
  }

  p = p / (float)numTrackPoints;

  return (p);
}

void Track::expandLine(const Vector3& _v1, const Vector3& _v2, float _expandLength, vector<Vector3>& _results)
{
  float dx = _v1.x - _v2.x;
  float dz = _v1.z - _v2.z;

  float angle = atan2(dz, dx);

  for (unsigned i = 0; i < 4; i++)
  {
    Vector3 offset;

    offset.x = cos(angle + PI / 2.0f) * _expandLength;
    offset.y = 0.0f;
    offset.z = sin(angle + PI / 2.0f) * _expandLength;

    _results.push_back(_v1 + offset);
    _results.push_back(_v1 - offset);

    _results.push_back(_v2 - offset);
    _results.push_back(_v2 + offset);
  }
}

void Track::generateSimpleTrackPoints(unsigned _count)
{
  Vector3 currPos;
  float currAngle = 0.0f;
  //currPos.x = randomf(3.0f, 8.0f);
  currPos.x = 0.0f;
  currPos.y = 0.1f;
  currPos.z = 0.0f;

  for (unsigned i = 0; i < _count; i++)
  {
    //float dist = 3.0f;
    //currAngle += 15.0f;

    float dist = randomf(3.0f, 10.0f);
    currAngle += randomf(-15.0f, 45.0f);

    currPos.x += cos(currAngle * DEG) * dist;
    currPos.y += 0.2f;
    currPos.z += sin(currAngle * DEG) * dist;
    
    Point point;
    point.vertex = currPos;
    point.color.x = (0.0f, 1.0f);
    point.color.y = (0.0f, 1.0f);
    point.color.z = (0.0f, 1.0f);

    array::push_back(mTrackPoints, point);
    debugPrint(0, "(%.3f,%.3f,%.3f)", currPos.x, currPos.y, currPos.z);
  }
}

void Track::generateSpiralTrackPoints(unsigned _count)
{
  // Generate random track points.
  float currElevation = 1.5f;
  float currAngle = 0.0f;
  float currRadius = 8.0f;
  for (unsigned i = 0; i < _count; i++)
  {
    Vector3 p;

    currElevation += .3f;
    currAngle += 45.0f;
    currRadius += 0.0f;

    p.x = cos(currAngle * DEG) * currRadius;
    p.y = currElevation;
    p.z = sin(currAngle * DEG) * currRadius;

    Point point;
    point.vertex = p;
    point.color.x = (0.0f, 1.0f);
    point.color.y = (0.0f, 1.0f);
    point.color.z = (0.0f, 1.0f);
    
    array::push_back(mTrackPoints, point);
    debugPrint(0, "(%.3f,%.3f,%.3f)", p.x, p.y, p.z);
  }
}