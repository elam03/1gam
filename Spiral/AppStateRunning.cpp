#include "AppStateRunning.h"

#include "SpiralApplication.h"

#include "MglUtils.h"
#include "MglTypes.h"

#include "MglGl.h"

using namespace foundation;
using namespace std;
using namespace Mgl;

void glCube2(float _x, float _y, float _z)
{
  static const GLfloat g_vertex_buffer_data[] = {
    -1.0f,-1.0f,-1.0f, // triangle 1 : begin
    -1.0f,-1.0f, 1.0f,
    -1.0f, 1.0f, 1.0f, // triangle 1 : end
    1.0f, 1.0f,-1.0f, // triangle 2 : begin
    -1.0f,-1.0f,-1.0f,
    -1.0f, 1.0f,-1.0f, // triangle 2 : end
    1.0f,-1.0f, 1.0f,
    -1.0f,-1.0f,-1.0f,
    1.0f,-1.0f,-1.0f,
    1.0f, 1.0f,-1.0f,
    1.0f,-1.0f,-1.0f,
    -1.0f,-1.0f,-1.0f,
    -1.0f,-1.0f,-1.0f,
    -1.0f, 1.0f, 1.0f,
    -1.0f, 1.0f,-1.0f,
    1.0f,-1.0f, 1.0f,
    -1.0f,-1.0f, 1.0f,
    -1.0f,-1.0f,-1.0f,
    -1.0f, 1.0f, 1.0f,
    -1.0f,-1.0f, 1.0f,
    1.0f,-1.0f, 1.0f,
    1.0f, 1.0f, 1.0f,
    1.0f,-1.0f,-1.0f,
    1.0f, 1.0f,-1.0f,
    1.0f,-1.0f,-1.0f,
    1.0f, 1.0f, 1.0f,
    1.0f,-1.0f, 1.0f,
    1.0f, 1.0f, 1.0f,
    1.0f, 1.0f,-1.0f,
    -1.0f, 1.0f,-1.0f,
    1.0f, 1.0f, 1.0f,
    -1.0f, 1.0f,-1.0f,
    -1.0f, 1.0f, 1.0f,
    1.0f, 1.0f, 1.0f,
    -1.0f, 1.0f, 1.0f,
    1.0f,-1.0f, 1.0f};

  glEnableClientState(GL_VERTEX_ARRAY);

  glVertexPointer(12*3, GL_FLOAT, 0, g_vertex_buffer_data);

  glColor3f(1.0f, 1.0f, 1.0f);
  //glDrawArrays(GL_TRIANGLES, 0, 12*3); // 12*3 indices starting at 0 -> 12 triangles -> 6 squares
  glDrawArrays(GL_TRIANGLES, 0, 12); // 12*3 indices starting at 0 -> 12 triangles -> 6 squares
  
  glDisableClientState(GL_VERTEX_ARRAY);

  //// This will identify our vertex buffer
  //GLuint vertexbuffer;

  //// Generate 1 buffer, put the resulting identifier in vertexbuffer
  //glGenBuffers(1, &vertexbuffer);
 
  //// The following commands will talk about our 'vertexbuffer' buffer
  //glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
 
  //// Give our vertices to OpenGL.
  //glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data), g_vertex_buffer_data, GL_STATIC_DRAW);

}

Camera::Camera()
{
  reset();
}

Camera::~Camera()
{
}

void Camera::reset()
{
  mPos.x = 0.0f;
  mPos.y = 0.0f;
  mPos.z = 100.0f;
  
  mDir.x = 0.0f;
  mDir.y = 0.0f;
  mDir.z = -1.0f;

  mUp.x = 0.0f;
  mUp.y = 1.0f;
  mUp.z = 0.0f;

  mAngleYaw = 90.0f;
  mAnglePitch = 0.0f;
}

void Camera::evaluate()
{
  mDir.x = cos(mAngleYaw) * cos(mAnglePitch);
  mDir.y = sin(mAnglePitch);
  mDir.z = -sin(mAngleYaw) * cos(mAnglePitch);
}

AppStateRunning::AppStateRunning() : 
  AppState(),
  mTrack()
{
  for (unsigned i = 0; i < MAX_LIGHTS; i++)
  {
    mLights[i].mLight.mIndex = i;
  }
}

AppStateRunning::~AppStateRunning()
{
}

const float gSize = 10.0f;

bool AppStateRunning::handleEvent(int _event, void* _eventData)
{
  switch (_event)
  {
    case EVENT_INIT:
    {
      debugPrint(0, "EVENT_INIT");
  
      //glEnable(GL_DEPTH_TEST);
      //glShadeModel(GL_SMOOTH);
      //glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);  // Nice perspective corrections

      glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // Set background color to black and opaque
      glClearDepth(1.0f);                   // Set background depth to farthest
      glEnable(GL_DEPTH_TEST);   // Enable depth testing for z-culling
      glDepthFunc(GL_LEQUAL);    // Set the type of depth-test
      glShadeModel(GL_SMOOTH);   // Enable smooth shading
      glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);  // Nice perspective corrections

      glEnable(GL_LIGHTING);
      glEnable(GL_COLOR_MATERIAL);

      setupCamera();
      
      mTrack.init();

      mCamera.reset();
      mCamera.mPos.x = 0.0f;
      mCamera.mPos.y = 8.0f;
      mCamera.mPos.z = 50.0f;
      mCamera.evaluate();
      mCameraDist = 18.0f;

      mPlayer.mPositionNormalized = 0.0f;
      mPlayer.mPosition = mTrack.mTrackPoints[0].vertex;
      mPlayer.mSpeed = 0.0f;
      mPlayer.mAccel = 0.05f;

      mNumLights = 6;
      for (unsigned i = 0; i < mNumLights; i++)
      {
        resetLight(mLights[i]);
      }

      mGameStarted = false;
      mLastUpdateTime = 0.0;
      mUpdateRate = .02; // 40.0 / 1000.0

      pushMessage(L"Press Spacebar to begin!", 10.0f);
    } break;
    
    case EVENT_SHOW:
    {
      debugPrint(0, "EVENT_SHOW");
      
      setupCamera();
    } break;

    case EVENT_CLOSE:
    {
      debugPrint(0, "EVENT_CLOSE");
      
      mTrack.close();
    } break;

    case EVENT_WINDOW_REFRESH:
    {
      debugPrint(0, "EVENT_WINDOW_REFRESH");

      setupCamera();
    } break;

    case EVENT_MOUSE_INPUT:
    {
      EventDataMouseInput& eventData = *(EventDataMouseInput *)_eventData;

      static float prevX = -1.0f;
      static float prevY = -1.0f;

      if (eventData.down)
      {
        float x = float(eventData.x) / mApplication->mWindowWidth;
        float y = float(eventData.y) / mApplication->mWindowHeight;

        static float initialYawAngle = 0.0f;
        static float initialPitchAngle = 0.0f;

        float yawDelta   = 0.0f;
        float pitchDelta = 0.0f;

        if (eventData.initialDown)
        {
          prevX = x;
          prevY = y;

          if (prevX == -1.0f && prevY == -1.0f)
          {
            prevX = x;
            prevY = y;
          }

          initialYawAngle   = mCamera.mAngleYaw;
          initialPitchAngle = mCamera.mAnglePitch;
          
          yawDelta   = x - prevX;
          pitchDelta = y - prevY;

          //debugPrint(0, "initial (%.2f,%.2f) (%.2f,%.2f)", x, y, yawDelta, pitchDelta);
        }
        else
        {
          yawDelta   = x - prevX;
          pitchDelta = y - prevY;
        }

        if (eventData.button & MOUSE_BUTTON1)
        {
          //debugPrint(0, "(%.2f,%.2f) (%.2f,%.2f)", x, y, yawDelta, pitchDelta);

          mCamera.mAngleYaw   = initialYawAngle + (yawDelta);
          mCamera.mAnglePitch = initialPitchAngle + (pitchDelta);

          mCamera.evaluate();
        }
      }
      else
      {
        prevX = prevY = -1.0f;
      }
    } break;
  }

  return false;
}

void AppStateRunning::render()
{
  double w = (double)mApplication->mWindowWidth;
  double h = (double)mApplication->mWindowHeight;
  glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0.0f, w, 0.0f, h, -1.0f, 1.0f);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  mApplication->renderBorder();
  renderMessage();


  setupCamera();
  
  glLoadIdentity();
  gluLookAt(mCamera.mPos.x, mCamera.mPos.y, mCamera.mPos.z,
            mCamera.mPos.x + mCamera.mDir.x, mCamera.mPos.y + mCamera.mDir.y, mCamera.mPos.z + mCamera.mDir.z,
            mCamera.mUp.x, mCamera.mUp.y, mCamera.mUp.z);

  glDisable(GL_LIGHTING);
  renderLights();
  mTrack.render();

  glEnable(GL_LIGHTING);
  applyLights();

  renderPlayer();


  drawStuff();
}

void AppStateRunning::update(double _deltaTime)
{
  static const float strafeSpeed = 1.0f;

  bool cameraMoved = false;

  mLastUpdateTime += _deltaTime;

  if (mLastUpdateTime >= mUpdateRate)
  {
    double updateTime = mLastUpdateTime;

    mLastUpdateTime -= mUpdateRate;

    updatePlayer(updateTime);

    updateLights(updateTime);

    updateCamera(updateTime);

    updatePlayerIsBoostable();

    updateMessage(updateTime);

    ///////////////////////////////////////////////////////////////////////////

    if (mApplication->isKeyPressed('W'))
    {
      mCamera.mPos = mCamera.mPos + mCamera.mDir * 5.0f * float(updateTime);
      cameraMoved = true;
    }
    if (mApplication->isKeyPressed('S'))
    {
      mCamera.mPos = mCamera.mPos - mCamera.mDir * 5.0f * float(updateTime);
      cameraMoved = true;
    }
    if (mApplication->isKeyPressed('A'))
    {
      strafe(strafeSpeed);
      cameraMoved = true;
    }
    if (mApplication->isKeyPressed('D'))
    {
      strafe(-strafeSpeed);
      cameraMoved = true;
    }
    if (mApplication->isKeyPressed(' '))
    {
      if (!mGameStarted)
        pushMessage(L"Press spacebar to boost when a light is nearby!", 1.0f);

      mGameStarted = true;

      mCamera.mPos.x = mCameraDist;
      mCamera.mPos.y = mCameraDist;
      mCamera.mPos.z = 0.0f;
    }

    if (mApplication->isKeyPressed('3'))
    {
      mCameraDist += 10.0f * float(updateTime);

      mCamera.mPos.x = mCameraDist;
      mCamera.mPos.y = mCameraDist;
      mCamera.mPos.z = 0.0f;
    }
    if (mApplication->isKeyPressed('4'))
    {
      mCameraDist -= 10.0f * float(updateTime);

      mCamera.mPos.x = mCameraDist;
      mCamera.mPos.y = mCameraDist;
      mCamera.mPos.z = 0.0f;
    }

    if (cameraMoved)
    {
      //debugPrint(0, "pos: (%.3f,%.3f,%.3f) dir: (%.3f,%.3f,%.3f)", mCamera.mPos.x, mCamera.mPos.y, mCamera.mPos.z, mCamera.mDir.x, mCamera.mDir.y, mCamera.mDir.z);
      mCamera.evaluate();
    }
  }
}

void AppStateRunning::setupCamera()
{
  float w = (float)mApplication->mWindowWidth;
  float h = (float)mApplication->mWindowHeight;
  float ratio = (w / h);
  
  glViewport(0, 0, (int)w, (int)h);
  
  glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45.0f, ratio, 0.1f, 100.0f);
  glMatrixMode(GL_MODELVIEW);
}

void AppStateRunning::drawStuff()
{
  glPushMatrix();
    glTranslatef(1.5f, 0.0f, -9.0f);
    glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
    
    glColor3f(0.0f, 1.0f, 1.0f);
    glutSolidCone(1.0, 2.0, 10, 10);
  glPopMatrix();

  glPushMatrix();
    glTranslatef(-1.5f, 0.0f, -8.0f);
    glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
    
    glColor3f(0.0f, 1.0f, 0.0f);
    glutSolidCone(1.0, 1.0, 10, 10);
  glPopMatrix();
}

void AppStateRunning::strafe(float _dir)
{
  float viewX = mCamera.mDir.x - mCamera.mPos.x;
  float viewY = mCamera.mDir.y - mCamera.mPos.y;
  float viewZ = mCamera.mDir.z - mCamera.mPos.z;

  float x = ((viewY * mCamera.mUp.z) - (viewZ * mCamera.mUp.y));
  float y = ((viewZ * mCamera.mUp.x) - (viewX * mCamera.mUp.z));
  float z = ((viewX * mCamera.mUp.y) - (viewY * mCamera.mUp.x));

  float magnitude = sqrt((x * x) + (y * y) + (z * z));

  x /= magnitude * _dir;
  y /= magnitude * _dir;
  z /= magnitude * _dir;

  mCamera.mPos.x -= x;
  mCamera.mPos.y -= y;
  mCamera.mPos.z -= z;

  mCamera.mDir.x -= x;
  mCamera.mDir.y -= y;
  mCamera.mDir.z -= z;
}

void AppStateRunning::pushMessage(wstring _message, float _messageDisplayTime)
{
  mMessage = _message;
  mMessageLastTime = Mgl::getTickTime();
  mMessageDueTime = mMessageLastTime + _messageDisplayTime;
}

void AppStateRunning::renderMessage()
{
  if (mMessage.length() > 0)
  {
    glPushMatrix();
      glRasterPos2f(50.0f, 50.0f);
      mApplication->printText(toString(mMessage).c_str());
    glPopMatrix();
  }
}

void AppStateRunning::updateMessage(double _deltaTime)
{
  if (mMessage.length() > 0)
  {
    mMessageLastTime += _deltaTime;
    //debugPrint(0, "%f %f", mMessageLastTime, mMessageDueTime);

    if (mMessageLastTime >= mMessageDueTime)
    {
      mMessage = L"";
    }
  }
}

void AppStateRunning::updateCamera(double _deltaTime)
{
  if (mGameStarted)
  {
    // Focus on the Player
    Vector3 dir = mPlayer.mPosition - mCamera.mPos;
      
    mCamera.mDir = normalize(dir);

    //// Focus on the center of the track
    //mCamera.mPos.x = 0.0f;
    //mCamera.mPos.y = 15.0f;
    //mCamera.mPos.z = mCameraDist;

    //Vector3 dir = mTrack.getCentralPosition() - mCamera.mPos;
    //  
    //mCamera.mDir = normalize(dir);
  }
}

void AppStateRunning::renderPlayer()
{
  const float PlayerScale = 1.0f;
  glPushMatrix();
    glColor3f(0.0f, 1.0f, 0.0f);
    glTranslatef(mPlayer.mPosition.x, mPlayer.mPosition.y, mPlayer.mPosition.z);
    glScalef(PlayerScale, PlayerScale, PlayerScale);
    glutSolidSphere(1.0, 20, 20);
  glPopMatrix();
}

void AppStateRunning::updatePlayer(double _deltaTime)
{
  if (mGameStarted)
  {
    mPlayer.mSpeed += mPlayer.mAccel * float(_deltaTime);
  }
  mPlayer.mPositionNormalized += (float)_deltaTime * mPlayer.mSpeed;
  mPlayer.mSpeed *= .98f;

  if (mPlayer.mPositionNormalized >= 1.0f)
    mPlayer.mPositionNormalized -= 1.0f;

  mPlayer.mPosition = mTrack.getPosition(mPlayer.mPositionNormalized);
}

void AppStateRunning::applyLights()
{
  for (unsigned i = 0; i < mNumLights; i++)
  {
    mLights[i].mLight.apply();
  }
}

void AppStateRunning::updateLights(double _deltaTime)
{
  for (unsigned i = 0; i < mNumLights; i++)
  {
    mLights[i].mPositionNormalized += float(mLights[i].mSpeed * _deltaTime);
    if (mLights[i].mPositionNormalized < 0.0f)
      mLights[i].mPositionNormalized += 1.0f;
    if (mLights[i].mPositionNormalized > 1.0f)
      mLights[i].mPositionNormalized -= 1.0f;

    mLights[i].mPosition = mTrack.getPosition(mLights[i].mPositionNormalized);
    mLights[i].mLight.mPosition = mLights[i].mPosition;
  }
}

void AppStateRunning::resetLight(LightEntity& _lightEntity)
{
  _lightEntity.mPositionNormalized = randomf(0.0f, 1.0f);
  _lightEntity.mPosition = mTrack.mTrackPoints[0].vertex;
  _lightEntity.mSpeed = randomf(0.15f, 0.25f);
  _lightEntity.mAccel = 0.0f;
  _lightEntity.mLight.mAmbient[0] = _lightEntity.mLight.mAmbient[1] = _lightEntity.mLight.mAmbient[2] = .05f;
  _lightEntity.mLight.mDiffuse[0] = _lightEntity.mLight.mDiffuse[1] = _lightEntity.mLight.mDiffuse[2] = .15f;
  _lightEntity.mLight.mSpecular[0] = _lightEntity.mLight.mSpecular[1] = _lightEntity.mLight.mSpecular[2] = .05f;
}

void AppStateRunning::renderLights()
{
  static const float LightSize = .5f;

  for (unsigned i = 0; i < mNumLights; i++)
  {
    glPushMatrix();
      glColor3f(1.0f, 1.0f, 1.0f);
      glTranslatef(mLights[i].mPosition);
      glScalef(LightSize, LightSize, LightSize);
      glutSolidSphere(1.0, 20, 20);
    glPopMatrix();
  }
}

void AppStateRunning::updatePlayerIsBoostable()
{
  // 'Perfect' 'Good' 'Okay' 'Bad'
  static const float grade[4] = { .9f, .8f, .7f, .6f };
  static const float closeness = 8.0f;

  if (mGameStarted)
  {
    if (mApplication->isKeyPressed('B') || mApplication->isKeyPressed(' '))
    {
      for (unsigned i = 0; i < mNumLights; i++)
      {
        Vector3 dist = mLights[i].mPosition - mPlayer.mPosition;
        float result = magnitudeSquaredOf(dist);

        if (result < closeness)
        {
          float goodness = 1.0f - (result / closeness);
          debugPrint(0, "result: %f closeness: %f goodness: %f", result, closeness, goodness);

          if (goodness > grade[0])      pushMessage(L"Perfect", 3.0f);
          else if (goodness > grade[1]) pushMessage(L"Good", 3.0f);
          else if (goodness > grade[2]) pushMessage(L"Okay", 3.0f);
          else if (goodness > grade[3]) pushMessage(L"Bad", 3.0f);

          mPlayer.mSpeed += (.15f * goodness);
          resetLight(mLights[i]);
          break;
        }
      }
    }
  }
}