#pragma once

#include "MglApplication.h"

class SpiralApplication : public MglApplication
{
public:
  SpiralApplication();
  virtual ~SpiralApplication();
  
protected:

  virtual bool appInit();
  virtual void appClose();
  virtual void appWindowRefresh();

};