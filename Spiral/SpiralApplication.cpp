#include "SpiralApplication.h"

#include "AppStateRunning.h"

using namespace foundation;
using namespace Mgl;

SpiralApplication::SpiralApplication() : 
  MglApplication()
{
}

SpiralApplication::~SpiralApplication()
{
}

bool SpiralApplication::appInit()
{
  addAppState(new AppStateRunning);

  return (true);
}

void SpiralApplication::appClose()
{
  MglApplication::close();
}

void SpiralApplication::appWindowRefresh()
{
  mAppStates[mAppState]->handleEvent(EVENT_WINDOW_REFRESH);
}
