#pragma once

#include "MglUtils.h"

#include "array.h"
#include <vector>

struct TrackSegment
{
  float normalizedLength;
  float length;
  bool boostSection;
  foundation::Vector3 points[2];
};

struct Point
{
  foundation::Vector3 vertex;
  foundation::Vector3 color;
  foundation::Vector3 normal;
};

class Track
{
public:
  Track();
  virtual ~Track();

  void init();
  void close();
  void render();

  foundation::Vector3 getPosition(float _normalizedPos);
  foundation::Vector3 getCentralPosition();
  
  bool getPosition(float _normalizedPos, foundation::Vector3& _pos, bool& _isBoostPos);

public:
  foundation::Array<Point>        mTrackPoints;
  foundation::Array<TrackSegment> mTrackSegments;

  float mWidth;
  float mTotalLength;

protected:
  void expandLine(const foundation::Vector3& _v1, const foundation::Vector3& _v2, float _expandLength, std::vector<foundation::Vector3>& _results);

  void generateSimpleTrackPoints(unsigned _count);
  void generateSpiralTrackPoints(unsigned _count);

  // VAO = { VBO, VBO, VBO, ...}
  unsigned mTrackVaoId;
  unsigned mTrackVboId[2];
};