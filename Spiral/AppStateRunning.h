#pragma once

#include "MglAppState.h"

#include "MglUtils.h"
#include "Track.h"


class Camera
{
public:
  Camera();
  virtual ~Camera();

  void reset();
  void evaluate();
  
  foundation::Vector3 mPos;
  foundation::Vector3 mDir;
  foundation::Vector3 mUp;

  float mAngleYaw;
  float mAnglePitch;
};

class Entity
{
public:
  Entity()
  {
    mPosition.x = mPosition.y = mPosition.z = 0.0f;
    mPositionNormalized = 0.0f;
    mSpeed = 0.0f;
  }
  virtual ~Entity() {}

  foundation::Vector3 mPosition;
  float mPositionNormalized;
  float mSpeed;
  float mAccel;
};

class LightEntity : public Entity
{
public:
  LightEntity() {}
  virtual ~LightEntity() {}

  Mgl::Light mLight;
};

class AppStateRunning : public AppState
{
public:
  AppStateRunning();
  virtual ~AppStateRunning();

  virtual bool handleEvent(int _event, void* _eventData = 0);
  virtual void render();
  virtual void update(double _deltaTime);

  void setupCamera();
protected:
  void drawStuff();
  void strafe(float _dir);

  // Global game data
  bool   mGameStarted;
  double mLastUpdateTime;
  double mUpdateRate;

  // Message
  std::wstring mMessage;
  double       mMessageDueTime;
  double       mMessageLastTime;
  void pushMessage(std::wstring _message, float _messageDisplayTime);
  void renderMessage();
  void updateMessage(double _deltaTime);

  // Track
  Track mTrack;

  // Camera
  Camera mCamera;
  float mCameraDist;
  void updateCamera(double _deltaTime);
  
  // Player
  Entity mPlayer;
  void renderPlayer();
  void updatePlayer(double _deltaTime);
  
  // Lights
  static const unsigned MAX_LIGHTS = 128;
  unsigned mNumLights;
  LightEntity mLights[MAX_LIGHTS];
  void applyLights();
  void renderLights();
  void updateLights(double _deltaTime);
  void resetLight(LightEntity& _lightEntity);

  void updatePlayerIsBoostable();
};