#include "BulletSystem.h"

#include "MglGl.h"
#include "MglResourceManager.h"
#include "MglUtils.h"

#include "memory.h"

using namespace foundation;
using namespace std;
using namespace Mgl;

BulletSystem::BulletSystem() : 
  mSystem()
{
}

BulletSystem::~BulletSystem()
{
}

bool BulletSystem::init()
{
  bool success = EventSystem::init();
  
  mSystem.removeAll();

  return (success);
}

void BulletSystem::close()
{
  EventSystem::close();

  mSystem.removeAll();
}

ID BulletSystem::add(Vector2 _position, Vector2 _velocity, int _type)
{
  ID id = mSystem.add();

  if (mSystem.has(id))
  {
    Bullet& bullet = mSystem.lookup(id);
    memset(&bullet, 0, sizeof(Bullet));

    bullet.id       = id;
    bullet.position = _position;
    bullet.velocity = _velocity;
    bullet.type     = _type;
    bullet.size     = 5.0f;
    bullet.timeLeft = 15.0;
  }

  return (id);
}

void BulletSystem::remove(ID _idToRemove)
{
  mSystem.remove(_idToRemove);
}

void BulletSystem::clear()
{
  mSystem.removeAll();
}

void BulletSystem::render(const CameraInfo& _cameraInfo)
{
  glPushAttrib(GL_BLEND);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    for (unsigned i = 0; i < mSystem._num_objects; i++)
    {
      Bullet& bullet = mSystem._objects[i];

      Rect rect(bullet.position.x, bullet.position.y, bullet.size, bullet.size);

      if (bullet.type == BULLET_TYPE_PLAYER_SHOT)
        glColor4f(0.0f, 1.0f, 0.0f, 1.0f);
      else
        glColor4f(1.0f, 1.0f, 0.0f, 1.0f);

      glQuadFilled(rect);
    }
  glPopAttrib();
}

void BulletSystem::update(double _updateDelta)
{
  for (unsigned i = 0; i < mSystem._num_objects; i++)
  {
    Bullet& bullet = mSystem._objects[i];

    bullet.prevPosition = bullet.position;
    bullet.position = bullet.position + bullet.velocity * _updateDelta;

    bullet.timeLeft -= _updateDelta;

    if (bullet.timeLeft <= 0.0)
    {
      bullet.timeLeft = 0.0;

      GenericEvent event(bullet.id);
      queueEvent(EVENT_BULLET_REMOVED, &event, sizeof(GenericEvent));
      
      remove(bullet.id);
      continue;
    }
  }
}
