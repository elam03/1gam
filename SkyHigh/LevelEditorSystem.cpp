#include "LevelEditorSystem.h"

#include "MglGl.h"
#include "MglResourceManager.h"
#include "MglUtils.h"

#include "array.h"
#include "hash.h"
#include "memory.h"
#include "murmur_hash.h"
#include "queue.h"

#include "pugixml.hpp"

#include <functional>
#include <sstream>

using namespace std;
using namespace foundation;
using namespace Mgl;

static const float MinDist = .05f;

int SpawnEventSort(const void* _lhs, const void* _rhs)
{
  SpawnEvent* e1 = (SpawnEvent *)_lhs;
  SpawnEvent* e2 = (SpawnEvent *)_rhs;

  if (e1->time < e2->time) return -1;
  if (e1->time == e2->time) return 0;
  if (e1->time > e2->time) return 1;

  return 0;
}

bool read(SpawnEvent& _spawnEvent, pugi::xml_node _node)
{
  bool success = false;

  if (!_node.empty() && wstring(L"SpawnEvent") == _node.name())
  {
    _spawnEvent.time          = _node.attribute(L"time").as_double();
    _spawnEvent.type          = getEntityType(_node.attribute(L"type").as_string());
    _spawnEvent.position.x    = _node.attribute(L"x").as_float();
    _spawnEvent.position.y    = _node.attribute(L"y").as_float();
    _spawnEvent.flightDir.x   = _node.attribute(L"dir_x").as_float();
    _spawnEvent.flightDir.y   = _node.attribute(L"dir_y").as_float();
    _spawnEvent.flightPattern = getFlightPattern(_node.attribute(L"flightPattern").as_string());

    success = true;
  }

  return (success);
}

bool write(SpawnEvent& _spawnEvent, pugi::xml_node _node)
{
  bool success = false;

  if (!_node.empty())
  {
    pugi::xml_node node = _node.append_child(L"SpawnEvent");

    node.append_attribute(L"time")          = _spawnEvent.time;
    node.append_attribute(L"type")          = toString((ENTITY_TYPE)_spawnEvent.type).c_str();
    node.append_attribute(L"x")             = _spawnEvent.position.x;
    node.append_attribute(L"y")             = _spawnEvent.position.y;
    node.append_attribute(L"dir_x")         = _spawnEvent.flightDir.x;
    node.append_attribute(L"dir_y")         = _spawnEvent.flightDir.y;
    node.append_attribute(L"flightPattern") = toString((FLIGHT_PATTERN)_spawnEvent.flightPattern).c_str();

    success = true;
  }

  return (success);
}

void LevelEditorSystem::insert(foundation::Hash<CommandData>& _hash, const CommandData& _value)
{
  uint64_t key = (uint64_t)_value.key;
  foundation::hash::set(mCommands, key, _value);
}

LevelEditorSystem::LevelEditorSystem() : 
  mSpawnEvents(memory_globals::default_allocator()),
  mSpawnEventsSubSetStartIndex(0),
  mSpawnEventsSubSetSize(0),
  mSpawnEventHovered(nullptr),
  mSpawnEventHoveredIndex(-1),
  mCommands(),
  mCommandHelpEnabled(false),
  mSelectedEntityType(ENTITY_TYPE_ENEMY_PLANE)
{
  insert(mCommands, CommandData('H', L"Toggle help menu", &LevelEditorSystem::commandHelp));
  insert(mCommands, CommandData('A', L"Add Enemy at mouse position", &LevelEditorSystem::commandAddEnemy));
  insert(mCommands, CommandData('D', L"Remove Enemy at mouse position", &LevelEditorSystem::commandRemoveEnemy));
  insert(mCommands, CommandData('C', L"Remove All Enemies at this time", &LevelEditorSystem::commandRemoveAllEnemies));
  insert(mCommands, CommandData('E', L"Cycle Enemy Types", &LevelEditorSystem::commandCycleEnemyTypes));
  insert(mCommands, CommandData('O', L"Previous Level", &LevelEditorSystem::commandLoadPrevLevel));
  insert(mCommands, CommandData('P', L"Next Level", &LevelEditorSystem::commandLoadNextLevel));
  insert(mCommands, CommandData('N', L"New Level", &LevelEditorSystem::commandLoadNewLevel));
  insert(mCommands, CommandData('S', L"Save Level", &LevelEditorSystem::commandSaveLevel));
  insert(mCommands, CommandData('J', L"Move back 1.0 seconds", &LevelEditorSystem::commandDecreaseTime));
  insert(mCommands, CommandData(VK_LEFT, L"Move back 1.0 seconds", &LevelEditorSystem::commandDecreaseTime));
  insert(mCommands, CommandData('K', L"Move forward 1.0 seconds", &LevelEditorSystem::commandIncreaseTime));
  insert(mCommands, CommandData(VK_RIGHT, L"Move forward 1.0 seconds", &LevelEditorSystem::commandIncreaseTime));

  mLevelPath = L"..\\config\\SkyHigh\\Levels";
  commandScanLevelsPath();
}

LevelEditorSystem::~LevelEditorSystem()
{
}

bool LevelEditorSystem::init()
{
  bool success = true;

  return (success);
}

void LevelEditorSystem::close()
{
}

bool LevelEditorSystem::start()
{
  return (reload());
}

bool LevelEditorSystem::stop()
{
  bool success = true;

  array::clear(mSpawnEvents);

  mCurrTime = 0.0f;
  mMaxTime  = 0.0f;

  mWindowWidth  = 0.0f;
  mWindowHeight = 0.0f;

  return (success);
}

bool LevelEditorSystem::reload()
{
  bool success = false;

  if (commandScanLevelsPath())
  {
    mLevelIndex = 0;
    wstring path = mLevelPath + L"\\" + getCurrLevelFileName();
    success = load(path);
  }

  return (success);
}

bool LevelEditorSystem::load(const wstring& _fileName)
{
  bool success = false;
  
  if (_fileName.length() > 0)
  {
    if (loadSpawnEvents(_fileName, mSpawnEvents, mMaxTime))
    {
      mCurrTime = 0.0f;
      if (mMaxTime == 0.0f)
        mMaxTime = 20.0f;

      mWindowWidth  = 0.0f;
      mWindowHeight = 0.0f;

      success = loadSpawnEventsAtTime(mCurrTime);
    }
  }

  return (success);
}

bool LevelEditorSystem::save(const wstring& _fileName)
{
  bool success = false;

  if (_fileName.length() > 0)
  {
    pugi::xml_document doc;

    pugi::xml_node root = doc.append_child(L"Level");

    root.append_attribute(L"maxTime").set_value(mMaxTime);

    for (unsigned i = 0; i < array::size(mSpawnEvents); i++)
    {
      write(mSpawnEvents[i], root);
    }

    success = doc.save_file(_fileName.c_str());
  }

  return (success);
}

void LevelEditorSystem::render(const CameraInfo& _cameraInfo)
{
  char buf[256];

  glPushAttrib(GL_BLEND);
    glEnable(GL_BLEND);

    renderTimeBar(_cameraInfo);

    sprintf_s(buf, 256, "Level Editor: %ws", getCurrLevelFileName().c_str());
    glColor4f(1.0f, 1.0f, 1.0f, 0.75f);
    glRasterPos2i(50, 150);
    glText(buf);

    sprintf_s(buf, 256, "Inserting Enemy Type: \"%ws\"", toString((ENTITY_TYPE)mSelectedEntityType).c_str());
    glColor4f(1.0f, 1.0f, 1.0f, 0.75f);
    glRasterPos2i(50, 100);
    glText(buf);

    sprintf_s(buf, 256, "Displaying %u (at index %u) out of %u total enemies...", mSpawnEventsSubSetSize, mSpawnEventsSubSetStartIndex, mSpawnEvents._size);
    glColor4f(1.0f, 1.0f, 1.0f, 0.75f);
    glRasterPos2i(50, 50);
    glText(buf);
  
    //static const float WindowOffsetY = (float)GetSystemMetrics(SM_CYMIN);
    //glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
    //glCross(mMousePos.x, _cameraInfo.height - (mMousePos.y + WindowOffsetY), 5.0f);

    glColor4f(0.5f, 0.5f, 0.5f, 1.0f);
    glQuadFilled(mSpawnEventsArea);

    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
    glCross(mMousePos.x, mMousePos.y, 5.0f);

    static const float EntitySize = 10.0f;
    static const float EntityDirSize = 10.0f;

    if (mSpawnEventHoveredIndex >= 0 && mSpawnEventHoveredIndex < mSpawnEvents._size)
    {
      SpawnEvent& spawnEvent = mSpawnEvents[mSpawnEventHoveredIndex];
      Vector2 position = spawnEvent.position;

      position.x = mSpawnEventsArea.x + mSpawnEventsArea.w * position.x;
      position.y = mSpawnEventsArea.y + mSpawnEventsArea.h * position.y;

      glColor4f(0.0f, 1.0f, 0.0f, 1.0f);
      glQuad(Rect(position.x - EntitySize, position.y - EntitySize, EntitySize * 2.0f, EntitySize * 2.0f));
    }

    glBegin(GL_LINES);
    for (unsigned i = 0; i < mSpawnEventsSubSetSize; i++)
    {
      SpawnEvent& spawnEvent = mSpawnEvents[mSpawnEventsSubSetStartIndex + i];
      Vector2 position = spawnEvent.position;

      position.x = mSpawnEventsArea.x + mSpawnEventsArea.w * position.x;
      position.y = mSpawnEventsArea.y + mSpawnEventsArea.h * position.y;

      switch (spawnEvent.type)
      {
        case ENTITY_TYPE_ENEMY_PLANE:        glColor4f(0.50f, 0.0f, 0.0f, 1.0f); break;
        case ENTITY_TYPE_ENEMY_PLANE_MEDIUM: glColor4f(0.75f, 0.0f, 0.0f, 1.0f); break;
        case ENTITY_TYPE_ENEMY_PLANE_LARGE:  glColor4f(1.00f, 0.0f, 0.0f, 1.0f); break;
      }

      glVertex2f(position.x - EntitySize, position.y);
      glVertex2f(position.x + EntitySize, position.y);

      glVertex2f(position.x, position.y - EntitySize);
      glVertex2f(position.x, position.y + EntitySize);

      glVertex2f(position);
      glVertex2f(position + spawnEvent.flightDir * EntityDirSize);
       
      //glCross(position.x, position.y, 10.0f);
    }
    glEnd();

    if (mCommandHelpEnabled)
    {
      glEnable(GL_BLEND);
      glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

      //Rect rect(0.0f, mWindowHeight / 4.0f, mWindowWidth / 2.0f, mWindowHeight / 2.0f);
      Rect rect(0.0f, 0.0f, mWindowWidth, mWindowHeight);

      glColor4f(0.35f, 0.35f, 0.35f, 0.75f);
      glQuadFilled(rect);
      glColor4f(1.0f, 1.0f, 1.0f, 0.75f);

      unsigned i = 0;
      for (const foundation::Hash<CommandData>::Entry* entry = foundation::hash::begin(mCommands); entry != foundation::hash::end(mCommands); entry++, i++)
      {
        const CommandData& commandData = entry->value;
        uint64_t key = entry->key;

        glRasterPos2f(rect.x, mWindowHeight - (rect.y + 30.0f * i) - 30.0f);
        if (isalnum(commandData.key))
          glText(string_format(L"%c - %s", commandData.key, commandData.description).c_str());
        else
          glText(string_format(L"'%i' - %s", commandData.key, commandData.description).c_str());
      }
    }

  glPopAttrib();
}

void LevelEditorSystem::update(double _updateDelta)
{
}

bool LevelEditorSystem::handle(int _event, void* _eventData)
{
  bool handled = false;

  if (isLevelLoaded())
  {
    switch (_event)
    {
      case EVENT_KEY_INPUT:
      {
        EventDataKeyInput& eventData = *(EventDataKeyInput *)_eventData;

        if (eventData.down)
        {
          if (foundation::hash::has(mCommands, eventData.key))
          {
            const CommandData& commandData = foundation::hash::get(mCommands, eventData.key, CommandData());

            handled = (this->*commandData.callback)();
          }
        }
      } break;

      case EVENT_MOUSE_INPUT:
      {
        static bool dragged = false;

        EventDataMouseInput& eventData = *(EventDataMouseInput *)_eventData;

        mMousePos.x = (float)eventData.x;
        mMousePos.y = (float)eventData.y;
    
        mMousePos.y = mWindowHeight - (mMousePos.y);

        Vector2 p;
        p.x = ((mMousePos.x - mSpawnEventsArea.x) / mSpawnEventsArea.w);
        p.y = ((mMousePos.y - mSpawnEventsArea.y) / mSpawnEventsArea.h);

        if (p.x >= 0.0f && p.x <= 1.0f && p.y >= 0.0f && p.y <= 1.0f)
        {
          if (eventData.event == MOUSE_EVENT_BUTTON_DOWN)
          {
            if (mSpawnEventHoveredIndex < mSpawnEvents._size)
            {
              SpawnEvent& spawnEvent = mSpawnEvents[mSpawnEventHoveredIndex];

              spawnEvent.position.x = p.x;
              spawnEvent.position.y = p.y;

              dragged = true;
            }
          }
          else if (eventData.event == MOUSE_EVENT_BUTTON_UP)
          {
            dragged = false;
          }
          else if (eventData.event == MOUSE_EVENT_MOVE)
          {
            if (!dragged)
            {
              mSpawnEventHoveredIndex = closestByIndex(mSpawnEvents, mSpawnEventsSubSetStartIndex, mSpawnEventsSubSetSize, p, MinDist);
            }
            else
            {
              if (mSpawnEventHoveredIndex < mSpawnEvents._size)
              {
                SpawnEvent& spawnEvent = mSpawnEvents[mSpawnEventHoveredIndex];

                spawnEvent.position.x = p.x;
                spawnEvent.position.y = p.y;
              } 
            }
          }
        }
        else
        {
          p.x = ((mMousePos.x - mTimeBarFull.x) / mTimeBarFull.w);
          p.y = ((mMousePos.y - mTimeBarFull.y) / mTimeBarFull.h);
          
          if (p.x >= 0.0f && p.x <= 1.0f && p.y >= 0.0f && p.y <= 1.0f)
          {
            if (eventData.down)
            {
              if (mMaxTime >= 0.0f)
              {
                mCurrTime = (float)(p.x * mMaxTime);

                refreshTimeBar();

                loadSpawnEventsAtTime(mCurrTime);
              }
            }
          }
        }

        handled = true;
      } break;
    }
  }

  return (handled);
}

bool LevelEditorSystem::loadSpawnEvents(const wstring& _fileName, Array<SpawnEvent>& _spawnEvents, float& _maxTime)
{
  pugi::xml_document document;
  bool success = false;
  
  array::clear(_spawnEvents);

  if (document.load_file(_fileName.c_str()))
  {
    pugi::xml_node root = document.root().first_child();

    if (wstring(L"Level") == root.name())
    {
      _maxTime = root.attribute(L"maxTime").as_float(30.0f);

      for (pugi::xml_node node = root.first_child(); node; node = node.next_sibling())
      {
        if (wstring(L"SpawnEvent") == node.name())
        {
          SpawnEvent spawnEvent;

          if (read(spawnEvent, node))
          {
            if (isNil(spawnEvent.flightDir))
              spawnEvent.flightDir.y = 1.0f;
            array::push_back(_spawnEvents, spawnEvent);
          }
        }
      }

      for (unsigned i = 0; i < _spawnEvents._size; i++)
        debugPrint(0, "before: %i %f", i, _spawnEvents[i].time);

      qsort(&_spawnEvents._data[0], _spawnEvents._size, sizeof(SpawnEvent), SpawnEventSort);

      for (unsigned i = 0; i < _spawnEvents._size; i++)
        debugPrint(0, "after: %i %f", i, _spawnEvents[i].time);

      success = true;
    }
  }

  return (success);
}

void LevelEditorSystem::refreshTimeBar()
{
  static const float BorderSize = 3.0f;

  mTimeBarFull.x = mWindowWidth   / 4.0f;
  mTimeBarFull.y = (mWindowHeight / 8.0f) * 7.0f;
  mTimeBarFull.w = mWindowWidth   / 2.0f;
  mTimeBarFull.h = mWindowHeight  / 16.0f;

  mTimeBar = mTimeBarFull;

  mTimeBar.x += BorderSize;
  mTimeBar.y += BorderSize;
  mTimeBar.w -= BorderSize * 2.0f;
  mTimeBar.h -= BorderSize * 2.0f;

  mSpawnEventsArea.x = mWindowWidth  / 4.0f;
  mSpawnEventsArea.y = mWindowHeight / 4.0f;
  mSpawnEventsArea.w = mWindowWidth  / 2.0f;
  mSpawnEventsArea.h = mWindowHeight / 2.0f;

  mTimeBar.w = (mCurrTime / mMaxTime) * mTimeBarFull.w;
}

void LevelEditorSystem::renderTimeBar(const CameraInfo& _cameraInfo)
{
  char buf[256];

  if (mWindowWidth != _cameraInfo.width || mWindowHeight != _cameraInfo.height)
  {
    mWindowWidth  = _cameraInfo.width;
    mWindowHeight = _cameraInfo.height;

    refreshTimeBar();
  }

  glColor4f(0.4f, 0.4f, 0.4f, 0.75f);
  glQuad(mTimeBarFull);

  glColor4f(1.0f, 1.0f, 0.0f, 0.75f);
  glQuad(mTimeBar);

  sprintf_s(buf, 256, "0.0");
  glColor4f(1.0f, 1.0f, 1.0f, 0.75f);
  glRasterPos2f(mTimeBarFull.x - 30.0f, mTimeBarFull.y - 30.0f);
  glText(buf);

  sprintf_s(buf, 256, "%.2f", mCurrTime);
  glColor4f(1.0f, 1.0f, 1.0f, 0.75f);
  glRasterPos2f(mTimeBarFull.x + mTimeBarFull.w / 2.0f - 30.0f, mTimeBarFull.y - 30.0f);
  glText(buf);

  sprintf_s(buf, 256, "%.2f", mMaxTime);
  glColor4f(1.0f, 1.0f, 1.0f, 0.75f);
  glRasterPos2f(mTimeBarFull.x + mTimeBarFull.w - 30.0f, mTimeBarFull.y - 30.0f);
  glText(buf);
}

bool LevelEditorSystem::loadSpawnEventsAtTime(double _currTime)
{
  static double TimeBuffer = 0.5;
  bool success = false;

  if (_currTime >= 0.0 && _currTime <= mMaxTime)
  {
    unsigned i = 0;

    unsigned startIndex = mSpawnEvents._size;
    unsigned endIndex   = 0;

    for (unsigned i = 0; i < mSpawnEvents._size; i++)
    {
      double t = mSpawnEvents[i].time;
      if (t >= (_currTime - TimeBuffer))
      {
        startIndex = i;
        break;
      }
    }

    if (startIndex != mSpawnEvents._size)
    {
      endIndex = mSpawnEvents._size;

      for (unsigned i = startIndex; i < mSpawnEvents._size; i++)
      {
        double t = mSpawnEvents[i].time;

        if (t >= (_currTime + TimeBuffer))
        {
          endIndex = i;
          break;
        }
      }

      mSpawnEventsSubSetStartIndex = startIndex;
      if (endIndex >= startIndex)
        mSpawnEventsSubSetSize = endIndex - startIndex;
      else
        mSpawnEventsSubSetSize = mSpawnEvents._size - startIndex;
    }
    else
    {
      mSpawnEventsSubSetStartIndex = 0;
      mSpawnEventsSubSetSize = 0;
    }

    success = true;
  }

  return (success);
}

unsigned LevelEditorSystem::closestByIndex(Array<SpawnEvent> _spawnEvents, unsigned _subSetStartIndex, unsigned _subSetSize, const Vector2& _pos, float _minDist)
{
  unsigned index = _spawnEvents._size;

  float distMin = 99999.9f;

  for (unsigned i = _subSetStartIndex; i < _subSetStartIndex + _subSetSize; i++)
  {
    float dist = distanceSquaredTo(_pos, _spawnEvents[i].position);

    if (dist < (_minDist * _minDist))
    {
      index = i;
      distMin = dist;
    }
  }

  return (index);
}

bool LevelEditorSystem::commandHelp()
{
  bool success = true;
  
  mCommandHelpEnabled = !mCommandHelpEnabled;

  return (success);
}

bool LevelEditorSystem::commandAddEnemy()
{
  bool success = false;

  if (mSpawnEventHoveredIndex >= mSpawnEvents._size)
  {
    Vector2 p;
    p.x = ((mMousePos.x - mSpawnEventsArea.x) / mSpawnEventsArea.w);
    p.y = ((mMousePos.y - mSpawnEventsArea.y) / mSpawnEventsArea.h);

    SpawnEvent spawnEvent;

    spawnEvent.time          = mCurrTime;
    spawnEvent.type          = mSelectedEntityType;
    spawnEvent.position      = p;
    //spawnEvent.flightDir     = normalize(p - mMousePosPrev);
    spawnEvent.flightDir.x   = 0.0f;
    spawnEvent.flightDir.y   = 1.0f;
    spawnEvent.flightPattern = FLIGHT_PATTERN_STRAIGHT;

    array::push_back(mSpawnEvents, spawnEvent);

    qsort(&mSpawnEvents._data[0], mSpawnEvents._size, sizeof(SpawnEvent), SpawnEventSort);

    mSpawnEventHoveredIndex = mSpawnEvents._size;

    loadSpawnEventsAtTime(mCurrTime);

    success = true;
  }

  return (success);
}

bool LevelEditorSystem::commandRemoveEnemy()
{
  bool success = false;

  if (mSpawnEventHoveredIndex < mSpawnEvents._size)
  {
    if (mSpawnEventHoveredIndex >= 0 && mSpawnEventHoveredIndex < mSpawnEvents._size)
    {
      unsigned size = mSpawnEvents._size - mSpawnEventHoveredIndex - 1;

      if (size > 0)
      {
        memcpy(&mSpawnEvents[mSpawnEventHoveredIndex], &mSpawnEvents[mSpawnEventHoveredIndex + 1], sizeof(SpawnEvent) * size);
        mSpawnEvents._size--;
      }
    }

    mSpawnEventHoveredIndex = mSpawnEvents._size;

    loadSpawnEventsAtTime(mCurrTime);

    success = true;
  }

  return (success);
}

bool LevelEditorSystem::commandRemoveAllEnemies()
{
  bool success = false;

  if (mSpawnEventsSubSetSize > 0)
  {
    if (mSpawnEventsSubSetStartIndex >= 0 && mSpawnEventsSubSetStartIndex < mSpawnEvents._size)
    {
      unsigned size = mSpawnEvents._size - mSpawnEventsSubSetSize;

      if (size > 0)
      {
        memcpy(&mSpawnEvents[mSpawnEventsSubSetStartIndex], &mSpawnEvents[mSpawnEventsSubSetStartIndex + mSpawnEventsSubSetSize], sizeof(SpawnEvent) * size);
        mSpawnEvents._size -= mSpawnEventsSubSetSize;
    
        mSpawnEventHoveredIndex = mSpawnEvents._size;

        loadSpawnEventsAtTime(mCurrTime);
        
        success = true;
      }
    }
  }

  return (success);
}

bool LevelEditorSystem::commandCycleEnemyTypes()
{
  bool success = true;

  do
  {
    ++mSelectedEntityType;
    mSelectedEntityType = mSelectedEntityType % ENTITY_TYPE_COUNT;
  } while (mSelectedEntityType == ENTITY_TYPE_NONE || mSelectedEntityType == ENTITY_TYPE_PLAYER);

  return (success);
}


bool LevelEditorSystem::commandScanLevelsPath()
{
  bool success = false;

  success = getFileList(mLevelPath, mLevelFiles);

  return (success);
}

bool LevelEditorSystem::commandLoadNextLevel()
{
  bool success = false;

  if (mLevelFiles.size() == 0)
    commandScanLevelsPath();

  if (mLevelIndex >= 0 && mLevelIndex < mLevelFiles.size())
  {
    mLevelIndex = (mLevelIndex + 1) % mLevelFiles.size();
  
    wstring levelFileName = mLevelFiles[mLevelIndex].fileName;
    wstring path = string_format(L"%s\\%s", mLevelPath.c_str(), levelFileName.c_str());

    success = load(path);
  }

  return (success);
}

bool LevelEditorSystem::commandLoadPrevLevel()
{
  bool success = false;

  if (mLevelIndex >= 0 && mLevelIndex < mLevelFiles.size())
  {
    if (mLevelIndex <= 0)
      mLevelIndex = mLevelFiles.size() - 1;
    else
      mLevelIndex = (mLevelIndex - 1) % mLevelFiles.size();
    
    wstring levelFileName = mLevelFiles[mLevelIndex].fileName;
    wstring path = string_format(L"%s\\%s", mLevelPath.c_str(), levelFileName.c_str());

    success = load(path);
  }

  return (success);
}

bool LevelEditorSystem::commandSaveLevel()
{
  bool success = false;

  if (mLevelIndex >= 0 && mLevelIndex < mLevelFiles.size())
  {
    wstring levelFileName = mLevelFiles[mLevelIndex].fileName;
    wstring path = string_format(L"%s\\%s", mLevelPath.c_str(), levelFileName.c_str());

    success = save(path);
  }
  
  return (success);
}

bool LevelEditorSystem::commandLoadNewLevel()
{
  bool success = false;

  array::clear(mSpawnEvents);

  mCurrTime = 0.0f;
  mMaxTime = 10.0f;

  wstring newFileName = string_format(L"%s\\Level%u.xml", mLevelPath.c_str(), random(0, 500));

  if (save(newFileName))
  {
    commandScanLevelsPath();
    for (unsigned i = 0; i < mLevelFiles.size(); i++)
    {
      wstring s = mLevelFiles[i].fileName;
      if (s.find(newFileName) != wstring::npos)
      {
        mLevelIndex = i;
        break;
      }
    }
    success = load(newFileName);
  }

  return (success);
}

bool LevelEditorSystem::commandIncreaseTime()
{
  bool success = false;

  if (mCurrTime < mMaxTime)
  {
    mCurrTime += 1.0f;
    refreshTimeBar();

    success = loadSpawnEventsAtTime(mCurrTime);
  }

  return (success);
}

bool LevelEditorSystem::commandDecreaseTime()
{
  bool success = false;

  if (mCurrTime > 0.0f)
  {
    mCurrTime -= 1.0f;
    refreshTimeBar();

    success = loadSpawnEventsAtTime(mCurrTime);
  }

  return (success);
}
