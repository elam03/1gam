#pragma once

#include "MglTypes.h"

enum ENTITY_TYPE
{
  ENTITY_TYPE_NONE,
  ENTITY_TYPE_PLAYER,
  ENTITY_TYPE_ENEMY_PLANE,
  ENTITY_TYPE_ENEMY_PLANE_MEDIUM,
  ENTITY_TYPE_ENEMY_PLANE_LARGE,
  ENTITY_TYPE_COUNT
};

enum BULLET_TYPE
{
  BULLET_TYPE_ENEMY_SHOT,
  BULLET_TYPE_PLAYER_SHOT,
  BULLET_TYPE_COUNT
};

enum EFFECT_TYPE
{
  EFFECT_TYPE_SMALL_EXPLOSION,
  EFFECT_TYPE_EXPLOSION,
  EFFECT_TYPE_SMOKE_PUFF,
  EFFECT_TYPE_COUNT
};

enum FLIGHT_PATTERN
{
  FLIGHT_PATTERN_NONE,
  FLIGHT_PATTERN_STRAIGHT,
  FLIGHT_PATTERN_LEFT,
  FLIGHT_PATTERN_RIGHT,
  FLIGHT_PATTERN_COUNT
};

static std::wstring toString(ENTITY_TYPE _type)
{
  switch (_type)
  {
    default:
    case ENTITY_TYPE_NONE:               return (L"None"); break;
    case ENTITY_TYPE_PLAYER:             return (L"Player"); break;
    case ENTITY_TYPE_ENEMY_PLANE:        return (L"EnemyPlane"); break;
    case ENTITY_TYPE_ENEMY_PLANE_MEDIUM: return (L"EnemyPlaneMedium"); break;
    case ENTITY_TYPE_ENEMY_PLANE_LARGE:  return (L"EnemyPlaneLarge"); break;
  }

  return (L"");
}

static ENTITY_TYPE getEntityType(const std::wstring& _type)
{
  if (_type == L"Player") return ENTITY_TYPE_PLAYER;
  else if (_type == L"EnemyPlane") return ENTITY_TYPE_ENEMY_PLANE;
  else if (_type == L"EnemyPlaneMedium") return ENTITY_TYPE_ENEMY_PLANE_MEDIUM;
  else if (_type == L"EnemyPlaneLarge") return ENTITY_TYPE_ENEMY_PLANE_LARGE;

  return ENTITY_TYPE_NONE;
}

static std::wstring toString(FLIGHT_PATTERN _flightPattern)
{
  switch (_flightPattern)
  {
    default:
    case FLIGHT_PATTERN_NONE:     return (L"None"); break;

    case FLIGHT_PATTERN_STRAIGHT: return (L"Straight"); break;
    case FLIGHT_PATTERN_LEFT:     return (L"Left"); break;
    case FLIGHT_PATTERN_RIGHT:    return (L"Right"); break;
  }

  return (L"");
}

static FLIGHT_PATTERN getFlightPattern(const std::wstring& _flightPattern)
{
  if (_flightPattern == L"Straight") return FLIGHT_PATTERN_STRAIGHT;
  else if (_flightPattern == L"Left") return FLIGHT_PATTERN_LEFT;
  else if (_flightPattern == L"Right") return FLIGHT_PATTERN_RIGHT;

  return FLIGHT_PATTERN_NONE;
}

struct GenericEvent
{
  GenericEvent() : id(ID_INVALID) {}
  GenericEvent(ID _id) : id(_id) {}
  virtual ~GenericEvent() {}
  
  ID id;
};

struct CollisionEvent
{
  CollisionEvent() : entityId(ID_INVALID), bulletId(ID_INVALID) {}
  CollisionEvent(ID _entityId, ID _bulletId) : entityId(_entityId), bulletId(_bulletId) {}
  virtual ~CollisionEvent() {}

  ID entityId;
  ID bulletId;
};

struct Entity
{
  Entity() { id = ID_INVALID; type = ENTITY_TYPE_NONE; size = 0.0f; flightPattern = FLIGHT_PATTERN_NONE; flightTime = 0.0; position.x = position.y = 0.0f; health = healthMax = 0.0f; }
  virtual ~Entity() {}

  ID    id;
  int   type;
  float size;

  int    flightPattern;
  double flightTime;

  foundation::Vector2 position;

  float health;
  float healthMax;

  bool   weaponFired;
  double weaponCooldown;
  double weaponLastFiredTime;
};

struct SpawnEvent
{
  SpawnEvent() : id(ID_INVALID), type(ENTITY_TYPE_NONE), flightPattern(FLIGHT_PATTERN_NONE), time(0.0), position(), flightDir() {}
  SpawnEvent(ID _id, int _type, int _flightPattern, double _time, const foundation::Vector2& _position, const foundation::Vector2& _flightDir) : id(_id), type(_type), flightPattern(_flightPattern), time(_time), position(_position), flightDir(_flightDir) {}

  ID  id;
  int type;
  int flightPattern;
  
  double              time;
  foundation::Vector2 position;
  foundation::Vector2 flightDir;
};

struct LevelInfo
{
  LevelInfo(const std::wstring& _filePath, unsigned _number) : number(_number) { wcscpy_s(filePath, 260, _filePath.c_str()); }
  LevelInfo() { wcscpy_s(filePath, 260, L""); number = 0; }
  ~LevelInfo() {}

  wchar_t  filePath[260];
  unsigned number;
};

struct Bullet
{
  Bullet() { id = ID_INVALID; type = 0; entityId = ID_INVALID; targetId = ID_INVALID; size = 0.0f; prevPosition.x = prevPosition.y = position.x = position.y = velocity.x = velocity.y = 0.0f; timeLeft = 0.0; }
  virtual ~Bullet() {}

  ID  id;
  int type;
  ID  entityId;
  ID  targetId;

  float size;
  
  foundation::Vector2 prevPosition;
  foundation::Vector2 position;
  foundation::Vector2 velocity;

  double timeLeft;
};

struct Effect
{
  Effect() { id = ID_INVALID; type = 0; textureId = 0; size = 0.0f; position.x = position.y = velocity.x = velocity.y = 0.0f; timeLeft = timeLeftInitial = 0.0; }
  ~Effect() {}

  ID  id;
  int type;

  unsigned textureId;

  float size;
  foundation::Vector2 position;
  foundation::Vector2 velocity;

  float  t;
  double timeLeft;
  double timeLeftInitial;
};