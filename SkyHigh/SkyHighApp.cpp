#include "SkyHighApp.h"

#include "MglGl.h"
#include "MglResourceManager.h"
#include "MglUtils.h"

#include "array.h"
#include "queue.h"
#include "memory.h"

#include "lua.hpp"
#include "pugixml.hpp"

#include <sstream>

using namespace std;
using namespace foundation;
using namespace Mgl;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////


SkyHighApp::SkyHighApp() :
  MglApplication(),
  mGameState(STATE_START),
  mGameLevel(0),
  mPlayerId(ID_INVALID),
  mPlayerCount(0),

  mEntitySystem(),
  mPlayerBulletSystem(),
  mEnemyBulletSystem(),
  mCollisionSystem(),
  mEffectSystem(),
  mBackgroundSystem(),
  mGuiSystem(),

  mEventHeaders(memory_globals::default_allocator()),
  mSpawnEvents(memory_globals::default_allocator()),

  mDebugEffectId(ID_INVALID)
{
}

SkyHighApp::~SkyHighApp()
{
}

bool SkyHighApp::appInit()
{
  bool success = false;

  mUpdatesPerSecond = 120;

  success = mEffectSystem.init() && mBackgroundSystem.init() && mEntitySystem.init() && mPlayerBulletSystem.init() && mEnemyBulletSystem.init() && mCollisionSystem.init(&mEntitySystem, &mPlayerBulletSystem, &mEnemyBulletSystem) && mGuiSystem.init(&mEntitySystem) && mLevelEditorSystem.init();

  if (success)
  {
    mGameState = STATE_START;

    mCameraInfo.x = mCameraInfo.y = 0.0f;
    mCameraInfo.width  = (float)mWindowWidth;
    mCameraInfo.height = (float)mWindowHeight;
  }

  return (success);
}

void SkyHighApp::appClose()
{
  mLevelEditorSystem.close();
  mGuiSystem.close();
  mBackgroundSystem.close();
  mEntitySystem.close();
  mPlayerBulletSystem.close();
  mEnemyBulletSystem.close();
  mCollisionSystem.close();
  mEffectSystem.close();

  MglApplication::close();
}

bool SkyHighApp::getLevelInfos()
{
  bool success = false;

  static const wstring levelPath = L"..\\config\\SkyHigh\\Levels";
  List<FileInfo> fileList;

  if (getFileList(levelPath, fileList))
  {
    if (queue::size(mLevelInfos) > 0)
      queue::consume(mLevelInfos, queue::size(mLevelInfos));

    for (unsigned i = 0; i < fileList.size(); i++)
    {
      wstring s = levelPath + L"\\" + fileList[i].fileName;

      if (s.find(L".xml") != wstring::npos)
        queue::push_back(mLevelInfos, LevelInfo(s, i));
    }

    success = true;
  }

  return (success);
}

bool SkyHighApp::loadNextLevel()
{
  bool success = false;

  if (queue::size(mLevelInfos) > 0)
  {
    mGameState = STATE_RUNNING;

    mEntitySystem.mSystem.removeAll();
    mEntitySystem.flushEvents();
    mPlayerBulletSystem.mSystem.removeAll();
    mPlayerBulletSystem.flushEvents();
    mEnemyBulletSystem.mSystem.removeAll();
    mEnemyBulletSystem.flushEvents();
    mCollisionSystem.flushEvents();
    mEffectSystem.mSystem.removeAll();
    mEffectSystem.flushEvents();
    //mBackgroundSystem.flushEvents();
    //mGuiSystem.flushEvents();
    //mLevelEditorSystem.flushEvents();

    if (!mEntitySystem.mSystem.has(mPlayerId))
    {
      Vector2 p(float(mWindowWidth) / 2.0f, 50.0f);
    
      mPlayerId = mEntitySystem.add(p, ENTITY_TYPE_PLAYER);

      if (mEntitySystem.mSystem.has(mPlayerId))
      {
        Entity& player = mEntitySystem.mSystem.lookup(mPlayerId);
        player.health = player.healthMax = 100.0f;
        player.weaponLastFiredTime = 0.0;
        player.weaponCooldown = .05;
        player.weaponFired = false;

        mPlayerCount = 1;
      }
    }

    mGuiSystem.setPlayer(mPlayerId);

    mCollisionSystem.mPlayerId = mPlayerId;
    mLevelTime = 0.0;
  
    LevelInfo& levelInfo = mLevelInfos[0];

    wstring levelFileName = levelInfo.filePath;

    if (queue::size(mSpawnEvents) > 0)
      queue::consume(mSpawnEvents, queue::size(mSpawnEvents));

    float maxTime = 0.0f;
    Array<SpawnEvent> spawnEvents;
    if (LevelEditorSystem::loadSpawnEvents(levelFileName, spawnEvents, maxTime))
    {
      mLevelTimeMax = (double)maxTime;
      for (unsigned i = 0; i < array::size(spawnEvents); i++)
        queue::push_back(mSpawnEvents, spawnEvents[i]);
    }
    else
    {
      debugPrint(0, "\"%ws\" failed to load!", levelFileName.c_str());
    }
    
    success = true;
  }

  return (success);
}

void SkyHighApp::luaTests()
{
  /////////////////////////////////////////////////////////////////////////////
  // LUA Test

  lua_State* luaState = luaL_newstate();
  luaL_openlibs(luaState);

  //if (luaL_loadfile(luaState, "..\\config\\E1.lua"))
  if (luaL_dofile(luaState, "..\\config\\SkyHigh\\Setup.lua"))
  {
    std::stringstream str_buf;
    str_buf << "script returned the string: " << lua_tostring(luaState, lua_gettop(luaState));

    debugPrint(0, "dofile failed! \"%s\"", str_buf.str().c_str());
  }
  else
  {
    lua_getglobal(luaState, "setup");
      lua_getfield(luaState, -1, "onStart");
      if (lua_isfunction(luaState, -1))
      {
        debugPrint(0, "yey function");
        lua_call(luaState, -1, 0);
      }
      lua_pop(luaState, 1);
    lua_pop(luaState, 1);

    lua_getglobal(luaState, "plane");

    if (!lua_isnil(luaState, -1))
    {
      lua_getfield(luaState, -1, "name");
    }

    //////////////
    std::cout << "[C++] Values returned from the script:" << std::endl;
    std::stringstream str_buf;
    while(lua_gettop(luaState))
    {
      str_buf.str(std::string());
      str_buf << " ";
      switch(lua_type(luaState, lua_gettop(luaState)))
      {
      case LUA_TNUMBER:
        str_buf << "script returned the number: " << lua_tonumber(luaState, lua_gettop(luaState));
        break;
      case LUA_TTABLE:
        str_buf << "script returned a table";
        break;
      case LUA_TSTRING:
        str_buf << "script returned the string: " << lua_tostring(luaState, lua_gettop(luaState));
        break;
      case LUA_TBOOLEAN:
        str_buf << "script returned the boolean: " << lua_toboolean(luaState, lua_gettop(luaState));
        break;
      default:
        str_buf << "script returned an unknown-type value";
      }
      lua_pop(luaState, 1);

      debugPrint(0, str_buf.str().c_str());
    }
    //////////////
  }
  /////////////////////////////////////////////////////////////////////////////
}

bool SkyHighApp::defaultHandleEvent(int _event, void* _eventData)
{
  bool handled = false;

  if (mGameState == STATE_LEVEL_EDITOR)
    handled = mLevelEditorSystem.handle(_event, _eventData);

  if (!handled)
  {
    switch (_event)
    {
      case EVENT_KEY_INPUT:
      {
        EventDataKeyInput& eventData = *(EventDataKeyInput *)_eventData;

        if (eventData.down)
        {
          switch (eventData.key)
          {
            case VK_LEFT:
            {
            } break;

            case VK_UP:
            {
            } break;

            case VK_RIGHT:
            {
            } break;

            case VK_DOWN:
            {
            } break;

            case VK_RETURN:
            {
              if (mGameState == STATE_START || mGameState == STATE_GAME_OVER)
              {
                if (getLevelInfos())
                  loadNextLevel();
              }
              else if (mGameState == STATE_TRANSITION)
              {
                bool loaded = false;

                do
                {
                  queue::pop_front(mLevelInfos);
                  loaded = loadNextLevel(); 
                } while (!loaded && queue::size(mLevelInfos) > 0);

                if (!loaded)
                {
                  mGameState = STATE_WIN;
                }
              }
            } break;

            case 'L':
            {
              if (mGameState == STATE_START)
              {
                if (mLevelEditorSystem.start())
                  mGameState = STATE_LEVEL_EDITOR;
              }
            } break;

            case 'W':
            {
            } break;

            case 'S':
            {
            } break;

            case 'A':
            {
            } break;

            case 'D':
            {
            } break;

            case 'Q':
            case 'X':
            {
              if (mGameState == STATE_LEVEL_EDITOR)
              {
                if (mLevelEditorSystem.stop())
                mGameState = STATE_START;
              }
            } break;

            #ifdef _DEBUG
            case '1':
            {
              Vector2 p(randomf(0.0f, float(mWindowWidth)), randomf(float(mWindowHeight) / 8.0f * 7.0f, float(mWindowHeight)));
              mEntitySystem.add(p, ENTITY_TYPE_ENEMY_PLANE);
            } break;

            case '2':
            {
              if (mEntitySystem.mSystem.has(mPlayerId))
              {
                Entity& player = mEntitySystem.mSystem.lookup(mPlayerId);
                int count = random(5, 25);
                for (int i = 0; i < count; i++)
                  mEffectSystem.add(EFFECT_TYPE_SMOKE_PUFF, randomf(15.0f, 35.0f), player.position + makeVector2(randomf(0.0f, 360.0f), randomf(3.0f, 50.0f)), makeVector2(randomf(0.0f, 360.0f), randomf(3.0f, 50.0f)), randomf(.5f, 5.0f));
              }
            } break;
            #endif
          }
        }
      } break;
    }
  }
  
  return (handled);
}

void SkyHighApp::defaultRender()
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

  glLoadIdentity();

  if (mGameState == STATE_RUNNING || mGameState == STATE_GAME_OVER || mGameState == STATE_TRANSITION)
  {
    glPushMatrix();
      glTranslatef(mCameraInfo.x, mCameraInfo.y, 0.0f);

      mBackgroundSystem.render(mCameraInfo);
      mEntitySystem.render(mCameraInfo);
      mPlayerBulletSystem.render(mCameraInfo);
      mEnemyBulletSystem.render(mCameraInfo);
      mCollisionSystem.render(mCameraInfo);
      mEffectSystem.render(mCameraInfo);
      mGuiSystem.render(mCameraInfo);
    glPopMatrix();
  }

  if (mGameState == STATE_START)
  {
    renderBorder();

    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
    glRasterPos2i(50, 100);
    glText("Sky High");
    glRasterPos2i(50, 80);
    glText("Press 'Enter' to start!");
  }
  else if (mGameState == STATE_TRANSITION)
  {
    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
    glRasterPos2i(50, 100);
    glText("Sky High");
    glRasterPos2i(50, 80);
    glText("Press 'Enter' to start next level...");
  }
  else if (mGameState == STATE_GAME_OVER)
  {
    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
    glRasterPos2i(50, 200);
    glText("Game over!");
    glRasterPos2i(50, 180);
    glText("Press 'Enter' to restart!");
  }
  else if (mGameState == STATE_WIN)
  {
    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
    glRasterPos2i(50, 200);
    glText("No more levels, you've won!");
    glRasterPos2i(50, 180);
    glText("Press 'Enter' to restart!");
  }
  else if (mGameState == STATE_LEVEL_EDITOR)
  {
    mCameraInfo.width  = (float)mWindowWidth;
    mCameraInfo.height = (float)mWindowHeight;
    mLevelEditorSystem.render(mCameraInfo);
  }
  #ifdef _DEBUG
  else if (mGameState == STATE_RUNNING)
  {
    char buf[256];

    if (mEffectSystem.mSystem.has(mDebugEffectId))
    {
      Effect& effect = mEffectSystem.mSystem.lookup(mDebugEffectId);

      //sprintf_s(buf, 256, "effect: %u %.2f %f %f", effect.id, effect.t, effect.timeLeftInitial, effect.timeLeft);
      sprintf_s(buf, 256, "effect:      %f", effect.timeLeft);
      glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
      glRasterPos2i(50, 100);
      glText(buf);
    }

    sprintf_s(buf, 256, "mLevelTime:  %.2f / %.2f", mLevelTime, mLevelTimeMax);
    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
    glRasterPos2i(50, 200);
    glText(buf);

    if (queue::size(mLevelInfos) > 0)
    {
      LevelInfo& levelInfo = mLevelInfos[0];
      sprintf_s(buf, 256, "\"%ws\"", levelInfo.filePath);
      glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
      glRasterPos2i(50, 300);
      glText(buf);
    }
  }
  #endif
}

void SkyHighApp::defaultUpdate(double _deltaTime)
{
  // Check the SpawnQueue if it's time to spawn entities or not.
  if (mGameState == STATE_RUNNING)
  {
    mLevelTime += _deltaTime;

    while (queue::size(mSpawnEvents) > 0 && mLevelTime >= mSpawnEvents[0].time)
    {
      SpawnEvent& spawnEvent = mSpawnEvents[0];

      spawnEvent.position.x *= mWindowWidth;
      spawnEvent.position.y *= mWindowHeight;
      
      mEntitySystem.add(spawnEvent);

      queue::pop_front(mSpawnEvents);
    }

    if (queue::size(mSpawnEvents) <= 0 && mEntitySystem.mSystem.has(mPlayerId) && mEntitySystem.mSystem.getNumObjects() <= 1)
    {
      mGameState = STATE_TRANSITION;
    }
  }

  // Check for player movement.
  if (mEntitySystem.mSystem.has(mPlayerId))
  {
    static const float PlayerSpeed = 500.0f;
    Entity& player = mEntitySystem.mSystem.lookup(mPlayerId);

    if (isKeyPressed('A'))
    {
      player.position.x -= float(PlayerSpeed * _deltaTime);
    }
    if (isKeyPressed('D'))
    {
      player.position.x += float(PlayerSpeed * _deltaTime);
    }
    if (isKeyPressed('W'))
    {
      player.position.y += float(PlayerSpeed * _deltaTime);
    }
    if (isKeyPressed('S'))
    {
      player.position.y -= float(PlayerSpeed * _deltaTime);
    }
    if (isKeyPressed('2'))
    {
      Entity& player = mEntitySystem.mSystem.lookup(mPlayerId);
      int count = random(5, 25);
      for (int i = 0; i < count; i++)
        mEffectSystem.add(EFFECT_TYPE_SMOKE_PUFF, randomf(15.0f, 35.0f), player.position + makeVector2(randomf(0.0f, 360.0f), randomf(3.0f, 50.0f)), makeVector2(randomf(0.0f, 360.0f), randomf(3.0f, 50.0f)), randomf(.5f, 5.0f));
    }
    if (isKeyPressed(' '))
    {
      player.weaponFired = true;
    }
    else
    {
      player.weaponFired = false;
    }

    //debugPrint(0, "(%f,%f)", player.position.x, player.position.y);
    
    player.position.x = clamp<float>(player.position.x, 0.0f, (float)mWindowWidth);
    player.position.y = clamp<float>(player.position.y, 0.0f, (float)mWindowHeight);
  }

  // Update all systems.
  mEntitySystem.update(_deltaTime);
  mPlayerBulletSystem.update(_deltaTime);
  mEnemyBulletSystem.update(_deltaTime);
  mCollisionSystem.update(_deltaTime);
  mBackgroundSystem.update(_deltaTime);
  mGuiSystem.update(_deltaTime);
  mEffectSystem.update(_deltaTime);
  mLevelEditorSystem.update(_deltaTime);

  // Check for events from the various systems.
  checkEvents();
}

void SkyHighApp::checkEvents()
{
  if (mEntitySystem.getEvents(mEventHeaders))
  {
    for (unsigned i = 0; i < array::size(mEventHeaders); i++)
    {
      EventHeader& event = *mEventHeaders[i];

      switch (event.type)
      {
        case EntitySystem::EVENT_ENTITY_WEAPON_READY:
        {
          GenericEvent& eventData = *(GenericEvent *)(event.data);

          if (mEntitySystem.mSystem.has(eventData.id))
          {
            Entity& entity = mEntitySystem.mSystem.lookup(eventData.id);

            entity.weaponFired = true;
          }
        } break;

        case EntitySystem::EVENT_ENTITY_WEAPON_FIRED:
        {
          GenericEvent& eventData = *(GenericEvent *)(event.data);

          if (mEntitySystem.mSystem.has(eventData.id) && mEntitySystem.mSystem.has(mPlayerId))
          {
            Entity& entity = mEntitySystem.mSystem.lookup(eventData.id);
            Entity& player = mEntitySystem.mSystem.lookup(mPlayerId);

            if (entity.type == ENTITY_TYPE_PLAYER)
            {
              ID bulletId = mPlayerBulletSystem.add(player.position, makeVector2(90.0f, 3000.0f), BULLET_TYPE_PLAYER_SHOT);
              Bullet& bullet = mPlayerBulletSystem.mSystem.lookup(bulletId);
              bullet.entityId = entity.id;
                
              entity.weaponLastFiredTime = entity.weaponCooldown;
            }
            else
            {
              ID bulletId = mEnemyBulletSystem.add(entity.position, makeVector2(angleTo(entity.position, player.position), 500.0f), BULLET_TYPE_ENEMY_SHOT);
              Bullet& bullet = mEnemyBulletSystem.mSystem.lookup(bulletId);
              bullet.entityId = entity.id;
              bullet.targetId = player.id;

              entity.weaponLastFiredTime = entity.weaponCooldown;
            }
          }
        } break;
      }
    }

    mEntitySystem.flushEvents();
  }

  if (mCollisionSystem.getEvents(mEventHeaders))
  {
    for (unsigned i = 0; i < array::size(mEventHeaders); i++)
    {
      EventHeader& event = *mEventHeaders[i];

      switch (event.type)
      {
        case CollisionSystem::EVENT_ENEMY_HIT:
        {
          CollisionEvent& eventData = *(CollisionEvent *)(event.data);

          if (mEntitySystem.mSystem.has(eventData.entityId))
          {
            Entity& entity = mEntitySystem.mSystem.lookup(eventData.entityId);

            if (mPlayerBulletSystem.mSystem.has(eventData.bulletId))
            {
              Bullet& bullet = mPlayerBulletSystem.mSystem.lookup(eventData.bulletId);
              
              int count = random(5, 15);

              for (int i = 0; i < count; i++)
              {
                float size = randomf(3.0f, 10.0f);
                float timeLeft = randomf(1.5f, 5.0f);

                mEffectSystem.add(EFFECT_TYPE_SMOKE_PUFF, size, bullet.position + makeVector2(randomf(0.0f, 360.0f), randomf(0.0f, size)), makeVector2(randomf(0.0f, 360.0f), randomf(3.0f, 50.0f)), timeLeft);
              }

              ID id = mEffectSystem.add(EFFECT_TYPE_EXPLOSION, randomf(15.0f, 25.0f), bullet.position, normalize(bullet.velocity) * randomf(1.0f, 3.0f), randomf(5.0f, 10.0f));

              if (!mEffectSystem.mSystem.has(mDebugEffectId))
              {
                mDebugEffectId = id;
              }

              mPlayerBulletSystem.remove(bullet.id);

              if (entity.type != ENTITY_TYPE_PLAYER)
              {
                entity.health -= 1.0f;

                if (entity.health <= 0.0f)
                  mEntitySystem.remove(entity.id);
              }

            } 
          }
        } break;

        case CollisionSystem::EVENT_PLAYER_HIT:
        {
          CollisionEvent& eventData = *(CollisionEvent *)(event.data);

          if (mEntitySystem.mSystem.has(eventData.entityId))
          {
            Entity& entity = mEntitySystem.mSystem.lookup(eventData.entityId);

            if (mEnemyBulletSystem.mSystem.has(eventData.bulletId))
            {
              Bullet& bullet = mEnemyBulletSystem.mSystem.lookup(eventData.bulletId);
              
              mEffectSystem.add(EFFECT_TYPE_EXPLOSION, randomf(5.0f, 25.0f), bullet.position, Vector2(), .75);

              // The only time we get this should be the player entity...
              // Though I guess there could be more than one player...
              if (entity.type == ENTITY_TYPE_PLAYER)
              {
                //entity.health -= bullet.damage;
                entity.health -= 35.0f;

                if (entity.health <= 0.0f)
                {
                  mEffectSystem.add(EFFECT_TYPE_EXPLOSION, randomf(25.0f, 50.0f), bullet.position, Vector2(), .75);

                  mEntitySystem.remove(entity.id);

                  if (--mPlayerCount <= 0)
                    mGameState = STATE_GAME_OVER;
                }
              }

              mEnemyBulletSystem.remove(bullet.id);
            } 
          }
        } break;
      }
    }

    mCollisionSystem.flushEvents();
  }

  if (mEffectSystem.getEvents(mEventHeaders))
  {
    for (unsigned i = 0; i < array::size(mEventHeaders); i++)
    {
      EventHeader& event = *mEventHeaders[i];

      switch (event.type)
      {
        case EffectSystem::EVENT_EFFECT_STARTED:
        {
          GenericEvent& eventData = *(GenericEvent *)(event.data);

          if (mEffectSystem.mSystem.has(eventData.id))
          {
            Effect& effect = mEffectSystem.mSystem.lookup(eventData.id);

            //// TODO: Start of effect
            //debugPrint(0, "Start of Effect %u", effect.id);
          }
        } break;

        case EffectSystem::EVENT_EFFECT_ENDED:
        {
          GenericEvent& eventData = *(GenericEvent *)(event.data);

          if (mEffectSystem.mSystem.has(eventData.id))
          {
            Effect& effect = mEffectSystem.mSystem.lookup(eventData.id);

            //// TODO: End of effect
            //debugPrint(0, "End of Effect %u", effect.id);

            if (effect.id == mDebugEffectId)
              mDebugEffectId = ID_INVALID;
          }
        } break;
      }
    }

    mEffectSystem.flushEvents();
  }
}
