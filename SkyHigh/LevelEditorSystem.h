#pragma once

#include "MglUtils.h"
#include "SkyHighTypes.h"

#include <string>

#include "array.h"
#include "queue.h"
#include "memory.h"

class LevelEditorSystem
{
public:
  LevelEditorSystem();
  virtual ~LevelEditorSystem();

  bool init();
  void close();

  bool start();
  bool stop();

  bool reload();

  bool load(const std::wstring& _fileName);
  bool save(const std::wstring& _fileName);
  void render(const Mgl::CameraInfo& _cameraInfo);
  void update(double _updateDelta);
  bool handle(int _event, void* _eventData);

  static bool loadSpawnEvents(const std::wstring& _fileName, foundation::Array<SpawnEvent>& _spawnEvents, float& _maxTime);

protected:
  void refreshTimeBar();
  void renderTimeBar(const Mgl::CameraInfo& _cameraInfo);

  bool loadSpawnEventsAtTime(double _currTime);

  Mgl::Rect mTimeBar;
  Mgl::Rect mTimeBarFull;

  foundation::Vector2 mMousePos;

  float mWindowWidth;
  float mWindowHeight;

  float mCurrTime;
  float mMaxTime;

  Mgl::Rect mSpawnEventsArea;

  foundation::Array<SpawnEvent>  mSpawnEvents;
  unsigned                       mSpawnEventsSubSetStartIndex;
  unsigned                       mSpawnEventsSubSetSize;

  SpawnEvent* mSpawnEventHovered;
  unsigned    mSpawnEventHoveredIndex;
  
  /////////////////////////////////////////////////////////////////////////////
  typedef bool (LevelEditorSystem::*Callback)(void);

  struct CommandData
  {
    CommandData() : key(0), callback(nullptr) { memset(description, 0, sizeof(wchar_t) * 128); }
    CommandData(int _key, const std::wstring& _description, Callback _callback = nullptr) : key(_key), callback(_callback) { wcscpy_s(description, 128, _description.c_str()); }
    virtual ~CommandData() {  }

    int      key;
    wchar_t  description[128];
    Callback callback;
  };
  
  void insert(foundation::Hash<CommandData>& _hash, const CommandData& _value);

  foundation::Hash<CommandData> mCommands;
  bool                          mCommandHelpEnabled;
  /////////////////////////////////////////////////////////////////////////////

  foundation::Vector2 mMousePosPrev;

  int mSelectedEntityType;

  unsigned closestByIndex(foundation::Array<SpawnEvent> _spawnEvents, unsigned _subSetStartIndex, unsigned _subSetSize, const foundation::Vector2& _pos, float _minDist);

  bool commandHelp();
  bool commandAddEnemy();
  bool commandRemoveEnemy();
  bool commandRemoveAllEnemies();
  bool commandCycleEnemyTypes();
  bool commandScanLevelsPath();
  bool commandLoadNextLevel();
  bool commandLoadPrevLevel();
  bool commandSaveLevel();
  bool commandLoadNewLevel();
  bool commandIncreaseTime();
  bool commandDecreaseTime();

  inline bool         isLevelLoaded() const { return (mLevelIndex >= 0 && mLevelIndex < mLevelFiles.size()); }
  inline std::wstring getCurrLevelFileName() { return (mLevelIndex >= 0 && mLevelIndex < mLevelFiles.size()) ? mLevelFiles[mLevelIndex].fileName : L""; }

  unsigned                 mLevelIndex;
  std::wstring             mLevelPath;
  Mgl::List<Mgl::FileInfo> mLevelFiles;
};