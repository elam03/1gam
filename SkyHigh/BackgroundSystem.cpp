#include "BackgroundSystem.h"

#include "MglGl.h"
#include "MglResourceManager.h"
#include "MglUtils.h"

using namespace foundation;
using namespace std;
using namespace Mgl;

BackgroundSystem::BackgroundSystem() : mWaterTexture(nullptr), mScrollPosition(0.0f), mScrollSpeed(0.0)
{
}

BackgroundSystem::~BackgroundSystem()
{
}

bool BackgroundSystem::init()
{
  bool success = false;

  ResourceManager::Instance().addTexture(L"water", L"..\\assets\\dngn_open_sea.png");
  
  mWaterTexture = ResourceManager::Instance().getTexture(L"water");

  if (mWaterTexture)
  {
    mScrollPosition = 0.0f;
    mScrollSpeed = 3.0;

    success = true;
  }

  return (success);
}

void BackgroundSystem::close()
{
}

void BackgroundSystem::render(const Mgl::CameraInfo& _cameraInfo)
{
  if (mWaterTexture)
  {
    glPushAttrib(GL_TEXTURE_2D);
      glEnable(GL_TEXTURE_2D);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

      glBindTexture(GL_TEXTURE_2D, mWaterTexture->mTextureId);
        glColor4f(1.0f, 1.0f, 1.0f, 1.0f);

        float TileSizeW = (float)mWaterTexture->mWidth;
        float TileSizeH = (float)mWaterTexture->mHeight;
        const float& w = _cameraInfo.width;
        const float& h = _cameraInfo.height;

        glBegin(GL_QUADS);
          glTexCoord2f(0.0f, mScrollPosition);                  glVertex2f(0.0f, h);
          glTexCoord2f(TileSizeW, mScrollPosition);             glVertex2f(w, h);
          glTexCoord2f(TileSizeW, TileSizeH + mScrollPosition); glVertex2f(w, 0.0f);
          glTexCoord2f(0.0f, TileSizeH + mScrollPosition);      glVertex2f(0.0f, 0.0f);
        glEnd();
      glBindTexture(GL_TEXTURE_2D, 0);
    glPopAttrib();
  }
}

void BackgroundSystem::update(double _updateDelta)
{
  mScrollPosition -= float(mScrollSpeed * _updateDelta);
}