#include "GuiSystem.h"

#include "MglGl.h"
#include "MglUtils.h"

#include "EntitySystem.h"

using namespace foundation;
using namespace std;
using namespace Mgl;

GuiSystem::GuiSystem() : mPlayerId(ID_INVALID), mEntitySystem(nullptr)
{
}

GuiSystem::~GuiSystem()
{
}

bool GuiSystem::init(EntitySystem* _entitySystem)
{
  bool success = false;

  if (_entitySystem)
  {
    mEntitySystem = _entitySystem;

    success = true;
  }

  return (success);
}

void GuiSystem::close()
{
}

void GuiSystem::render(const Mgl::CameraInfo& _cameraInfo)
{
  static const float BorderSize = 3.0f;

  glPushAttrib(GL_BLEND);
    glEnable(GL_BLEND);

    // Draw the back border first.
    glColor4f(0.0f, 0.0f, 0.0f, 0.5f);
    glQuad(50.0f - BorderSize, 50.0f - BorderSize, 300.0f + BorderSize * 2.0f, 30.0f + BorderSize * 2.0f);

    // Then the actual health bar.
    glColor4f(0.0f, 1.0f, 0.0f, .5f);
    float t = 0.0f;
    if (mEntitySystem->mSystem.has(mPlayerId))
    {
      Entity& entity = mEntitySystem->mSystem.lookup(mPlayerId);

      if (entity.healthMax != 0.0f && entity.health <= entity.healthMax)
        t = entity.health / entity.healthMax;
    }
    glQuad(50.0f, 50.0f, 300.0f * t, 30.0f);
  glPopAttrib();
}

void GuiSystem::update(double _updateDelta)
{
}