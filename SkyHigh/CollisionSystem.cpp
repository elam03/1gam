#include "CollisionSystem.h"

#include "BulletSystem.h"
#include "EntitySystem.h"
#include "MglGl.h"
#include "MglResourceManager.h"
#include "MglUtils.h"

#include "memory.h"

using namespace foundation;
using namespace std;
using namespace Mgl;

CollisionSystem::CollisionSystem() :
  EventSystem(),
  mEntitySystem(nullptr),
  mPlayerBulletSystem(nullptr),
  mEnemyBulletSystem(nullptr)
{
}

CollisionSystem::~CollisionSystem()
{
}

bool CollisionSystem::init(EntitySystem* _entitySystem, BulletSystem* _playerBulletSystem, BulletSystem* _enemyBulletSystem)
{
  bool success = false;

  if (EventSystem::init() && _entitySystem && _playerBulletSystem && _enemyBulletSystem)
  {
    mEntitySystem = _entitySystem;
    mPlayerBulletSystem = _playerBulletSystem;
    mEnemyBulletSystem = _enemyBulletSystem;

    success = true;
  }

  return (success);
}

void CollisionSystem::close()
{
  EventSystem::close();

  mEntitySystem = nullptr;
  mPlayerBulletSystem = nullptr;
  mEnemyBulletSystem = nullptr;
}

void CollisionSystem::render(const Mgl::CameraInfo& _cameraInfo)
{
}

void CollisionSystem::update(double _updateDelta)
{
  for (unsigned i = 0; i < mEntitySystem->mSystem.getNumObjects(); i++)
  {
    Entity& entity = mEntitySystem->mSystem._objects[i];

    for (unsigned j = 0; j < mPlayerBulletSystem->mSystem.getNumObjects(); j++)
    {
      Bullet& bullet = mPlayerBulletSystem->mSystem._objects[j];

      if (bullet.entityId == entity.id)
        continue;

      //if (distanceSquaredTo(entity.position, bullet.position) < (entity.size * entity.size))
      if (circleLineSegmentCheck(entity.position, entity.size, bullet.prevPosition, bullet.position))
      {
        CollisionEvent event(entity.id, bullet.id);
        queueEvent(EVENT_ENEMY_HIT, &event, sizeof(CollisionEvent));
      }
    } 
  }

  if (mEntitySystem->mSystem.has(mPlayerId))
  {
    Entity& player = mEntitySystem->mSystem.lookup(mPlayerId);

    for (unsigned j = 0; j < mEnemyBulletSystem->mSystem.getNumObjects(); j++)
    {
      Bullet& bullet = mEnemyBulletSystem->mSystem._objects[j];

      if (bullet.entityId == player.id)
        continue;

      //if (distanceSquaredTo(player.position, bullet.position) < (player.size * player.size))
      if (circleLineSegmentCheck(player.position, player.size, bullet.prevPosition, bullet.position))
      {
        CollisionEvent event(player.id, bullet.id);
        queueEvent(EVENT_PLAYER_HIT, &event, sizeof(CollisionEvent));
      }
    } 
  }
}