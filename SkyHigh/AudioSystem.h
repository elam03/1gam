#pragma once
 
#include "MglUtils.h"

#include <mmsystem.h>

class AudioSystem
{
private:
  bool pPaused;
  bool pErr;
  wchar_t pErrStr[128];
  MCIDEVICEID pDevice;
  std::wstring pFileName;
  bool open(const std::wstring& fName, int deviceType, bool playNow);
  int setErrCode(int errCode);
  void setErrStr(wchar_t* estr);
public:
  AudioSystem();
  ~AudioSystem();
  bool openWav(const std::wstring& fName, bool playNow = false);
  bool openMp3(const std::wstring& fName, bool playNow = false);
  bool openMidi(const std::wstring& fName, bool playNow = false);
  std::wstring getFileName() const;
  bool play();
  void stop();
  void close();
  void pause();
  int length();
  int position();
  bool isOpen();
  bool playing();
  bool paused();
  bool err() const;
  std::wstring errStr() const;
};