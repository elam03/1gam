#pragma once

#include "MglApplication.h"

#include "BackgroundSystem.h"
#include "BulletSystem.h"
#include "CollisionSystem.h"
#include "EffectSystem.h"
#include "EntitySystem.h"
#include "GuiSystem.h"
#include "LevelEditorSystem.h"

class AppState;
class ApplicationEvent;

class SkyHighApp : public MglApplication
{
public:
  SkyHighApp();
  virtual ~SkyHighApp();

protected:
  virtual bool appInit();
  virtual void appClose();

public:
  virtual bool defaultHandleEvent(int _event, void* _eventData = 0);
  virtual void defaultRender();
  virtual void defaultUpdate(double _deltaTime);

protected:
  bool getLevelInfos();
  bool loadNextLevel();
  void luaTests();

  void checkEvents();

protected:
  enum
  {
    STATE_START,
    STATE_RUNNING,
    STATE_TRANSITION,
    STATE_GAME_OVER,
    STATE_LEVEL_EDITOR,
    STATE_WIN,
    STATE_COUNT
  };

  Mgl::CameraInfo mCameraInfo;

  int mGameState;
  int mGameLevel;
  ID  mPlayerId;
  int mPlayerCount;

  EntitySystem      mEntitySystem;
  BulletSystem      mPlayerBulletSystem;
  BulletSystem      mEnemyBulletSystem;
  CollisionSystem   mCollisionSystem;
  EffectSystem      mEffectSystem;
  BackgroundSystem  mBackgroundSystem;
  GuiSystem         mGuiSystem;
  LevelEditorSystem mLevelEditorSystem;

  foundation::Array<Mgl::EventHeader*> mEventHeaders;

  double mLevelTime;
  double mLevelTimeMax;
  foundation::Queue<SpawnEvent> mSpawnEvents;

  foundation::Queue<LevelInfo> mLevelInfos;

  ID mDebugEffectId;
};