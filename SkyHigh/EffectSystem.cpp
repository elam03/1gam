#include "EffectSystem.h"

#include "MglGl.h"
#include "MglResourceManager.h"
#include "MglUtils.h"

#include "memory.h"

using namespace foundation;
using namespace std;
using namespace Mgl;

EffectSystem::EffectSystem() : EventSystem(), mSystem()
{
}

EffectSystem::~EffectSystem()
{
}

bool EffectSystem::init()
{
  bool success = false;

  success = EventSystem::init();

  if (success)
  {
    ResourceManager::Instance().addTexture(L"explosion", L"..\\assets\\explosion_2_38_128.png");

    mExplosionAnimationInfo.textureId     = ResourceManager::Instance().getTexture(L"explosion")->mTextureId;
    mExplosionAnimationInfo.textureWidth  = (float)ResourceManager::Instance().getTexture(L"explosion")->mWidth;
    mExplosionAnimationInfo.textureHeight = (float)ResourceManager::Instance().getTexture(L"explosion")->mHeight;

    mExplosionAnimationInfo.frameCount  = 40;
    mExplosionAnimationInfo.frameWidth  = 128.0f;
    mExplosionAnimationInfo.frameHeight = 128.0f;

    calculateAnimationInfo(mExplosionAnimationInfo);

    ///////////////////////////////////////////////////////////////////////////
    
    ResourceManager::Instance().addTexture(L"smoke", L"..\\assets\\smoke_1_40_128_corrected.png");

    mSmokeAnimationInfo.textureId     = ResourceManager::Instance().getTexture(L"smoke")->mTextureId;
    mSmokeAnimationInfo.textureWidth  = (float)ResourceManager::Instance().getTexture(L"smoke")->mWidth;
    mSmokeAnimationInfo.textureHeight = (float)ResourceManager::Instance().getTexture(L"smoke")->mHeight;

    mSmokeAnimationInfo.frameCount  = 40;
    mSmokeAnimationInfo.frameWidth  = 128.0f;
    mSmokeAnimationInfo.frameHeight = 128.0f;

    calculateAnimationInfo(mSmokeAnimationInfo);

    ///////////////////////////////////////////////////////////////////////////
  }

  return (success);
}

void EffectSystem::close()
{
  EventSystem::close();
}

ID EffectSystem::add(int _type, float _size, foundation::Vector2 _position, foundation::Vector2 _velocity, double _timeLeft)
{
  ID id = mSystem.add();

  if (mSystem.has(id))
  {
    Effect& effect = mSystem.lookup(id);
    memset(&effect, 0, sizeof(Effect));

    effect.id = id;
    effect.type = _type;
    effect.size = _size;
    effect.position = _position;
    effect.velocity = _velocity;
    effect.timeLeft = _timeLeft;
    effect.timeLeftInitial = _timeLeft;

    GenericEvent event(effect.id);
    queueEvent(EVENT_EFFECT_STARTED, &event, sizeof(GenericEvent));
  }

  return (id);
}

void EffectSystem::remove(ID _idToRemove)
{
  mSystem.remove(_idToRemove);
}

void EffectSystem::clear()
{
  mSystem.removeAll();
}

void EffectSystem::render(const Mgl::CameraInfo& _cameraInfo)
{
  glPushAttrib(GL_BLEND);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    for (unsigned i = 0; i < mSystem._num_objects; i++)
    {
      Effect& effect = mSystem._objects[i];
      float& size = effect.size;

      Rect rect(effect.position.x - size, effect.position.y - size, size * 2.0f, size * 2.0f);

      switch (effect.type)
      {
        case EFFECT_TYPE_EXPLOSION:  render(mExplosionAnimationInfo, rect, effect.t); break;
        case EFFECT_TYPE_SMOKE_PUFF: render(mSmokeAnimationInfo, rect, effect.t); break;

        default:
        {
          glColor4f(effect.t, 0.0f, 0.0f, 0.75f);

          Rect rect(effect.position.x - size, effect.position.y - size, size * 2.0f, size * 2.0f);
          glQuadFilled(rect);
        } break;
      }
    }
  glPopAttrib();
}

void EffectSystem::update(double _updateDelta)
{
  for (unsigned i = 0; i < mSystem._num_objects; i++)
  {
    Effect& effect = mSystem._objects[i];
    
    effect.position = effect.position + effect.velocity * _updateDelta;

    effect.timeLeft -= _updateDelta;
    effect.t = float(1.0 - effect.timeLeft / effect.timeLeftInitial);

    if (effect.type == EFFECT_TYPE_EXPLOSION)
    {
      effect.textureId = mExplosionAnimationInfo.textureId;
    }

    if (effect.timeLeft <= 0.0)
    {
      effect.timeLeft = 0.0;

      GenericEvent event(effect.id);
      queueEvent(EVENT_EFFECT_ENDED, &event, sizeof(GenericEvent));

      remove(effect.id);
      continue;
    }
  }
}

void EffectSystem::render(const AnimationInfo& _animationInfo, const Rect& _rect, float _t)
{
  int frame = int(_t * _animationInfo.frameCount);

  const float& tx = _animationInfo.texCoords[frame].x;
  const float& ty = _animationInfo.texCoords[frame].y;
  const float& tw = _animationInfo.texCoords[frame].w;
  const float& th = _animationInfo.texCoords[frame].h;

  glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
  glBindTexture(GL_TEXTURE_2D, _animationInfo.textureId);
  glBegin(GL_QUADS);
    glTexCoord2f(tx, ty);           glVertex2f(_rect.x, _rect.y + _rect.h);
    glTexCoord2f(tx + tw, ty);      glVertex2f(_rect.x + _rect.w, _rect.y + _rect.h);
    glTexCoord2f(tx + tw, ty + th); glVertex2f(_rect.x + _rect.w, _rect.y);
    glTexCoord2f(tx, ty + th);      glVertex2f(_rect.x, _rect.y);
  glEnd();

  glBindTexture(GL_TEXTURE_2D, 0);
}

void EffectSystem::calculateAnimationInfo(AnimationInfo& _animationInfo)
{
  float x = 0.0f, y = 0.0f;

  for (int i = 0; i < _animationInfo.frameCount; i++)
  {
    _animationInfo.texCoords[i] = Rect(x, y, _animationInfo.frameWidth, _animationInfo.frameHeight);

    _animationInfo.texCoords[i].x /= _animationInfo.textureWidth;
    _animationInfo.texCoords[i].y /= _animationInfo.textureHeight;
    _animationInfo.texCoords[i].w /= _animationInfo.textureWidth;
    _animationInfo.texCoords[i].h /= _animationInfo.textureHeight;

    x += _animationInfo.frameWidth;

    if (x >= _animationInfo.textureWidth)
    {
      x = 0.0f;
      y += _animationInfo.frameHeight;
    }
  }
}