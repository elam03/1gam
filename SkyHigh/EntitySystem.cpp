#include "EntitySystem.h"

#include "MglGl.h"
#include "MglResourceManager.h"
#include "MglUtils.h"

#include "memory.h"

using namespace foundation;
using namespace std;
using namespace Mgl;

EntitySystem::EntitySystem() : EventSystem(),
  mSystem()
{
}

EntitySystem::~EntitySystem()
{
}

bool EntitySystem::init()
{
  bool success = EventSystem::init();
  
  mSystem.removeAll();

  return (success);
}

void EntitySystem::close()
{
  EventSystem::close();

  mSystem.removeAll();
}

ID EntitySystem::add(const SpawnEvent& _spawnEvent)
{
  ID id = add(_spawnEvent.position, _spawnEvent.type);

  if (mSystem.has(id))
  {
    Entity& entity = mSystem.lookup(id);

    entity.flightPattern = _spawnEvent.flightPattern;
  }

  return (id);
}

ID EntitySystem::add(Vector2 _position, int _type)
{
  ID id = mSystem.add();

  if (mSystem.has(id))
  {
    Entity& entity = mSystem.lookup(id);
    memset(&entity, 0, sizeof(Entity));

    entity.id       = id;
    entity.position = _position;
    entity.type     = _type;
    entity.size     = 10.0f;

    entity.weaponFired = false;
    entity.weaponCooldown = 2.0;

    switch (entity.type)
    {
      case ENTITY_TYPE_ENEMY_PLANE:
      {
        entity.health         = 1.0f;
        entity.healthMax      = entity.health;
        entity.weaponCooldown = 2.0;
      } break;

      case ENTITY_TYPE_ENEMY_PLANE_MEDIUM:
      {
        entity.health         = 5.0f;
        entity.healthMax      = entity.health;
        entity.size           = 15.0f;
        entity.weaponCooldown = 1.0;
      } break;

      case ENTITY_TYPE_ENEMY_PLANE_LARGE:
      {
        entity.health         = 25.0f;
        entity.healthMax      = entity.health;
        entity.size           = 20.0f;
        entity.weaponCooldown = 0.5;
      } break;
    }
    
    entity.weaponLastFiredTime = entity.weaponCooldown;
  }

  return (id);
}

void EntitySystem::remove(ID _idToRemove)
{
  mSystem.remove(_idToRemove);
}

void EntitySystem::clear()
{
  mSystem.removeAll();
}

void EntitySystem::render(const CameraInfo& _cameraInfo)
{
  glPushAttrib(GL_BLEND);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    for (unsigned i = 0; i < mSystem._num_objects; i++)
    {
      Entity& entity = mSystem._objects[i];

      if (entity.type == ENTITY_TYPE_PLAYER)
        glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
      else
        glColor4f(0.0f, 1.0f, 0.0f, 1.0f);

      float size = entity.size;
      Rect rect(entity.position.x - size, entity.position.y - size, entity.size * 2.0f, entity.size * 2.0f);
      glQuadFilled(rect);
    }
  glPopAttrib();
}

void EntitySystem::update(double _updateDelta)
{
  for (unsigned i = 0; i < mSystem._num_objects; i++)
  {
    Entity& entity = mSystem._objects[i];

    entity.flightTime += _updateDelta;

    if (entity.flightPattern == FLIGHT_PATTERN_STRAIGHT)
      entity.position.y -= float(50.0 * _updateDelta);

    if (entity.weaponLastFiredTime > 0.0)
    {
      entity.weaponLastFiredTime -= _updateDelta;
      //debugPrint(0, "%f", entity.weaponLastFiredTime);
      
      if (entity.weaponLastFiredTime <= 0.0)
      {
        entity.weaponLastFiredTime = 0.0;

        GenericEvent event(entity.id);
        queueEvent(EVENT_ENTITY_WEAPON_READY, &event, sizeof(GenericEvent));
      }
    }
    else
    {
      if (entity.weaponFired)
      {
        GenericEvent event(entity.id);
        queueEvent(EVENT_ENTITY_WEAPON_FIRED, &event, sizeof(GenericEvent));
                
        entity.weaponFired = false;
      }
    }
  }
}
