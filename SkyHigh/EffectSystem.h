#pragma once

#include "SkyHighTypes.h"

#include "MglEventSystem.h"
#include "MglSystem.h"
#include "MglUtils.h"

class EffectSystem : public EventSystem
{
public:
  EffectSystem();
  virtual ~EffectSystem();

  bool init();
  void close();

  ID   add(int _type, float _size, foundation::Vector2 _position, foundation::Vector2 _velocity, double _timeLeft);
  void remove(ID _idToRemove);
  void clear();
  void render(const Mgl::CameraInfo& _cameraInfo);
  void update(double _updateDelta);

  System<Effect> mSystem;

public:
  enum EVENT_ENUM
  {
    EVENT_EFFECT_STARTED,
    EVENT_EFFECT_ENDED,
    EVENT_EFFECT_COUNT
  } EVENT;

protected:
  struct AnimationInfo
  {
    unsigned textureId;
    float    textureWidth;
    float    textureHeight;

    int      frameCount;
    float    frameWidth;
    float    frameHeight;
    
    Mgl::Rect texCoords[64];
  };

  AnimationInfo mExplosionAnimationInfo;
  AnimationInfo mSmokeAnimationInfo;

  void render(const AnimationInfo& _animationInfo, const Mgl::Rect& _rect, float _t);
  void calculateAnimationInfo(AnimationInfo& _animationInfo);
};