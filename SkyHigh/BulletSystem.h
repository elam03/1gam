#pragma once

#include "SkyHighTypes.h"

#include "MglEventSystem.h"
#include "MglSystem.h"

class BulletSystem : public EventSystem
{
public:
  BulletSystem();
  virtual ~BulletSystem();

  bool init();
  void close();

  ID   add(foundation::Vector2 _position, foundation::Vector2 _velocity, int _type);
  void remove(ID _idToRemove);
  void clear();
  void render(const Mgl::CameraInfo& _cameraInfo);
  void update(double _updateDelta);

  System<Bullet> mSystem;

public:
  enum EVENT_ENUM
  {
    EVENT_BULLET_REMOVED,
    EVENT_ENTITY_COUNT
  } EVENT;
};