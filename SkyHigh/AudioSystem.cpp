#include "AudioSystem.h"

#include <string.h>

using namespace std;

bool AudioSystem::open(const wstring& fName, int deviceType, bool playNow)
{
  // Open new file
  close();
 
  if (!pErr)
  {
    MCI_OPEN_PARMS op;
    op.dwCallback = NULL;
    op.lpstrDeviceType = (wchar_t*)deviceType;
    op.lpstrElementName = fName.c_str();
    op.lpstrAlias = NULL;
 
    int flags = 0;
    if (deviceType != MCI_ALL_DEVICE_ID)
      flags = MCI_OPEN_TYPE | MCI_OPEN_TYPE_ID;
 
    // Send command to
    if ( setErrCode( mciSendCommand( NULL, MCI_OPEN,
      MCI_OPEN_ELEMENT | MCI_WAIT | flags,
      (DWORD)&op) ) == 0)
    {
      // Success on open - set time format to milliseconds
      pDevice = op.wDeviceID;
      pFileName = fName;
 
      MCI_SET_PARMS sp;
      sp.dwCallback = NULL;
      sp.dwTimeFormat = MCI_FORMAT_MILLISECONDS;
      if ( setErrCode( mciSendCommand(pDevice, MCI_SET,
        MCI_SET_TIME_FORMAT | MCI_WAIT, (DWORD)&sp) ) == 0)
      {
        if (playNow)
          return play();
        else
          return true;
      }
      else
        close();
    }
  }
 
  return false;
}
//---------------------------------------------------------------------------
int AudioSystem::setErrCode(int errCode)
{
  // Set error code
  if (errCode == 0)
  {
    pErr = false;
    pErrStr[0] = '\0';
  }
  else
  {
    if (!mciGetErrorString(errCode, pErrStr, sizeof(pErrStr)))
      wcscpy_s(pErrStr, 128, L"Unknown media device error");
 
    pErr = true;
  }
 
  return errCode;
}
//---------------------------------------------------------------------------
void AudioSystem::setErrStr(wchar_t* estr)
{
  // Set error string
  if (estr == NULL)
  {
    pErr = false;
    pErrStr[0] = '\0';
  }
  else
  {
    wcscpy_s(pErrStr, 128, estr);
    pErr = true;
  }
}
//---------------------------------------------------------------------------
// CLASS AudioSystem : PUBLIC METHODS
//---------------------------------------------------------------------------
AudioSystem::AudioSystem()
{
  // Constructor
  pPaused = false;
  pDevice = 0;

  setErrCode(0);
}
//---------------------------------------------------------------------------
AudioSystem::~AudioSystem()
{
  // Destructor - stop playing
  try
  {
    close();
  }
  catch(...)
  {
  }
}
//---------------------------------------------------------------------------
bool AudioSystem::openWav(const wstring& fName, bool playNow)
{
  // Open wave audio
  return open(fName, MCI_DEVTYPE_WAVEFORM_AUDIO, playNow);
}
//---------------------------------------------------------------------------
bool AudioSystem::openMp3(const wstring& fName, bool playNow)
{
  // open mp3
  return open(fName, MCI_ALL_DEVICE_ID, playNow);
}
//---------------------------------------------------------------------------
bool AudioSystem::openMidi(const wstring& fName, bool playNow)
{
  // Open midi
  return open(fName, MCI_DEVTYPE_SEQUENCER, playNow);
}
//---------------------------------------------------------------------------
wstring AudioSystem::getFileName() const
{
  // Return open filename
  return pFileName;
}
//---------------------------------------------------------------------------
bool AudioSystem::play()
{
  // Play current audio file or resume playing
  if (pDevice != NULL)
  {
    if (pPaused)
    {
      // Play from current position
      MCI_PLAY_PARMS pp;
      pp.dwCallback = NULL;
 
      if ( setErrCode( mciSendCommand(pDevice, MCI_PLAY, MCI_NOTIFY, (DWORD)&pp) ) == 0)
      {
        pPaused = false;
        return true;
      }
    }
    else
    {
      // Play file from start
      stop();
 
      if (!pErr)
      {
        MCI_PLAY_PARMS pp;
        pp.dwCallback = NULL;
        pp.dwFrom = 0;
        if ( setErrCode( mciSendCommand(pDevice, MCI_PLAY, MCI_NOTIFY | MCI_FROM, (DWORD)&pp) ) == 0)
          return true;
      }
    }
  }
  else
    setErrStr(L"MCI audio device is closed");
 
  return false;
}
//---------------------------------------------------------------------------
void AudioSystem::stop()
{
  // Stop playing (& rewind)
  pPaused = false;
  setErrCode(0);
 
  if (pDevice != NULL)
  {
    MCI_GENERIC_PARMS gp;
    gp.dwCallback = NULL;
 
    setErrCode( mciSendCommand(pDevice, MCI_STOP, MCI_WAIT, (DWORD)&gp) );
  }
}
//---------------------------------------------------------------------------
void AudioSystem::close()
{
  // Stop & reset filename
  stop();
 
  if (pDevice != NULL)
  {
    MCI_GENERIC_PARMS gp;
    gp.dwCallback = NULL;
 
    if ( setErrCode( mciSendCommand(pDevice, MCI_CLOSE, MCI_WAIT, (DWORD)&gp) ) == 0)
    {
      pFileName = L"";
      pDevice = NULL;
    }
  }
}
//---------------------------------------------------------------------------
void AudioSystem::pause()
{
  // Pause playing (by calling stop)
  stop();
 
  if (pErr == 0)
    pPaused = true;
}
//---------------------------------------------------------------------------
int AudioSystem::length()
{
  // Get audio length in millisecs
  setErrCode(0);
 
  if (pDevice != NULL)
  {
    MCI_STATUS_PARMS sp;
    sp.dwCallback = NULL;
    sp.dwItem = MCI_STATUS_LENGTH;
 
    if ( setErrCode( mciSendCommand(pDevice, MCI_STATUS,
      MCI_WAIT | MCI_STATUS_ITEM, (DWORD)&sp) ) == 0)
      return (int)sp.dwReturn;
  }
 
  return -1;
}
//---------------------------------------------------------------------------
int AudioSystem::position()
{
  // Get audio position in millisecs
  setErrCode(0);
 
  if (pDevice != NULL)
  {
    MCI_STATUS_PARMS sp;
    sp.dwCallback = NULL;
    sp.dwItem = MCI_STATUS_POSITION;
 
    if ( setErrCode( mciSendCommand(pDevice, MCI_STATUS,
      MCI_WAIT | MCI_STATUS_ITEM, (DWORD)&sp) ) == 0)
      return (int)sp.dwReturn;
  }
 
  return -1;
}
//---------------------------------------------------------------------------
bool AudioSystem::isOpen()
{
  // Is audio file open
  setErrCode(0);
  return (pDevice != NULL);
}
//---------------------------------------------------------------------------
bool AudioSystem::playing()
{
  // Is audio playing
  setErrCode(0);
 
  if (pDevice != NULL)
  {
    MCI_STATUS_PARMS sp;
    sp.dwCallback = NULL;
    sp.dwItem = MCI_STATUS_MODE;
 
    if ( setErrCode(mciSendCommand(pDevice, MCI_STATUS,
      MCI_WAIT | MCI_STATUS_ITEM, (DWORD)&sp) ) == 0)
      return (sp.dwReturn == MCI_MODE_PLAY);
  }
 
  return false;
}
//---------------------------------------------------------------------------
bool AudioSystem::paused()
{
  // Is audio paused
  setErrCode(0);
  return pPaused;
}
//---------------------------------------------------------------------------
bool AudioSystem::err() const
{
  // Was an error raised in the last operation
  return pErr;
}
//---------------------------------------------------------------------------
wstring AudioSystem::errStr() const
{
  // Get error string
  if (pErr)
    return pErrStr;
  else
    return L"";
}
//---------------------------------------------------------------------------