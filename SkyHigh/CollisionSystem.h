#pragma once

#include "SkyHighTypes.h"

#include "MglEventSystem.h"
#include "MglSystem.h"

class BulletSystem;
class EntitySystem;

class CollisionSystem : public EventSystem
{
public:
  CollisionSystem();
  virtual ~CollisionSystem();

  bool init(EntitySystem* _entitySystem, BulletSystem* _playerBulletSystem, BulletSystem* _enemyBulletSystem);
  void close();

  void render(const Mgl::CameraInfo& _cameraInfo);
  void update(double _updateDelta);

  EntitySystem* mEntitySystem;
  BulletSystem* mPlayerBulletSystem;
  BulletSystem* mEnemyBulletSystem;

  ID mPlayerId;

public:
  enum EVENT_ENUM
  {
    EVENT_ENEMY_HIT,
    EVENT_PLAYER_HIT,
    EVENT_COUNT
  } EVENT;
};