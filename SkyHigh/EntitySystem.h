#pragma once

#include "SkyHighTypes.h"

#include "MglEventSystem.h"
#include "MglSystem.h"

class EntitySystem : public EventSystem
{
public:
  EntitySystem();
  virtual ~EntitySystem();

  bool init();
  void close();

  ID   add(const SpawnEvent& _spawnEvent);
  ID   add(foundation::Vector2 _position, int _type);
  void remove(ID _idToRemove);
  void clear();
  void render(const Mgl::CameraInfo& _cameraInfo);
  void update(double _updateDelta);

  System<Entity> mSystem;

public:
  enum EVENT_ENUM
  {
    EVENT_ENTITY_WEAPON_READY,
    EVENT_ENTITY_WEAPON_FIRED,
    EVENT_ENTITY_COUNT
  } EVENT;
};