#include <iostream>
#include <windows.h>
#include <windowsx.h>

#include "MglUtils.h"

#include "SkyHighApp.h"

#include "memory.h"

using namespace foundation;
using namespace std;

///////////////////////////////////////////////////////////////////////////////

#include "portaudio.h"
#include "AudioSystem.h"
#include <conio.h>

#define SAMPLE_RATE (44100)

typedef struct
{
    float left_phase;
    float right_phase;
} paTestData;

/* This routine will be called by the PortAudio engine when audio is needed.
   It may called at interrupt level on some machines so don't do anything
   that could mess up the system like calling malloc() or free().
*/ 
static int patestCallback( const void *inputBuffer, void *outputBuffer,
                           unsigned long framesPerBuffer,
                           const PaStreamCallbackTimeInfo* timeInfo,
                           PaStreamCallbackFlags statusFlags,
                           void *userData )
{
    /* Cast data passed through stream to our structure. */
    paTestData *data = (paTestData*)userData; 
    float *out = (float*)outputBuffer;
    unsigned int i;
    (void) inputBuffer; /* Prevent unused variable warning. */
    
    for( i=0; i<framesPerBuffer; i++ )
    {
        *out++ = data->left_phase;  /* left */
        *out++ = data->right_phase;  /* right */
        /* Generate simple sawtooth phaser that ranges between -1.0 and 1.0. */
        data->left_phase += 0.01f;
        /* When signal reaches top, drop back down. */
        if( data->left_phase >= 1.0f ) data->left_phase -= 2.0f;
        /* higher pitch so we can distinguish left and right. */
        data->right_phase += 0.03f;
        if( data->right_phase >= 1.0f ) data->right_phase -= 2.0f;
    }
    return 0;
}

void playSomething()
{
  static paTestData data;

  memset(&data, 0, sizeof(paTestData));

  PaStream* stream = nullptr;
  PaError   err = paNoError;

  err = Pa_OpenDefaultStream(&stream,
                             0,          /* no input channels */
                             2,          /* stereo output */
                             paFloat32,  /* 32 bit floating point output */
                             SAMPLE_RATE,
                             256,        /* frames per buffer, i.e. the number
                                            of sample frames that PortAudio will
                                            request from the callback. Many apps
                                            may want to use
                                            paFramesPerBufferUnspecified, which
                                            tells PortAudio to pick the best,
                                            possibly changing, buffer size.*/
                             patestCallback, /* this is your callback function */
                             &data /*This is a pointer that will be passed to your callback*/);

  if (err == paNoError)
    err = Pa_StartStream(stream);
  
  /* Sleep for several seconds. */
  while (!_kbhit()) ;
  //if (err == paNoError)
  //  Pa_Sleep(3000);
  
  if (err == paNoError)
    err = Pa_StopStream(stream);

  if (err == paNoError)
    err = Pa_CloseStream(stream);
  
}

void playSomethingElse()
{
  PlaySound(L"..\\assets\\sound_fx\\cantdothingslikethat.wav", nullptr, SND_FILENAME | SND_ASYNC | SND_LOOP);

  Sleep(3000);
  
  PlaySound(L"..\\assets\\sound_fx\\none_shall_pass.mp3", nullptr, SND_FILENAME | SND_ASYNC | SND_LOOP);

  while (!_kbhit()) ;
  
  PlaySound(nullptr, nullptr, 0);
}

void playAudioSystemTest()
{
  AudioSystem audioSystem;

  if (audioSystem.openMp3(L"..\\assets\\sound_fx\\none_shall_pass.mp3"))
  {
    audioSystem.play();

    while (!_kbhit()) ;

    audioSystem.stop();

    audioSystem.close();
  }
}

void testVariousAudioSystems()
{
  //Pa_Initialize();

  //playSomething();
  
  //Pa_Terminate();

  /////////////////////////////////////////////////////////////////////////////

  //playSomethingElse();
  
  /////////////////////////////////////////////////////////////////////////////

  //playAudioSystemTest();

  /////////////////////////////////////////////////////////////////////////////
}

///////////////////////////////////////////////////////////////////////////////

int WINAPI WinMain(HINSTANCE _hInstance, HINSTANCE _hInstancePrev, LPSTR _commandLine, int _cmdShow)
{
  Mgl::initConsole();

  memory_globals::init();

  #if _DEBUG
  testVariousAudioSystems();
  #endif

  MglApplication* application = new SkyHighApp;

  if (application)
  {
    MglApplication::InitParams initParams;

    initParams.mHInstance = _hInstance;
    initParams.mWindowX = 5;
    initParams.mWindowY = 5;
    initParams.mWindowWidth  = 800;
    initParams.mWindowHeight = 800;
    initParams.mConsoleX = 810;
    initParams.mConsoleY = 5;
    initParams.mConsoleWidth  = 700;
    initParams.mConsoleHeight = 500;
    initParams.mWindowTitle = TEXT("Sky High");

    if (application->init(initParams))
    {
      application->run();
    }
   
    application->close();
  }

  delete application;
  
  memory_globals::shutdown();
  
  Mgl::closeConsole();

  return 0;
}
