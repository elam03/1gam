#pragma once

#include "SkyHighTypes.h"

#include "MglResourceManager.h"

class BackgroundSystem
{
public:
  BackgroundSystem();
  virtual ~BackgroundSystem();

  bool init();
  void close();

  void render(const Mgl::CameraInfo& _cameraInfo);
  void update(double _updateDelta);

protected:
  Mgl::Texture* mWaterTexture;

  float  mScrollPosition;
  double mScrollSpeed;


};