#pragma once

#include "SkyHighTypes.h"

#include "MglResourceManager.h"

class EntitySystem;

class GuiSystem
{
public:
  GuiSystem();
  virtual ~GuiSystem();

  bool init(EntitySystem* _entitySystem);
  void close();

  void setPlayer(ID _playerId) { mPlayerId = _playerId; }

  void render(const Mgl::CameraInfo& _cameraInfo);
  void update(double _updateDelta);

protected:
  ID mPlayerId;

  EntitySystem* mEntitySystem;

};