#include "GuiSystem.h"

#include "MglGl.h"
#include "MglUtils.h"

using namespace foundation;
using namespace std;
using namespace Mgl;

GuiSystem::GuiSystem()
{
}

GuiSystem::~GuiSystem()
{
}

bool GuiSystem::init()
{
  bool success = true;

  mGlow = 0.0f;

  mPlayerStatusRect = Rect(15.0f, 15.0f, 100.0f, 20.0f);
  mPlayerStatusShow = true;
  memset(&mPlayer, 0, sizeof(Entity));

  mPlayerInventoryRect = Rect(10.0f, 10.0f, 200.0f, 200.0f);
  mPlayerInventoryShow = false;
  memset(&mPlayerInventory, 0, sizeof(Inventory));

  mMessageRect = Rect(15.0f, 50.0f, 1000.0f, 50.0f);
  mMessageShow = false;
  mMessageTimeStampCurr = 0.0;
  mMessageTimeStamp = 0.0;
  mMessageString = L"";

  return (success);
}

void GuiSystem::close()
{
}

void GuiSystem::render()
{
  //float c = (mGlow <= .5f) ? (mGlow * 2.0f) : 1.0f - ((mGlow - .5f) * 2.0f);
  
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  renderPlayerStatus();
  renderMessage();
  renderPlayerInventory();
  
  glDisable(GL_BLEND);
}

void GuiSystem::update(double _updateDelta)
{
  mGlow += (float)(_updateDelta) * 10.0f;

  if (mGlow > 1.0f)
    mGlow = 0.0f;

  if (mMessageShow)
  {
    mMessageTimeStampCurr += _updateDelta * 10;
    if (mMessageTimeStampCurr > mMessageTimeStamp)
      mMessageShow = false;
  }
}

void GuiSystem::renderPlayerStatus()
{
  if (mPlayer.hpMax > 0)
  {
    //glColor4f(c, c, c, .5f);
    //glQuad(mPlayerInventoryRect);

    float hp = float(mPlayer.hp) / mPlayer.hpMax;
    Rect hpRect = mPlayerStatusRect;

    hpRect.w = hp * 100.0f;

    glColor4f(0.8f, 0.8f, 0.8f, .5f);
    glQuadFilled(mPlayerStatusRect);
    glColor4f(0.0f, 1.0f, 0.0f, .5f);
    glQuadFilled(hpRect);
  }
}

void GuiSystem::renderPlayerInventory()
{
  if (mPlayerInventoryShow)
  {
    glColor4f(0.7f, 0.7f, 0.7f, .5f);
    glQuad(15.0f, 15.0f, 400.0f, 400.0f);
  
    char buf[256];

    for (unsigned i = 0; i < ITEM_TYPE_COUNT; i++)
    {
      wstring itemName = toString((ITEM_TYPE)i);
      sprintf_s(buf, 256, "%ws: %i", itemName.c_str(), mPlayerInventory.items[i]);

      glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
      glRasterPos2f(25.0f, 25.0f + i * 25.0f);
      glText(buf);
    }
  }
}

void GuiSystem::renderMessage()
{
  if (mMessageShow)
  {
    glColor3f(1.0f, 1.0f, 0.0f);
    glRasterPos2f(mMessageRect.x, mMessageRect.y);
    glText(toString(mMessageString).c_str());
  }
}

void GuiSystem::pushMessage(wstring _message, double _messageDuration)
{
  mMessageShow = true;
  mMessageString = _message;
  mMessageTimeStamp = _messageDuration;
  mMessageTimeStampCurr = 0.0;
}