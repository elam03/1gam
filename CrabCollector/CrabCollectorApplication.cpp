#include "CrabCollectorApplication.h"

#include "AppStateRunning.h"
#include "MglResourceManager.h"

using namespace foundation;
using namespace Mgl;

CrabCollectorApplication::CrabCollectorApplication() : 
  MglApplication()
{
}

CrabCollectorApplication::~CrabCollectorApplication()
{
}

bool CrabCollectorApplication::appInit()
{
  ResourceManager::Instance().addTexture(L"mountain", L"..\\assets\\cobble_blood1.png");
  ResourceManager::Instance().addTexture(L"hill", L"..\\assets\\cobble_blood10.png");
  ResourceManager::Instance().addTexture(L"grass", L"..\\assets\\grass0.png");
  ResourceManager::Instance().addTexture(L"shore", L"..\\assets\\floor_sand_stone4.png");
  ResourceManager::Instance().addTexture(L"water", L"..\\assets\\dngn_open_sea.png");

  addAppState(new AppStateRunning);

  return (true);
}

void CrabCollectorApplication::appClose()
{
  MglApplication::close();
}

//void CrabCollectorApplication::appWindowRefresh()
//{
//  mAppStates[mAppState]->handleEvent(EVENT_WINDOW_REFRESH);
//}
