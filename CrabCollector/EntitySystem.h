#pragma once

#include "CrabCollectorTypes.h"

#include "MglSystem.h"

class EntitySystem
{
public:
  EntitySystem();
  virtual ~EntitySystem();

  bool init();
  void close();

  ID   add(int _type);
  void remove(ID _idToRemove);
  void clear();
  void render();
  void update(double _updateDelta);

  System<Entity> mSystem;
};