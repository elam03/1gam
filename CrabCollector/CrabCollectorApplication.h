#pragma once

#include "MglApplication.h"

class CrabCollectorApplication : public MglApplication
{
public:
  CrabCollectorApplication();
  virtual ~CrabCollectorApplication();
  
protected:

  virtual bool appInit();
  virtual void appClose();
  //virtual void appWindowRefresh();

};