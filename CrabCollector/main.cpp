#include <iostream>
#include <windows.h>
#include <windowsx.h>

#include "MglGl.h"
#include "MglUtils.h"

#include "CrabCollectorApplication.h"

#include "memory.h"

using namespace foundation;

int WINAPI WinMain(HINSTANCE _hInstance, HINSTANCE _hInstancePrev, LPSTR _commandLine, int _cmdShow)
{
  Mgl::initConsole();

  memory_globals::init();

  MglApplication* application = new CrabCollectorApplication;

  if (application)
  {
    MglApplication::InitParams initParams;

    initParams.mHInstance = _hInstance;
    initParams.mWindowX = 50;
    initParams.mWindowY = 50;
    initParams.mWindowWidth  = 1000;
    initParams.mWindowHeight = 500;
    initParams.mWindowTitle = TEXT("CrabCollector");
    initParams.mConsoleEnabled = true;

    if (application->init(initParams))
    {
      application->run();
    }
   
    application->close();
  }

  delete application;
  
  memory_globals::shutdown();

  Mgl::closeConsole();

  return 0;
}
