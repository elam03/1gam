#pragma once

#include "MglTypes.h"

enum CELL_TYPE
{
  CELL_TYPE_NONE,
  CELL_TYPE_MOUNTAIN,
  CELL_TYPE_HILL,
  CELL_TYPE_GRASS,
  CELL_TYPE_SHORE,
  CELL_TYPE_WATER,
  CELL_TYPE_COUNT
};

enum ENTITY_TYPE
{
  ENTITY_TYPE_CRAB,
  ENTITY_TYPE_PLAYER,
  ENTITY_TYPE_COUNT
};

enum ITEM_TYPE
{
  ITEM_TYPE_SHELL,
  ITEM_TYPE_LEG,
  ITEM_TYPE_MINOR_POTION,
  ITEM_TYPE_MAJOR_POTION,
  ITEM_TYPE_COUNT
};

enum DIR_TYPE
{
  DIR_LEFT,
  DIR_UP,
  DIR_RIGHT,
  DIR_DOWN,
  DIR_COUNT
};

static const unsigned MAX_ENTITIES_PER_CELL = 4;
static const unsigned MAX_ITEMS_PER_CELL = 16;

struct CellData
{
  int type;
  int itemCount;
  int items[MAX_ITEMS_PER_CELL];
  int entityCount;
  int entityIds[MAX_ENTITIES_PER_CELL]; // Who's in the cell? Don't know if I really need to know if there are that many in a cell.
};

struct Entity
{
  int id;   // ID
  int type; // Entity Type

  int cx; // Cell X
  int cy; // Cell Y
  
  int hp; // Health
  int hpMax; // Health Max

  int damage; // The damage this entity does
};

struct Inventory
{
  int items[ITEM_TYPE_COUNT];
};

static std::wstring toString(ENTITY_TYPE _type)
{
  std::wstring s;

  switch (_type)
  {
    default: s = L""; break;
    case ENTITY_TYPE_CRAB:   s = L"Crab"; break;
    case ENTITY_TYPE_PLAYER: s = L"Player"; break;
  }

  return (s);
}

static std::wstring toString(ITEM_TYPE _type)
{
  std::wstring s;

  switch (_type)
  {
    default: s = L""; break;
    case ITEM_TYPE_SHELL:          s = L"Shell"; break;
    case ITEM_TYPE_LEG:            s = L"Leg"; break;
    case ITEM_TYPE_MINOR_POTION:   s = L"Minor Potion"; break;
    case ITEM_TYPE_MAJOR_POTION:   s = L"Major Potion"; break;
  }

  return (s);
}

