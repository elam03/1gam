#include "EntitySystem.h"

#include "MglUtils.h"
#include "MglGl.h"
#include "memory.h"

using namespace foundation;
using namespace std;
using namespace Mgl;

EntitySystem::EntitySystem() : 
  mSystem()
{
}

EntitySystem::~EntitySystem()
{
}

bool EntitySystem::init()
{
  bool success = true;
  
  mSystem.removeAll();

  return (success);
}

void EntitySystem::close()
{
  mSystem.removeAll();
}

ID EntitySystem::add(int _type)
{
  ID id = mSystem.add();

  if (mSystem.has(id))
  {
    Entity& entity = mSystem.lookup(id);
    memset(&entity, 0, sizeof(Entity));

    entity.id = id;
    entity.type = _type;

    switch (_type)
    {
      case ENTITY_TYPE_CRAB:
      {
        entity.hp = entity.hpMax = random(20, 30);
        entity.damage = 2;
      } break;

      case ENTITY_TYPE_PLAYER:
      {
        entity.hp = entity.hpMax = 100;
        entity.damage = 5;
      } break;
    }
  }

  return (id);
}

void EntitySystem::remove(ID _idToRemove)
{
}

void EntitySystem::clear()
{
}

void EntitySystem::render()
{
}

void EntitySystem::update(double _updateDelta)
{
  //for (unsigned i = 0; i < mSystem._num_objects; i++)
  //{
  //  Entity& entity = mSystem._objects[i];
  //}
}