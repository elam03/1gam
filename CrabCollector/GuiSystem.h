#pragma once

#include "CrabCollectorTypes.h"
#include "MglUtils.h"

class GuiSystem
{
public:
  GuiSystem();
  virtual ~GuiSystem();

  bool init();
  void close();

  void render();
  void update(double _updateDelta);

  float mGlow;

  void renderPlayerStatus();
  Mgl::Rect mPlayerStatusRect;
  bool      mPlayerStatusShow;
  Entity    mPlayer;

  void renderPlayerInventory();
  Mgl::Rect mPlayerInventoryRect;
  bool      mPlayerInventoryShow;
  Inventory mPlayerInventory;

  void renderMessage();
  void pushMessage(std::wstring _message, double _messageDuration);

  Mgl::Rect    mMessageRect;
  bool         mMessageShow;
  double       mMessageTimeStampCurr;
  double       mMessageTimeStamp;
  std::wstring mMessageString;
};