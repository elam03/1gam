#pragma once

#include "memory.h"
#include "CrabCollectorTypes.h"

class EntitySystem;

class WorldSystem
{
public:
  WorldSystem();
  virtual ~WorldSystem();

  bool init(EntitySystem* _entitySystem, unsigned _width, unsigned _height);
  void close();

  void generate();
  void generateCrabs();
  ID   generatePlayer();

  void render();
  void update(double _deltaTime);

  CellData& lookup(unsigned _x, unsigned _y);
  bool      has(unsigned _x, unsigned _y);
  
  inline unsigned getWorldSize() const { return (sizeof(CellData) * mWidth * mHeight); }

  inline void setCameraX(int _offset) { mCameraOffsetX = _offset; }
  inline int  getCameraX() const { return (mCameraOffsetX); }
  inline void setCameraY(int _offset) { mCameraOffsetY = _offset; }
  inline int  getCameraY() const { return (mCameraOffsetY); }

  inline void setScrollX(int _offset) { mScrollX = _offset; }
  inline int  getScrollX() const { return (mScrollX); }
  inline void setScrollY(int _offset) { mScrollY = _offset; }
  inline int  getScrollY() const { return (mScrollY); }

  void moveCamera(int _dir);

  bool isVisible(int _cx, int _cy); 

  bool attackEntity(ID _entityId, ID _targetId);
  bool moveEntity(ID _id, int _dir);
  ID   getEntityFromCell(int _cx, int _cy);
  ID   getEntityFromCell(ID _entityId, int _dir);

  bool removeEntityIdFromCell(ID _entityIdToRemove, int _cx, int _cy);
  bool addItem(int _cx, int _cy, ITEM_TYPE _item);
  bool clearItems(int _cx, int _cy);

protected:
  void drawCell(CellData& _cellData, unsigned _cellX, unsigned _cellY);
  void randomlyShiftBoundry(unsigned& _y, unsigned _lowerBound, unsigned _upperBound);
  
  CellData* mWorld;
  unsigned  mWidth;
  unsigned  mHeight;

  unsigned mCellWidth;
  unsigned mCellHeight;

  int mCameraOffsetX;
  int mCameraOffsetY;

  int mScrollX;
  int mScrollY;
  int mScrollW;
  int mScrollH;

  EntitySystem* mEntitySystem;
};