#include "AppStateRunning.h"

#include "CrabCollectorApplication.h"

#include "MglUtils.h"
#include "MglTypes.h"

#include "MglGl.h"

using namespace foundation;
using namespace std;
using namespace Mgl;

AppStateRunning::AppStateRunning() : 
  AppState(),
  
  mEntitySystem(),
  mGuiSystem(),
  mWorldSystem()
{
}

AppStateRunning::~AppStateRunning()
{
}

bool AppStateRunning::handleEvent(int _event, void* _eventData)
{
  switch (_event)
  {
    case EVENT_INIT:
    {
      debugPrint(0, "EVENT_INIT");

      mUpdateRate = 2.0 / 1000.0;
      mUpdateRateCurr = 0.0;

      mEntitySystem.init();
      mWorldSystem.init(&mEntitySystem, 25, 100);
      mGuiSystem.init();

      mWorldSystem.setCameraX(-15);
      mWorldSystem.setCameraY(-4);

      mGameOver = false;
    } break;
    
    case EVENT_SHOW:
    {
      debugPrint(0, "EVENT_SHOW");

      mWorldSystem.generate();
      mWorldSystem.generateCrabs();

      mPlayerId = mWorldSystem.generatePlayer();
      memset(&mPlayerInventory, 0, sizeof(Inventory));

      mGameOver = false;
    } break;

    case EVENT_CLOSE:
    {
      debugPrint(0, "EVENT_CLOSE");
      
      mWorldSystem.close();
      mEntitySystem.close();
      mGuiSystem.close();
    } break;

    case EVENT_WINDOW_REFRESH:
    {
      debugPrint(0, "EVENT_WINDOW_REFRESH");
    } break;

    case EVENT_MOUSE_INPUT:
    {
      EventDataMouseInput& eventData = *(EventDataMouseInput *)_eventData;
    } break;
    
    case EVENT_KEY_INPUT:
    {
      EventDataKeyInput& eventData = *(EventDataKeyInput *)_eventData;

      if (eventData.down)
      {
        switch (eventData.key)
        {
          case VK_LEFT:
          {
            Mgl::debugPrint(0, "VK_LEFT");
            if (GetKeyState(VK_CONTROL) < 0)
              mWorldSystem.setCameraX(mWorldSystem.getCameraX() - 1);
            else
              mWorldSystem.setScrollX(mWorldSystem.getScrollX() - 1);
          } break;

          case VK_UP:
          {
            Mgl::debugPrint(0, "VK_UP");
            if (GetKeyState(VK_CONTROL) < 0)
              mWorldSystem.setCameraY(mWorldSystem.getCameraY() + 1);
            else
              mWorldSystem.setScrollY(mWorldSystem.getScrollY() + 1);
          } break;

          case VK_RIGHT:
          {
            Mgl::debugPrint(0, "VK_RIGHT");
            if (GetKeyState(VK_CONTROL) < 0)
              mWorldSystem.setCameraX(mWorldSystem.getCameraX() + 1);
            else
              mWorldSystem.setScrollX(mWorldSystem.getScrollX() + 1);
          } break;

          case VK_DOWN:
          {
            Mgl::debugPrint(0, "VK_DOWN");
            if (GetKeyState(VK_CONTROL) < 0)
              mWorldSystem.setCameraY(mWorldSystem.getCameraY() - 1);
            else
              mWorldSystem.setScrollY(mWorldSystem.getScrollY() - 1);
          } break;

          case 'W':
          {
            Mgl::debugPrint(0, "W");
            playerAttack(DIR_UP);
          } break;

          case 'S':
          {
            Mgl::debugPrint(0, "S");
            playerAttack(DIR_DOWN);
          } break;

          case 'A':
          {
            Mgl::debugPrint(0, "A");
            playerAttack(DIR_LEFT);
          } break;

          case 'D':
          {
            Mgl::debugPrint(0, "D");
            playerAttack(DIR_RIGHT);
          } break;

          case 'I':
          {
            Mgl::debugPrint(0, "I");
            mGuiSystem.mPlayerInventoryShow = !mGuiSystem.mPlayerInventoryShow;
          } break;

          case ' ':
          {
            Mgl::debugPrint(0, "SPACE");
            playerPickUpItems();
          } break;

          case 'M':
          {
            Mgl::debugPrint(0, "M");
            mGuiSystem.pushMessage(L"MESSAGE!", 3.0);
          } break;
        }
      }
    } break;
  }

  return false;
}

void AppStateRunning::render()
{
  double w = (double)mApplication->mWindowWidth;
  double h = (double)mApplication->mWindowHeight;

  mApplication->renderBorder();

  if (mGameOver)
  {
    glRasterPos2f(50.0f, 50.0f);
    glColor3f(1.0f, 1.0f, 1.0f);
    glText("Game Over!");
  }
  else
  {
    mWorldSystem.render();
    mEntitySystem.render();
    mGuiSystem.render();
  }
}

void AppStateRunning::update(double _deltaTime)
{
  if (!mGameOver)
  {
    mUpdateRateCurr += _deltaTime;

    if (mUpdateRateCurr > mUpdateRate)
    {
      //debugPrint(0, "TICK");
      mUpdateRateCurr -= mUpdateRate;
      mGuiSystem.mPlayer = mEntitySystem.mSystem.lookup(mPlayerId);
      mGuiSystem.mPlayerInventory = mPlayerInventory;
    }

    mWorldSystem.update(_deltaTime);
    mEntitySystem.update(_deltaTime);
    mGuiSystem.update(_deltaTime);
  }
}

void AppStateRunning::playerAttack(int _dir)
{
  if (mEntitySystem.mSystem.has(mPlayerId))
  {
    Entity& player = mEntitySystem.mSystem.lookup(mPlayerId);

    if (!mWorldSystem.moveEntity(mPlayerId, _dir))
    {
      ID targetId = mWorldSystem.getEntityFromCell(mPlayerId, _dir);

      if (targetId != ID_INVALID)
      {
        if (mWorldSystem.attackEntity(mPlayerId, targetId))
        {
          Entity& target = mEntitySystem.mSystem.lookup(targetId);

          char buf[256];
          sprintf_s(buf, 256, "%s hits %s for %i damage! %s attacks back for %i", toString((ENTITY_TYPE)player.type).c_str(), toString((ENTITY_TYPE)target.type).c_str(), player.damage, toString((ENTITY_TYPE)target.type).c_str(), target.damage);

          mGuiSystem.pushMessage(toWstring(buf), 1.0);
      
          if (target.hp < 0)
          {
            CellData& cell = mWorldSystem.lookup(target.cx, target.cy);

            mWorldSystem.removeEntityIdFromCell(target.id, target.cx, target.cy);

            sprintf_s(buf, 256, "%s kills %s!", toString((ENTITY_TYPE)player.type).c_str(), toString((ENTITY_TYPE)target.type).c_str());
            mGuiSystem.pushMessage(toWstring(buf), 1.0);

            unsigned numShells = random(0, 2);
            for (unsigned i = 0; i < numShells; i++)
              mWorldSystem.addItem(target.cx, target.cy, ITEM_TYPE_SHELL);

            unsigned numLegs = random(1, 6);
            for (unsigned i = 0; i < numLegs; i++)
              mWorldSystem.addItem(target.cx, target.cy, ITEM_TYPE_LEG);
          }

          if (player.hp < 0)
            mGameOver = true;
        }
      }
    }
    else
    {
      // Player has moved check if we need to scroll.
      int cx = player.cx;
      int cy = player.cy;
      
      if (mWorldSystem.has(cx, cy))
      {
        if (!mWorldSystem.isVisible(cx, cy))
        {
          mWorldSystem.moveCamera(_dir);
        }
      }
    }
  }
}

void AppStateRunning::playerPickUpItems()
{
  if (mEntitySystem.mSystem.has(mPlayerId))
  {
    Entity& player = mEntitySystem.mSystem.lookup(mPlayerId);

    if (mWorldSystem.has(player.cx, player.cy))
    {
      CellData& cell = mWorldSystem.lookup(player.cx, player.cy);

      if (cell.itemCount > 0)
      {
        char buf[256];
        sprintf_s(buf, 256, "You picked up %i things!", cell.itemCount);
        mGuiSystem.pushMessage(toWstring(buf), 1.0);

        for (unsigned i = 0; i < (unsigned)cell.itemCount; i++)
        {
          mPlayerInventory.items[cell.items[i]]++;
        }

        mWorldSystem.clearItems(player.cx, player.cy);
      }
      else
      {
        char buf[256];
        sprintf_s(buf, 256, "");
        mGuiSystem.pushMessage(L"No items to pick up!", 1.0);
      }
    }
  }
}