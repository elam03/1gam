#pragma once

#include "MglAppState.h"

#include "EntitySystem.h"
#include "GuiSystem.h"
#include "MglUtils.h"
#include "WorldSystem.h"

class AppStateRunning : public AppState
{
public:
  AppStateRunning();
  virtual ~AppStateRunning();

  virtual bool handleEvent(int _event, void* _eventData = 0);
  virtual void render();
  virtual void update(double _deltaTime);

protected:

  void playerAttack(int _dir);
  void playerPickUpItems();

  EntitySystem mEntitySystem;
  GuiSystem    mGuiSystem;
  WorldSystem  mWorldSystem;

  ID           mPlayerId;
  Inventory    mPlayerInventory;

  double mUpdateRate;
  double mUpdateRateCurr;

  bool mGameOver;
};