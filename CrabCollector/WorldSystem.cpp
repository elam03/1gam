#include "WorldSystem.h"

#include "EntitySystem.h"
#include "MglGl.h"
#include "MglResourceManager.h"
#include "MglUtils.h"
#include <map>

using namespace foundation;
using namespace std;
using namespace Mgl;

WorldSystem::WorldSystem() :
  mWorld(0),
  mWidth(0),
  mHeight(0),
  mCellWidth(0),
  mCellHeight(0),

  mCameraOffsetX(0),
  mCameraOffsetY(0),

  mScrollX(0),
  mScrollY(0),
  mScrollW(0),
  mScrollH(0),

  mEntitySystem(0)
{
}

WorldSystem::~WorldSystem()
{

}

bool WorldSystem::init(EntitySystem* _entitySystem, unsigned _width, unsigned _height)
{
  bool success = false;

  if (!mWorld)
  {
    mEntitySystem = _entitySystem;

    mCellWidth  = 20;
    mCellHeight = 20;

    mWidth  = _width;
    mHeight = _height;
    mWorld = (CellData *)memory_globals::default_allocator().allocate(getWorldSize());

    memset(mWorld, 0, getWorldSize());

    mScrollX = 0;
    mScrollY = 0;
    mScrollW = 8;
    mScrollH = 8;

    success = true;
  }

  return (success);
}

void WorldSystem::close()
{
  if (mWorld)
  {
    memory_globals::default_allocator().deallocate(mWorld);

    mWorld = 0;
    mWidth = 0;
    mHeight = 0;
  }
}

void WorldSystem::generate()
{
  memset(mWorld, CELL_TYPE_NONE, getWorldSize());

  // Boundry: 0   1   2   3 4  5
  //           MMM HHH GGG S WWW

  unsigned boundry[CELL_TYPE_COUNT];

  boundry[0] = 0;       // LEFT SIDE
  boundry[1] = 3;       // CELL_TYPE_MOUNTAIN
  boundry[2] = 6;       // CELL_TYPE_HILL
  boundry[3] = 9;       // CELL_TYPE_GRASS      
  boundry[4] = 12;      // CELL_TYPE_WATER
  boundry[5] = mHeight; // RIGHT SIDE

  for (unsigned y = 0; y < mHeight; y++)
  {
    for (unsigned x = 0; x < mWidth; x++)
    {
      CellData& cell = lookup(x, y);

      if (x < boundry[1])      cell.type = CELL_TYPE_MOUNTAIN;
      else if (x < boundry[2]) cell.type = CELL_TYPE_HILL;
      else if (x < boundry[3]) cell.type = CELL_TYPE_GRASS;
      else if (x < boundry[4]) cell.type = CELL_TYPE_SHORE;
      else                     cell.type = CELL_TYPE_WATER;
    }

    for (unsigned i = 1; i < 5; i++)
      randomlyShiftBoundry(boundry[i], boundry[i - 1], boundry[i + 1]);
  }

  mScrollX = mScrollY = 0;
}

void WorldSystem::generateCrabs()
{
  for (unsigned y = 0; y < mHeight; y++)
  {
    for (unsigned x = 0; x < mWidth; x++)
    {
      CellData& cell = lookup(x, y);

      memset(&cell.entityIds, 0, sizeof(unsigned) * 4);

      if (random(0, 10) == 0 && cell.type == CELL_TYPE_SHORE)
      {
        ID id = mEntitySystem->add(ENTITY_TYPE_CRAB);

        cell.entityIds[cell.entityCount++] = id;

        if (mEntitySystem->mSystem.has(id))
        {
          Entity& entity = mEntitySystem->mSystem.lookup(id);

          entity.cx = x;
          entity.cy = y;

          //debugPrint(0, "entity: %i hp: %i / %i", entity.id, entity.hp, entity.hpMax);
        }
      }
    }
  }
}

ID WorldSystem::generatePlayer()
{
  ID id = ID_INVALID;
  unsigned x = 0;
  unsigned y = 0;

  for (x = 0; x < mWidth; x++)
  {
    CellData& cell = lookup(x, y);

    if (cell.type == CELL_TYPE_GRASS)
    {
      id = mEntitySystem->add(ENTITY_TYPE_PLAYER);
    
      cell.entityIds[cell.entityCount++] = id;

      if (mEntitySystem->mSystem.has(id))
      {
        Entity& entity = mEntitySystem->mSystem.lookup(id);

        entity.cx = x;
        entity.cy = y;
      }   
      break;
    }
  }

  return (id);
}

void drawItems(float _x, float _y)
{
  glColor3f(0.8f, 0.2f, 0.0f);
  glBegin(GL_POINTS);
    glPointSize(2.0f);
    glVertex2f(_x + 3.0f, _y);
    glPointSize(4.0f);
    glVertex2f(_x - 2.0f, _y + 2);
    glPointSize(6.0f);
    glVertex2f(_x + 1.0f, _y -3);
  glEnd();
}

void drawCrab(float _x, float _y)
{
  glColor3f(1.0f, 0.0f, 0.0f);
  glPointSize(5.0f);
  glBegin(GL_POINTS);
    glVertex2f(_x, _y);
  glEnd();
        
  glBegin(GL_LINES);
    glVertex2f(_x - 5.0f, _y);
    glVertex2f(_x + 5.0f, _y);

    glVertex2f(_x - 5.0f, _y + 3.0f);
    glVertex2f(_x + 5.0f, _y - 3.0f);

    glVertex2f(_x - 5.0f, _y - 3.0f);
    glVertex2f(_x + 5.0f, _y + 3.0f);
  glEnd();
}

void drawPlayer(float _x, float _y)
{
  glColor3f(1.0f, 1.0f, 1.0f);
  glPointSize(5.0f);
  glBegin(GL_POINTS);
    glVertex2f(_x, _y);
  glEnd();
}

void WorldSystem::render()
{
  int offsetx = -mCameraOffsetX;
  int offsety = -mCameraOffsetY;
  
  glEnable(GL_TEXTURE_2D);

  glPushMatrix();
  glTranslatef((float)offsetx * mCellWidth, (float)offsety * mCellHeight, 0.0f);

  for (int j = 0; j < mScrollH; j++)
  {
    for (int i = 0; i < mScrollW; i++)
    {
      int cx = i + mScrollX;
      int cy = j + mScrollY;

      if (has(cx, cy))
      {
        CellData& cell = lookup(cx, cy);
        
        Texture* texture = NULL;

        switch (cell.type)
        {
          case CELL_TYPE_NONE:     texture = ResourceManager::Instance().getTexture(L""); break;
          case CELL_TYPE_WATER:    texture = ResourceManager::Instance().getTexture(L"water"); break;
          case CELL_TYPE_SHORE:    texture = ResourceManager::Instance().getTexture(L"shore"); break;
          case CELL_TYPE_GRASS:    texture = ResourceManager::Instance().getTexture(L"grass"); break;
          case CELL_TYPE_HILL:     texture = ResourceManager::Instance().getTexture(L"hill"); break;
          case CELL_TYPE_MOUNTAIN: texture = ResourceManager::Instance().getTexture(L"mountain"); break;
          default:                 texture = ResourceManager::Instance().getTexture(L""); break;
        }

        int x = int((i) * mCellWidth);
        int y = int((j) * mCellHeight);

        if (texture)
        {
          texture->bind();

          glColor3f(1.0f, 1.0f, 1.0f);

          glBegin(GL_QUADS);
            glTexCoord2f(0.0f, 0.0f);   glVertex2i(x, y);
            glTexCoord2f(1.0f, 0.0f);   glVertex2i(x + mCellWidth, y);
            glTexCoord2f(1.0f, 1.0f);   glVertex2i(x + mCellWidth, y + mCellHeight);
            glTexCoord2f(0.0f, 1.0f);   glVertex2i(x, y + mCellHeight);
          glEnd();
          
          texture->unbind();
        }
        else
        {
          switch (cell.type)
          {
            case CELL_TYPE_NONE:     glColor3f(0.2f, 0.2f, 0.2f); break;
            case CELL_TYPE_WATER:    glColor3f(0.0f, 0.0f, 1.0f); break;
            case CELL_TYPE_SHORE:    glColor3f(0.9f, 0.9f, 0.0f); break;
            case CELL_TYPE_GRASS:    glColor3f(0.0f, 1.0f, 0.0f); break;
            case CELL_TYPE_HILL:     glColor3f(0.8f, 0.6f, 0.4f); break;
            case CELL_TYPE_MOUNTAIN: glColor3f(0.5f, 0.4f, 0.3f); break;
            default:                 glColor3f(0.2f, 0.2f, 0.2f); break;
          }

          glBegin(GL_QUADS);
            glVertex2i(x, y);
            glVertex2i(x + mCellWidth, y);
            glVertex2i(x + mCellWidth, y + mCellHeight);
            glVertex2i(x, y + mCellHeight);
          glEnd();
        }

        glColor3f(1.0f, 1.0f, 1.0f);
        glBegin(GL_LINE_LOOP);
          glVertex2i(x, y);
          glVertex2i(x + mCellWidth, y);
          glVertex2i(x + mCellWidth, y + mCellHeight);
          glVertex2i(x, y + mCellHeight);
        glEnd();

        if (cell.itemCount > 0)
        {
          for (int i = 0; i < cell.itemCount; i++)
          {
            drawItems((float)x + mCellWidth / 2, (float)y + mCellHeight / 2);
          }
        }

        if (cell.entityCount > 0)
        {
          for (int i = 0; i < cell.entityCount; i++)
          {
            if (mEntitySystem->mSystem.has(cell.entityIds[i]))
            {
              Entity& entity = mEntitySystem->mSystem.lookup(cell.entityIds[i]);
              if (entity.type == ENTITY_TYPE_CRAB)
              {
                drawCrab((float)x + mCellWidth / 2, (float)y + mCellHeight / 2);
              }
              else if (entity.type == ENTITY_TYPE_PLAYER)
              {
                drawPlayer((float)x + mCellWidth / 2, (float)y + mCellHeight / 2);
              }
            }
          }
        }
      }
    }
  }

  glPopMatrix();
}

void WorldSystem::update(double _deltaTime)
{
}

CellData& WorldSystem::lookup(unsigned _x, unsigned _y)
{
  return mWorld[_y * mWidth + _x];
}

bool WorldSystem::has(unsigned _x, unsigned _y)
{
  return (_x >= 0 && _x < mWidth && _y >= 0 && _y < mHeight);
}

void WorldSystem::moveCamera(int _dir)
{
  switch (_dir)
  {
    case DIR_LEFT:  mScrollX--; break;
    case DIR_RIGHT: mScrollX++; break;
    case DIR_DOWN:  mScrollY--; break;
    case DIR_UP:    mScrollY++; break;
  }
}

bool WorldSystem::isVisible(int _cx, int _cy)
{
  bool visible = (_cx >= (mScrollX + 1) && _cx < (mScrollX + mScrollW - 1) && _cy >= (mScrollY + 1) && _cy < (mScrollY + mScrollH - 1));

  return (visible);
}

bool WorldSystem::attackEntity(ID _entityId, ID _targetId)
{
  bool attacked = false;

  if (mEntitySystem->mSystem.has(_entityId))
  {
    Entity& entity = mEntitySystem->mSystem.lookup(_entityId);

    if (mEntitySystem->mSystem.has(_targetId))
    {
      Entity& target = mEntitySystem->mSystem.lookup(_targetId);

      target.hp -= entity.damage;
      entity.hp -= target.damage;

      attacked = true;
    }
  }

  return (attacked);
}

bool WorldSystem::moveEntity(ID _id, int _dir)
{
  bool moved = false;

  if (mEntitySystem->mSystem.has(_id))
  {
    Entity& entity = mEntitySystem->mSystem.lookup(_id);

    int targetCellX = entity.cx;
    int targetCellY = entity.cy;

    if (_dir == DIR_LEFT)  targetCellX--;
    if (_dir == DIR_UP)    targetCellY++;
    if (_dir == DIR_RIGHT) targetCellX++;
    if (_dir == DIR_DOWN)  targetCellY--;

    // Make sure we're not moving totally out of bounds
    if (has(targetCellX, targetCellY))
    {
      CellData& targetCell = lookup(targetCellX, targetCellY);
      CellData& cell = lookup(entity.cx, entity.cy);

      if (targetCell.type != CELL_TYPE_WATER)
      {
        // Make sure there's space to move into the cell.
        if (targetCell.entityCount == 0)
        {
          removeEntityIdFromCell(_id, entity.cx, entity.cy);

          // Add to the cell target
          targetCell.entityIds[targetCell.entityCount++] = entity.id;

          // Update the cell to know which cell it's in
          entity.cx = targetCellX;
          entity.cy = targetCellY;

          moved = true;
        }
      }
    }
  }

  return (moved);
}

ID WorldSystem::getEntityFromCell(int _cx, int _cy)
{
  ID id = ID_INVALID;

  if (has(_cx, _cy))
  {
    CellData& cell = lookup(_cx, _cy);

    if (cell.entityCount > 0)
    {
      // Grab the first entity in the cell. For now.
      id = cell.entityIds[0];
    }
  }

  return (id);
}

ID WorldSystem::getEntityFromCell(ID _id, int _dir)
{
  ID id = ID_INVALID;

  if (mEntitySystem->mSystem.has(_id))
  {
    Entity& entity = mEntitySystem->mSystem.lookup(_id);

    int targetCellX = entity.cx;
    int targetCellY = entity.cy;

    if (_dir == DIR_LEFT)  targetCellX--;
    if (_dir == DIR_UP)    targetCellY++;
    if (_dir == DIR_RIGHT) targetCellX++;
    if (_dir == DIR_DOWN)  targetCellY--;

    id = getEntityFromCell(targetCellX, targetCellY);
  }

  return (id);
}

bool WorldSystem::removeEntityIdFromCell(ID _entityIdToRemove, int _cx, int _cy)
{
  bool removed = false;

  if (has(_cx, _cy))
  {
    CellData& cell = lookup(_cx, _cy);

    // Remove from id that the cell has
    bool found = false;

    for (int i = 0; i < cell.entityCount; i++)
    {
      if (cell.entityIds[i] == _entityIdToRemove)
      {
        found = true;
        cell.entityIds[i] = cell.entityIds[cell.entityCount - 1];
        break;
      }
    }

    if (found)
    {
      cell.entityCount--;
      removed = true;
    }
  }

  return (removed);
}

bool WorldSystem::addItem(int _cx, int _cy, ITEM_TYPE _item)
{
  bool added = false;
  
  if (has(_cx, _cy))
  {
    CellData& cell = lookup(_cx, _cy);

    if (cell.itemCount < MAX_ITEMS_PER_CELL)
    {
      cell.items[cell.itemCount++] = _item;

      added = true;
    }
  }

  return (added);
}

bool WorldSystem::clearItems(int _cx, int _cy)
{
  bool cleared = false;

  if (has(_cx, _cy))
  {
    CellData& cell = lookup(_cx, _cy);

    cell.itemCount = 0;
    memset(cell.items, 0, sizeof(int) * MAX_ITEMS_PER_CELL);
  }

  return (cleared);
}

void WorldSystem::drawCell(CellData& _cellData, unsigned _cellX, unsigned _cellY)
{
}

void WorldSystem::randomlyShiftBoundry(unsigned& _y, unsigned _lowerBound, unsigned _upperBound)
{
  int action = random(0, 10);
  
  if (action == 0 && _y < (_upperBound - 1)) _y++;
  if (action == 1 && _y > (_lowerBound + 1)) _y--;
}
