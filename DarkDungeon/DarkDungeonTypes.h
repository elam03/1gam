#pragma once

#include "MglTypes.h"

static const int CellWidth  = 16;
static const int CellHeight = 16;

enum CELL_TYPE
{
  CELL_EMPTY,
  CELL_WALL,
  CELL_BRICK,
  CELL_WATER,
  CELL_BRIDGE,
  CELL_COUNT
};

static const int ENTITY_FLAG_PLAYER      = 0x00000001;
static const int ENTITY_FLAG_CHEST       = 0x00000002;
static const int ENTITY_FLAG_EMPTY_CHEST = 0x00000004;

struct Cell
{
  Cell() { type = CELL_EMPTY; traversable = false; entityFlags = 0; goalPath = false; }
  ~Cell() {}

  int  type;
  bool traversable;
  int  entityFlags;
  bool goalPath;
};

struct Room
{
  Room() { x = y = w = h = 0; }
  ~Room() {}
  int x;
  int y;
  int w;
  int h;
};

struct Point2i
{
  Point2i() { x = y = 0; }
  Point2i(int _x, int _y) : x(_x), y(_y) {}
  int x;
  int y;

  Point2i operator-(const Point2i& _rhs) { return (Point2i(x - _rhs.x, y - _rhs.y)); }
  Point2i operator-() { return Point2i(-x, -y); }
  void set(int _x, int _y) { x = _x; y = _y; }
};

struct Point2f
{
  Point2f() { x = y = 0.0f; }
  Point2f(float _x, float _y) : x(_x), y(_y) {}
  float x;
  float y;
  
  Point2f operator-(const Point2f& _rhs) { return (Point2f(x - _rhs.x, y - _rhs.y)); }
  Point2f operator-() { return Point2f(-x, -y); }
  void set(float _x, float _y) { x = _x; y = _y; }
};

enum ENTITY_TYPE
{
  ENTITY_TYPE_NONE,
  ENTITY_TYPE_PLAYER,
  ENTITY_TYPE_MONSTER,
  ENTITY_TYPE_COUNT
};

struct Entity
{
  Entity() { id = ID_INVALID; x = y = 0; type = ENTITY_TYPE_NONE; health = healthMax = 0.0f; }
  ~Entity() {}

  ID  id;
  int x;
  int y;
  int type;

  float health;
  float healthMax;
};