#pragma once

#include "MglApplication.h"

#include "Dungeon.h"
#include "EntitySystem.h"

class AppState;
class ApplicationEvent;

class DarkDungeonApp : public MglApplication
{
public:
  DarkDungeonApp();
  virtual ~DarkDungeonApp();

protected:
  virtual bool appInit();
  virtual void appClose();

public:
  virtual bool defaultHandleEvent(int _event, void* _eventData = 0);
  virtual void defaultRender();
  virtual void defaultUpdate(double _deltaTime);

protected:
  enum
  {
    STATE_RUNNING,
    STATE_GAME_OVER,
    STATE_WIN,
    STATE_COUNT
  };

  Mgl::CameraInfo mCameraInfo;

  Dungeon      mDungeon;
  EntitySystem mEntitySystem;

  ID mPlayerId;

  int mGameState;
  int mGameFloor;

  bool moveEntity(Entity& _entity, int _dx, int _dy);
  bool hasEntityReachedGoal(Entity& _entity);
};