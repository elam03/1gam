﻿#include "DarkDungeonApp.h"

#include "MglGl.h"
#include "MglResourceManager.h"
#include "MglUtils.h"

using namespace std;
using namespace foundation;
using namespace Mgl;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////


DarkDungeonApp::DarkDungeonApp() :
  MglApplication(),
  mDungeon(),
  mEntitySystem(),

  mPlayerId(ID_INVALID),
  
  mGameState(STATE_GAME_OVER),
  mGameFloor(0)
{
}

DarkDungeonApp::~DarkDungeonApp()
{
}

bool DarkDungeonApp::appInit()
{
  bool success = false;

  mUpdatesPerSecond = 120;

  //ResourceManager::Instance().addTexture(L"particle1", L"..\\assets\\cloud_mutagenic_large1.png");
  //ResourceManager::Instance().addTexture(L"particle2", L"..\\assets\\cloud_mutagenic_large2.png");
  //ResourceManager::Instance().addTexture(L"particle3", L"..\\assets\\cloud_mutagenic_large3.png");
  //ResourceManager::Instance().addTexture(L"particle4", L"..\\assets\\cloud_mutagenic_large4.png");
  //ResourceManager::Instance().addTexture(L"fireball", L"..\\assets\\fireball.png");
  //ResourceManager::Instance().addTexture(L"grass", L"..\\assets\\grass0.png");
  
  ResourceManager::Instance().addTexture(L"empty", L"..\\assets\\undead0.png");
  ResourceManager::Instance().addTexture(L"floor", L"..\\assets\\floor_vines0.png");
  ResourceManager::Instance().addTexture(L"wall", L"..\\assets\\stone2_gray0.png");
  ResourceManager::Instance().addTexture(L"brick", L"..\\assets\\brick_gray0.png");
  ResourceManager::Instance().addTexture(L"water", L"..\\assets\\dngn_open_sea.png");
  ResourceManager::Instance().addTexture(L"bridge", L"..\\assets\\grate.png");
  
  ResourceManager::Instance().addTexture(L"player", L"..\\assets\\dragon_form.png");
  ResourceManager::Instance().addTexture(L"monster0", L"..\\assets\\bat_form.png");
  ResourceManager::Instance().addTexture(L"monster1", L"..\\assets\\dragon_form.png");
  
  //ResourceManager::Instance().addTexture(L"pigMouthClosed", L"..\\assets\\pig_form.png");
  //ResourceManager::Instance().addTexture(L"pigMouthOpen", L"..\\assets\\cloud_mutagenic_large1.png");

  success = mDungeon.init() && mEntitySystem.init();

  if (success)
  {
    mPlayerId = mEntitySystem.add(mDungeon.mStartPosX, mDungeon.mStartPosY);

    mGameState = STATE_RUNNING;
    mGameFloor = 1;

    while (!mDungeon.generate(mGameFloor * 2)) ;

    if (mEntitySystem.mSystem.has(mPlayerId))
    {
      Entity& player = mEntitySystem.mSystem.lookup(mPlayerId);
      player.x = mDungeon.mStartPosX;
      player.y = mDungeon.mStartPosY;
    }

    mCameraInfo.x = mCameraInfo.y = 0.0f;
    mCameraInfo.width  = mWindowWidth;
    mCameraInfo.height = mWindowHeight;
  }

  return (success);
}

void DarkDungeonApp::appClose()
{
  mEntitySystem.close();
  mDungeon.close();

  MglApplication::close();
}

bool DarkDungeonApp::defaultHandleEvent(int _event, void* _eventData)
{
  bool handled = false;

  switch (_event)
  {
    case EVENT_KEY_INPUT:
    {
      EventDataKeyInput& eventData = *(EventDataKeyInput *)_eventData;

      if (eventData.down)
      {
        if (mEntitySystem.mSystem.has(mPlayerId))
        {
          Entity& entity = mEntitySystem.mSystem.lookup(mPlayerId);

          switch (eventData.key)
          {
            case VK_LEFT:
            {
            } break;

            case VK_UP:
            {
            } break;

            case VK_RIGHT:
            {
            } break;

            case VK_DOWN:
            {
            } break;

            case 'W':
            {
              if (moveEntity(entity, 0, 1))
                if (hasEntityReachedGoal(entity))
                  mGameState = STATE_WIN;
            } break;

            case 'S':
            {
              if (moveEntity(entity, 0, -1))
                if (hasEntityReachedGoal(entity))
                  mGameState = STATE_WIN;
            } break;

            case 'A':
            {
              if (moveEntity(entity, -1, 0))
                if (hasEntityReachedGoal(entity))
                  mGameState = STATE_WIN;
            } break;

            case 'D':
            {
              if (moveEntity(entity, 1, 0))
                if (hasEntityReachedGoal(entity))
                  mGameState = STATE_WIN;
            } break;

            case 'I':
            {
              Mgl::debugPrint(0, "I");
            } break;

            case ' ':
            {
              if (mGameState == STATE_WIN)
              {
                mGameState = STATE_RUNNING;
                mGameFloor++;
                while (!mDungeon.generate(mGameFloor * 2)) ;

                if (mEntitySystem.mSystem.has(mPlayerId))
                {
                  Entity& player = mEntitySystem.mSystem.lookup(mPlayerId);
                  player.x = mDungeon.mStartPosX;
                  player.y = mDungeon.mStartPosY;
                }

                mCameraInfo.x = 0;
                mCameraInfo.y = 0;
              }
            } break;

            #if _DEBUG
            case 'C':
            {
              mDungeon.mCheat = !mDungeon.mCheat;
            } break;

            case 'V':
            {
              Mgl::debugPrint(0, "V");
              while (!mDungeon.generate()) ;

              if (mEntitySystem.mSystem.has(mPlayerId))
              {
                Entity& entity = mEntitySystem.mSystem.lookup(mPlayerId);

                entity.x = mDungeon.mStartPosX;
                entity.y = mDungeon.mStartPosY;
              }
            } break;

            case 'B':
            {
              Mgl::debugPrint(0, "B");
              while (!mDungeon.generate(true)) ;

              if (mEntitySystem.mSystem.has(mPlayerId))
              {
                Entity& entity = mEntitySystem.mSystem.lookup(mPlayerId);

                entity.x = mDungeon.mStartPosX;
                entity.y = mDungeon.mStartPosY;
              }
            } break;
            #endif
          }
        }
      }
    } break;
  }
  return (handled);
}

void DarkDungeonApp::defaultRender()
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

  glLoadIdentity();

  //renderBorder();

  glPushMatrix();
    glTranslatef(mCameraInfo.x, mCameraInfo.y, 0.0f);
    mDungeon.render(mCameraInfo);
    mEntitySystem.render(mCameraInfo);
  glPopMatrix();

  if (mGameState == STATE_WIN)
  {
    glRasterPos2i(50, 100);
    glColor3f(1.0f, 1.0f, 1.0f);
    glText("You've reached the exit! Time for the next floor: %i", mGameFloor);
    glRasterPos2i(50, 80);
    glText("Press spacebar to continue...");
  }
}

void DarkDungeonApp::defaultUpdate(double _deltaTime)
{
  static const double CameraSpeed = 1500.0;

  if (isKeyPressed(VK_LEFT))
  {
    mCameraInfo.x += (float)(CameraSpeed * _deltaTime);
  }
  if (isKeyPressed(VK_RIGHT))
  {
    mCameraInfo.x -= (float)(CameraSpeed * _deltaTime);
  }
  if (isKeyPressed(VK_UP))
  {
    mCameraInfo.y -= (float)(CameraSpeed * _deltaTime);
  }
  if (isKeyPressed(VK_DOWN))
  {
    mCameraInfo.y += (float)(CameraSpeed * _deltaTime);
  }

  mDungeon.update(_deltaTime);
  mEntitySystem.update(_deltaTime);
}

bool DarkDungeonApp::moveEntity(Entity& _entity, int _dx, int _dy)
{
  bool success = false;

  int targetX = _entity.x + _dx;
  int targetY = _entity.y + _dy;

  Cell& cell = mDungeon.cellAt(targetX, targetY);

  if (cell.traversable)
  {
    _entity.x = targetX;
    _entity.y = targetY;

    mDungeon.mPlayerPos.set(targetX, targetY);

    success = true;
  }

  return (success);
}

bool DarkDungeonApp::hasEntityReachedGoal(Entity& _entity)
{
  bool success = false;

  if (_entity.x == mDungeon.mEndPosX && _entity.y == mDungeon.mEndPosY)
    success = true;

  return (success);
}

