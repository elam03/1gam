#pragma once

#include "DarkDungeonTypes.h"

#include "MglSystem.h"

class EntitySystem
{
public:
  EntitySystem();
  virtual ~EntitySystem();

  bool init();
  void close();

  ID   add(int _x, int _y);
  void remove(ID _idToRemove);
  void clear();
  void render(const Mgl::CameraInfo& _cameraInfo);
  void update(double _updateDelta);

  System<Entity> mSystem;

  void drawEntity(int _x, int _y, const Entity& _entity);
};