#include "Dungeon.h"

#include "MglGl.h"
#include "MglUtils.h"
#include "MglResourceManager.h"

#include "memory.h"
#include "stlastar.h"

#include <iostream>
#include <stdio.h>
#include <vector>

using namespace std;
using namespace foundation;
using namespace Mgl;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define DEBUG_LISTS 0
#define DEBUG_LIST_LENGTHS_ONLY 0

Dungeon* gDungeon = nullptr;

int GetMap(int x, int y)
{
  int value = 9;
  Cell& cell = gDungeon->cellAt(x, y);
  
  switch (cell.type)
  {
    case CELL_EMPTY: value = 0; break;
    case CELL_BRIDGE: value = 0; break;
    default: value = 9; break;
    //case CELL_WALL: value = 9; break;
    //case CELL_BRICK: value = 9; break;
    //case CELL_WATER: value = 9; break;
  }

  return (value);
}

// Definitions

class DungeonState
{
public:
  DungeonState() { mX = mY = 0; }
  DungeonState(int _x, int _y) : mX(_x), mY(_y) {}
  virtual ~DungeonState() {}

  float GoalDistanceEstimate(DungeonState& _nodeGoal)
  {
    // Heuristic function which computes the estimated cost to the goal node
    float dx = float(((float)mX - (float)_nodeGoal.mX));
    float dy = float(((float)mY - (float)_nodeGoal.mY));

    return dx + dy;
  }

	bool IsGoal(DungeonState& _nodeGoal)
  {
    // Returns true if this node is the goal node
    if (mX == _nodeGoal.mX && mY == _nodeGoal.mY)
      return true;

    return false;
  }

	bool GetSuccessors(AStarSearch<DungeonState>* _astarsearch, DungeonState* _parent_node)
  {
    // Retrieves all successors to this node and adds them via _astarsearch.addSuccessor()
    int parent_x = -1; 
    int parent_y = -1; 

    if (_parent_node)
    {
      parent_x = _parent_node->mX;
      parent_y = _parent_node->mY;
    }

    DungeonState NewNode;

    // push each possible move except allowing the search to go backwards

    if ((GetMap(mX - 1, mY) < 9) && !((parent_x == (mX - 1)) && (parent_y == mY))) 
    {
      NewNode = DungeonState(mX - 1, mY);
      _astarsearch->AddSuccessor( NewNode );
    }  

    if ((GetMap(mX, mY - 1) < 9) && !((parent_x == mX) && (parent_y == (mY - 1)))) 
    {
      NewNode = DungeonState(mX, mY - 1);
      _astarsearch->AddSuccessor(NewNode);
    }  

    if ((GetMap(mX + 1, mY) < 9) && !((parent_x == (mX + 1)) && (parent_y == mY)))
    {
      NewNode = DungeonState(mX + 1, mY);
      _astarsearch->AddSuccessor(NewNode);
    }  


    if ((GetMap(mX, mY + 1) < 9) && !((parent_x == mX) && (parent_y == (mY + 1))))
    {
      NewNode = DungeonState(mX, mY + 1);
      _astarsearch->AddSuccessor(NewNode);
    }  

    return true;
  }
	
  float GetCost(DungeonState &successor )
  {
    // Computes the cost of travelling from this node to the successor node
    return (float) GetMap(mX, mY);
  }
	bool IsSameState(DungeonState& _rhs)
  {
    // Returns true if this node is the same as the rhs node
    if (mX == _rhs.mX && mY == _rhs.mY)
      return true;

    return false;
  }

public:
  int mX;
  int mY;

  Dungeon* mDungeon;
};

bool Dungeon::canGetToGoal()
{
  bool success = false;

  // Create an instance of the search class...

  AStarSearch<DungeonState> astarsearch;

  unsigned int SearchCount = 0;

  const unsigned int NumSearches = 1;

  while (SearchCount < NumSearches)
  {
    // Create a start state
    DungeonState nodeStart;
    nodeStart.mX = mStartPosX;
    nodeStart.mY = mStartPosY;

    // Define the goal state
    DungeonState nodeEnd;
    nodeEnd.mX = mEndPosX;
    nodeEnd.mY = mEndPosY;

    // Set Start and goal states

    astarsearch.SetStartAndGoalStates(nodeStart, nodeEnd);

    unsigned int SearchState;
    unsigned int SearchSteps = 0;

    do
    {
      SearchState = astarsearch.SearchStep();

      SearchSteps++;

#if DEBUG_LISTS
      cout << "Steps:" << SearchSteps << "\n";

      int len = 0;

      cout << "Open:\n";
      DungeonState *p = astarsearch.GetOpenListStart();
      while (p)
      {
        len++;
#if !DEBUG_LIST_LENGTHS_ONLY      
        ((DungeonState *)p)->PrintNodeInfo();
#endif
        p = astarsearch.GetOpenListNext();
      }

      cout << "Open list has " << len << " nodes\n";

      len = 0;

      cout << "Closed:\n";
      p = astarsearch.GetClosedListStart();
      while (p)
      {
        len++;
#if !DEBUG_LIST_LENGTHS_ONLY      
        p->PrintNodeInfo();
#endif      
        p = astarsearch.GetClosedListNext();
      }

      cout << "Closed list has " << len << " nodes\n";
#endif

    } while (SearchState == AStarSearch<DungeonState>::SEARCH_STATE_SEARCHING);

    if (SearchState == AStarSearch<DungeonState>::SEARCH_STATE_SUCCEEDED)
    {
      cout << "Search found goal state\n";

      DungeonState* node = astarsearch.GetSolutionStart();

#if DISPLAY_SOLUTION
      cout << "Displaying solution\n";
#endif
      int steps = 0;

      //node->PrintNodeInfo();
      for (;;)
      {
        node = astarsearch.GetSolutionNext();

        if (!node)
        {
          break;
        }

        node->mX;

        // Only use this to display the goalPath for debugging.
        //gDungeon->cellAt(node->mX, node->mY).goalPath = true;

        //node->PrintNodeInfo();
        steps++;

      }

      cout << "Solution steps " << steps << endl;

      // Once you're done with the solution you can free the nodes up
      astarsearch.FreeSolutionNodes();
      
      success = true;
    }
    else if (SearchState == AStarSearch<DungeonState>::SEARCH_STATE_FAILED) 
    {
      cout << "Search terminated. Did not find goal state\n";
      success = false;
    }

    // Display the number of loops the search went through
    cout << "SearchSteps : " << SearchSteps << "\n";

    SearchCount++;

    astarsearch.EnsureMemoryFreed();
  }

  return (success);
}

Point2i Dungeon::positionToCell(const Point2f& _position)
{
  Point2i result;

  result.x = (int)(_position.x / CellWidth);
  result.y = (int)(_position.y / CellHeight);

  return (result);
}

Dungeon::Dungeon() : 
  mCells(memory_globals::default_allocator()),
  mRoomVertices(memory_globals::default_allocator()),
  mCheat(false)
{
  gDungeon = this;
}

Dungeon::~Dungeon()
{
}

bool Dungeon::init()
{
  bool success = true;

  mWidth  = 64;
  mHeight = 64;

  array::resize(mCells, mWidth * mHeight);

  //generateRandom();
  while (!generate()) ;

  return (success);
}

void Dungeon::close()
{
  array::clear(mCells);
  array::clear(mRoomVertices);
}

void Dungeon::render(const CameraInfo& _cameraInfo)
{
  glEnable(GL_TEXTURE_2D);

  glColor4f(1.0f, 1.0f, 1.0f, 1.0f);

  Point2i begin = positionToCell(Point2f(-_cameraInfo.x, -_cameraInfo.y));
  Point2i end   = positionToCell(Point2f(-_cameraInfo.x + _cameraInfo.width, -_cameraInfo.y + _cameraInfo.height));
  Point2i size  = end - begin;

  glPushAttrib(GL_BLEND);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    static float CellLightRange = 1.0f;

    for (int y = 0; y < size.y; y++)
    {
      for (int x = 0; x < size.x; x++)
      {
        int dx = abs(mPlayerPos.x - (begin.x + x));
        int dy = abs(mPlayerPos.y - (begin.y + y));
      
        float t = clamp<float>(CellLightRange / float(dx + dy), 0.0f, 1.0f);

        if (t < .3f)
          t = mCheat ? .25f : 0.0f;
        else if (t < .5f)
          t = .5f;
        else if (t < .8f)
          t = 0.75f;

        glColor4f(1.0f, 1.0f, 1.0f, t);

        drawCell(begin.x + x, begin.y + y, cellAt(begin.x + x, begin.y + y));
      }
    }
  glPopAttrib();

  glPushAttrib(GL_BLEND);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    {
      // Render the exit portal
      glColor4f(1.0f, 0.0f, 0.0f, 0.25f);
      glQuad((float)(mEndPosX) * CellWidth, (float)(mEndPosY) * CellHeight, (float)(CellWidth), (float)(CellHeight));
    }

  glPopAttrib();
}

void Dungeon::update(double _deltaTime)
{
}

void Dungeon::drawCell(int _x, int _y, Cell& _cell)
{
  Texture* texture = nullptr;

  if (_x < 0 || _x >= mWidth || _y < 0 || _y >= mHeight)
    return;

  switch (_cell.type)
  {
    default:          { texture = ResourceManager::Instance().getTexture(L"empty"); } break;
    case CELL_EMPTY:  { texture = ResourceManager::Instance().getTexture(L"floor"); } break;
    case CELL_WALL:   { texture = ResourceManager::Instance().getTexture(L"wall"); } break;
    case CELL_BRICK:  { texture = ResourceManager::Instance().getTexture(L"brick"); } break;
    case CELL_WATER:  { texture = ResourceManager::Instance().getTexture(L"water"); } break;
    case CELL_BRIDGE: { texture = ResourceManager::Instance().getTexture(L"bridge"); } break;
  }
  
  if (texture)
  {
    //glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
    glQuadTexture(texture->mTextureId, (float)_x * CellWidth, (float)_y * CellHeight, (float)CellWidth, (float)CellHeight);

    if (_cell.goalPath)
    {
      glPushAttrib(GL_BLEND);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
          
        glColor4f(1.0f, 1.0f, 0.0f, 0.25f);
        glQuad((float)_x * CellWidth + CellWidth * .25f, (float)_y * CellHeight + CellHeight * .25f, (float)CellWidth / 2.0f, (float)CellHeight / 2.0f);
      glPopAttrib();
    } 
  }
}

bool Dungeon::generateRandom()
{
  bool success = true;

  for (int y = 0; y < mHeight; y++)
  {
    for (int x = 0; x < mWidth; x++)
    {
      cellAt(x, y).type = random(0, CELL_COUNT);
    }
  }

  return (success);
}

bool Dungeon::doesCollide(vector<Room>& _rooms, Room _room, int _ignoreIndex)
{
  Room& room = _room;

  for (int i = 0; i < _rooms.size(); i++)
  {
    if (i == _ignoreIndex) continue;

    Room& check = _rooms[i];

    if (!((room.x + room.w < check.x) || (room.x > check.x + check.w) || (room.y + room.h < check.y) || (room.y > check.y + check.h))) return true;
    //if (!((room.x + room.w < check.x) || (room.x > check.x + check.w) || (room.y + room.h < check.y) || (room.y > check.y + check.h)))
    //  return true;
  }

  return false;
}

void Dungeon::squashRooms(vector<Room>& _rooms)
{
  for (int i = 0; i < 10; i++)
  {
    for (int j = 0; j < _rooms.size(); j++)
    {
      Room& room = _rooms[j];

      while (true)
      {
        Point2i oldPosition(room.x, room.y);

        if (room.x > 1) room.x--;
        if (room.y > 1) room.y--;
        if ((room.x == 1) && (room.y == 1)) break;
        if (doesCollide(_rooms, room, j))
        {
          room.x = oldPosition.x;
          room.y = oldPosition.y;
          break;
        }
      }
    }
  }
}

Room& Dungeon::findClosestRoom(vector<Room>& _rooms, Room& _room)
{
  Point2f mid(float(_room.x) + float(_room.w / 2.0f), float(_room.y) + float(_room.h / 2.0f));

  Room* closest = nullptr;
  float closestDistance = 1000.0f;

  for (int i = 0; i < _rooms.size(); i++)
  {
    Room& check = _rooms[i];
    if (&check == &_room) continue;

    Point2f checkMid(float(check.x) + float(check.w / 2.0f), float(check.y) + float(check.h / 2.0f));

    float distance = min(fabs(mid.x - checkMid.x) - (_room.w / 2.0f) - (check.w / 2.0f), fabs(mid.y - checkMid.y) - (_room.h / 2.0f) - (check.h / 2.0f));

    if (distance < closestDistance)
    {
      closestDistance = distance;
      closest = &check;
    }
  }

  return (*closest);
}

bool Dungeon::generate(bool _debug)
{
  return (generate(random(10, 20), _debug));
}

bool Dungeon::generate(int _numRooms, bool _debug)
{
  bool success = true;

  array::clear(mRoomVertices);
  
  memset(&mCells._data[0], 0, sizeof(Cell) * array::size(mCells));

  for (int y = 0; y < mHeight; y++)
  {
    for (int x = 0; x < mWidth; x++)
    {
      cellAt(x, y).type = CELL_BRICK;
    }
  }
  
  // Credits to http://bigbadwofl.me/random-dungeon-generator/

  int numRooms = _numRooms;
  int minSize = 5;
  int maxSize = 15;

  vector<Room> rooms;
  
  rooms.clear();

  Room initialRoom;
  initialRoom.x = 3;
  initialRoom.y = 3;
  initialRoom.w = 3;
  initialRoom.h = 3;
  rooms.push_back(initialRoom);

  for (int i = 1; i < numRooms; i++)
  {
    Room room;

    room.x = random(1, mWidth - maxSize - 1);
    room.y = random(1, mHeight - maxSize - 1);
    room.w = random(minSize, maxSize);
    room.h = random(minSize, maxSize);

    if (doesCollide(rooms, room))
    {
      i--;
      continue;
    }

    room.w--;
    room.h--;

    rooms.push_back(room);
  }

  //squashRooms(rooms);

  for (int i = 0; i < rooms.size(); i++)
  {
    Room& roomA = rooms[i];
    Room& roomB = findClosestRoom(rooms, roomA);

    Point2i pointA(random(roomA.x, roomA.x + roomA.w), random(roomA.y, roomA.y + roomA.h));
    Point2i pointB(random(roomB.x, roomB.x + roomB.w), random(roomB.y, roomB.y + roomB.h));

    while ((pointB.x != pointA.x) || (pointB.y != pointA.y))
    {
      if (pointB.x != pointA.x)
      {
        if (pointB.x > pointA.x) pointB.x--;
        else pointB.x++;
      }
      else if (pointB.y != pointA.y)
      {
        if (pointB.y > pointA.y) pointB.y--;
        else pointB.y++;
      }

      if (_debug)
      {
        cellAt(pointB.x, pointB.y).type = CELL_WATER;
        cellAt(pointB.x, pointB.y).traversable = false;
      }
      else
      {
        cellAt(pointB.x, pointB.y).type = CELL_EMPTY;
        cellAt(pointB.x, pointB.y).traversable = true;
      }
    }
  }

  // Carve all the rooms
  for (int i = 0; i < rooms.size(); i++)
  {
    Room& room = rooms[i];

    for (int x = room.x; x < room.x + room.w; x++)
    {
      for (int y = room.y; y < room.y + room.h; y++)
      {
        cellAt(x, y).type = CELL_EMPTY;
        cellAt(x, y).traversable = true;
      }
    }
  }

  // Turn the borders into walls.
  for (int x = 0; x < mWidth; x++)
  {
    for (int y = 0; y < mHeight; y++)
    {
      if (cellAt(x, y).type == CELL_EMPTY)
      {
        for (int xx = x - 1; xx <= x + 1; xx++)
        {
          for (int yy = y - 1; yy <= y + 1; yy++)
          {
            //if (map[xx][yy] == 0) map[xx][yy] = 2;
            if (cellAt(xx, yy).type == CELL_BRICK)
            {
              cellAt(xx, yy).type = CELL_WALL;
              cellAt(xx, yy).traversable = false;
            }
          }
        }
      }
    }
  }

  mStartPosX = rooms[0].x + rooms[0].w / 2;
  mStartPosY = rooms[0].y + rooms[0].h / 2;

  mPlayerPos.set(mStartPosX, mStartPosY);

  mEndPosX = rooms[rooms.size() - 1].x + rooms[rooms.size() - 1].w / 2;
  mEndPosY = rooms[rooms.size() - 1].y + rooms[rooms.size() - 1].h / 2;

  for (unsigned i = 0; i < rooms.size(); i++)
  {
    array::push_back(mRoomVertices, rooms[i]);
  }

  success = canGetToGoal();

  return (success);
}

bool Dungeon::digHorizontalCorridor(int _y, int _x1, int _x2)
{
  bool success = false;

  if (_y >= 0 && _y < mHeight)
  {
    if (_x1 >= 0 && _x1 < mWidth && _x2 >= 0 && _x2 < mWidth)
    {
      if (_x2 < _x1)
      {
        int temp = _x2;
        _x2 = _x1;
        _x1 = temp;
      }

      for (int i = _x1; i < _x2; i++)
      {
        int x = i;
        int y = _y;

        cellAt(x, y).type        = CELL_EMPTY;
        cellAt(x, y).traversable = true;
      }
      
      success = true;
    }
  }

  return (success);
}

bool Dungeon::digVerticalCorridor(int _x, int _y1, int _y2)
{
  bool success = false;

  if (_x >= 0 && _x < mWidth)
  {
    if (_y1 >= 0 && _y1 < mHeight && _y2 >= 0 && _y2 < mHeight)
    {
      if (_y2 < _y1)
      {
        int temp = _y2;
        _y2 = _y1;
        _y1 = temp;
      }

      for (int j = _y1; j < _y2; j++)
      {
        int x = _x;
        int y = j;

        cellAt(x, y).type        = CELL_EMPTY;
        cellAt(x, y).traversable = true;
      }

      success = true;
    }
  }

  return (success);
}

bool Dungeon::digRoom(int _x, int _y, int _w, int _h)
{
  bool success = false;

  if (_w > 2 && _h > 2)
  {
    if ((_x - _w / 2) >= 0 && (_y - _h / 2) >= 0 && (_x + _w / 2) < mWidth && (_y + _h / 2) < mHeight)
    {
      for (int j = 0; j < _h; j++)
      {
        for (int i = 0; i < _w; i++)
        {
          int x = _x - _w / 2 + i;
          int y = _y - _h / 2 + j;

          bool border = (i == 0) || (j == 0) || (i == _w - 1) || (j == _h - 1);

          if (border)
          {
            cellAt(x, y).type        = CELL_WALL;
            cellAt(x, y).traversable = false;
          }
          else
          {
            cellAt(x, y).type        = CELL_EMPTY;
            cellAt(x, y).traversable = true;
          }
        }
      }

      success = true;
    }
  }

  return (success);
}

bool Dungeon::digRoom(Room& _room)
{
  return (digRoom(_room.x, _room.y, _room.w, _room.h));
}


/*
var Dungeon = {
    map: null,
    map_size: 64,
    rooms: [],
    Generate: function () {
        this.map = [];
        for (var x = 0; x < this.map_size; x++) {
            this.map[x] = [];
            for (var y = 0; y < this.map_size; y++) {
                this.map[x][y] = 0;
            }
        }

        var room_count = Helpers.GetRandom(10, 20);
        var min_size = 5;
        var max_size = 15;

        for (var i = 0; i < room_count; i++) {
            var room = {};

            room.x = Helpers.GetRandom(1, this.map_size - max_size - 1);
            room.y = Helpers.GetRandom(1, this.map_size - max_size - 1);
            room.w = Helpers.GetRandom(min_size, max_size);
            room.h = Helpers.GetRandom(min_size, max_size);

            if (this.DoesCollide(room)) {
                i--;
                continue;
            }
            room.w--;
            room.h--;

            this.rooms.push(room);
        }

        this.SquashRooms();

        for (i = 0; i < room_count; i++) {
            var roomA = this.rooms[i];
            var roomB = this.FindClosestRoom(roomA);

            pointA = {
                x: Helpers.GetRandom(roomA.x, roomA.x + roomA.w),
                y: Helpers.GetRandom(roomA.y, roomA.y + roomA.h)
            };
            pointB = {
                x: Helpers.GetRandom(roomB.x, roomB.x + roomB.w),
                y: Helpers.GetRandom(roomB.y, roomB.y + roomB.h)
            };

            while ((pointB.x != pointA.x) || (pointB.y != pointA.y)) {
                if (pointB.x != pointA.x) {
                    if (pointB.x > pointA.x) pointB.x--;
                    else pointB.x++;
                } else if (pointB.y != pointA.y) {
                    if (pointB.y > pointA.y) pointB.y--;
                    else pointB.y++;
                }

                this.map[pointB.x][pointB.y] = 1;
            }
        }

        for (i = 0; i < room_count; i++) {
            var room = this.rooms[i];
            for (var x = room.x; x < room.x + room.w; x++) {
                for (var y = room.y; y < room.y + room.h; y++) {
                    this.map[x][y] = 1;
                }
            }
        }

        for (var x = 0; x < this.map_size; x++) {
            for (var y = 0; y < this.map_size; y++) {
                if (this.map[x][y] == 1) {
                    for (var xx = x - 1; xx <= x + 1; xx++) {
                        for (var yy = y - 1; yy <= y + 1; yy++) {
                            if (this.map[xx][yy] == 0) this.map[xx][yy] = 2;
                        }
                    }
                }
            }
        }
    },
    FindClosestRoom: function (room) {
        var mid = {
            x: room.x + (room.w / 2),
            y: room.y + (room.h / 2)
        };
        var closest = null;
        var closest_distance = 1000;
        for (var i = 0; i < this.rooms.length; i++) {
            var check = this.rooms[i];
            if (check == room) continue;
            var check_mid = {
                x: check.x + (check.w / 2),
                y: check.y + (check.h / 2)
            };
            var distance = Math.min(Math.abs(mid.x - check_mid.x) - (room.w / 2) - (check.w / 2), Math.abs(mid.y - check_mid.y) - (room.h / 2) - (check.h / 2));
            if (distance < closest_distance) {
                closest_distance = distance;
                closest = check;
            }
        }
        return closest;
    },
    SquashRooms: function () {
        for (var i = 0; i < 10; i++) {
            for (var j = 0; j < this.rooms.length; j++) {
                var room = this.rooms[j];
                while (true) {
                    var old_position = {
                        x: room.x,
                        y: room.y
                    };
                    if (room.x > 1) room.x--;
                    if (room.y > 1) room.y--;
                    if ((room.x == 1) && (room.y == 1)) break;
                    if (this.DoesCollide(room, j)) {
                        room.x = old_position.x;
                        room.y = old_position.y;
                        break;
                    }
                }
            }
        }
    },
    DoesCollide: function (room, ignore) {
        for (var i = 0; i < this.rooms.length; i++) {
            if (i == ignore) continue;
            var check = this.rooms[i];
            if (!((room.x + room.w < check.x) || (room.x > check.x + check.w) || (room.y + room.h < check.y) || (room.y > check.y + check.h))) return true;
        }

        return false;
    }
}

var Renderer = {
    canvas: null,
    ctx: null,
    size: 512,
    scale: 0,
    Initialize: function () {
        this.canvas = document.getElementById('canvas');
        this.canvas.width = this.size;
        this.canvas.height = this.size;
        this.ctx = canvas.getContext('2d');
        this.scale = this.canvas.width / Dungeon.map_size;
    },
    Update: function () {
        for (var y = 0; y < Dungeon.map_size; y++) {
            for (var x = 0; x < Dungeon.map_size; x++) {
                var tile = Dungeon.map[x][y];
                if (tile == 0) this.ctx.fillStyle = '#351330';
                else if (tile == 1) this.ctx.fillStyle = '#64908A';
                else this.ctx.fillStyle = '#424254';
                this.ctx.fillRect(x * this.scale, y * this.scale, this.scale, this.scale);
            }
        }
    }
};

var Helpers = {
    GetRandom: function (low, high) {
        return~~ (Math.random() * (high - low)) + low;
    }
};

Dungeon.Generate();
Renderer.Initialize();
Renderer.Update(Dungeon.map);
*/