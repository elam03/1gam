#include "EntitySystem.h"

#include "MglGl.h"
#include "MglResourceManager.h"
#include "MglUtils.h"

#include "memory.h"

using namespace foundation;
using namespace std;
using namespace Mgl;

EntitySystem::EntitySystem() : 
  mSystem()
{
}

EntitySystem::~EntitySystem()
{
}

bool EntitySystem::init()
{
  bool success = true;
  
  mSystem.removeAll();

  return (success);
}

void EntitySystem::close()
{
  mSystem.removeAll();
}

ID EntitySystem::add(int _x, int _y)
{
  ID id = mSystem.add();

  if (mSystem.has(id))
  {
    Entity& entity = mSystem.lookup(id);
    memset(&entity, 0, sizeof(Entity));

    entity.id = id;
    entity.x = _x;
    entity.y = _y;
    entity.type = ENTITY_TYPE_PLAYER;
    entity.health = entity.healthMax = 100.0f;
  }

  return (id);
}

void EntitySystem::remove(ID _idToRemove)
{
}

void EntitySystem::clear()
{
}

void EntitySystem::render(const CameraInfo& _cameraInfo)
{
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
  for (unsigned i = 0; i < mSystem._num_objects; i++)
  {
    Entity& entity = mSystem._objects[i];
    drawEntity(entity.x, entity.y, entity);
  }
  glDisable(GL_BLEND);
}

void EntitySystem::update(double _updateDelta)
{
}

void EntitySystem::drawEntity(int _x, int _y, const Entity& _entity)
{
  Texture* texture = nullptr;

  switch (_entity.type)
  {
    default:                  { texture = ResourceManager::Instance().getTexture(L"blah"); } break;
    case ENTITY_TYPE_NONE:    { texture = ResourceManager::Instance().getTexture(L"blah"); } break;
    case ENTITY_TYPE_PLAYER:  { texture = ResourceManager::Instance().getTexture(L"player"); } break;
    case ENTITY_TYPE_MONSTER: { texture = ResourceManager::Instance().getTexture(L"monster"); } break;
  }
  
  if (texture)
  {
    glQuadTexture(texture->mTextureId, (float)_x * CellWidth, (float)_y * CellHeight, (float)CellWidth, (float)CellHeight);
  }
}
