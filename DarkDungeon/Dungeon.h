#pragma once

#include "DarkDungeonTypes.h"
#include "MglTypes.h"

#include "array.h"

#include <vector>

class Dungeon
{
public:
  Dungeon();
  virtual ~Dungeon();

  bool init();
  void close();

  void render(const Mgl::CameraInfo& _cameraInfo);
  void update(double _deltaTime);

  inline Cell& cellAt(int _x, int _y) { return mCells[_y * mWidth + _x]; }
  void drawCell(int _x, int _y, Cell& _cell);

  foundation::Array<Cell> mCells;

  int mWidth;
  int mHeight;

  int mStartPosX;
  int mStartPosY;

  int mEndPosX;
  int mEndPosY;

  Point2i mPlayerPos;

  bool mCheat;

  // Generation stuff
  bool generateRandom();
  bool generate(bool _debug = false);
  bool generate(int _numRooms, bool _debug = false);
  bool digHorizontalCorridor(int _y, int _x1, int _x2);
  bool digVerticalCorridor(int _x, int _y1, int _y2);
  bool digRoom(int _x, int _y, int _w, int _h);
  bool digRoom(Room& _room);

  bool doesCollide(std::vector<Room>& _rooms, Room _room, int _ignoreIndex = -1);
  void squashRooms(std::vector<Room>& _rooms);
  Room& findClosestRoom(std::vector<Room>& _rooms, Room& _room);
  
  bool canGetToGoal();

  Point2i positionToCell(const Point2f& _position);

  foundation::Array<Room> mRoomVertices;
};