#include "cinder/app/AppNative.h"
#include "cinder/gl/gl.h"
#include "cinder/Vector.h"
#include "cinder/Path2d.h"

#include <deque>

using namespace ci;
using namespace ci::app;
using namespace std;

static inline float randf(float _min, float _max) { return (_min + ((_max - _min) * ((float)(rand() % 1000) / 1000.0f))); }
static inline float randf(float _max) { return randf(0.0f, _max); }

void generatePath(const Vec2f& _central_position, Path2d& _path)
{
  /////////////////////////////////////////////////////////////////////////////
  //// Curvy path
  //vector<Vec2f> points;

  //float angle = randf(360.0f);

  ////points.push_back(Vec2f(15.0f, 0.0f));

  //for (int i = 0; i < 4; i++)
  //{
  //  Vec2f p(15.0f, 0.0f);
  //  p.rotate(angle / M_PI);

  //  p += _central_position;

  //  angle += randf(60.0f, 90.0f);

  //  points.push_back(p);
  //}

  ////_path.moveTo(_position);
  //_path.moveTo(points[0]);
  //_path.curveTo(points[0], points[1], points[2]);
  //_path.curveTo(points[2], points[3], points[0]);
  ////_path.lineTo(_position + dir);
  ////_path.close();
  /////////////////////////////////////////////////////////////////////////////
  //// Straight back and forth path

  //_path.moveTo(_central_position);
  //
  //float angle = randf(360.0f);
  //Vec2f p(15.0f, 0.0f);
  //p.rotate(angle / M_PI);

  //p += _central_position;
  //
  //_path.lineTo(p);
  //_path.close();
  /////////////////////////////////////////////////////////////////////////////
  // Quadratic back and forth?
  //_path.moveTo(_central_position);
  //
  //float angle = randf(360.0f);
  //float dist = randf(5.0f, 25.0f);
  //Vec2f p(dist, 0.0f);
  //p.rotate(angle / M_PI);
  //p += _central_position;

  //Vec2f p2(dist, 0.0f);
  //p2.rotate((angle + 30.0f) / M_PI);
  //p2 += _central_position;
  //
  //_path.quadTo(p, p2);
  //_path.close();
  /////////////////////////////////////////////////////////////////////////////
  _path.moveTo(_central_position);
  int num_positions = 4;
  float angle_inc = 360.0f / num_positions;
  float angle_offset = randf(0.0f, 90.0f);
  float radius = randf(5.0f, 25.0f);
  for (int i = 0; i < num_positions; i++)
  {
    float angle = angle_offset + angle_inc * i;
    Vec2f p(radius, 0.0f);
    p.rotate(angle / M_PI);

    p += _central_position;
    
    _path.lineTo(p);
  }
  _path.close();
  /////////////////////////////////////////////////////////////////////////////
}

struct Blob
{
  static const int MAX_CHILDREN = 4;
  //Blob() : position(50.0f, 50.0f), size(5.0f), color(1.0f, 1.0f, 1.0f), growth(1.0f) {}
  Blob(const Vec2f& _position, float _size, const Colorf& _color) : position(_position), size(_size), color(_color), growth(1.0f), reproduction_t(0.0f), reproduction_rate(10.0f), path(), path_t(0.0f)
  {
    growth = randf(.5f, 1.5f);
    generatePath(_position, path);

    reproduction_rate = 10.0f;

    memset(children_ids, 0, sizeof(int) * MAX_CHILDREN);
    num_children = 0;

    vibration_speed = randf(.5f, 1.5f);
  }

  virtual ~Blob() {}

  Vec2f getPos() const { return (path.getPosition(path_t)); }
  bool contains(const Vec2f& _pos) const { return (getPos().distanceSquared(_pos) < (size * size)); }
    
  Vec2f  position;
  float  size;
  Colorf color;

  float growth;
  float reproduction_t;
  float reproduction_rate;
  int   children_ids[MAX_CHILDREN];
  int   num_children;

  Path2d path;
  float  path_t;
  float  vibration_speed;
};

enum ACTION_ENUM
{
  ACTION_REMOVE_WEAK,
  ACTION_REMOVE_MEDIUM,
  ACTION_REMOVE_STRONG,
  ACTION_COUNT
} ACTION;

struct Action
{
  Action(int _index, int _type) : index(_index), type(_type) {}

  int index;
  int type;
};

class BlobsApp : public AppNative
{
public:
	void setup();
	void keyDown(KeyEvent event);
	void mouseDown(MouseEvent event);
	void mouseMove(MouseEvent event);
	void update();
	void draw();

  void renderBlobs();
  void updateBlobs();
  int  getBlobIndex(const Vec2f& _pos);
  bool getNearestBlobs(const Vec2f& _pos, vector<Blob*>& _blobs);
  bool popBlob(Blob* _blob);
  bool popBlobByIndex(int _index);

  void renderUi();

  double m_delta_time;
  double m_last_updated_time;
  Vec2f  m_mouse_position;

  vector<Blob> m_blobs;
  int          m_highlighted_blob;

  deque<Action> m_actions;
  int           m_action_max;
  double        m_action_cooldown;
  double        m_action_cooldown_curr;
};

void BlobsApp::setup()
{
  srand(time(nullptr));

  m_delta_time = 0.0;
  m_last_updated_time = getElapsedSeconds();
  m_mouse_position = Vec2f(getWindowWidth() / 2.0f, getWindowHeight() / 2.0f);

  int count = 5;
  for (int i = 0; i < count; i++)
  {
    float x = randf(float(getWindowWidth()));
    float y = randf(float(getWindowHeight()));
    float size = randf(5.0f, 9.0f);
    float c = randf(.5f, 1.0f);
    Colorf color(c, c, c);

    m_blobs.push_back(Blob(Vec2f(x, y), size, color));
  }
  m_highlighted_blob = -1;

  m_actions.clear();
  m_action_max = 10;
  m_action_cooldown = 2.0f;
  m_action_cooldown_curr = m_action_cooldown;
}

void BlobsApp::keyDown(KeyEvent event)
{
  if (event.getCode() == KeyEvent::KEY_ESCAPE)
  {
    quit();
  }
}

void BlobsApp::mouseDown(MouseEvent event)
{
  m_mouse_position = event.getPos();

  if (event.isLeft())
  {
    if (m_actions.size() > 0)
    {
      int index = getBlobIndex(m_mouse_position);

      if (index >= 0 && index < m_blobs.size())
      {
        popBlobByIndex(index);

        Action& action = m_actions.front();

        switch (action.type)
        {
          default:
          case ACTION_REMOVE_WEAK:
          {
          } break;
          
          case ACTION_REMOVE_MEDIUM:
          {
            vector<Blob*> blobs;

            if (getNearestBlobs(m_mouse_position, blobs))
            {
              for (int i = 0; i < 3; i++)
                popBlob(blobs[i]);
            }
          } break;

          case ACTION_REMOVE_STRONG:
          {
            vector<Blob*> blobs;

            if (getNearestBlobs(m_mouse_position, blobs))
            {
              for (int i = 0; i < 5; i++)
                popBlob(blobs[i]);
            }
          } break;
        }
        
        m_actions.pop_front();
      }
    }
  }
}

void BlobsApp::mouseMove(MouseEvent event)
{
  m_mouse_position = event.getPos();
}

void BlobsApp::update()
{
  m_delta_time = (float)(getElapsedSeconds() - m_last_updated_time);

  updateBlobs();
  
  m_highlighted_blob = getBlobIndex(m_mouse_position);

  m_action_cooldown_curr -= m_delta_time;

  if (m_actions.size() < m_action_max)
  {
    if (m_action_cooldown_curr <= 0.0)
    {
      m_action_cooldown_curr += m_action_cooldown;

      m_actions.push_back(Action(m_actions.size(), rand() % ACTION_COUNT));
    }
  }
  
  m_last_updated_time = getElapsedSeconds();
}

void BlobsApp::draw()
{
	// clear out the window with black
	gl::clear(Color(0, 0, 0));

  gl::setMatricesWindow(getWindowSize(), true);
  glLoadIdentity();

  renderBlobs();
  renderUi();
}

void BlobsApp::renderBlobs()
{
  for (int i = 0; i < m_blobs.size(); i++)
  {
    Blob& blob = m_blobs[i];

    if (m_highlighted_blob == i)
      gl::color(1.0f, 0.0f, 0.0f);
    else
      gl::color(blob.color);

    gl::drawSolidCircle(blob.getPos(), blob.size);
  }
}

void BlobsApp::updateBlobs()
{
  vector<Blob> newBlobs;

  for (auto& blob : m_blobs)
  {
    blob.size += (blob.growth) * m_delta_time;

    blob.path_t = fmod(blob.path_t + m_delta_time * blob.vibration_speed, 1.0f);
    blob.reproduction_t -= m_delta_time;

    if (blob.size > 10.0f)
    {
      if (blob.reproduction_t <= 0.0f)
      {
        Vec2f pos = blob.getPos();
        Vec2f dir(blob.size * 5.0f, 0.0f);
        dir.rotate(randf(360.0f) / M_PI);

        Colorf c = (blob.color + randf(-0.15f, 0.15f));

        newBlobs.push_back(Blob(pos + dir, randf(3.0f, 6.0f), c));

        blob.reproduction_t += blob.reproduction_rate;
      }
    }
  }

  for (auto& newBlob : newBlobs)
  {
    m_blobs.push_back(newBlob);
  }
}

int BlobsApp::getBlobIndex(const Vec2f& _pos)
{
  int index = -1;
  bool check_all_blobs = true;

  if (m_highlighted_blob >= 0 && m_highlighted_blob < m_blobs.size())
  {
    Blob& blob = m_blobs[m_highlighted_blob];

    if (blob.contains(_pos))
    {
      check_all_blobs = false;
      index = m_highlighted_blob;
    }
  }

  if (check_all_blobs)
  {
    for (int i = 0; i < m_blobs.size(); i++)
    {
      Blob& blob = m_blobs[i];

      if (blob.contains(_pos))
      {
        index = i;
        break;
      }
    }
  }

  return (index);
}

bool BlobsApp::getNearestBlobs(const Vec2f& _pos, vector<Blob*>& _blobs)
{
  bool success = true;

  struct Info
  {
    Info(float _dist, Blob* _blob) : dist(_dist), blob(_blob) {}
    ~Info() {}

    bool operator<(const Info& _rhs)
    {
      return (dist < _rhs.dist);
    }

    float dist;
    Blob* blob;
  };

  vector<Info> dists;

  for (auto& blob : m_blobs)
  {
    Vec2f delta = blob.getPos() - _pos;
    float dist = delta.lengthSquared();

    dists.push_back(Info(dist, &blob));
  }

  sort(dists.begin(), dists.end());

  for (int i = 0; i < dists.size(); i++)
  {
    _blobs.push_back(dists[i].blob);
  }

  return (success);
}

bool BlobsApp::popBlob(Blob* _blob)
{
  bool success = false;

  if (_blob)
  {
    for (int i = 0; i < m_blobs.size(); i++)
    {
      Blob& blob = m_blobs[i];

      if (&blob == _blob)
      {
        m_blobs.erase(m_blobs.begin() + i);
        success = true;
        break;
      }
    }
  }

  return (success);
}

bool BlobsApp::popBlobByIndex(int _index)
{
  bool success = false;

  if (_index >= 0 && _index < m_blobs.size())
  {
    if (_index == m_highlighted_blob)
      m_highlighted_blob = -1;

    m_blobs.erase(m_blobs.begin() + _index);

    success = true;
  }

  return (success);
}

void BlobsApp::renderUi()
{
  static float action_width  = 25.0f;
  static float action_height = 25.0f;

  int i = 0;
  for (auto& action : m_actions)
  {
    Vec2f pos(100.0f + float(i) * action_width, 100.0f);
    Rectf rect;

    action.index;
    
    //gl::color(c, c, c);

    switch (action.type)
    {
      default:
      case ACTION_REMOVE_WEAK:   gl::color(0.0f, 0.0f, 1.0f); break;
      case ACTION_REMOVE_MEDIUM: gl::color(0.0f, 1.0f, 0.0f); break;
      case ACTION_REMOVE_STRONG: gl::color(1.0f, 0.0f, 0.0f); break;
    }

    rect.set(pos.x, pos.y, pos.x + action_width, pos.y + action_height);
    
    gl::drawSolidRect(rect);

    i++;
  }
}

CINDER_APP_NATIVE(BlobsApp, RendererGl)
