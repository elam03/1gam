#pragma once

#include "MglUtils.h"

namespace Mgl
{
  class ThreadEvent
  {
  public:
    ThreadEvent() { mHandle = CreateEvent(nullptr, false, false, L"ThreadEvent"); }
    virtual ~ThreadEvent() { if (mHandle) { CloseHandle(mHandle); mHandle = nullptr; } }

    void set() { SetEvent(mHandle); }
    void wait(int _waitTimeInMilliseconds = -1) { WaitForSingleObject(mHandle, _waitTimeInMilliseconds == -1 ? INFINITE : (unsigned long)_waitTimeInMilliseconds); }

    HANDLE mHandle;
  };

  class WorkerThread
  {
  public:
    typedef void (*Callback)(void* _callbackData);

  public:
    WorkerThread() : mHandle(nullptr), mCallback(nullptr), mCallbackData(nullptr), mCloseEvent() {}
    virtual ~WorkerThread() { close(); }

    static unsigned long StaticCallback(void* _threadData)
    {
      WorkerThread* thread = ((WorkerThread *)_threadData);

      //bool done = false;

      //while (!done)
      //{
      //}

      //Sleep(5000);

      Mgl::debugPrint(0, "waiting...");

      thread->mCloseEvent.wait();
      
      Mgl::debugPrint(0, "done waiting!");

      thread->mClosedEvent.set();

      //thread->mCallback(thread->mCallbackData);
      return 0;
    }

    bool init()
    {
      bool success = false;

      if (mHandle == nullptr)
      {
        mHandle = CreateThread(nullptr, 0, (LPTHREAD_START_ROUTINE)&StaticCallback, this, 0, nullptr);

        if (mHandle)
        {
          ResumeThread(mHandle);

          success = true;
        }
      }

      return (success);
    }

    bool addWork(Callback _work, void* _workData)
    {
      bool success = false;

      return (success);
    }

    void close()
    {
      if (mHandle)
      {
        mCloseEvent.set();
        mClosedEvent.wait();

        TerminateThread(mHandle, 0);

        CloseHandle(mHandle);
        mHandle = nullptr;
      }
    }

  private:
    HANDLE mHandle;

    Callback mCallback;
    void*    mCallbackData;

    ThreadEvent mCloseEvent;
    ThreadEvent mClosedEvent;
  };
};