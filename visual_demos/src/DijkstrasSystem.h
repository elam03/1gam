﻿#pragma once

//#include "cinder/Vector.h"

//#include "boost/graph/adjacency_list.hpp"


//
// Represent the graph as an adjacency list.
// http://en.wikipedia.org/wiki/Graph_(abstract_data_type)
// 

//static const int MAX_ADJAECENT_VERTICES = 8;

//struct Vertex
//{
//  int       index;
//  ci::Vec2f position;
//  int       adjacentVertices[MAX_ADJAECENT_VERTICES];
//};

//class Graph
//{
//public:
//  Graph();
//  virtual ~Graph();
//
//  static const int MAX_VERTICES = 256;
//  Vertex mVertices[MAX_VERTICES];
//
//  void add();
//};

//typedef boost::adjacency_list<> Graph;

class DijkstrasSystem
{
public:
  DijkstrasSystem();
  virtual ~DijkstrasSystem();

  void test();

  //Graph mGraph;
};


//#include "VisualDemosTypes.h"
//
//#include "MglEventSystem.h"
//#include "MglSystem.h"
//#include "MglThread.h"
//#include "MglUtils.h"
//
//#include <queue>
//#include <deque>
//
//static const float INF = 99999.9f;
//
//struct Node
//{
//  unsigned            index;
//  foundation::Vector2 point;
//  bool                visited;
//};
//
//struct Edge
//{
//  Edge() { cost = index[0] = index[1] = 0; }
//  Edge(unsigned _index1, unsigned _index2, int _cost) { index[0] = _index1; index[1] = _index2; cost = _cost; }
//
//  unsigned index[2];
//  int      cost;
//};
//
//struct Graph
//{
//  Graph() : num_nodes(0), num_edges(0) {}
//  ~Graph() {}
//
//  unsigned num_nodes;
//  Node     nodes[256];
//
//  unsigned num_edges;
//  Edge     edges[256];
//
//  float getCost(unsigned _node_index1, unsigned _node_index2) const
//  {
//    float cost = INF;
//
//    for (unsigned i = 0; i < num_edges; i++)
//    {
//      const Edge& edge = edges[i];
//
//      if ((edge.index[0] == _node_index1 && edge.index[1] == _node_index2) ||
//          (edge.index[1] == _node_index1 && edge.index[0] == _node_index2))
//      {
//        cost = (float)edge.cost;
//        break;
//      }
//    }
//
//    return (cost);
//  }
//
//  unsigned getNeighbors(unsigned _node_index, std::vector<unsigned>& _neighbors)
//  {
//    unsigned count = 0;
//
//    if (_node_index < num_nodes)
//    {
//      _neighbors.clear();
//      for (unsigned i = 0; i < num_edges; i++)
//      {
//        Edge& edge = edges[i];
//
//        if (edge.index[0] == _node_index)
//          _neighbors.push_back(edge.index[1]);
//        else if (edge.index[1] == _node_index)
//          _neighbors.push_back(edge.index[0]);
//      }
//
//      count = (unsigned)_neighbors.size();
//    }
//
//    return (count);
//  }
//};
//
//class DijkstrasSystem : public EventSystem
//{
//public:
//  DijkstrasSystem();
//  virtual ~DijkstrasSystem();
//
//  bool init();
//  void close();
//
//  bool addGraph(const Graph& _graph);
//  
//  unsigned getClosestNode(foundation::Vector2& _p);
//
//  bool start();
//
//  bool handleEvent(int _event, void* _eventData);
//  void render(const Mgl::CameraInfo& _cameraInfo);
//  void update(double _updateDelta);
//
//public:
//  bool preprocessGraph();
//
//  enum EVENT_ENUM
//  {
//    EVENT_STARTED,
//    EVENT_ENDED,
//    EVENT_COUNT
//  } EVENT;
//
//  Mgl::WorkerThread mThread;
//
//  Graph mGraph;
//
//  Mgl::Rect mGraphArea;
//
//  foundation::Vector2 mWindowSize;
//
//  foundation::Vector2 mMousePos;
//
//  unsigned mSourceNode;
//  unsigned mDestNode;
//  unsigned mSelectedNode;
//  unsigned mHoveredNode;
//  
//  std::deque<unsigned> mPath;
//
//  std::priority_queue<Node> mUnvisitedNodes;
//
//
//  unsigned getNeighbors(unsigned _nodeIndex, std::vector<unsigned>& _edges)
//  {
//    unsigned count = 0;
//
//    if (_nodeIndex < mGraph.num_nodes)
//    {
//      _edges.clear();
//      for (unsigned i = 0; i < mGraph.num_edges; i++)
//      {
//        Edge& edge = mGraph.edges[i];
//
//        if (edge.index[0] == _nodeIndex || edge.index[1] == _nodeIndex)
//          _edges.push_back(i);
//      }
//    }
//
//    return (count);
//  }
//
//  /* // WITHOUT PRIORITY_QUEUE
//  function Dijkstra(Graph, source):
//        dist[source]  := 0                     // Distance from source to source
//        for each vertex v in Graph:            // Initializations
//            if v ≠ source
//                dist[v]  := infinity           // Unknown distance function from source to v
//                previous[v]  := undefined      // Previous node in optimal path from source
//            end if 
//            add v to Q                         // All nodes initially in Q
//        end for
//        
//        while Q is not empty:                  // The main loop
//            u := vertex in Q with min dist[u]  // Source node in first case
//            remove u from Q 
//            
//            for each neighbor v of u:           // where v has not yet been removed from Q.
//                alt := dist[u] + length(u, v)
//                if alt < dist[v]:               // A shorter path to v has been found
//                    dist[v]  := alt 
//                    previous[v]  := u 
//                end if
//            end for
//        end while
//        return dist[], previous[]
//    end function
//  */
//
//
//  
//  /* // WITH PRIORITY_QUEUE
//     function Dijkstra(Graph, source):
//         dist[source] := 0                     // Initializations
//         for each vertex v in Graph:           
//             if v ≠ source
//                 dist[v] := infinity           // Unknown distance from source to v
//                 previous[v] := undefined      // Predecessor of v
//             end if
//             PQ.add_with_priority(v,dist[v])
//         end for 
//     
//     
//         while PQ is not empty:                // The main loop
//             u := PQ.extract_min()             // Remove and return best vertex
//             for each neighbor v of u:         // where v has not yet been removed from PQ.
//                 alt = dist[u] + length(u, v) 
//                 if alt < dist[v]              
//                     dist[v] := alt 
//                     previous[v] := u
//                     PQ.decrease_priority(v,alt)
//                 end if
//             end for
//         end while
//         return previous[]
//  */
//};