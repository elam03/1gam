#include "VisualDemosApp.h"

#include "MglGl.h"
#include "MglResourceManager.h"
#include "MglUtils.h"

#include "array.h"
#include "queue.h"
#include "memory.h"

#include "lua.hpp"
#include "pugixml.hpp"

#include <sstream>

using namespace std;
using namespace foundation;
using namespace Mgl;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////


VisualDemosApp::VisualDemosApp() :
  MglApplication(),
  mEventHeaders(memory_globals::default_allocator())
{
}

VisualDemosApp::~VisualDemosApp()
{
}

bool VisualDemosApp::appInit()
{
  bool success = false;

  mUpdatesPerSecond = 120;

  static const float InsetSize = 50.0f;

  mCameraInfo.x = InsetSize;
  mCameraInfo.y = InsetSize;
  mCameraInfo.width  = mWindowWidth  - InsetSize * 2.0f;
  mCameraInfo.height = mWindowHeight - InsetSize * 2.0f;

  success = mDijkstrasSystem.init();

  if (success)
  {
    makeGraph();
  }

  return (success);
}

void VisualDemosApp::appClose()
{
  mDijkstrasSystem.close();

  MglApplication::close();
}

static int strlen2(char* str)
{
  int len = 0;
  while (*str++)
          len++;
  return len;
}

static void reverse(char* str)
{
  char* end = str;

  int size = (int)strlen(str);
  
  for (; *end; end++) ;
  
  int length = (int)(end - str);

  char* rev = new char[length];

  for (int i = 0; i < length; i++)
    rev[length - i - 1] = str[i];
  
  for (int i = 0; i < length; i++)
    str[i] = rev[i];

  delete[] rev;
}

static void reverseInPlace(char* str)
{
  char* end = 0;
  int length = 0;

  for (end = str; *end; end++)
    length++;

  char temp;

  for (int i = 0; i < length / 2; i++)
  {
    temp = str[i];
    str[i] = str[length - i - 1];
    str[length - i - 1] = temp;
  }
}

bool VisualDemosApp::defaultHandleEvent(int _event, void* _eventData)
{
  bool handled = false;

  handled = mDijkstrasSystem.handleEvent(_event, _eventData);

  if (!handled)
  {
    switch (_event)
    {
      case EVENT_KEY_INPUT:
      {
        EventDataKeyInput& eventData = *(EventDataKeyInput *)_eventData;

        if (eventData.down)
        {
          switch (eventData.key)
          {
            case VK_LEFT:
            {
            } break;

            case VK_UP:
            {
            } break;

            case VK_RIGHT:
            {
            } break;

            case VK_DOWN:
            {
            } break;

            case VK_RETURN:
            {
            } break;

            case 'L':
            {
            } break;

            case 'W':
            {
            } break;

            case 'S':
            {
            } break;

            case 'A':
            {
              char buf[256];
              sprintf_s(buf, 256, "edcba");

              sprintf_s(buf, 256, "edcba");
              debugPrint(0, "before reverse: buf: \"%s\"", buf);
              reverse(buf);
              debugPrint(0, "after reverse: buf: \"%s\"", buf);
              
              sprintf_s(buf, 256, "edcba");
              debugPrint(0, "before reverseInPlace: buf: \"%s\"", buf);
              reverseInPlace(buf);
              debugPrint(0, "after reverseInPlace: buf: \"%s\"", buf);
            } break;

            case '1':
            {
              makeGraph();
            } break;

            case '2':
            {
              makeGraphRandomGrid();
            } break;

            case 'Q':
            case 'X':
            {
            } break;
          }
        }
      } break;
    }
  }
  
  return (handled);
}

void VisualDemosApp::defaultRender()
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

  glLoadIdentity();

  renderBorder();

  glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
  glRasterPos2i(50, 100);
  glText("Visual Demos App");

  mDijkstrasSystem.render(mCameraInfo);
}

void VisualDemosApp::defaultUpdate(double _deltaTime)
{
  mDijkstrasSystem.update(_deltaTime);

  // Check for events from the various systems.
  checkEvents();
}

void VisualDemosApp::checkEvents()
{
  if (mDijkstrasSystem.getEvents(mEventHeaders))
  {
    for (unsigned i = 0; i < mEventHeaders._size; i++)
    {
      EventHeader& eventHeader = *mEventHeaders[i];

      switch (eventHeader.type)
      {
        case DijkstrasSystem::EVENT_STARTED:
        {
        } break;

        case DijkstrasSystem::EVENT_ENDED:
        {
        } break;
      }
    }

    mDijkstrasSystem.flushEvents();
  }
}

void VisualDemosApp::makeGraph()
{
  unsigned count = 0;
  Graph graph;

  // Add some Nodes
  graph.nodes[graph.num_nodes++].point = Vector2(.1f, .2f);
  graph.nodes[graph.num_nodes++].point = Vector2(.3f, .1f);
  graph.nodes[graph.num_nodes++].point = Vector2(.4f, .4f);
  graph.nodes[graph.num_nodes++].point = Vector2(.7f, .4f);
  graph.nodes[graph.num_nodes++].point = Vector2(.5f, .6f);
  graph.nodes[graph.num_nodes++].point = Vector2(.15f, .55f);

  // Add some Edges
  graph.edges[graph.num_edges++] = Edge(0, 1, 7);
  graph.edges[graph.num_edges++] = Edge(0, 5, 14);
  graph.edges[graph.num_edges++] = Edge(0, 2, 9);
  graph.edges[graph.num_edges++] = Edge(1, 2, 10);
  graph.edges[graph.num_edges++] = Edge(1, 3, 15);
  graph.edges[graph.num_edges++] = Edge(2, 5, 2);
  graph.edges[graph.num_edges++] = Edge(2, 3, 11);
  graph.edges[graph.num_edges++] = Edge(3, 4, 6);
  graph.edges[graph.num_edges++] = Edge(4, 5, 9);

  mDijkstrasSystem.addGraph(graph);
}

void VisualDemosApp::makeGraphRandomGrid()
{
  unsigned count = 0;
  Graph graph;

  static const float wiggleRoom = .05f;
  unsigned width  = 5;
  unsigned height = 5;
  unsigned num_vertices = width * height;

  for (unsigned j = 0; j < height; j++)
  {
    for (unsigned i = 0; i < width; i++)
    {
      Vector2 v = Vector2((1.0f / width) * i, (1.0f / height) * j);
      v += Vector2(randomf(-wiggleRoom, wiggleRoom), randomf(-wiggleRoom, wiggleRoom));
      graph.nodes[graph.num_nodes++].point = v;
    }
  }

  unsigned v = 0;
  while (v < num_vertices)
  {
    if ((v + width) < num_vertices)
      graph.edges[graph.num_edges++] = Edge(v, v + width, random(1, 10));

    if ((v % width) < (width - 1))
    {
      graph.edges[graph.num_edges++] = Edge(v, v + 1, random(1, 10));
      
      if ((v + width + 1) < num_vertices)
        graph.edges[graph.num_edges++] = Edge(v, v + width + 1, random(1, 10));
    }

    v++;
  }

  mDijkstrasSystem.addGraph(graph);
}
