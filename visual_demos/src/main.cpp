#include <iostream>
#include <windows.h>
#include <windowsx.h>

#include "MglUtils.h"

#include "VisualDemosApp.h"

#include "memory.h"

using namespace foundation;
using namespace std;

int WINAPI WinMain(HINSTANCE _hInstance, HINSTANCE _hInstancePrev, LPSTR _commandLine, int _cmdShow)
{
  Mgl::initConsole();

  memory_globals::init();

  MglApplication* application = new VisualDemosApp;

  if (application)
  {
    MglApplication::InitParams initParams;

    initParams.mHInstance = _hInstance;
    initParams.mWindowX = 5;
    initParams.mWindowY = 5;
    initParams.mWindowWidth  = 800;
    initParams.mWindowHeight = 800;
    initParams.mConsoleX = 810;
    initParams.mConsoleY = 5;
    initParams.mConsoleWidth  = 700;
    initParams.mConsoleHeight = 500;
    initParams.mWindowTitle = TEXT("VisualDemosApp");

    if (application->init(initParams))
    {
      application->run();
    }
   
    application->close();
  }

  delete application;
  
  memory_globals::shutdown();
  
  Mgl::closeConsole();

  return 0;
}
