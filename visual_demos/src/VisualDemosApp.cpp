//#include "cinder/app/AppNative.h"
//#include "cinder/gl/gl.h"
//#include "cinder/gl/Light.h"
//#include "cinder/Vector.h"
//
//#include "DijkstrasSystem.h"
//
//#include "boost/graph/adjacency_list.hpp"
//#include "boost/graph/graphviz.hpp"
//
//#include <string>
//
//using namespace ci;
//using namespace ci::app;
//using namespace std;
//
//static const float VertexRadius = 10.0f;
//
//// Define a class that has the data you want to associate to every vertex and edge
//struct Vertex
//{
//  Vertex() : position() {}
//  Vertex(const Vec3f& _p) : position(_p) {}
//  
//  Vec3f position;
//};
//
//struct Edge
//{
//  Edge() : u(-1), v(-1), cost(-1) {}
//  Edge(int _u, int _v, int _cost) : u(_u), v(_v), cost(_cost) {}
//  
//  int u, v;
//  int cost;
//};
//
////Define the graph using those classes
//typedef boost::adjacency_list<boost::listS, boost::vecS, boost::undirectedS, Vertex, Edge > Graph;
////Some typedefs for simplicity
//typedef boost::graph_traits<Graph>::vertex_descriptor vertex_t;
//typedef boost::graph_traits<Graph>::edge_descriptor edge_t;
//
//class VisualDemosApp : public AppNative
//{
//public:
//  void setup();
//  void mouseDown(MouseEvent event);
//  void mouseMove(MouseEvent event);
//  void keyDown(KeyEvent event);
//  void update();
//  void draw();
//
//  DijkstrasSystem mDijkstrasSystem;
//  
//  Graph          mGraph;
//  vector<Vertex> mVertices;
//  vector<Edge>   mEdges;
//
//  Vec3f mMousePos;
//
//  Vec3f mWindowSize;
//
//  int mVertexHovered;
//  int mVertexStart;
//  int mVertexEnd;
//
//  inline bool contains(const Vertex& _vertex, const Vec3f& _position) const { return (_position.distanceSquared(_vertex.position * mWindowSize) < (VertexRadius * VertexRadius)); }
//  inline bool isIndexValid(int _index) const { return (_index >= 0 && _index < mVertices.size()); }
//
//  gl::Light* mLight;
//};
//
//void VisualDemosApp::setup()
//{
//  AllocConsole();
//  AttachConsole(GetCurrentProcessId());
//  freopen("CON", "w", stdout);
//
//  mMousePos.set(0.0f, 0.0f, 0.0f);
//
//  mVertexHovered = mVertexStart = mVertexEnd = -1;
//
//  mWindowSize = Vec3f(getWindowSize().x, getWindowSize().y, 0.0f);
//
//  mLight = new gl::Light(gl::Light::POINT, 0);
//  mLight->lookAt(Vec3f(0.0f, 0.0f, 1.0f), Vec3f());
//  mLight->setAmbient(Color(1.0f, 1.0f, 1.0f));
//  mLight->setDiffuse(Color(1.0f, 1.0f, 1.0f));
//  mLight->setSpecular(Color(1.0f, 1.0f, 1.0f));
//  mLight->setShadowParams(40.0f, 1.0f, 30.0f);
//  mLight->enable();
//}
//
//void VisualDemosApp::mouseDown(MouseEvent event)
//{
//  mMousePos = Vec3f(event.getPos().x, event.getPos().y, 0.0f);
//
//  if (isIndexValid(mVertexHovered))
//  {
//    if (event.isLeftDown())
//      mVertexStart = mVertexHovered;
//  
//    if (event.isRightDown())
//      mVertexEnd = mVertexHovered;
//  }
//}
//
//void VisualDemosApp::mouseMove(MouseEvent event)
//{
//  mMousePos = Vec3f(event.getPos().x, event.getPos().y, 0.0f);
//
//  bool handled = false;
//
//  if (mVertexHovered >= 0 && mVertexHovered < mVertices.size())
//  {
//    auto vertex = mVertices[mVertexHovered];
//    
//    if (contains(vertex, mMousePos))
//    {
//      handled = true;
//    }
//  }
//
//  if (!handled)
//  {
//    mVertexHovered = -1;
//
//    int i = 0;
//    for (auto vertex : mVertices)
//    {
//      if (contains(vertex, mMousePos))
//      {
//        mVertexHovered = i;
//        break;
//      }
//
//      i++;
//    }
//  }
//}
//
//void VisualDemosApp::keyDown(KeyEvent event)
//{
//  if (event.getCode() == KeyEvent::KEY_ESCAPE)
//  {
//    quit();
//  }
//  else if (event.getChar() == 'p')
//  {
//    mDijkstrasSystem.test();
//    console();
//  }
//  else if (event.getChar() == '1')
//  {
//    Graph& g = mGraph;
//
//    mGraph.clear();
//
//    mVertices.clear();
//    mVertices.push_back(Vertex(Vec3f(.1f, .2f, 0.0f)));
//    mVertices.push_back(Vertex(Vec3f(.3f, .1f, 0.0f)));
//    mVertices.push_back(Vertex(Vec3f(.4f, .4f, 0.0f)));
//    mVertices.push_back(Vertex(Vec3f(.7f, .4f, 0.0f)));
//    mVertices.push_back(Vertex(Vec3f(.5f, .6f, 0.0f)));
//    mVertices.push_back(Vertex(Vec3f(.15f, .55f, 0.0f)));
//
//    for (auto vertex : mVertices)
//    {
//      auto u = boost::add_vertex(g);
//
//      g[u] = vertex;
//    }
//
//    mEdges.clear();
//    mEdges.push_back(Edge(0, 1, 7));
//    mEdges.push_back(Edge(0, 5, 14));
//    mEdges.push_back(Edge(0, 2, 9));
//    mEdges.push_back(Edge(1, 2, 10));
//    mEdges.push_back(Edge(1, 3, 15));
//    mEdges.push_back(Edge(2, 5, 2));
//    mEdges.push_back(Edge(2, 3, 11));
//    mEdges.push_back(Edge(3, 4, 6));
//    mEdges.push_back(Edge(4, 5, 9));
//
//    for (auto edge : mEdges)
//    {
//      auto u = edge.u;
//      auto v = edge.v;
//
//      //auto e = add_edge(u, v, g);
//      
//      edge_t e;
//      bool b;
//      boost::tie(e, b) = add_edge(u, v, g);
//      g[e] = edge;
//    }
//
//    for (auto v : g.m_vertices)
//    {
//      cout << v.m_property.position << endl;
//    }
//
//    //boost::tie();
//
//    //// Create two vertices in that graph
//    //vertex_t u = boost::add_vertex(g);
//    //vertex_t v = boost::add_vertex(g);
//
//    //// Create an edge conecting those two vertices
//    //edge_t e;
//    //bool b;
//    //boost::tie(e, b) = boost::add_edge(u, v, g);
//
//
//    //// Set the properties of a vertex and the edge
//    //g[u].position.set(5.0f, 5.0f, 5.0f);
//    //g[e].cost = 7;
//
//    write_graphviz(cout, g);
//
//  }
//}
//
//void VisualDemosApp::update()
//{
//}
//
//void VisualDemosApp::draw()
//{
//  // clear out the window with black
//  gl::clear(Color(0, 0, 0)); 
//
//  gl::enableAlphaTest();
//  gl::enableDepthRead();
//
//  gl::setMatricesWindow(getWindowSize());
//  glLoadIdentity();
//  
//  Vec3f mWindowSize = Vec3f(getWindowSize().x, getWindowSize().y, 0.0f);
//
//  int i = 0;
//  for (auto vertex : mVertices)
//  {
//    if (isIndexValid(mVertexHovered) && i == mVertexHovered)
//      gl::color(1.0f, 0.0f, 1.0f, 1.0f);
//    else
//      gl::color(1.0f, 1.0f, 1.0f, 1.0f);
//
//    glPushMatrix();
//      Vec3f p = vertex.position * mWindowSize;
//    
//      gl::translate(p);
//
//      if (isIndexValid(mVertexStart) && i == mVertexStart || isIndexValid(mVertexEnd) && i == mVertexEnd)
//        gl::enableWireframe();
//      else
//        gl::disableWireframe();
//
//      gl::drawSolidCircle(Vec2f(), VertexRadius);
//
//      if (isIndexValid(mVertexStart) && i == mVertexStart)
//      {
//        gl::color(1.0f, 1.0f, 1.0f, 1.0f);
//        gl::drawString("START", Vec2f());
//        gl::drawStrokedCube(Vec3f(), Vec3f(VertexRadius, VertexRadius, VertexRadius));
//      }
//      else if (isIndexValid(mVertexEnd) && i == mVertexEnd)
//      {
//        gl::color(1.0f, 1.0f, 1.0f, 1.0f);
//        gl::drawString("END", Vec2f());
//        gl::drawStrokedCube(Vec3f(), Vec3f(VertexRadius, VertexRadius, VertexRadius));
//      }
//
//    glPopMatrix();
//    
//    i++;
//  }
//
//  gl::color(0.0f, 0.0f, 1.0f, 1.0f);
//  for (auto edge : mEdges)
//  {
//    Vec3f p1 = mVertices[edge.u].position * mWindowSize;
//    Vec3f p2 = mVertices[edge.v].position * mWindowSize;
//
//    gl::drawLine(p1, p2);
//  }
//
//  gl::color(1.0f, 0.0f, 0.0f, 1.0f);
//  gl::drawLine(Vec3f(0.0f, 0.0f, 0.0f), mMousePos);
//}
//
//CINDER_APP_NATIVE(VisualDemosApp, RendererGl)

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

#include "cinder/app/AppBasic.h"
#include "cinder/gl/gl.h"
#include "cinder/gl/Texture.h"
#include "cinder/gl/Light.h"
#include "cinder/gl/Material.h"
#include "cinder/MayaCamUI.h"
 
using namespace ci;
using namespace ci::app;
using namespace std;
 
GLfloat no_mat[]			= { 0.0, 0.0, 0.0, 1.0 };
ci::ColorA cNoMat			= ci::ColorA( 0.0f, 0.0f, 0.0f, 1.0f );
 
GLfloat mat_ambient[]		= { 0.6, 0.3, 0.4, 1.0 };
ci::ColorA cAmbient			= ci::ColorA( 0.6f, 0.3f, 0.4f, 1.0f );
 
GLfloat mat_diffuse[]		= { 0.3, 0.5, 0.8, 1.0 };
ci::ColorA cDiffuse			= ci::ColorA( 0.3f, 0.5f, 0.8f, 1.0f );
 
GLfloat mat_specular[]		= { 1.0, 1.0, 1.0, 1.0 };
ci::ColorA cSpecular		= ci::ColorA( 1.0f, 1.0f, 1.0f, 1.0f );
 
GLfloat mat_emission[]		= { 0.0, 0.1, 0.3, 0.0 };
ci::ColorA cEmission		= ci::ColorA( 0.0f, 0.1f, 0.3f, 0.0f );
 
GLfloat mat_shininess[]		= { 128.0 };
GLfloat no_shininess[]		= { 0.0 };
 
bool useMaterial;
 
class CinderLightApp : public AppBasic {
  public:
	void prepareSettings( Settings *settings );
	void setup();
	void setupCamera();
	void setupLights();
	
	// Keyboard
	bool _isOptionDown;
	void keyUp	( KeyEvent event);
	void keyDown( KeyEvent event);
	
	// Mouse
	void mouseMove( MouseEvent event );
	void mouseDown( MouseEvent event );
	void mouseDrag( MouseEvent event );
	void mouseUp  ( MouseEvent event );
	
	// Loop
	void update();
	void draw();
	
	
	MayaCamUI				mMayaCam;
	ci::CameraPersp			*_cameraPerspective;
	ci::Quatf				*_sceneRotation;
	
	// Light
	ci::gl::Light			*_light;
	ci::gl::Material		*_ribbonMaterial;
	
	// animation
	double				mTimePrevious;
	double				mTime;
	bool				bAnimate;
	
	// mat
	bool DIFFUSE;
	bool AMBIENT;
	bool SPECULAR;
	bool EMISSIVE;
	
	Vec2f			mMousePos;
 
};
 
#define APP_INITIAL_WIDTH 1024
#define APP_INITIAL_HEIGHT 768
 
void CinderLightApp::prepareSettings( Settings* settings )
{
	settings->setWindowSize( APP_INITIAL_WIDTH, APP_INITIAL_HEIGHT );
	settings->setFrameRate( 60.0f );
}
void CinderLightApp::setup()
{
	setupCamera();
	setupLights();
	
	DIFFUSE = true;
	AMBIENT = true;
	SPECULAR = true;
	EMISSIVE = true;
	useMaterial = false;
	
	
	
	_isOptionDown = false;
	
	mTimePrevious = getElapsedSeconds();
	mTime = 0.0;
	bAnimate = false;
}
 
void CinderLightApp::setupCamera()
{
	// Create camera
	Vec3f p = Vec3f::one() * 10.0f;
	CameraPersp cam = CameraPersp( getWindowWidth(), getWindowHeight(), 45.0f );
	cam.setEyePoint( p );
	cam.setCenterOfInterestPoint( Vec3f::zero() );
	cam.setPerspective( 45.0f, getWindowAspectRatio(), 0.1f, 500.0f );
	
	// Set mayacamera
	mMayaCam.setCurrentCam( cam );
	
	// set up our projector (a special type of light)
	_light = new gl::Light( gl::Light::POINT, 0 );
	//mLight = new gl::Light( gl::Light::DIRECTIONAL, 0 );
	_light->lookAt( ci::Vec3f::one() * 20, Vec3f( 0, 0, 0 ) );
	_light->setAmbient( Color( 1.0f, 1.0f, 1.0f ) );
	_light->setDiffuse( Color( 1.0f, 1.0f, 1.0f ) );
	_light->setSpecular( Color( 1.0f, 1.0f, 1.0f ) );
	_light->setShadowParams( 40.0f, 1.0f, 30.0f );
	_light->enable();
	
	// setup our scene (a simple gray cube)
	_ribbonMaterial = new gl::Material();
	_ribbonMaterial->setSpecular( Color::white() );
	_ribbonMaterial->setDiffuse( Color(0.9f, 0.1f, 0.6f) );
	_ribbonMaterial->setAmbient( Color( 0.6f, 0.2f, 0.2f ) );
	_ribbonMaterial->setShininess( 5.0f );
	
 
}
 
void CinderLightApp::setupLights()
{
	
}
 
void CinderLightApp::mouseMove( MouseEvent event )
{
	mMousePos.x = event.getX() - getWindowWidth() * 0.5f;
	mMousePos.y = getWindowHeight() * 0.5f - event.getY();
}
 
void CinderLightApp::mouseDown( MouseEvent event )
{	
	// let the camera handle the interaction
	mMayaCam.mouseDown( event.getPos() );
}
 
void CinderLightApp::mouseDrag( MouseEvent event )
{
	// let the camera handle the interaction
	mMayaCam.mouseDrag( event.getPos(), event.isLeftDown(), event.isMetaDown(), event.isRightDown() );
}
 
void CinderLightApp::mouseUp( MouseEvent event )
{	
	// let the camera handle the interaction
	mMayaCam.mouseDown( event.getPos() );
}
 
void CinderLightApp::keyDown( KeyEvent event ) {
//	_isOptionDown = event.isMetaDown();
	if(event.getCode() == KeyEvent::KEY_b) {
		bAnimate = !bAnimate;
	}
	
	if(event.getCode() == KeyEvent::KEY_m) {
		useMaterial = !useMaterial;
	}
	
	if( event.getChar() == 'd' || event.getChar() == 'd' ){
		DIFFUSE = ! DIFFUSE;
	}
	else if( event.getChar() == 'a' || event.getChar() == 'A' ){
		AMBIENT = ! AMBIENT;
	}
	else if( event.getChar() == 's' || event.getChar() == 'S' ){
		SPECULAR = ! SPECULAR;
	}
	else if( event.getChar() == 'e' || event.getChar() == 'E' ){
		EMISSIVE = ! EMISSIVE;
	}
	else if( event.getChar() == 'f' || event.getChar() == 'F' ){
		setFullScreen( ! isFullScreen() );
	}
	else if( event.getChar() == '/' || event.getChar() == '?' ){
//		mInfoPanel.toggleState();
	}
	else if( event.getChar() == 'r' || event.getChar() == 'R' ){
//		renderInfoPanel = ! renderInfoPanel;
	}
	else if( event.getChar() == ',' || event.getChar() == '<' ){
		mat_shininess[0] *= 0.5f;
		if( mat_shininess[0] < 8.0f )
			mat_shininess[0] = 8.0f;
	}
	else if( event.getChar() == '.' || event.getChar() == '>' ){
		mat_shininess[0] *= 2.0f;
		if( mat_shininess[0] > 128.0f )
			mat_shininess[0] = 128.0f;
	}
}
 
void CinderLightApp::keyUp( KeyEvent event ) {
}
 
 
void CinderLightApp::update()
{
	double elapsed = getElapsedSeconds() - mTimePrevious;
	mTimePrevious = getElapsedSeconds();
	
	if(bAnimate) mTime += 0.25 * elapsed;
	
	/*// animate camera
	 if(bAnimate) {
	 Vec3f	eye = Vec3f( cosf(mTime), 1.0f, sinf(mTime) ) * 8.0f;
	 mCamera->lookAt( eye, Vec3f::zero() );
	 } //*/
	
	// animate light
	if(bAnimate) {
		Vec3f	p = Vec3f( cosf(mTime), 1.0f - 0.5f * sinf(0.1f * mTime), sinf(mTime) ) * 55.0f;
		_light->lookAt(p, ci::Vec3f::zero() );
	}//*/
}
 
 
 
void CinderLightApp::draw()
{
	// clear out the window with black
//	gl::clear( Color( 0, 0, 0 ) ); 
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	gl::enableDepthWrite();
	gl::enableDepthRead();
	gl::enableAlphaBlending();
	
	glEnable( GL_LIGHTING );
	gl::setMatrices( mMayaCam.getCamera() );
 
//	_light->update( mMayaCam.getCamera() );
//	_light->enable();
	
	glEnable( GL_LIGHTING );
	glEnable( GL_LIGHT0 );
	
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	GLfloat light_position[] = { mMousePos.x, mMousePos.y, 75.0f, 0.0f };
	glLightfv( GL_LIGHT0, GL_POSITION, light_position );
	
	_ribbonMaterial->apply();
	// DRAW
	float sphereSpacing		= 75.0f;
	float sphereRadius		= 35.0f;
	int sphereDetail		= 32;
	int spheresPerRow		= 9;
	int spheresPerColumn	= 5;
	
	for( int x=0; x<spheresPerRow; x++ ){
		float xPer = (float)x/(float)(spheresPerRow - 1 );
		
		for( int y=0; y<spheresPerColumn; y++ ){
			float yPer = (float)y/(float)(spheresPerColumn - 1 );
			
			float xp = sphereSpacing * ( x - ( spheresPerRow - 1 ) * 0.5f );
			float yp = sphereSpacing * ( y - ( spheresPerColumn - 1 ) * 0.5f );
			glPushMatrix();
			glTranslatef( xp, yp, 0.0);
			
			if( DIFFUSE ){
				ci::ColorA color( CM_HSV, xPer, yPer, 1.0f, 1.0f );
//				glMaterialfv( GL_FRONT, GL_DIFFUSE,	color );
				_ribbonMaterial->setDiffuse( color );
			} else {
//				glMaterialfv( GL_FRONT, GL_DIFFUSE,	no_mat );				
				_ribbonMaterial->setDiffuse( cNoMat );
			}
			
			if( AMBIENT ) {
//				glMaterialfv( GL_FRONT, GL_AMBIENT,	mat_ambient );
				_ribbonMaterial->setAmbient( cAmbient );
 
			} else {
//				glMaterialfv( GL_FRONT, GL_AMBIENT,	no_mat );
				_ribbonMaterial->setAmbient( cNoMat );
			}
			
			
//#define USE_MATERIAL 1
//#if USE_MATERIAL 
			if(useMaterial) 
			{
				if( SPECULAR )
				{
						_ribbonMaterial->setSpecular( cSpecular );
						_ribbonMaterial->setShininess( mat_shininess[0] );
				} else {
						_ribbonMaterial->setSpecular( ci::ColorA( 0.0f, 0.0f, 0.0f, 1.0f ) );
						_ribbonMaterial->setShininess( no_shininess[0] );
				}
			}
			else
			{
				
//#else
				if( SPECULAR ){
					
					glMaterialfv( GL_FRONT, GL_SPECULAR, mat_specular );
					glMaterialfv( GL_FRONT, GL_SHININESS, mat_shininess );
				} else {
					glMaterialfv( GL_FRONT, GL_SPECULAR, no_mat );
					glMaterialfv( GL_FRONT, GL_SHININESS, no_shininess );
				}
			}
//#endif
			
			if( EMISSIVE ) {
//				glMaterialfv( GL_FRONT, GL_EMISSION, mat_emission );
				_ribbonMaterial->setEmission( cEmission );
			} else {
//				glMaterialfv( GL_FRONT, GL_EMISSION, no_mat );			
				_ribbonMaterial->setEmission( cNoMat );
			}
			
			gl::drawSphere( Vec3f::zero(), sphereRadius, sphereDetail );
			glPopMatrix();
		}
	}
 
	// DRAW NO LIGHTING
	glDisable( GL_LIGHTING );
	
	// Draw the lighting frustum
	glColor3f( 1.0f, 1.0f, 1.0f );
	gl::drawFrustum( _light->getShadowCamera() );
}
 
 
CINDER_APP_BASIC( CinderLightApp, RendererGl )