#pragma once

#include "MglApplication.h"

#include "DijkstrasSystem.h"

class VisualDemosApp : public MglApplication
{
public:
  VisualDemosApp();
  virtual ~VisualDemosApp();

protected:
  virtual bool appInit();
  virtual void appClose();

public:
  virtual bool defaultHandleEvent(int _event, void* _eventData = 0);
  virtual void defaultRender();
  virtual void defaultUpdate(double _deltaTime);

protected:
  void checkEvents();
  void makeGraph();
  void makeGraphRandomGrid();

protected:
  Mgl::CameraInfo mCameraInfo;

  foundation::Array<Mgl::EventHeader*> mEventHeaders;

  DijkstrasSystem mDijkstrasSystem;
};