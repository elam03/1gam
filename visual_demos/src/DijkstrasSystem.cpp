#include "DijkstrasSystem.h"

#include <iostream>
#include <deque>
#include <iterator>

#include "boost/graph/topological_sort.hpp"
#include "boost/graph/adjacency_list.hpp"
#include "boost/graph/graphviz.hpp"

using namespace std;

DijkstrasSystem::DijkstrasSystem()
{
}

DijkstrasSystem::~DijkstrasSystem()
{
}

void DijkstrasSystem::test()
{
  //using namespace std;
  //using namespace boost;

  ///* define the graph type
  //listS: selects the STL list container to store 
  //the OutEdge list
  //vecS: selects the STL vector container to store 
  //the vertices
  //directedS: selects directed edges
  //*/
  //typedef adjacency_list< listS, vecS, directedS > digraph;

  //// instantiate a digraph object with 8 vertices
  //digraph g(8);

  //// add some edges
  //add_edge(0, 1, g);
  //add_edge(1, 5, g);
  //add_edge(5, 6, g);
  //add_edge(2, 3, g);
  //add_edge(2, 4, g);
  //add_edge(3, 5, g);
  //add_edge(4, 5, g);
  //add_edge(5, 7, g);

  //// represent graph in DOT format and send to cout
  //write_graphviz(cout, g);
}

//#include "MglGl.h"
//#include "MglUtils.h"
//
//using namespace std;
//using namespace foundation;
//using namespace Mgl;
//
/////////////////////////////////////////////////////////////////////////////////
//
//template <class Item>   
//void exch(Item &A, Item &B)   
//{   
//  Item t = A;   
//  A = B;   
//  B = t;   
//}   
//
//template <class Item>  
//class PQ   
//{  
//private:  
//  Item* pq;
//
//  unsigned N;
//public:  
//  PQ(int maxN)  
//  {   
//    pq = new Item[maxN];  
//    N = 0;  
//  }
//  virtual ~PQ() { if (pq) delete[] pq; }
//
//  inline bool empty() const { return N == 0; }  
//  inline void insert(Item item) { pq[N++] = item; }  
//  inline unsigned size() const { return N; }
//
//  Item& operator[](unsigned _index) { return pq[_index]; }
//  const Item& operator[](unsigned _index) const { return pq[_index]; }
//
//  Item getmax()  
//  {
//    int max = 0;  
//    for (unsigned j = 1; j < N; j++)  
//      if (pq[max] < pq[j])   
//        max = j;  
//    exch(pq[max], pq[N - 1]);    
//    return pq[--N];  
//  }  
//};  
//
/////////////////////////////////////////////////////////////////////////////////
//
//DijkstrasSystem::DijkstrasSystem() :
//  EventSystem(),
//  mGraph()
//{
//}
//
//DijkstrasSystem::~DijkstrasSystem()
//{
//}
//
//void work(void* _data)
//{
//  DijkstrasSystem* system = (DijkstrasSystem *)_data;
//  
//  Mgl::debugPrint(0, "doing some work...");
//  system->start();
//  Mgl::debugPrint(0, "done doing some work...");
//}
//
//bool DijkstrasSystem::init()
//{
//  bool success = true;
//
//  memset(&mGraph, 0, sizeof(mGraph));
//
//  //mThread.init(work, this);
//  mThread.init();
//
//  return (success);
//}
//
//void DijkstrasSystem::close()
//{
//  mThread.close();
//}
//
//bool DijkstrasSystem::addGraph(const Graph& _graph)
//{
//  bool success = false;
//
//  mGraph = _graph;
//
//  success = preprocessGraph();
//
//  return (success);
//}
//
//unsigned DijkstrasSystem::getClosestNode(Vector2& _p)
//{
//  unsigned result = mGraph.num_nodes;
//
//  for (unsigned i = 0; i < mGraph.num_nodes; i++)
//  {
//    const Vector2& nodePoint = mGraph.nodes[i].point;
//
//    if (distanceTo(_p, nodePoint) <= .05f)
//    {
//      result = i;
//      break;
//    }
//  }
//
//  return (result);
//}
//
//bool DijkstrasSystem::start()
//{
//  bool success = false;
//  
//  mThread.addWork(work, &mGraph);
//
//  if (mSourceNode < mGraph.num_nodes && mDestNode < mGraph.num_nodes)
//  {
//    float*    dist = new float[mGraph.num_nodes];
//    unsigned* prev = new unsigned[mGraph.num_nodes];
//
//    deque<unsigned> q;
//
//    ///////////////////////////////////////////////////////////////////////////
//    memset(dist, 0, sizeof(float) * mGraph.num_nodes);
//    memset(prev, 9999, sizeof(unsigned) * mGraph.num_nodes);
//
//    dist[mSourceNode] = 0;
//
//    for (unsigned v = 0; v < mGraph.num_nodes; v++)
//    {
//      mGraph.nodes[v].visited = false;
//      if (v != mSourceNode)
//      {
//        dist[v] = INF;
//        prev[v] = mGraph.num_nodes;
//      }
//      q.push_back(v);
//    }
//
//    mGraph.nodes[mSourceNode].visited = true;
//
//    while (!q.empty())
//    {
//      unsigned u = q[0];
//      for (unsigned i = 1; i < q.size(); i++)
//        if (dist[q[i]] < dist[u])
//          u = q[i];
//      q.pop_front();
//
//      vector<unsigned> neighbors;
//
//      if (mGraph.getNeighbors(u, neighbors) > 0)
//      {
//        for (unsigned i = 0; i < neighbors.size(); i++)
//        {
//          Node& node = mGraph.nodes[neighbors[i]];
//
//          if (!node.visited)
//          {
//            unsigned v = node.index;
//
//            float cost = dist[u] + mGraph.getCost(u, v);
//
//            if (cost < dist[v])
//            {
//              dist[v] = cost;
//              prev[v] = u;
//            }
//          }
//        }
//      }
//
//      mGraph.nodes[u].visited = true;
//    }
//
//    for (unsigned i = 0; i < mGraph.num_nodes; i++)
//    {
//      Node& node = mGraph.nodes[i];
//
//      debugPrint(0, "%u: %.2f %u", node.index, dist[i], prev[i]);
//    }
//    
//    debugPrint(0, "");
//
//    mPath.clear();
//
//    bool pathExists = true;
//    unsigned index = mDestNode;
//
//    do
//    {
//      mPath.push_front(index);
//      index = prev[index];
//
//      if (index >= mGraph.num_nodes)
//      {
//        pathExists = false;
//        break;
//      }
//    } while (index != mSourceNode);
//    
//    if (pathExists)
//    {
//      mPath.push_front(mSourceNode);
//    
//      wstring path = L"";
//      for (unsigned i = 0; i < mPath.size(); i++)
//      {
//        path += string_format(L"%i%s", mPath[i] >= mGraph.num_nodes ? -1 : mPath[i], i == (mPath.size() - 1) ? L"" : L" -> ");
//      }
//
//      //path += string_format(L"%i", mSourceNode);
//
//      debugPrint(0, "dijkstra's path: %ws", path.c_str());
//    }
//    else
//    {
//      debugPrint(0, "dijkstra's path: n/a");
//    }
//    ///////////////////////////////////////////////////////////////////////////
//
//    if (dist)
//      delete[] dist;
//
//    if (prev)
//      delete[] prev;
//
//    success = true;
//  }
//
//  return (success);
//}
//
//Vector2 operator*(const Vector2& _p, const Rect& _rect)
//{
//  return Vector2(_rect.x + _p.x * _rect.w, _rect.y + _p.y * _rect.h);
//}
//
//Vector2 operator/(const Vector2& _p, const Rect& _rect)
//{
//  return Vector2((_p.x - _rect.x) / _rect.w, (_p.y - _rect.y) / _rect.h);
//}
//
//bool DijkstrasSystem::handleEvent(int _event, void* _eventData)
//{
//  bool handled = false;
//
//  switch (_event)
//  {
//    case EVENT_KEY_INPUT:
//    {
//      EventDataKeyInput& eventData = *(EventDataKeyInput *)_eventData;
//
//      if (eventData.down)
//      {
//        switch (eventData.key)
//        {
//          case ' ':
//          {
//            if (start())
//            {
//              debugPrint(0, "DijkstrasSystem::start() okay!");
//            }
//            else
//            {
//              mPath.clear();
//              debugPrint(0, "DijkstrasSystem::start() failed!");
//            }
//          } break;
//
//          case 'N':
//          {
//            vector<unsigned> neighbors;
//            
//            if (mGraph.getNeighbors(mHoveredNode, neighbors))
//            {
//              for (unsigned i = 0; i < neighbors.size(); i++)
//              {
//                debugPrint(0, "'%u' is neighbors with '%u'", mHoveredNode, neighbors[i]);
//              }
//            }
//          } break;
//
//          case 'Q':
//          case 'X':
//          {
//          } break;
//        }
//      }
//    } break;
//
//    case EVENT_MOUSE_INPUT:
//    {
//      EventDataMouseInput& eventData = *(EventDataMouseInput *)_eventData;
//
//      switch (eventData.event)
//      {
//        case MOUSE_EVENT_BUTTON_DOWN:
//        case MOUSE_EVENT_MOVE:
//        {
//          Vector2 p(float(eventData.x), float(mWindowSize.y - eventData.y));
//
//          mMousePos = p;
//
//          mHoveredNode = getClosestNode(mMousePos / mGraphArea);
//
//          if (eventData.down)
//          {
//            if (eventData.button & MOUSE_BUTTON1)
//              mSourceNode = mSelectedNode = mHoveredNode;
//            if (eventData.button & MOUSE_BUTTON3)
//              mDestNode = mSelectedNode = mHoveredNode;
//          }
//        } break;
//      }
//    } break;
//  }
//  return (handled);
//}
//
//void DijkstrasSystem::render(const CameraInfo& _cameraInfo)
//{
//  static const float NodeSize = 3.0f;
//
//  mWindowSize.x = _cameraInfo.width;
//  mWindowSize.y = _cameraInfo.height;
//
//  mGraphArea.x = _cameraInfo.x;
//  mGraphArea.y = _cameraInfo.y;
//  mGraphArea.w = _cameraInfo.width;
//  mGraphArea.h = _cameraInfo.height;
//
//  glQuad(mGraphArea);
//
//  for (unsigned i = 0; i < mGraph.num_nodes; i++)
//  {
//    string s;
//    Node& node = mGraph.nodes[i]; 
//    Vector2 p = node.point * mGraphArea;
//
//    if (mSourceNode == i)    { glColor4f(1.0f, 0.0f, 0.0f, 1.0f); s = "SRC"; }
//    else if (mDestNode == i) { glColor4f(1.0f, 0.0f, 1.0f, 1.0f); s = "DST"; }
//    else                     { glColor4f(1.0f, 1.0f, 1.0f, 1.0f); s = ""; }
//
//    glQuadFilled(Rect(p.x - NodeSize, p.y - NodeSize, NodeSize * 2.0f, NodeSize * 2.0f));
//
//    if (mHoveredNode == i)
//    {
//      glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
//      glQuad(Rect(p.x - NodeSize * 2.0f, p.y - NodeSize * 2.0f, NodeSize * 4.0f, NodeSize * 4.0f));
//    }
//
//    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
//    glRasterPos2f(p.x, p.y);
//    glText(string_format("%u %s", node.index, s.c_str()).c_str());
//  }
//
//  for (unsigned i = 0; i < mGraph.num_edges; i++)
//  {
//    Edge& edge = mGraph.edges[i];
//
//    Node& n1 = mGraph.nodes[edge.index[0]];
//    Node& n2 = mGraph.nodes[edge.index[1]];
//
//    glColor4f(1.0f, 0.0f, 0.0f, 1.0f);
//
//    glBegin(GL_LINES);
//      glVertex2f(n1.point * mGraphArea);
//      glVertex2f(n2.point * mGraphArea);
//    glEnd();
//
//    Vector2 mid = ((n1.point + n2.point) / 2.0f) * mGraphArea;
//
//    glColor4f(0.0f, 1.0f, 0.0f, 1.0f);
//    glCross(mid.x, mid.y, NodeSize);
//
//    glRasterPos2f(mid.x, mid.y);
//    glText(string_format("%i", edge.cost).c_str());
//  }
//
//  glPushAttrib(GL_LINE_BIT);
//    
//    glLineWidth(3.0f);
//    
//    glColor4f(0.0f, 0.0f, 1.0f, 1.0f);
//    glBegin(GL_LINE_STRIP);
//      for (unsigned i = 0; i < mPath.size(); i++)
//      {
//        Node& node = mGraph.nodes[mPath[i]];
//
//        glVertex2f(node.point * mGraphArea);
//      }
//    glEnd();
//  glPopAttrib();
//
//  glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
//  glCross(mMousePos, 15.0f);
//
//  Vector2 p = mMousePos / mGraphArea;
//  glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
//  glRasterPos2i(50, 50);
//  glText(string_format("((%.2f,%.2f)) (%.2f,%.2f)", mMousePos.x, mMousePos.y, p.x, p.y).c_str());
//}
//
//void DijkstrasSystem::update(double _updateDelta)
//{
//}
//
//bool DijkstrasSystem::preprocessGraph()
//{
//  bool success = true;
//
//  for (unsigned i = 0; i < mGraph.num_nodes; i++)
//  {
//    Node& node = mGraph.nodes[i];
//
//    node.index = i;
//  }
//
//  return (success);
//}
