#pragma once

#include "MglAppState.h"

#include "EffectSystem.h"
#include "EntitySystem.h"
#include "WeaponSystem.h"
#include "ProximitySystem.h"
#include "RadarSystem.h"
#include "StarSystem.h"

class AppStateRunning : public AppState
{
public:
  enum
  {
    STATE_TITLE,
    STATE_NEXT_LEVEL,
    STATE_RUNNING,
    STATE_GAME_OVER,
    STATE_COUNT
  };

public:
  AppStateRunning();
  virtual ~AppStateRunning();

  virtual bool handleEvent(int _event, void* _eventData);
  virtual void render();
  virtual void update(double _deltaTime);

protected:
  void checkEvents();

  void setLevel(int _level);

protected:
  foundation::Array<Mgl::EventHeader*> mEvents;

  Mgl::CameraInfo mCameraInfo;

  EffectSystem    mEffectSystem;
  EntitySystem    mEntitySystem;
  WeaponSystem  mParticleSystem;
  ProximitySystem mProximitySystem;
  StarSystem      mStarSystem;
  RadarSystem     mRadarSystem;

  ID  mPlayerId;
  int mState; 
  int mCurrLevel;
};