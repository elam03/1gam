#pragma once

#include "MglSystem.h"
#include "ShipsTypes.h"

#include "MglEventSystem.h"

#include "collection_types.h"

class EntitySystem;
class WeaponSystem;

class ProximitySystem
{
public:
  ProximitySystem();
  virtual ~ProximitySystem();

  void init(EntitySystem* _entitySystem, WeaponSystem* _ParticleSystem);

  void clear();
  void render(const Mgl::CameraInfo& _cameraInfo);
  void update(double _deltaTime);

protected:
  void sendEvent(int _event, int _collisionType, ID _id1, ID _id2, const foundation::Vector3& _where);

  EntitySystem*   mEntitySystem;
  WeaponSystem* mParticleSystem;

  double mLastUpdatedTime;
  double mRunTime;

  /////////////////////////////////////////////////////////////////////////////
public:
  enum
  {
    TYPE_ENTITY_TO_ENTITY,
    TYPE_ENTITY_TO_PARTICLE,
    TYPE_COUNT
  };

  enum
  {
    EVENT_COLLISION,
    EVENT_WARNING,
    EVENT_COUNT
  };

  bool getEvents(foundation::Array<Mgl::EventHeader*>& _packets);
  bool flushEvents();

protected:
  EventSystem mOutputEvents;
  /////////////////////////////////////////////////////////////////////////////

};
