#pragma once

#include "MglApplication.h"

class AppState;
class ApplicationEvent;

class ShipsApplication : public MglApplication
{
public:
  ShipsApplication();
  virtual ~ShipsApplication();

protected:
  virtual bool appInit();
  virtual void appClose();
};