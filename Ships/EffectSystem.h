#pragma once

#include "MglEventSystem.h"
#include "MglSystem.h"
#include "ShipsTypes.h"

#include <string>

class EffectSystem
{
public:
  EffectSystem();
  virtual ~EffectSystem();

  //ID   add(int _type, double _duration, int _numParticles, const foundation::Vector3& _color, const foundation::Vector3& _position, const foundation::Vector3& _velocity);
  ID   addExplosion(double _duration, float _baseVelocity, int _numParticles, const foundation::Vector3& _color, const foundation::Vector3& _position, const foundation::Vector3& _velocity);
  ID   addField(double _duration, float _angle, float _arcLength, const foundation::Vector3& _color, const foundation::Vector3& _position, const foundation::Vector3& _velocity);
  void remove(ID _idToRemove);
  void clear();
  void render(const Mgl::CameraInfo& _cameraInfo);
  void update(double _deltaTime);
  
  System<Effect> mSystem;

  /////////////////////////////////////////////////////////////////////////////
public:
  enum
  {
    EVENT_EFFECT_FINISHED,
    EVENT_COUNT
  };

  bool getEvents(foundation::Array<Mgl::EventHeader*>& _packets);
  bool flushEvents();

protected:
  EventSystem mOutputEvents;

  /////////////////////////////////////////////////////////////////////////////
};