#include "AppStateRunning.h"

#include "MglUtils.h"
#include "ShipsApplication.h"

#include "array.h"
#include "memory.h"

#include <fstream>
#include "MglGl.h"

using namespace foundation;
using namespace std;
using namespace Mgl;

AppStateRunning::AppStateRunning() : 
  AppState(),

  mEvents(memory_globals::default_allocator()),
  
  mCameraInfo(),

  mEffectSystem(),
  mEntitySystem(),
  mParticleSystem(),
  mProximitySystem(),
  mStarSystem(),
  mRadarSystem(),

  mPlayerId(ID_INVALID)
{
}

AppStateRunning::~AppStateRunning()
{
}

void AppStateRunning::setLevel(int _level)
{
  float w = (float)mApplication->mWindowWidth;
  float h = (float)mApplication->mWindowHeight;

  float fieldSize = 500.0f;
  float gameFieldW = fieldSize;
  float gameFieldH = fieldSize;

  /////////////////////////////////////////////////////////////////////////
  mEffectSystem.clear();
  mEntitySystem.clear();
  mParticleSystem.clear();
  mProximitySystem.clear();
  /////////////////////////////////////////////////////////////////////////

  if (_level > 0)
  {
    Vector3 p = { w / 2.0f, h / 2.0f, 0.0f };
    mPlayerId = mEntitySystem.add(ENTITY_TYPE_PLAYER, p);
  
    /////////////////////////////////////////////////////////////////////////
    mRadarSystem.setPlayerEntityId(mPlayerId);
    /////////////////////////////////////////////////////////////////////////
    int count = 0;

    count = random(5, 15);
    //count = 1;
    for (int i = 0; i < count; i++)
    {
      Vector3 p = { randomf(-gameFieldW, gameFieldW), randomf(-gameFieldH, gameFieldH), 0.0f };
      mEntitySystem.mSystem.lookup(mEntitySystem.add(ENTITY_TYPE_SMALL_SHIP, p)).targetId = mPlayerId;
    }

    count = random(0, 3);
    //count = 0;
    for (int i = 0; i < count; i++)
    {
      Vector3 p = { randomf(-gameFieldW, gameFieldW), randomf(-gameFieldH, gameFieldH), 0.0f };
      mEntitySystem.mSystem.lookup(mEntitySystem.add(ENTITY_TYPE_MEDIUM_SHIP, p)).targetId = mPlayerId;
    }

    count = mCurrLevel;
    //count = 0;
    for (int i = 0; i < count; i++)
    {
      Vector3 p = { randomf(-gameFieldW, gameFieldW), randomf(-gameFieldH, gameFieldH), 0.0f };
      mEntitySystem.mSystem.lookup(mEntitySystem.add(ENTITY_TYPE_MOTHER_SHIP, p)).targetId = mPlayerId;
    }

    mCameraInfo.x = 0.0f;
    mCameraInfo.y = 0.0f;
  }
}

bool AppStateRunning::handleEvent(int _event, void* _eventData)
{
  bool handled = false;

  switch (_event)
  {
    case (EVENT_INIT):
    {
      mEntitySystem.mApplication = mApplication;
      mProximitySystem.init(&mEntitySystem, &mParticleSystem);
      mRadarSystem.init(&mEntitySystem);

      float w = (float)mApplication->mWindowWidth;
      float h = (float)mApplication->mWindowHeight;
      mCameraInfo.width  = w;
      mCameraInfo.height = h;
    } break;

    case (EVENT_SHOW):
    {
      mState = STATE_TITLE;
      mCurrLevel = 0;
      setLevel(0);

      mStarSystem.generate(mCameraInfo);
    } break;

    case (EVENT_CLOSE):
    {
      mStarSystem.clear();
      setLevel(0);
    } break;

    case EVENT_KEY_INPUT:
    {
      EventDataKeyInput& eventData = *(EventDataKeyInput *)_eventData;

      if (eventData.down)
      {
        switch (eventData.key)
        {
          case 'R':
          {
            debugPrint(0, "radar toggled!");
            mRadarSystem.enable(!mRadarSystem.isEnabled());
          } break;
        }
      }
    } break;
  }

  return (handled);
}

void AppStateRunning::render()
{
  float w = (float)mApplication->mWindowWidth;
  float h = (float)mApplication->mWindowHeight;
  
  mApplication->renderBorder();

  switch (mState)
  {
    case STATE_TITLE:
    {
      glRasterPos2f(w / 4.0f, h / 4.0f * 3.0f);
      glText("Ships");

      glRasterPos2f(w / 4.0f, h / 2.0f);
      glText("Press 'Enter' to begin...");
    } break;

    case STATE_NEXT_LEVEL:
    {
      char buf[32];
      sprintf_s(buf, 32, "You finished level %i!", mCurrLevel);
      glRasterPos2f(w / 4.0f, h / 4.0f * 3.0f);
      glText(buf);

      glRasterPos2f(w / 4.0f, h / 2.0f);
      glText("Press 'Enter' to begin the next level...");
    } break;

    case STATE_RUNNING:
    {
    } break;

    case STATE_GAME_OVER:
    {
      glRasterPos2f(w / 4.0f, h / 4.0f * 3.0f);
      glText("Game over...");

      glRasterPos2f(w / 4.0f, h / 2.0f);
      glText("Press 'Enter' to begin again...");
    } break;
  }

  glPushMatrix();
    glTranslatef(-mCameraInfo.x, -mCameraInfo.y, 0.0f);
    mEffectSystem.render(mCameraInfo);
    mEntitySystem.render(mCameraInfo);
    mParticleSystem.render(mCameraInfo);
    mProximitySystem.render(mCameraInfo);
  glPopMatrix();

  mStarSystem.render(mCameraInfo);
  mRadarSystem.render(mCameraInfo);
}

void AppStateRunning::update(double _deltaTime)
{
  float updateDelta = float(_deltaTime);

  static const float cameraMovementSpeed = 100.0f;

  switch (mState)
  {
    case STATE_TITLE:
    {
      mCameraInfo.x += cameraMovementSpeed * updateDelta;

      if (mApplication->isKeyPressed(VK_RETURN))
      {
        mState = STATE_RUNNING;
        mCurrLevel = 1;
        setLevel(mCurrLevel);
      }
      mStarSystem.updateCamera(-cameraMovementSpeed * updateDelta, 0.0f);
    } break;

    case STATE_RUNNING:
    {
    } break;

    case STATE_NEXT_LEVEL:
    {
      if (mApplication->isKeyPressed(VK_RETURN))
      {
        mState = STATE_RUNNING;
        mCurrLevel++;
        setLevel(mCurrLevel);
      }
    } break;

    case STATE_GAME_OVER:
    {
      if (mApplication->isKeyPressed(VK_RETURN))
      {
        mState = STATE_RUNNING;
        mCurrLevel = 1;
        setLevel(mCurrLevel);
      }
    } break;
  }

  if (mApplication->isKeyPressed(VK_LEFT))
  {
    mCameraInfo.x -= cameraMovementSpeed * updateDelta;
  }
  if (mApplication->isKeyPressed(VK_RIGHT))
  {
    mCameraInfo.x += cameraMovementSpeed * updateDelta;
  }
  if (mApplication->isKeyPressed(VK_UP))
  {
    mCameraInfo.y += cameraMovementSpeed * updateDelta;
  }
  if (mApplication->isKeyPressed(VK_DOWN))
  {
    mCameraInfo.y -= cameraMovementSpeed * updateDelta;
  }

  if (mPlayerId != ID_INVALID && mEntitySystem.mSystem.has(mPlayerId))
  {
    Entity& player = mEntitySystem.mSystem.lookup(mPlayerId);

    if (mApplication->isKeyPressed(VK_SPACE)) player.weapons[0].fire = true;
    else                                      player.weapons[0].fire = false;

    if (mApplication->isKeyPressed('O')) player.weapons[1].fire = true;
    else                                 player.weapons[1].fire = false;

    if (mApplication->isKeyPressed('P')) player.weapons[2].fire = true;
    else                                 player.weapons[2].fire = false;

    if (mApplication->isKeyPressed('L')) player.weapons[3].fire = true;
    else                                 player.weapons[3].fire = false;

    if (mApplication->isKeyPressed('W'))
      player.thrust = 1.0f;
    else if (mApplication->isKeyPressed('S'))
      player.thrust = -1.0f;
    else
      player.thrust = 0.0f;

    if (mApplication->isKeyPressed('A'))
      player.turn = 1.0f;
    else if (mApplication->isKeyPressed('D'))
      player.turn = -1.0f;
    else
      player.turn = 0.0f;

    float offset = 200.0f;

    static float multiplier = 1.0f;

    if (player.position.x < (mCameraInfo.x) || player.position.x > (mCameraInfo.x + mCameraInfo.width) || player.position.y < (mCameraInfo.y) || player.position.y > (mCameraInfo.y + mCameraInfo.height))
      multiplier *= 1.5f;
    else
      multiplier = 1.0f;

    if (player.position.x < (mCameraInfo.x + offset))
    {
      mCameraInfo.x -= cameraMovementSpeed * updateDelta * multiplier;
      mStarSystem.updateCamera(-cameraMovementSpeed * updateDelta * multiplier, 0.0f);
    }
    if (player.position.x > (mCameraInfo.x + mCameraInfo.width - offset))
    {
      mCameraInfo.x += cameraMovementSpeed * updateDelta * multiplier;
      mStarSystem.updateCamera(cameraMovementSpeed * updateDelta * multiplier, 0.0f);
    }
    if (player.position.y < (mCameraInfo.y + offset))
    {
      mCameraInfo.y -= cameraMovementSpeed * updateDelta * multiplier;
      mStarSystem.updateCamera(0.0f, -cameraMovementSpeed * updateDelta * multiplier);
    }
    if (player.position.y > (mCameraInfo.y + mCameraInfo.height - offset))
    {
      mCameraInfo.y += cameraMovementSpeed * updateDelta * multiplier;
      mStarSystem.updateCamera(0.0f, cameraMovementSpeed * updateDelta * multiplier);
    }
  }
  
  mParticleSystem.update(&mEntitySystem);

  mEffectSystem.update(updateDelta);
  mEntitySystem.update(updateDelta);
  mParticleSystem.update(updateDelta);
  mProximitySystem.update(updateDelta);
  mStarSystem.update(updateDelta);
  mRadarSystem.update(updateDelta);

  checkEvents();
}

void AppStateRunning::checkEvents()
{
  if (mProximitySystem.getEvents(mEvents))
  {
    for (unsigned i = 0; i < array::size(mEvents); i++)
    {
      switch (mEvents[i]->type)
      {
        case ProximitySystem::EVENT_COLLISION:
        {
          ProximityEvent& e = *((ProximityEvent *)mEvents[i]->data);

          if (e.type == ProximitySystem::TYPE_ENTITY_TO_PARTICLE)
          {
            ID entityId   = e.ids[0];
            ID particleId = e.ids[1];
          
            if (mEntitySystem.mSystem.has(entityId) && mParticleSystem.mSystem.has(particleId))
            {
              Entity& entity     = mEntitySystem.mSystem.lookup(entityId);
              Shot& shot = mParticleSystem.mSystem.lookup(particleId);

              if (shot.entityId != entityId)
              {
                if (entityId == mPlayerId || shot.entityId == mPlayerId)
                {
                  Vector3 color = {0.0f, 1.0f, 0.0f};
                  mEffectSystem.addField(1.0, 180.0f + angleOf(shot.entity.velocity), entity.size / 2.0f, color, e.where, entity.velocity);

                  if (entity.life > 0.0f)
                  {
                    entity.life -= 5.0f;

                    if (entity.life <= 0.0f)
                    {
                      Vector3 color = {1.0f, 0.0f, 0.0f};
                      mEffectSystem.addExplosion(3.0, 15.0f + entity.size, random(64, 128), color, entity.position, entity.velocity / 4.0f);

                      if (entityId == mPlayerId)
                      {
                        mState = STATE_GAME_OVER;
                        mPlayerId = ID_INVALID;
                      }

                      entity.life = 0.0f;
                      mEntitySystem.remove(entityId);

                      int motherShipCount = 0;
                      for (unsigned int i = 0; i < mEntitySystem.mSystem.getNumObjects(); i++)
                      {
                        if (mEntitySystem.mSystem._objects[i].type == ENTITY_TYPE_MOTHER_SHIP)
                          motherShipCount++;
                      }

                      if (motherShipCount <= 0)
                      {
                        mState = STATE_NEXT_LEVEL;

                        int count = mEntitySystem.mSystem.getNumObjects();
                        
                        for (int i = count - 1; i >= 0; i--)
                        {
                          Entity& e = mEntitySystem.mSystem._objects[i];
                          int id = e.id;

                          if (id != mPlayerId)
                          {
                            Vector3 color = {1.0f, 0.0f, 0.0f};
                            mEffectSystem.addExplosion(3.0, 15.0f + e.size, random(64, 128), color, e.position, e.velocity / 4.0f);
                            
                            mEntitySystem.remove(id);
                          }
                        }
                      }
                    }
                  }

                  mParticleSystem.remove(particleId);
                }
              }
            }
          }
        } break;
      }
    }
    mProximitySystem.flushEvents();
  }

  if (mEntitySystem.getEvents(mEvents))
  {
    for (unsigned i = 0; i < array::size(mEvents); i++)
    {
      switch (mEvents[i]->type)
      {
        case EntitySystem::EVENT_WEAPON_FIRE:
        {
          EntityTargetInfo& e = *((EntityTargetInfo *)mEvents[i]->data);

          ID entityId = e.entityId;
          ID targetId = e.targetId;

          if (mEntitySystem.mSystem.has(entityId))
          {
            Entity& entity = mEntitySystem.mSystem.lookup(entityId);
            Weapon& weapon = entity.weapons[e.entityWeaponIndex];
            Vector3 p = entity.position;

            switch (weapon.type)
            {
              case SHOT_TYPE_NORMAL:
              {
                Vector3 v = makeVector3(makeVector2(entity.angle + weapon.offsetAngle, 150.0f)) + weapon.offsetPosition;
                mParticleSystem.addShot(p, v, entityId, entity.targetId, .7);
              } break;
              
              case SHOT_TYPE_MISSLE:
              {
                Vector3 v = makeVector3(makeVector2(entity.angle + weapon.offsetAngle, 150.0f)) + weapon.offsetPosition;
                mParticleSystem.addMissle(p, v, entityId, entity.targetId, 5.0);
              } break;
              case SHOT_TYPE_SPAWN:
              {
                Vector3 v = makeVector3(makeVector2(entity.angle + weapon.offsetAngle, 150.0f)) + weapon.offsetPosition;
                mEntitySystem.mSystem.lookup(mEntitySystem.add(ENTITY_TYPE_SMALL_SHIP, p, v)).targetId = entity.targetId;
              } break;
            }
          }
        } break;
      }
    }
    mEntitySystem.flushEvents();
  }

  if (mEffectSystem.getEvents(mEvents))
  {
    for (unsigned i = 0; i < array::size(mEvents); i++)
    {
      switch (mEvents[i]->type)
      {
        case EffectSystem::EVENT_EFFECT_FINISHED:
        {
          EffectEvent& e = *((EffectEvent *)mEvents[i]->data);

          ID effectId = e.effectId;

          if (mEffectSystem.mSystem.has(effectId))
          {
            Effect& effect = mEffectSystem.mSystem.lookup(effectId);

            mEffectSystem.remove(effect.id);
          }
        } break;
      }
    }
    mEffectSystem.flushEvents();
  }
}