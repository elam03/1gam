#include "EffectSystem.h"

#include "MglResourceManager.h"
#include "MglUtils.h"
#include "ShipsApplication.h"

#include "MglGl.h"
#include "MglUtils.h"

using namespace foundation;
using namespace std;
using namespace Mgl;

EffectSystem::EffectSystem() :
  mOutputEvents(),
  mSystem()
{
  mOutputEvents.init();
}

EffectSystem::~EffectSystem()
{
  mOutputEvents.close();
}

ID EffectSystem::addExplosion(double _duration, float _baseVelocity, int _numParticles, const Vector3& _color, const foundation::Vector3& _position, const Vector3& _velocity)
{
  ID id = mSystem.add();

  if (mSystem.has(id))
  {
    Effect& effect = mSystem.lookup(id);
    memset(&effect, 0, sizeof(Effect));

    effect.id    = id;
    effect.type  = EFFECT_TYPE_EXPLOSION;
    effect.timeLeft = _duration;
    effect.duration = effect.timeLeft;

    effect.position = _position;
    effect.velocity = _velocity;

    int count = (_numParticles == 0) ? random(5, 15) : _numParticles;
    effect.unitCount = count;

    for (int i = 0; i < count; i++)
    {
      Vector3 p;
      p.x = p.y = p.z = 0.0f;

      Vector3 v = makeVector(randomf(0.0f, 360.0f), randomf(0.1f, _baseVelocity));

      effect.units[i].color = _color;
      effect.units[i].timeLeft = (float)(_duration) - randomf(0.0f, (float)_duration / 2.0f);
      effect.units[i].duration = effect.units[i].timeLeft;
      effect.units[i].position = p;
      effect.units[i].velocity = v;
    }
  }

  return (id);
}

ID EffectSystem::addField(double _duration, float _angle, float _arcLength, const Vector3& _color, const Vector3& _position, const Vector3& _velocity)
{
  ID id = mSystem.add();

  if (mSystem.has(id))
  {
    Effect& effect = mSystem.lookup(id);
    memset(&effect, 0, sizeof(Effect));

    effect.id    = id;
    effect.type  = EFFECT_TYPE_FIELD;
    effect.timeLeft = _duration;
    effect.duration = effect.timeLeft;
    effect.angle = _angle;
    effect.arcLength = _arcLength;

    effect.position = _position;
    effect.velocity = _velocity;
    effect.color    = _color;
  }

  return (id);
}

void EffectSystem::remove(ID _idToRemove)
{
  mSystem.remove(_idToRemove);
}

void EffectSystem::clear()
{
  mSystem.reset();
}

void EffectSystem::render(const CameraInfo& _cameraInfo)
{
  for (unsigned i = 0; i < mSystem.getNumObjects(); i++)
  {
    Effect& effect = mSystem._objects[i];

    if (!contains(_cameraInfo, makeVector2(effect.position)))
      continue;

    glPushMatrix();
      glTranslatef(effect.position);

      switch (effect.type)
      {
        case EFFECT_TYPE_EXPLOSION:
        {
          glColor3f(1.0f, 1.0f, 1.0f);
          glBegin(GL_LINES);
            glVertex2f(-3.0f,  0.0f);
            glVertex2f( 3.0f,  0.0f);
            glVertex2f( 0.0f, -3.0f);
            glVertex2f( 0.0f,  3.0f);
          glEnd();

          glBegin(GL_LINES);
            for (int i = 0; i < effect.unitCount; i++)
            {
            //effect.units[i].color = effect.units[i].color * (effect.units[i].timeLeft / effect.units[i].duration);
              glColor3f(effect.units[i].color * (effect.units[i].timeLeft / effect.units[i].duration));
              //glColor3f(effect.units[i].color);
              glVertex2f(effect.units[i].position.x - 3.0f, effect.units[i].position.y);
              glVertex2f(effect.units[i].position.x + 3.0f, effect.units[i].position.y);

              glVertex2f(effect.units[i].position.x, effect.units[i].position.y - 3.0f);
              glVertex2f(effect.units[i].position.x, effect.units[i].position.y + 3.0f);
            }
          glEnd();
        } break;
        
        case EFFECT_TYPE_FIELD:
        {
          float r = effect.arcLength;
          glColor3f(1.0f, 1.0f, 1.0f);
          glPushMatrix();
            glRotatef(effect.angle, 0.0f, 0.0f, 1.0f);
            glColor3f(effect.color);

            float factor = (effect.arcLength / 20.0f);

            glBegin(GL_LINE_STRIP);
              glVertex2f(-5.0f * factor, -effect.arcLength);
              glVertex2f(-4.0f * factor, -effect.arcLength / 4.0f * 3.0f);
              glVertex2f(-2.0f * factor, -effect.arcLength / 2.0f);
              glVertex2f(-1.0f * factor, -effect.arcLength / 4.0f);
              glVertex2f(-0.0f * factor, 0.0f);
              glVertex2f(-1.0f * factor,  effect.arcLength / 4.0f);
              glVertex2f(-2.0f * factor,  effect.arcLength / 2.0f);
              glVertex2f(-4.0f * factor,  effect.arcLength / 4.0f * 3.0f);
              glVertex2f(-5.0f * factor,  effect.arcLength);
            glEnd();
          glPopMatrix();
        } break;
      }
    glPopMatrix();
  }
}

void EffectSystem::update(double _deltaTime)
{
  for (unsigned i = 0; i < mSystem.getNumObjects(); i++)
  {
    Effect& effect = mSystem._objects[i];

    effect.position = effect.position + effect.velocity * (float)_deltaTime;

    if (effect.timeLeft < 0.0)
    {
      EffectEvent e;
      e.effectId = effect.id;
      
      mOutputEvents.queueEvent(EVENT_EFFECT_FINISHED, &e, sizeof(EffectEvent));
    }
    else
    {
      effect.timeLeft -= _deltaTime;

      switch (effect.type)
      {
        case EFFECT_TYPE_EXPLOSION:
        {
          for (int i = 0; i < effect.unitCount; i++)
          {
            effect.units[i].position = effect.units[i].position + effect.units[i].velocity * (float)_deltaTime;
            effect.units[i].timeLeft -= (float)_deltaTime;
            //effect.units[i].color = effect.units[i].color * (effect.units[i].timeLeft / effect.units[i].duration);
          } 
        } break;

        case EFFECT_TYPE_FIELD:
        {
          effect.color.x -= (float)_deltaTime;
          effect.color.y -= (float)_deltaTime;
          effect.color.z -= (float)_deltaTime;
        } break;
      }
    }
  }
}

bool EffectSystem::getEvents(Array<EventHeader*>& _packets)
{
  return (mOutputEvents.getEvents(_packets));
}

bool EffectSystem::flushEvents()
{
  return (mOutputEvents.flushEvents());
}