#pragma once

#include "MglEventSystem.h"
#include "MglSystem.h"
#include "ShipsTypes.h"

#include <string>

class MglApplication;

class EntitySystem
{
public:
  EntitySystem();
  virtual ~EntitySystem();

  ID   add(int _type, const foundation::Vector3& _position, const foundation::Vector3& _velocity = foundation::Vector3());
  void remove(ID _idToRemove);
  void clear();
  void render(const Mgl::CameraInfo& _cameraInfo);
  void update(double _deltaTime);
  
  System<Entity> mSystem;
  MglApplication* mApplication;

protected:
  void drawThrusters();
  void drawBoxShip(const std::wstring& _textureTag = L"");
  void drawHealthBar(Entity& _entity);
  void track(Entity& _entity, const foundation::Vector3& _targetPos, EntityTargetInfo& _targetInfo, bool _tracking = true);
  void updateEntity(Entity& _entity, double _deltaTime);
  void updateEntityAi(Entity& _entity);

  /////////////////////////////////////////////////////////////////////////////
public:
  enum
  {
    EVENT_LOW_HEALTH,
    EVENT_TARGET_LOST,
    EVENT_TARGET_LOCKED,
    EVENT_WEAPON_FIRE,
    EVENT_WEAPON_READY,
    EVENT_THRUSTING,
    EVENT_COUNT
  };

  bool getEvents(foundation::Array<Mgl::EventHeader*>& _packets);
  bool flushEvents();

protected:
  EventSystem mOutputEvents;

  bool initGL();
  void closeGL();

  unsigned mVao;
  /////////////////////////////////////////////////////////////////////////////
};