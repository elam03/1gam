#include "ShipsApplication.h"

#include "AppStateRunning.h"
#include "MglResourceManager.h"

using namespace std;
using namespace foundation;
using namespace Mgl;

ShipsApplication::ShipsApplication() : MglApplication()
{
}

ShipsApplication::~ShipsApplication()
{
}

bool ShipsApplication::appInit()
{
  bool success = false;

  mUpdatesPerSecond = 120;

  ResourceManager::Instance().addTexture(L"player", L"..\\assets\\cobble_blood1.png");
  ResourceManager::Instance().addTexture(L"hill", L"..\\assets\\cobble_blood10.png");
  ResourceManager::Instance().addTexture(L"grass", L"..\\assets\\grass0.png");
  ResourceManager::Instance().addTexture(L"shore", L"..\\assets\\floor_sand_stone4.png");
  ResourceManager::Instance().addTexture(L"water", L"..\\assets\\dngn_open_sea.png");

  addAppState(new AppStateRunning);

  success = true;

  return (success);
}

void ShipsApplication::appClose()
{
  MglApplication::close();
}