#pragma once

#include "MglEventSystem.h"
#include "MglSystem.h"
#include "MglUtils.h"
#include "ShipsTypes.h"

#include <string>

class EntitySystem;

class RadarSystem
{
public:
  RadarSystem();
  virtual ~RadarSystem();

  void init(EntitySystem* _entitySystem);
  void setPlayerEntityId(ID _playerEntityId);

  void render(const Mgl::CameraInfo& _cameraInfo);
  void update(double _deltaTime);

  void enable(bool _enabled) { mEnabled = _enabled; }
  bool isEnabled() const { return mEnabled; }
  
  /////////////////////////////////////////////////////////////////////////////
protected:
  Mgl::Rect mDimensions;
  float     mRadarRange;
  bool      mEnabled;

  EntitySystem* mEntitySystem;
  ID            mPlayerId;

  struct RadarInfo
  {
    foundation::Vector2 position;
    float               size;
    foundation::Vector3 color;
  };

  foundation::Array<RadarInfo> mRadarInfo;
  
  /////////////////////////////////////////////////////////////////////////////
  // Events
public:
  bool getEvents(foundation::Array<Mgl::EventHeader*>& _packets);
  bool flushEvents();

protected:
  EventSystem mOutputEvents;

  /////////////////////////////////////////////////////////////////////////////
};