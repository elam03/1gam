#pragma once

#include "MglSystem.h"
#include "ShipsTypes.h"

class MglApplication;

class StarSystem
{
public:
  StarSystem();
  virtual ~StarSystem();

  bool generate(const Mgl::CameraInfo& _cameraInfo);
  ID   add(const foundation::Vector3& _position, float _size);
  void remove(ID _idToRemove);
  void clear();
  void render(const Mgl::CameraInfo& _cameraInfo);
  void update(double _deltaTime);
  bool updateCamera(float _dx, float _dy);

  System<Star> mSystem;
  Mgl::CameraInfo mCameraInfo;
};