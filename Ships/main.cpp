#include <iostream>
#include <windows.h>
#include <windowsx.h>

#include "MglUtils.h"

#include "ShipsApplication.h"

#include "memory.h"

using namespace foundation;
using namespace std;

int WINAPI WinMain(HINSTANCE _hInstance, HINSTANCE _hInstancePrev, LPSTR _commandLine, int _cmdShow)
{
  Mgl::initConsole();

  memory_globals::init();

  MglApplication* application = new ShipsApplication;


  if (application)
  {
    MglApplication::InitParams initParams;

    initParams.mHInstance = _hInstance;
    initParams.mWindowX = 5;
    initParams.mWindowY = 5;
    initParams.mWindowWidth  = 500;
    initParams.mWindowHeight = 500;
    initParams.mWindowTitle = TEXT("Ships");

    if (application->init(initParams))
    {
      application->run();
    }
   
    application->close();
  }

  delete application;
  
  memory_globals::shutdown();
  
  Mgl::closeConsole();

  return 0;
}
