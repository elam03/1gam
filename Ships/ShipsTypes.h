#pragma once

#include "MglTypes.h"

#define MAX_STRING_SIZE 128
#define MAX_VERTICES 64
#define MAX_LINES 64
#define MAX_PARTICLES 64
#define MAX_TARGETS 3
#define MAX_WEAPONS 4

#define MAX_UNITS_PER_EFFECT 128

enum
{
  APP_STATE_INTRO,
  APP_STATE_RUNNING,
  APP_STATE_GAME_OVER,
  APP_STATE_COUNT
};

enum
{
  ENTITY_TYPE_PLAYER,
  ENTITY_TYPE_SMALL_SHIP,
  ENTITY_TYPE_MEDIUM_SHIP,
  ENTITY_TYPE_MOTHER_SHIP,
  ENTITY_TYPE_COUNT
};

enum
{
  SHOT_TYPE_NORMAL,
  SHOT_TYPE_MISSLE,
  SHOT_TYPE_SPAWN,
  SHOT_TYPE_COUNT
};

enum
{
  EFFECT_TYPE_EXPLOSION,
  EFFECT_TYPE_FIELD,
  EFFECT_TYPE_COUNT
};

enum
{
  ACTION_FIRE     = 0x0001,
  ACTION_ALT_FIRE = 0x0002,
  ACTION_TELEPORT = 0x0004
};

///////////////////////////////////////////////////////////////////////////////
// Various Event Structures

struct ProximityEvent
{
  int                 type;
  ID                  ids[2];
  foundation::Vector3 where;
};

struct EntityEvent
{
  int type;
  ID ids[2];
};

struct EntityTargetInfo
{
  ID    entityId;
  int   entityWeaponIndex;
  ID    targetId;
  float angleTo;
  float distanceSquaredTo;
  bool  locked;
};

struct EffectEvent
{
  ID  effectId;
};


///////////////////////////////////////////////////////////////////////////////

struct Weapon
{
  int   type;         // type
  bool  fire;         // action

  bool  ready;        // is it ready?
  float waitTime;     // current time left to wait until it's ready
  float cooldownTime; // the cooldown time

  foundation::Vector3 offsetPosition;
  float               offsetAngle;
};

struct Entity
{
  ID    id;
  int   type;
  float angle;
  float size;
  float life;
  float lifeMax;

  // Actions
  float  thrust;
  float  turn;
  Weapon weapons[MAX_WEAPONS];

  float accelRate;
  float turnRate;

  // Target info
  ID    targetId;
  float targetDistThreshold;
  float targetAngleThreshold;
  bool  targetLocked;

  foundation::Vector3 position;
  foundation::Vector3 velocity;
};

///////////////////////////////////////////////////////////////////////////////

struct Shot
{
  ID     id;
  int    type;
  ID     entityId;
  double timeLeft;

  Entity entity;
  
  ID                  targetId;
  float               targetThrust;
  float               targetTurn;
  foundation::Vector3 targetPosition;
};

///////////////////////////////////////////////////////////////////////////////

struct Star
{
  ID                  id;
  float               size;
  foundation::Vector3 color;
  foundation::Vector3 position;
  foundation::Vector3 position2;
};

///////////////////////////////////////////////////////////////////////////////

struct EffectUnit
{
  float timeLeft;
  float duration;
  foundation::Vector3 position;
  foundation::Vector3 velocity;
  foundation::Vector3 color;
};

struct Effect
{
  ID     id;
  int    type;
  double timeLeft;
  double duration;
  float  angle;
  float  arcLength;

  foundation::Vector3 position;
  foundation::Vector3 velocity;
  foundation::Vector3 color;

  int        unitCount;
  EffectUnit units[MAX_UNITS_PER_EFFECT];
};
