#include "RadarSystem.h"

#include "array.h"
#include "memory.h"
#include "EntitySystem.h"
#include "MglGl.h"
#include "MglUtils.h"

using namespace foundation;
using namespace std;
using namespace Mgl;

RadarSystem::RadarSystem() :
  mEntitySystem(nullptr),
  mRadarInfo(memory_globals::default_allocator()),
  mPlayerId(ID_INVALID),
  mEnabled(true)
{
  mDimensions.x = 10;
  mDimensions.y = 10;
  mDimensions.w = 100;
  mDimensions.h = 100;

  mRadarRange = 1000.0f;
}

RadarSystem::~RadarSystem()
{
}

void RadarSystem::init(EntitySystem* _entitySystem)
{
  mEntitySystem = _entitySystem;
}

void RadarSystem::setPlayerEntityId(ID _playerEntityId)
{
  mPlayerId = _playerEntityId;
}

void RadarSystem::render(const CameraInfo& _cameraInfo)
{
  if (!mEnabled)
    return;

  glColor3f(0.0f, 0.0f, 0.0f);
  glQuadFilled(mDimensions);

  glColor3f(1.0f, 1.0f, 1.0f);
  glQuad(mDimensions);

  glEnable(GL_SCISSOR_TEST);
  glScissor((int)mDimensions.x, (int)mDimensions.y, (int)mDimensions.w, (int)mDimensions.h);

  glBegin(GL_LINES);
    for (unsigned i = 0; i < array::size(mRadarInfo); i++)
    {
      RadarInfo& radarInfo = mRadarInfo[i];

      glColor3f(radarInfo.color);
      glVertex2f(radarInfo.position.x - radarInfo.size + .5f, radarInfo.position.y);
      glVertex2f(radarInfo.position.x + radarInfo.size + .5f, radarInfo.position.y);
      glVertex2f(radarInfo.position.x, radarInfo.position.y - radarInfo.size + .5f);
      glVertex2f(radarInfo.position.x, radarInfo.position.y + radarInfo.size + .5f);
    }
  glEnd();
  
  glDisable(GL_SCISSOR_TEST);
}

void RadarSystem::update(double _deltaTime)
{
  if (!mEnabled)
    return;

  if (mEntitySystem)
  {
    array::clear(mRadarInfo);

    if (mEntitySystem->mSystem.has(mPlayerId))
    {
      Entity& player = mEntitySystem->mSystem.lookup(mPlayerId);
      Vector2 origin = makeVector2(player.position);

      for (unsigned i = 0; i < mEntitySystem->mSystem.getNumObjects(); i++)
      {
        Entity& entity = mEntitySystem->mSystem._objects[i];

        if (entity.id != mPlayerId)
        {
          Vector2 p = makeVector2(entity.position);
      
          RadarInfo radarInfo;
          radarInfo.position.x = .5f + ((p.x - origin.x) / (mRadarRange));
          radarInfo.position.y = .5f + ((p.y - origin.y) / (mRadarRange));

          radarInfo.position.x = mDimensions.x + (radarInfo.position.x * mDimensions.w);
          radarInfo.position.y = mDimensions.y + (radarInfo.position.y * mDimensions.h);

          if (entity.type == ENTITY_TYPE_MEDIUM_SHIP) radarInfo.size = 2.0f;
          else if (entity.type == ENTITY_TYPE_MOTHER_SHIP) radarInfo.size = 3.0f;
          else radarInfo.size = 1.0f;
          
          radarInfo.color.x = 1.0f;
          radarInfo.color.y = 0.0f;
          radarInfo.color.z = 0.0f;

          array::push_back(mRadarInfo, radarInfo);
        }
      }

      RadarInfo radarInfo;
      radarInfo.position.x = .5f;
      radarInfo.position.y = .5f;
      
      radarInfo.position.x = mDimensions.x + (radarInfo.position.x * mDimensions.w);
      radarInfo.position.y = mDimensions.y + (radarInfo.position.y * mDimensions.h);

      radarInfo.size = 3.0f;

      radarInfo.color.x = 1.0f;
      radarInfo.color.y = 1.0f;
      radarInfo.color.z = 1.0f;
      
      array::push_back(mRadarInfo, radarInfo);
    }
  }
}
