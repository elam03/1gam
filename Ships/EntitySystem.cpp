#include "EntitySystem.h"

#include "MglResourceManager.h"
#include "MglUtils.h"
#include "ShipsApplication.h"

#include "MglGl.h"

using namespace foundation;
using namespace std;
using namespace Mgl;

static const float DefaultAccelRate = 100.0f;
static const float DefaultTurnRate  = 180.0f;

static const float DefaultPlayerAccelRate = 200.0f;
static const float DefaultPlayerTurnRate  = 360.0f;

EntitySystem::EntitySystem() :
  mOutputEvents(),
  mSystem(),
  mApplication(0)

{
  mOutputEvents.init();

  initGL();
}

EntitySystem::~EntitySystem()
{
  closeGL();
  mOutputEvents.close();
}

bool EntitySystem::initGL()
{
  bool success = true;

  Vertex v[] = {
     1.0f,  1.0f, 0.0f,   1.0f,  1.0f,
    -1.0f,  1.0f, 0.0f,   0.0f,  1.0f,
     1.0f, -1.0f, 0.0f,   1.0f,  0.0f,
    -1.0f, -1.0f, 0.0f,   0.0f,  0.0f
  };

  unsigned int indices[] = {0, 1, 2, 3};

  ResourceManager::Instance().addGeometry(L"quad", 4, &v[0], 4, &indices[0]);

  mVao = ResourceManager::Instance().getGeometry(L"quad");

  return (success);
}

void EntitySystem::closeGL()
{
}

ID EntitySystem::add(int _type, const Vector3& _position, const Vector3& _velocity)
{
  ID id = mSystem.add();

  if (mSystem.has(id))
  {
    float life = 1.0f;
    float targetAngleThreshold = 0.0f;
    float targetDistThreshold = 0.0f;

    Entity& entity = mSystem.lookup(id);
    memset(&entity, 0, sizeof(Entity));

    entity.id    = id;
    entity.type  = _type;
    entity.angle = 0.0f;
    entity.size  = 10.0f;

    //for (int i = 0; i < MAX_WEAPONS; i++)
    //{
    //  entity.weapons[i].type = SHOT_TYPE_NORMAL;
    //  entity.weapons[i].offsetAngle = -20.0f + (i * 10.0f);
    //  entity.weapons[i].offsetPosition;
    //  entity.weapons[i].ready = true;
    //  entity.weapons[i].waitTime = 0.0f;
    //  entity.weapons[i].cooldownTime = weaponCooldown;
    //}

    switch (_type)
    {
      case ENTITY_TYPE_PLAYER:
      {
        entity.accelRate = DefaultPlayerAccelRate;
        entity.turnRate  = DefaultPlayerTurnRate;
        life = 100.0f;

        entity.weapons[0].type = SHOT_TYPE_NORMAL;
        entity.weapons[0].offsetAngle = 0.0f;
        entity.weapons[0].offsetPosition;
        entity.weapons[0].ready = true;
        entity.weapons[0].waitTime = 0.0f;
        entity.weapons[0].cooldownTime = 0.25f;

        entity.weapons[1].type = SHOT_TYPE_MISSLE;
        entity.weapons[1].offsetAngle = 0.0f;
        entity.weapons[1].offsetPosition;
        entity.weapons[1].ready = true;
        entity.weapons[1].waitTime = 0.0f;
        entity.weapons[1].cooldownTime = 0.05f;

        entity.weapons[2].type = SHOT_TYPE_MISSLE;
        entity.weapons[2].offsetAngle = 0.0f;
        entity.weapons[2].offsetPosition;
        entity.weapons[2].ready = true;
        entity.weapons[2].waitTime = 0.0f;
        entity.weapons[2].cooldownTime = 5.0f;
      } break;

      default:
      case ENTITY_TYPE_SMALL_SHIP:
      {
        entity.size  = 10.0f;
        entity.accelRate = randomf(90.0f, 120.0f);
        entity.turnRate  = randomf(45.0f, 60.0f);
        life = 10.0f;
        targetAngleThreshold = 10.0f;
        targetDistThreshold = 100.0f * 100.0f;

        //entity.weapons[0].type = SHOT_TYPE_NORMAL;
        //entity.weapons[0].offsetAngle = 0.0f;
        //entity.weapons[0].offsetPosition;
        //entity.weapons[0].ready = true;
        //entity.weapons[0].waitTime = 0.0f;
        //entity.weapons[0].cooldownTime = 1.5f;

        entity.weapons[0].type = SHOT_TYPE_MISSLE;
        entity.weapons[0].offsetAngle = 0.0f;
        entity.weapons[0].offsetPosition;
        entity.weapons[0].ready = true;
        entity.weapons[0].waitTime = 0.0f;
        entity.weapons[0].cooldownTime = 0.5f;
      } break;

      case ENTITY_TYPE_MEDIUM_SHIP:
      {
        entity.size  = 15.0f;
        entity.accelRate = randomf(50.0f, 70.0f);
        entity.turnRate  = randomf(90.0f, 270.0f);
        life = 20.0f;
        targetAngleThreshold = 5.0f;
        targetDistThreshold = 125.0f * 125.0f;

        entity.weapons[0].type = SHOT_TYPE_NORMAL;
        entity.weapons[0].offsetAngle = 0.0f;
        entity.weapons[0].offsetPosition;
        entity.weapons[0].ready = true;
        entity.weapons[0].waitTime = 0.0f;
        entity.weapons[0].cooldownTime = 1.0f;

        entity.weapons[1].type = SHOT_TYPE_MISSLE;
        entity.weapons[1].offsetAngle = 0.0f;
        entity.weapons[1].offsetPosition;
        entity.weapons[1].ready = true;
        entity.weapons[1].waitTime = 0.0f;
        entity.weapons[1].cooldownTime = 15.0f;
      } break;

      case ENTITY_TYPE_MOTHER_SHIP:
      {
        entity.size  = 30.0f;
        entity.accelRate = randomf(10.0f, 20.0f);
        entity.turnRate  = randomf(30.0f, 60.0f);
        life = 100.0f;
        targetAngleThreshold = 5.0f;
        targetDistThreshold = 700.0f * 700.0f;

        for (int i = 0; i < MAX_WEAPONS; i++)
        {
          entity.weapons[i].type = SHOT_TYPE_SPAWN;
          entity.weapons[i].offsetAngle = -0.0f + (i * 90.0f);
          entity.weapons[i].offsetPosition;
          entity.weapons[i].ready = true;
          entity.weapons[i].waitTime = 0.0f;
          entity.weapons[i].cooldownTime = 10.0f;
        }
      } break;
    }
    
    entity.life = entity.lifeMax = life;
    entity.position = _position;
    entity.velocity = _velocity;
    entity.targetAngleThreshold = targetAngleThreshold;
    entity.targetDistThreshold = targetDistThreshold;
  }

  return (id);
}

void EntitySystem::remove(ID _idToRemove)
{
  mSystem.remove(_idToRemove);
}

void EntitySystem::clear()
{
  mSystem.reset();
}

void EntitySystem::render(const CameraInfo& _cameraInfo)
{
  glEnable(GL_TEXTURE_2D);
  for (unsigned i = 0; i < mSystem.getNumObjects(); i++)
  {
    Entity& entity = mSystem._objects[i];
    float size = entity.size / 2.0f;

    if (!contains(_cameraInfo, makeVector2(entity.position)))
      continue;

    glColor3f(1.0f, 1.0f, 1.0f);
    glPushMatrix();
      glTranslatef(entity.position);

      glPushMatrix();
        glScalef(size, size, size);
        drawHealthBar(entity);
      glPopMatrix();

      glRotatef(entity.angle, 0.0f, 0.0f, 1.0f);
      glScalef(size, size, size);

      switch (entity.type)
      {
        case ENTITY_TYPE_PLAYER:
        {
          glColor3f(1.0f, 1.0f, 1.0f);
          drawBoxShip(L"");
          //drawBoxShip(L"player");
        } break;
        
        default:
        case ENTITY_TYPE_SMALL_SHIP:
        {
          glColor3f(0.5f, 0.5f, 0.5f);
          drawBoxShip();
        } break;
      }
      if (entity.thrust > 0.0f)
      {
        glColor3f(1.0f, 0.0f, 0.0f);
        drawThrusters();
      }
      else if (entity.thrust < 0.0f)
      {
        glColor3f(0.0f, 0.0f, 1.0f);
        glPushMatrix();
          glRotatef(180.0f, 0.0f, 0.0f, 1.0f);
          drawThrusters();
        glPopMatrix();
      }
    glPopMatrix();
  }
  glDisable(GL_TEXTURE_2D);
}

void EntitySystem::update(double _deltaTime)
{
  for (unsigned i = 0; i < mSystem.getNumObjects(); i++)
  {
    Entity& entity = mSystem._objects[i];

    // All entities are subject to the same physics.
    updateEntity(entity, _deltaTime);
  }
}

void EntitySystem::drawThrusters()
{
  float size = 1.0f;

  glBegin(GL_LINES);
    glVertex2f(-size, 0.0f);
    glVertex2f(-size * 1.5f, 0.0f);
    
    glVertex2f(-size, 0.0f);
    glVertex2f(-size * 1.5f, -size);

    glVertex2f(-size, 0.0f);
    glVertex2f(-size * 1.5f, size);
    
    //    
    //glVertex2f(-size, -size);
    //glVertex2f(-size, -size);
    //    
    //glVertex2f(-size, -size);
    //glVertex2f(size, -size);
    //    
    //glVertex2f(size, -size);
    //glVertex2f(size, size);
  glEnd();
}

void EntitySystem::drawBoxShip(const wstring& _textureTag)
{
  //Texture* texture = ResourceManager::Instance().getTexture(_textureTag);

  //if (texture)
  //{
  //  texture->bind();

  //  glBegin(GL_QUADS);
  //    glTexCoord2f(1.0f, 1.0f); glVertex2f(1.0f, 1.0f);
  //    glTexCoord2f(-1.0f, 1.0f); glVertex2f(-1.0f, 1.0f);
  //    glTexCoord2f(-1.0f, -1.0f); glVertex2f(-1.0f, -1.0f);
  //    glTexCoord2f(1.0f, -1.0f); glVertex2f(1.0f, -1.0f);
  //  glEnd();

  //  texture->unbind();
  //}
  //else
  {
    //glBegin(GL_LINE_LOOP);
    //  glVertex2f(1.0f, 1.0f);
    //  glVertex2f(-1.0f, 1.0f);
    //  glVertex2f(-1.0f, -1.0f);
    //  glVertex2f(1.0f, -1.0f);
    //    
    //  //glVertex2f(-1.0f, 1.0f);
    //  //glVertex2f(-1.0f, -1.0f);
    //  //  
    //  //glVertex2f(-1.0f, -1.0f);
    //  //glVertex2f(1.0f, -1.0f);
    //  //  
    //  //glVertex2f(1.0f, -1.0f);
    //  //glVertex2f(1.0f, 1.0f);
    //glEnd();

    glBindVertexArray(mVao);
      //glEnableVertexAttribArray(1);
      //glEnableVertexAttribArray(0);
      //glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_INT, 0);
      glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        //glDrawArrays(GL_QUADS, 0, 4);
      //glDisableVertexAttribArray(0);
      //glDisableVertexAttribArray(1);
    glBindVertexArray(0);
  }
}

void EntitySystem::drawHealthBar(Entity& _entity)
{
  float hp = (_entity.life / _entity.lifeMax) * 2.0f;

  glColor3f(0.0f, 1.0f, 0.0f);
  glPushMatrix();
    glQuad(-1.0f, 1.5f, hp, .25f);
  glPopMatrix();
}

void EntitySystem::track(Entity& _entity, const Vector3& _targetPos, EntityTargetInfo& _targetInfo, bool _tracking)
{
  //static const float angleThreshold = 3.0f;
  //static const float distSquaredThresholdMin = 50.0f * 50.0f;
  //static const float distSquaredThresholdMax = 100.0f * 100.0f;

  float angleThreshold = _entity.targetAngleThreshold;
  float distSquaredThresholdMin = _entity.targetDistThreshold;// - 25.0f;
  float distSquaredThresholdMax = _entity.targetDistThreshold;// + 25.0f;

  float angleToTarget = angleTo(_entity.position, _targetPos) - _entity.angle;
  float distToTargetSquared = distanceSquaredTo(_entity.position, _targetPos);

  if (_tracking)
  {
    // Track
    angleToTarget = fmod(angleToTarget, 360.0f);
  }
  else
  {
    // Evade
    angleToTarget = fmod(angleToTarget + 180.0f, 360.0f);
  }

  if (angleToTarget > 180.0f)
    angleToTarget -= 360.0f;

  if (angleToTarget < -angleThreshold)
    _entity.turn = -1.0f;
  else if (angleToTarget > angleThreshold)
    _entity.turn = 1.0f;
  else
    _entity.turn = 0.0f;

  if (_entity.turn == 0.0f)
  {
    if (_tracking)
    {
      if (distToTargetSquared < distSquaredThresholdMin)
        _entity.thrust = -1.0f;
      else if (distToTargetSquared > (distSquaredThresholdMax))
        _entity.thrust = 1.0f;
      else
        _entity.thrust = 0.0f;
    }
    else
    {
      if (distToTargetSquared < distSquaredThresholdMin)
        _entity.thrust = 1.0f;
      else if (distToTargetSquared > distSquaredThresholdMax)
        _entity.thrust = 0.0f;
      else
        _entity.thrust = 0.0f;
    }
  }
  else
  {
    _entity.thrust = 0.0f;
  }

  _targetInfo.angleTo = angleToTarget;
  _targetInfo.distanceSquaredTo = distToTargetSquared;
}

void EntitySystem::updateEntity(Entity& _entity, double _deltaTime)
{
  float accelRate = _entity.accelRate;
  float turnRate = _entity.turnRate;

  updateEntityAi(_entity);

  if (_entity.thrust != 0.0f)
  {
    Vector3 dir = makeVector(_entity.angle, accelRate * _entity.thrust);
      
    _entity.velocity = _entity.velocity + dir * (float)_deltaTime;

    //EntityEvent e;
    //e.ids[0] = _entity.id;
    //e.ids[1] = ID_INVALID;
    //e.type = EVENT_THRUSTING;
    //mOutputEvents.queueEvent(EVENT_THRUSTING, &e, sizeof(EntityEvent));
  }

  if (_entity.turn != 0.0f)
  {
    _entity.angle += (turnRate * _entity.turn * (float)_deltaTime);
  }

  for (int i = 0; i < MAX_WEAPONS; i++)
  {
    if (_entity.weapons[i].waitTime > 0.0f)
    {
      _entity.weapons[i].waitTime -= (float)_deltaTime;

      if (_entity.weapons[i].waitTime < 0.0f)
      {
        _entity.weapons[i].ready = true;
        _entity.weapons[i].waitTime = 0.0f;

        EntityEvent e;
        e.ids[0] = _entity.id;
        e.ids[1] = ID_INVALID;
        e.type = EVENT_WEAPON_READY;
        mOutputEvents.queueEvent(EVENT_WEAPON_READY, &e, sizeof(EntityEvent));
      } 
    }

    if (_entity.weapons[i].ready && _entity.weapons[i].fire)
    {
      _entity.weapons[i].ready = false;
      _entity.weapons[i].waitTime = _entity.weapons[i].cooldownTime;

      EntityTargetInfo targetInfo;

      targetInfo.entityId = _entity.id;
      targetInfo.entityWeaponIndex = i;
      targetInfo.targetId = _entity.targetId;

      mOutputEvents.queueEvent(EVENT_WEAPON_FIRE, &targetInfo, sizeof(EntityTargetInfo));
    } 
  }

  _entity.position = _entity.position + _entity.velocity * (float)_deltaTime;

  _entity.velocity = _entity.velocity * 0.98f;
}

void EntitySystem::updateEntityAi(Entity& _entity)
{
  switch (_entity.type)
  {
    default:
    {
      if (mSystem.has(_entity.targetId))
      {
        Entity& target = mSystem.lookup(_entity.targetId);

        EntityTargetInfo targetInfo;
        
        targetInfo.entityId = _entity.id;
        targetInfo.targetId = target.id;

        track(_entity, target.position, targetInfo, _entity.type != ENTITY_TYPE_MOTHER_SHIP);

        switch (_entity.type)
        {
          default:
          {
            if (fabs(targetInfo.angleTo) < (_entity.targetAngleThreshold * 2.0f) && targetInfo.distanceSquaredTo < (_entity.targetDistThreshold + 50.0f * 50.0f))
            {
              for (int i = 0; i < MAX_WEAPONS; i++)
                _entity.weapons[i].fire = true;

              if (!_entity.targetLocked)
              {
                _entity.targetLocked = true;

                mOutputEvents.queueEvent(EVENT_TARGET_LOCKED, &targetInfo, sizeof(EntityTargetInfo));
              }
            }
            else
            {
              for (int i = 0; i < MAX_WEAPONS; i++)
                _entity.weapons[i].fire = false;

              if (_entity.targetLocked)
              {
                _entity.targetLocked = false;

                mOutputEvents.queueEvent(EVENT_TARGET_LOST, &targetInfo, sizeof(EntityTargetInfo));
              }
            } 
          } break;

          case ENTITY_TYPE_MOTHER_SHIP:
          {
            if (targetInfo.distanceSquaredTo < _entity.targetDistThreshold)
            {
              for (int i = 0; i < MAX_WEAPONS; i++)
                _entity.weapons[i].fire = true;

              if (!_entity.targetLocked)
              {
                _entity.targetLocked = true;

                mOutputEvents.queueEvent(EVENT_TARGET_LOCKED, &targetInfo, sizeof(EntityTargetInfo));
              }
            }
            else
            {
              _entity.weapons[0].fire = false;

              if (_entity.targetLocked)
              {
                _entity.targetLocked = false;

                mOutputEvents.queueEvent(EVENT_TARGET_LOST, &targetInfo, sizeof(EntityTargetInfo));
              }
            } 
          } break;
        }
        
      }
      else
      {
        _entity.turn = 0.0f;
        _entity.thrust = 0.0f;
      }
    } break;
    
    case ENTITY_TYPE_PLAYER:
    {
    } break;
  }
}

bool EntitySystem::getEvents(foundation::Array<Mgl::EventHeader*>& _packets)
{
  return (mOutputEvents.getEvents(_packets));
}

bool EntitySystem::flushEvents()
{
  return (mOutputEvents.flushEvents());
}
