#pragma once

#include "MglSystem.h"
#include "ShipsTypes.h"

class EntitySystem;

class WeaponSystem
{
public:
  WeaponSystem();
  virtual ~WeaponSystem();

  ID   addShot(const foundation::Vector3& _position, const foundation::Vector3& _velocity, ID _entityId, ID _targetId, double _timeLeftInSeconds);
  ID   addMissle(const foundation::Vector3& _position, const foundation::Vector3& _velocity, ID _entityId, ID _targetId, double _timeLeftInSeconds);
  void remove(ID _idToRemove);
  void clear();
  void render(const Mgl::CameraInfo& _cameraInfo);
  void update(EntitySystem* _entitySystem);
  void update(double _deltaTime);

  System<Shot> mSystem;
};