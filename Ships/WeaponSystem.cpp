#include "WeaponSystem.h"

#include "EntitySystem.h"
#include "MglGl.h"
#include "MglUtils.h"

using namespace foundation;
using namespace Mgl;

WeaponSystem::WeaponSystem() :
  mSystem()
{
}

WeaponSystem::~WeaponSystem()
{
}

ID WeaponSystem::addShot(const Vector3& _position, const Vector3& _velocity, ID _entityId, ID _targetId, double _timeLeftInSeconds)
{
  ID id = mSystem.add();

  if (mSystem.has(id))
  {
    float timeLeft = 1.0f;
    float radius = 5.0f;
    Shot& shot = mSystem.lookup(id);
    memset(&shot, 0, sizeof(Shot));

    shot.id = id;
    shot.type = SHOT_TYPE_NORMAL;
    shot.entityId = _entityId;
    shot.targetId = _targetId;
    shot.timeLeft = _timeLeftInSeconds;
    shot.entity.position = _position;
    shot.entity.velocity = _velocity;
    shot.entity.size = 5.0f;
    shot.entity.turn = Mgl::randomf(-90.0f, 90.0f);
  }

  return (id);
}

ID WeaponSystem::addMissle(const Vector3& _position, const Vector3& _velocity, ID _entityId, ID _targetId, double _timeLeftInSeconds)
{
  ID id = mSystem.add();

  if (mSystem.has(id))
  {
    float timeLeft = 1.0f;
    float radius = 5.0f;
    Shot& shot = mSystem.lookup(id);
    memset(&shot, 0, sizeof(Shot));

    shot.id = id;
    shot.type = SHOT_TYPE_MISSLE;
    shot.entityId = _entityId;
    shot.targetId = _targetId;
    shot.timeLeft = _timeLeftInSeconds;
    shot.targetThrust = 0.0f;
    shot.targetTurn = 0.0f;

    shot.entity.position = _position;
    shot.entity.velocity = _velocity;
    shot.entity.size = 5.0f;
    shot.entity.turn = 0.0f;//Mgl::randomf(-90.0f, 90.0f);
  }

  return (id);
}

void WeaponSystem::remove(ID _idToRemove)
{
  mSystem.remove(_idToRemove);
}

void WeaponSystem::clear()
{
  mSystem.reset();
}

void WeaponSystem::render(const CameraInfo& _cameraInfo)
{
  static const float size = 2.0f;

  for (unsigned i = 0; i < mSystem.getNumObjects(); i++)
  {
    Shot& shot = mSystem._objects[i];
    
    Entity& entity = shot.entity;

    if (!contains(_cameraInfo, makeVector2(entity.position)))
      continue;

    glPushMatrix();
      glTranslatef(shot.entity.position);
      glRotatef(shot.entity.angle, 0.0f, 0.0f, 1.0f);

      if (shot.type == SHOT_TYPE_NORMAL)
        glColor3f(1.0f, 0.0f, 0.0f);
      else
        glColor3f(1.0f, 1.0f, 0.0f);

      glBegin(GL_QUADS);
        glVertex2f(size, size);
        glVertex2f(-size, size);
        
        glVertex2f(-size, size);
        glVertex2f(-size, -size);
        
        glVertex2f(-size, -size);
        glVertex2f(size, -size);
        
        glVertex2f(size, -size);
        glVertex2f(size, size);
      glEnd();

    glPopMatrix();
  }
}

void track(Shot& _shot, const Vector3& _targetPos)
{
  static const float angleThreshold = 3.0f;

  float angleToTarget = angleTo(_shot.entity.position, _targetPos) - _shot.entity.angle;
  float distToTargetSquared = distanceSquaredTo(_shot.entity.position, _targetPos);

  angleToTarget = fmod(angleToTarget, 360.0f);

  if (angleToTarget > 180.0f)
    angleToTarget -= 360.0f;

  if (angleToTarget < -angleThreshold)
    _shot.targetTurn = -1.0f;
  else if (angleToTarget > angleThreshold)
    _shot.targetTurn = 1.0f;
  else
    _shot.targetTurn = 0.0f;

  _shot.targetThrust = 1.0f;
}

void WeaponSystem::update(EntitySystem* _entitySystem)
{
  if (_entitySystem)
  {
    for (unsigned i = 0; i < mSystem.getNumObjects(); i++)
    {
      Shot& shot = mSystem._objects[i];

      if (shot.type == SHOT_TYPE_MISSLE)
      {
        Entity& entity = shot.entity;

        if (_entitySystem->mSystem.has(shot.targetId))
        {
          shot.targetPosition = _entitySystem->mSystem.lookup(shot.targetId).position;
        }
      }
    }
  }
}

void WeaponSystem::update(double _deltaTime)
{
  for (unsigned i = 0; i < mSystem.getNumObjects(); i++)
  {
    Shot& shot = mSystem._objects[i];

    Entity& entity = shot.entity;

    shot.timeLeft -= _deltaTime;

    if (shot.timeLeft < 0.0)
    {
      mSystem.remove(shot.id);
      continue;
    }

    //if (shot.targetThrust > 0.0f)
    if (shot.type == SHOT_TYPE_MISSLE)
    {
      static const float accelRate = 150.0f;
      static const float turnRate  = 90.0f;

      track(shot, shot.targetPosition);

      if (shot.targetThrust != 0.0f)
      {
        Vector3 dir = makeVector(shot.entity.angle, accelRate * shot.targetThrust);

        shot.entity.velocity = dir;

        shot.entity.velocity = shot.entity.velocity + dir * (float)_deltaTime;
      }

      if (shot.targetTurn != 0.0f)
      {
        shot.entity.angle += (turnRate * shot.targetTurn * (float)_deltaTime);
      }

      entity.position = entity.position + entity.velocity * (float)_deltaTime;
  
      //entity.velocity = entity.velocity * 0.98f;

      //entity.angle += entity.turn * (float)_deltaTime;
    }
    else
    {
      entity.position = entity.position + entity.velocity * (float)_deltaTime;

      entity.angle += entity.turn * (float)_deltaTime;
    }
  }
}