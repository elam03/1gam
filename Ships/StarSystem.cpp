#include "StarSystem.h"

#include "MglUtils.h"
#include "ShipsApplication.h"

#include "MglGl.h"

using namespace foundation;
using namespace Mgl;

StarSystem::StarSystem() :
  mSystem()
{
}

StarSystem::~StarSystem()
{
}

bool StarSystem::generate(const Mgl::CameraInfo& _cameraInfo)
{
  bool success = false;

  mCameraInfo = _cameraInfo;

  int count = random(10, 20);

  for (int i = 0; i < count; i++)
  {
    Vector3 p;
    float size = randomf(1.0f, 5.0f);
    p.x = randomf(0.0, _cameraInfo.width);
    p.y = randomf(0.0, _cameraInfo.height);
    p.z = 0.0f;

    add(p, size);
  }

  return (success);
}

ID StarSystem::add(const Vector3& _position, float _size)
{
  ID id = mSystem.add();

  if (mSystem.has(id))
  {
    Star& star = mSystem.lookup(id);
    memset(&star, 0, sizeof(Star));

    star.id        = id;
    star.size      = _size;
    star.position  = _position;
    star.position2 = _position;

    star.color.x = randomf(.5f, 1.0f);
    star.color.y = randomf(.5f, 1.0f);
    star.color.z = randomf(.5f, 1.0f);
  }

  return (id);
}

void StarSystem::remove(ID _idToRemove)
{
  mSystem.remove(_idToRemove);
}

void StarSystem::clear()
{
  mSystem.reset();
}

void StarSystem::render(const CameraInfo& _cameraInfo)
{
  //mCameraInfo = _cameraInfo;

  glBegin(GL_LINES);

  for (unsigned i = 0; i < mSystem.getNumObjects(); i++)
  {
    Star& star = mSystem._objects[i];
    Vector3 p = star.position;

    //if (!contains(_cameraInfo, makeVector2(star.position)))
    //  continue;
    
    //p.x = (int)(p.x - _cameraInfo.x) % (int)mCameraInfo.width;
    //p.y = (int)(p.y - _cameraInfo.y) % (int)mCameraInfo.height;

    float size = star.size;

    //glTranslatef(star.position);

    glColor3f(star.color);

    glVertex2f(p.x - size, p.y);
    glVertex2f(p.x + size, p.y);

    glVertex2f(p.x, p.y - size);
    glVertex2f(p.x, p.y + size);
  }

  glEnd();
}

void StarSystem::update(double _deltaTime)
{
  for (unsigned i = 0; i < mSystem.getNumObjects(); i++)
  {
    Star& star = mSystem._objects[i];

    //star.position.x = (int)(star.position.x - mCameraInfo.x);// % (int)mCameraInfo.width;
    //star.position.y = (int)(star.position.y - mCameraInfo.y);// % (int)mCameraInfo.height;

    if (star.position.x < 0)                 star.position.x += mCameraInfo.width;
    if (star.position.x > mCameraInfo.width) star.position.x -= mCameraInfo.width;

    if (star.position.y < 0)                  star.position.y += mCameraInfo.height;
    if (star.position.y > mCameraInfo.height) star.position.y -= mCameraInfo.height;

    //star.position.x = (int)star.position.x % (int)mCameraInfo.width;
    //star.position.y = (int)star.position.y % (int)mCameraInfo.height;

    //star.target = mSystem._objects[0].position;

    // All entities are subject to the same physics.
    //updateEntity(star, _deltaTime);
  }
}

bool StarSystem::updateCamera(float _dx, float _dy)
{
  bool success = false;

  for (unsigned i = 0; i < mSystem.getNumObjects(); i++)
  {
    Star& star = mSystem._objects[i];

    star.position.x = star.position.x - _dx / star.size;
    star.position.y = star.position.y - _dy / star.size;
  }

  return (success);
}
