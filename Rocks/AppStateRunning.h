#pragma once

#include "MglAppState.h"
#include "EntitySystem.h"
#include "ParticleSystem.h"
#include "RockSystem.h"
#include "ProximitySystem.h"
#include "ScoreSystem.h"

class AppStateRunning : public AppState
{
public:
  enum
  {
    STATE_RUNNING,
    STATE_GAME_OVER,
    STATE_COUNT
  };

public:
  AppStateRunning();
  virtual ~AppStateRunning();

  virtual bool handleEvent(int _event, void* _eventData);
  virtual void render();
  virtual void update(double _deltaTime);

protected:
  void checkEvents();
  void destroyRock(Rock& _rock);
  void reset();
  void setState(int _gameState);

protected:
  EntitySystem    mEntitySystem;
  ParticleSystem  mParticleSystem;
  RockSystem      mRockSystem;
  ProximitySystem mProximitySystem;
  ScoreSystem     mScoreSystem;
  
  foundation::Array<Mgl::EventHeader*> mEvents;

  ID     mPlayerId;
  double mPlayerWeaponCooldown;
  double mPlayerWeaponLastFired;

  int mGameState;

  static const std::wstring HIGH_SCORE_FILE;
};