#include "ParticleSystem.h"

#include "MglUtils.h"

#include "MglGl.h"

using namespace foundation;
using namespace Mgl;

ParticleSystem::ParticleSystem() :
  mSystem()
{
}

ParticleSystem::~ParticleSystem()
{
}

ID ParticleSystem::add(const foundation::Vector3& _position, const foundation::Vector3& _velocity, ID _entityId)
{
  ID id = mSystem.add();
  float _radius = 5.0f;

  if (mSystem.has(id))
  {
    Particle& particle = mSystem.lookup(id);
    memset(&particle, 0, sizeof(Particle));

    particle.id = id;
    particle.entityId = _entityId;
    particle.timeLeft = 3.0;
    particle.entity.position = _position;
    particle.entity.velocity = _velocity;
    particle.entity.size = _radius;
    particle.entity.turn = Mgl::randomf(-90.0f, 90.0f);

    particle.geometry.numVertices = 6;
    float angleInc = (360.0f / particle.geometry.numVertices);
        
    for (unsigned j = 0; j < particle.geometry.numVertices; j++)
    {
      float angle = angleInc * j;
      float radius = Mgl::randomf(_radius / 2.0f, _radius);

      particle.geometry.vertices[j].x = cos(angle * Mgl::DEG) * radius;
      particle.geometry.vertices[j].y = sin(angle * Mgl::DEG) * radius;
      particle.geometry.vertices[j].z = 0.0f;
    }

    float r = 0.0f;
    float g = 1.0f;
    float b = 0.0f;

    particle.geometry.numLines = particle.geometry.numVertices;

    for (unsigned j = 0; j < particle.geometry.numLines; j++)
    {
      particle.geometry.lines[j].r = r;
      particle.geometry.lines[j].g = g;
      particle.geometry.lines[j].b = b;
    }

    for (unsigned j = 0; j < particle.geometry.numLines - 1; j++)
    {
      particle.geometry.lines[j].vertexIndexes[0] = j;
      particle.geometry.lines[j].vertexIndexes[1] = j + 1;
    }

    particle.geometry.lines[particle.geometry.numLines - 1].vertexIndexes[0] = particle.geometry.numLines - 1;
    particle.geometry.lines[particle.geometry.numLines - 1].vertexIndexes[1] = 0;
  }

  return (id);
}

void ParticleSystem::remove(ID _idToRemove)
{
  mSystem.remove(_idToRemove);
}

void ParticleSystem::clear()
{
  mSystem.reset();
}

void ParticleSystem::render()
{
  for (unsigned i = 0; i < mSystem.getNumObjects(); i++)
  {
    Particle& particle = mSystem._objects[i];
    
    Entity& entity = particle.entity;
    Geometry& geometry = particle.geometry;

    glPushMatrix();
      glTranslatef(particle.entity.position);
      glRotatef(particle.entity.angle, 0.0f, 0.0f, 1.0f);

      // Filled rocks
      glColor3f(geometry.lines[0].r, geometry.lines[0].g, geometry.lines[0].b);
      glBegin(GL_TRIANGLE_FAN);
      
        glVertex3f(0.0f, 0.0f, 0.0f);
        for (unsigned j = 0; j < geometry.numVertices; j++)
        {
          glVertex3f(geometry.vertices[j].x, geometry.vertices[j].y, geometry.vertices[j].z);
        }
        glVertex3f(geometry.vertices[0].x, geometry.vertices[0].y, geometry.vertices[0].z);

      glEnd();

      // Not-filled rocks
      //glBegin(GL_LINES);
      //for (unsigned j = 0; j < geometry.numLines; j++)
      //{
      //  int index0 = geometry.lines[j].vertexIndexes[0];
      //  int index1 = geometry.lines[j].vertexIndexes[1];
    
      //  glColor3f(geometry.lines[j].r, geometry.lines[j].g, geometry.lines[j].b);

      //  glVertex3f(geometry.vertices[index0].x, geometry.vertices[index0].y, geometry.vertices[index0].z);
      //  glVertex3f(geometry.vertices[index1].x, geometry.vertices[index1].y, geometry.vertices[index1].z);
      //}
      //glEnd();

      //glColor3f(1.0f, 1.0f, 1.0f);
      //glCircle(Vector3(), entity.size);

    glPopMatrix();
  }
}

void ParticleSystem::update(double _deltaTime)
{
  for (unsigned i = 0; i < mSystem.getNumObjects(); i++)
  {
    Particle& particle = mSystem._objects[i];

    Entity& entity = particle.entity;

    particle.timeLeft -= _deltaTime;

    if (particle.timeLeft < 0.0)
    {
      mSystem.remove(particle.id);
    }

    entity.position = entity.position + entity.velocity * (float)_deltaTime;

    entity.angle += entity.turn * (float)_deltaTime;
  }
}