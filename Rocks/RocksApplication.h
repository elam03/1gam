#pragma once

#include "MglApplication.h"

class AppState;
class ApplicationEvent;

class RocksApplication : public MglApplication
{
public:
  RocksApplication();
  virtual ~RocksApplication();

protected:
  virtual bool appInit();
  virtual void appClose();
};