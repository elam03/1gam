#pragma once

#include "MglSystem.h"
#include "RocksTypes.h"

class ParticleSystem
{
public:
  ParticleSystem();
  virtual ~ParticleSystem();

  ID   add(const foundation::Vector3& _position, const foundation::Vector3& _velocity, ID _entityId);
  void remove(ID _idToRemove);
  void clear();
  void render();
  void update(double _deltaTime);

  System<Particle> mSystem;
};