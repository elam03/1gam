#include "RockSystem.h"

#include "MglUtils.h"
#include "RocksApplication.h"

#include "MglGl.h"

using namespace foundation;
using namespace Mgl;

RockSystem::RockSystem() :
  mSystem(),
  mApplication(0)
{
}

RockSystem::~RockSystem()
{
}

ID RockSystem::add(const foundation::Vector3& _position, const foundation::Vector3& _velocity, float _radius)
{
  ID id = mSystem.add();

  if (mSystem.has(id))
  {
    Rock& rock = mSystem.lookup(id);
    memset(&rock, 0, sizeof(Rock));

    rock.id = id;
    rock.entity.position = _position;
    rock.entity.velocity = _velocity;
    rock.entity.size = _radius;
    rock.entity.turn = Mgl::randomf(-90.0f, 90.0f);

    rock.geometry.numVertices = 5 + rand() % 10;
    float angleInc = (360.0f / rock.geometry.numVertices);
        
    for (unsigned j = 0; j < rock.geometry.numVertices; j++)
    {
      float angle = angleInc * j;
      //float radius = Mgl::randomf(_radius / 2.0f, _radius);
      float radius = _radius;

      rock.geometry.vertices[j].x = cos(angle * Mgl::DEG) * radius;
      rock.geometry.vertices[j].y = sin(angle * Mgl::DEG) * radius;
      rock.geometry.vertices[j].z = 0.0f;
    }

    float r = float(128 + rand() % 128) / 256.0f;
    float g = float(128 + rand() % 128) / 256.0f;
    float b = float(128 + rand() % 128) / 256.0f;

    rock.geometry.numLines = rock.geometry.numVertices;

    for (unsigned j = 0; j < rock.geometry.numLines; j++)
    {
      rock.geometry.lines[j].r = r;
      rock.geometry.lines[j].g = g;
      rock.geometry.lines[j].b = b;
    }

    for (unsigned j = 0; j < rock.geometry.numLines - 1; j++)
    {
      rock.geometry.lines[j].vertexIndexes[0] = j;
      rock.geometry.lines[j].vertexIndexes[1] = j + 1;
    }

    rock.geometry.lines[rock.geometry.numLines - 1].vertexIndexes[0] = rock.geometry.numLines - 1;
    rock.geometry.lines[rock.geometry.numLines - 1].vertexIndexes[1] = 0;
  }

  return (id);
}

void RockSystem::remove(ID _idToRemove)
{
  mSystem.remove(_idToRemove);
}

void RockSystem::clear()
{
  mSystem.reset();
}

void RockSystem::render()
{
  for (unsigned i = 0; i < mSystem.getNumObjects(); i++)
  {
    Rock& rock = mSystem._objects[i];
    
    Entity& entity = rock.entity;
    Geometry& geometry = rock.geometry;

    glPushMatrix();
      glTranslatef(rock.entity.position);
      glRotatef(rock.entity.angle, 0.0f, 0.0f, 1.0f);

      // Filled rocks
      //glColor3f(geometry.lines[0].r, geometry.lines[0].g, geometry.lines[0].b);
      //glBegin(GL_TRIANGLE_FAN);
      //
      //  glVertex3f(0.0f, 0.0f, 0.0f);
      //  for (unsigned j = 0; j < geometry.numVertices; j++)
      //  {
      //    glVertex3f(geometry.vertices[j].x, geometry.vertices[j].y, geometry.vertices[j].z);
      //  }
      //  glVertex3f(geometry.vertices[0].x, geometry.vertices[0].y, geometry.vertices[0].z);

      //glEnd();

      // Not-filled rocks
      glBegin(GL_LINES);
      for (unsigned j = 0; j < geometry.numLines; j++)
      {
        int index0 = geometry.lines[j].vertexIndexes[0];
        int index1 = geometry.lines[j].vertexIndexes[1];
    
        glColor3f(geometry.lines[j].r, geometry.lines[j].g, geometry.lines[j].b);

        glVertex3f(geometry.vertices[index0].x, geometry.vertices[index0].y, geometry.vertices[index0].z);
        glVertex3f(geometry.vertices[index1].x, geometry.vertices[index1].y, geometry.vertices[index1].z);
      }
      glEnd();

      //glColor3f(1.0f, 1.0f, 1.0f);
      //glCircle(Vector3(), entity.size);

    glPopMatrix();
  }
}

void RockSystem::update(double _deltaTime)
{
  float w = (float)mApplication->mWindowWidth;
  float h = (float)mApplication->mWindowHeight;

  for (unsigned i = 0; i < mSystem.getNumObjects(); i++)
  {
    Rock& rock = mSystem._objects[i];

    Entity& entity = rock.entity;

    entity.position = entity.position + entity.velocity * (float)_deltaTime;

    //entity.velocity = entity.velocity * 0.98f;

    if ((entity.position.x) > w)    entity.position.x -= w;
    if ((entity.position.x) < 0.0f) entity.position.x += w;
    if ((entity.position.y) > h)    entity.position.y -= h;
    if ((entity.position.y) < 0.0f) entity.position.y += h;

    entity.angle += entity.turn * (float)_deltaTime;
  }
}