#pragma once

#include "MglSystem.h"
#include "RocksTypes.h"

class MglApplication;

class EntitySystem
{
public:
  EntitySystem();
  virtual ~EntitySystem();

  ID add(const foundation::Vector3& _position);
  void remove(ID _idToRemove);
  void clear();
  void render();
  void update(double _deltaTime);

  void updateEntity(Entity& _entity, double _deltaTime);

  System<Entity> mSystem;
  MglApplication* mApplication;
};