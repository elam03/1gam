#include "EntitySystem.h"

#include "MglUtils.h"
#include "RocksApplication.h"

#include "MglGl.h"

using namespace foundation;
using namespace Mgl;

EntitySystem::EntitySystem() :
  mSystem(),
  mApplication(0)
{
}

EntitySystem::~EntitySystem()
{
}

ID EntitySystem::add(const foundation::Vector3& _position)
{
  ID id = mSystem.add();

  if (mSystem.has(id))
  {
    Entity& entity = mSystem.lookup(id);
    memset(&entity, 0, sizeof(Entity));

    entity.id = id;
    entity.angle = 0.0f;
    entity.size = 10.0f;
    entity.life = entity.lifeMax = 1.0f;

    entity.position = _position;

    entity.invincibleDuration = 0.0;
  }

  return (id);
}

void EntitySystem::remove(ID _idToRemove)
{
  mSystem.remove(_idToRemove);
}

void EntitySystem::clear()
{
  mSystem.reset();
}

void EntitySystem::render()
{
  for (unsigned i = 0; i < mSystem.getNumObjects(); i++)
  {
    Entity& entity = mSystem._objects[i];

    //glColor3f(entity.r, entity.g, entity.b);
    glColor3f(1.0f, 1.0f, 1.0f);
    glPushMatrix();
      glTranslatef(entity.position);

      /////////////////////////////////////////////////////////////////////////
      glColor3f(1.0f, 1.0f, 0.0f);
      //Vector3 p = {-15.0f, 15.0f, 0.0f};
      Vector3 p = {-entity.size, entity.size, 0.0f};
      int flag = 1;
      
      for (unsigned i = 0; i < 8; i++)
      {
        flag = flag << 1;

        if (entity.thoughts & flag)
          glQuad(p.x + i * 3.0f, p.y, 2.0f, 4.0f);
      }
      
      glColor3f(0.50f, 0.75f, 0.0f);
      glQuad(p.x, p.y + 5.0f, (16.0f * (float(entity.thinkingTime) / 5.0f)), 4.0f);
      /////////////////////////////////////////////////////////////////////////
      if (entity.lifeMax > 0.0f)
      {
        float life = entity.life / entity.lifeMax;
        if (life < .33f)
          glColor3f(0.9f, 0.0f, 0.0f);
        else if (life < .66f)
          glColor3f(0.75f, 0.75f, 0.0f);
        else
          glColor3f(0.0f, 0.75f, 0.0f);
        //glQuad(p.x, p.y + 0.0f, ((entity.size * 2.0f) * (float(entity.life) / entity.lifeMax)), 4.0f);
        glQuad(p.x, p.y, ((entity.size * 2.0f) * (life)), 4.0f);
      }
      /////////////////////////////////////////////////////////////////////////

      glRotatef(entity.angle, 0.0f, 0.0f, 1.0f);
      
      glColor3f(1.0f, 1.0f, 1.0f);
      
      glBegin(GL_LINE_LOOP);
        glVertex3f(5.0f, 0.0f, 0.0f);
        glVertex3f(-5.0f, 3.0f, 0.0f);
        glVertex3f(-5.0f, -3.0f, 0.0f);
      glEnd();
      
    glPopMatrix();

    if (entity.invincibleDuration > 0.0)
    {
      float& size = entity.size; 
      glColor3f(1.0f, 1.0f, 1.0f);
      glCircle(entity.position, entity.size);
    }
  }
}

void EntitySystem::update(double _deltaTime)
{
  for (unsigned i = 0; i < mSystem.getNumObjects(); i++)
  {
    Entity& entity = mSystem._objects[i];

    // All entities are subject to the same physics.
    updateEntity(entity, _deltaTime);
  }
}

void EntitySystem::updateEntity(Entity& _entity, double _deltaTime)
{
  float w = (float)mApplication->mWindowWidth;
  float h = (float)mApplication->mWindowHeight;

  if (_entity.invincibleDuration > 0.0)
  {
    if (_entity.invincibleDuration > _deltaTime)
      _entity.invincibleDuration -= _deltaTime;
    else
      _entity.invincibleDuration = 0.0;
  }

  if (_entity.thrust != 0.0f)
  {
    static const float accelRate = 140.0f;

    Vector3 dir = makeVector(_entity.angle, accelRate * _entity.thrust);
      
    _entity.velocity = _entity.velocity + dir * (float)_deltaTime;
  }

  if (_entity.turn != 0.0f)
  {
    static const float turnRate = 360.0f;

    _entity.angle += (turnRate * _entity.turn * (float)_deltaTime);
  }

  _entity.position = _entity.position + _entity.velocity * (float)_deltaTime;

  _entity.velocity = _entity.velocity * 0.98f;

  if ((_entity.position.x) > w)    _entity.position.x -= w;
  if ((_entity.position.x) < 0.0f) _entity.position.x += w;
  if ((_entity.position.y) > h)    _entity.position.y -= h;
  if ((_entity.position.y) < 0.0f) _entity.position.y += h;
}
