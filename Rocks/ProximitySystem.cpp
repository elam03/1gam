#include "ProximitySystem.h"

#include "EntitySystem.h"
#include "RockSystem.h"
#include "ParticleSystem.h"
#include "MglUtils.h"

#include "MglGl.h"

#include "hash.h"
#include "memory.h"

using namespace foundation;
using namespace Mgl;

ProximitySystem::ProximitySystem() :
  mOutputEvents(),

  mEntitySystem(NULL),
  mRockSystem(NULL),
  mParticleSystem(NULL),

  mLastUpdatedTime(0.0),
  mRunTime(0.0)
{
  mOutputEvents.init();
}

ProximitySystem::~ProximitySystem()
{
  mOutputEvents.close();
}

void ProximitySystem::init(EntitySystem* _entitySystem, RockSystem* _rockSystem, ParticleSystem* _particleSystem)
{
  mEntitySystem   = _entitySystem;
  mRockSystem     = _rockSystem;
  mParticleSystem = _particleSystem;
}

void ProximitySystem::clear()
{
}

void ProximitySystem::render()
{
}

void ProximitySystem::update(double _deltaTime)
{
  static const unsigned fps = 60;
  static const double tickRate = (double(fps) / 1000.0);
  mRunTime += _deltaTime;

  if ((mRunTime - mLastUpdatedTime) >= tickRate)
  {
    for (unsigned i = 0; i < mEntitySystem->mSystem.getNumObjects(); i++)
    {
      Entity& entity = mEntitySystem->mSystem._objects[i];

      for (unsigned j = 0; j < mParticleSystem->mSystem.getNumObjects(); j++)
      {
        Particle& particle = mParticleSystem->mSystem._objects[j];

        if (entity.id != particle.entityId)
        {
          Vector3 where;
          if (Mgl::circleCircleCheck(entity.position, entity.size, particle.entity.position, particle.entity.size, where))
          {
            sendEvent(EVENT_COLLISION, TYPE_ENTITY_TO_PARTICLE, entity.id, particle.id, where);
          }
        }
      }

      for (unsigned i = 0; i < mParticleSystem->mSystem.getNumObjects(); i++)
      {
        Particle& particle = mParticleSystem->mSystem._objects[i];

        for (unsigned j = 0; j < mRockSystem->mSystem.getNumObjects(); j++)
        {
          Rock& rock = mRockSystem->mSystem._objects[j];

          Vector3 where;
          if (Mgl::circleCircleCheck(particle.entity.position, particle.entity.size, rock.entity.position, rock.entity.size, where))
          {
            sendEvent(EVENT_COLLISION, TYPE_PARTICLE_TO_ROCK, particle.id, rock.id, where);
          }
        }
      }

      for (unsigned i = 0; i < mEntitySystem->mSystem.getNumObjects(); i++)
      {
        Entity& entity = mEntitySystem->mSystem._objects[i];
        
        for (unsigned j = 0; j < mRockSystem->mSystem.getNumObjects(); j++)
        {
          Rock& rock = mRockSystem->mSystem._objects[j];
         
          Vector3 where;
          if (Mgl::circleCircleCheck(entity.position, entity.size, rock.entity.position, rock.entity.size, where))
          {
            sendEvent(EVENT_COLLISION, TYPE_ENTITY_TO_ROCK, entity.id, rock.id, where);
          }
        }
      }
    }

    mLastUpdatedTime = mRunTime;
  }
}

bool ProximitySystem::getEvents(Array<EventHeader*>& _packets)
{
  return (mOutputEvents.getEvents(_packets));
}

bool ProximitySystem::flushEvents()
{
  return (mOutputEvents.flushEvents());
}

void ProximitySystem::sendEvent(int _event, int _collisionType, ID _id1, ID _id2, const foundation::Vector3& _where)
{
  ProximityEvent event;
  event.type = _collisionType;
  event.ids[0] = _id1;
  event.ids[1] = _id2;
  event.where = _where;

  mOutputEvents.queueEvent(_event, &event, sizeof(ProximityEvent));
}
