#pragma once

#include "MglSystem.h"
#include "RocksTypes.h"

class MglApplication;

class RockSystem
{
public:
  RockSystem();
  virtual ~RockSystem();

  ID   add(const foundation::Vector3& _position, const foundation::Vector3& _velocity, float _radius);
  void remove(ID _idToRemove);
  void clear();
  void render();
  void update(double _deltaTime);

  System<Rock> mSystem;

  MglApplication* mApplication;
};