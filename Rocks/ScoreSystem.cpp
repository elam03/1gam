#include "ScoreSystem.h"

#include "MglUtils.h"
#include "RocksApplication.h"

#include <fstream>
#include "MglGl.h"

using namespace foundation;
using namespace std;

ScoreSystem::ScoreSystem()
{
  mApplication = NULL;
  clear();
  mHighScore = 0;
}

ScoreSystem::~ScoreSystem()
{
  clear();
}

void ScoreSystem::clear()
{
  mScore = 0;
}

bool ScoreSystem::loadHighScore(wstring _fileName)
{
  bool success = false;

  ifstream is;

  is.open(_fileName.c_str());

  if (is.is_open())
  {
    is >> mHighScore;
    is.close();

    success = true;
  }

  return (success);
}

bool ScoreSystem::saveHighScore(wstring _fileName)
{
  bool success = false;

  ofstream os;

  os.open(_fileName.c_str());

  if (os.is_open())
  {
    os << mHighScore;
    os.close();

    success = true;
  }

  return (success);
}

void ScoreSystem::render()
{
  if (mApplication)
  {
    char buf[256];
    sprintf_s(buf, 256, "High Score: %i Score: %i", mHighScore, mScore);
    glRasterPos2f(10.0f, 10.0f);
    glColor3f(1.0f, 1.0f, 1.0f);

    mApplication->printText(buf);
  }
}

void ScoreSystem::update(double _deltaTime)
{
}