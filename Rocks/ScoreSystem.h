#pragma once

#include <string>

class MglApplication;

class ScoreSystem
{
public:
  ScoreSystem();
  virtual ~ScoreSystem();

  void clear();
  bool loadHighScore(std::wstring _fileName);
  bool saveHighScore(std::wstring _fileName);
  void render();
  void update(double _deltaTime);

  int mScore;
  int mHighScore;

  MglApplication* mApplication;
};