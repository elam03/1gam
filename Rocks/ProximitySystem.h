#pragma once

#include "MglSystem.h"
#include "RocksTypes.h"

#include "MglEventSystem.h"

#include "collection_types.h"

class EntitySystem;
class RockSystem;
class ParticleSystem;

struct ProximityEvent
{
  int                 type;
  ID                  ids[2];
  foundation::Vector3 where;
};

class ProximitySystem
{
public:
  enum
  {
    TYPE_ENTITY_TO_ENTITY,
    TYPE_ENTITY_TO_ROCK,
    TYPE_ENTITY_TO_PARTICLE,
    TYPE_PARTICLE_TO_ROCK,
    TYPE_COUNT
  };

  enum
  {
    EVENT_COLLISION,
    EVENT_WARNING,
    EVENT_COUNT
  };

public:
  ProximitySystem();
  virtual ~ProximitySystem();

  void init(EntitySystem* _entitySystem, RockSystem* _rockSystem, ParticleSystem* _ParticleSystem);

  void clear();
  void render();
  void update(double _deltaTime);

  bool getEvents(foundation::Array<Mgl::EventHeader*>& _packets);
  bool flushEvents();

protected:
  void sendEvent(int _event, int _collisionType, ID _id1, ID _id2, const foundation::Vector3& _where);

  EventSystem mOutputEvents;

  EntitySystem*   mEntitySystem;
  RockSystem*     mRockSystem;
  ParticleSystem* mParticleSystem;

  double mLastUpdatedTime;
  double mRunTime;
};
