#pragma once

#include "MglTypes.h"

#define MAX_STRING_SIZE 128
#define MAX_VERTICES 64
#define MAX_LINES 64
#define MAX_PARTICLES 64
#define MAX_TARGETS 3

enum
{
  APP_STATE_INTRO,
  APP_STATE_RUNNING,
  APP_STATE_GAME_OVER,
  APP_STATE_COUNT
};

enum
{
  ACTION_FIRE     = 0x0001,
  ACTION_ALT_FIRE = 0x0002,
  ACTION_TELEPORT = 0x0004
};

enum
{
  THOUGHT_IDLE    = 0x0001,
  THOUGHT_CHASE   = 0x0002,
  THOUGHT_EVADE   = 0x0004,
  THOUGHT_ROAM    = 0x0008,

  THOUGHT_CURIOUS = 0x0010,
  THOUGHT_ANGRY   = 0x0020,
  THOUGHT_SCARED  = 0x0040,
  THOUGHT_LOVE    = 0x0080
};

enum
{
  EMITTER_TYPE_EXPLOSION,
  EMITTER_TYPE_TELEPORT,
  EMITTER_TYPE_INVERTED_EXPLOSION,
  EMITTER_TYPE_COUNT
};

///////////////////////////////////////////////////////////////////////////////

struct Line
{
  float r, g, b;
  int vertexIndexes[2];
};

struct Geometry
{
  unsigned numVertices;
  unsigned numLines;

  foundation::Vector3 vertices[MAX_VERTICES];
  Line lines[MAX_LINES];
};

struct Entity
{
  ID    id;
  float angle;
  float size;
  float life;
  float lifeMax;

  float thrust;
  float turn;
  int   action;

  double invincibleDuration;
  double invincibleBlinkTime;

  ID     targets[MAX_TARGETS];
  int    thoughts;
  double thinkingTime;

  foundation::Vector3 targetPosition;

  foundation::Vector3 position;
  foundation::Vector3 velocity;
};

struct Rock
{
  ID id;
  Entity entity;
  Geometry geometry;
};

struct Particle
{
  ID     id;
  ID     entityId;
  double timeLeft;

  Entity   entity;
  Geometry geometry;
};