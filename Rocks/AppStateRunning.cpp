#include "AppStateRunning.h"

#include "MglUtils.h"
#include "RocksApplication.h"

#include "array.h"
#include "memory.h"

#include <fstream>
#include "MglGl.h"

using namespace foundation;
using namespace std;
using namespace Mgl;

const wstring AppStateRunning::HIGH_SCORE_FILE = TEXT("highscore.txt");

AppStateRunning::AppStateRunning() : 
  AppState(),

  mEntitySystem(),
  mParticleSystem(),
  mRockSystem(),
  mProximitySystem(),
  mScoreSystem(),

  mEvents(memory_globals::default_allocator()),

  mPlayerId(ID_INVALID),
  mPlayerWeaponCooldown(0.0),
  mPlayerWeaponLastFired(0.0),

  mGameState(STATE_COUNT)
{
}

AppStateRunning::~AppStateRunning()
{
}

bool AppStateRunning::handleEvent(int _event, void* _eventData)
{
  bool handled = false;

  switch (_event)
  {
    case (EVENT_INIT):
    {
      mEntitySystem.mApplication = mApplication;
      mRockSystem.mApplication   = mApplication;
      mScoreSystem.mApplication  = mApplication;

      mScoreSystem.loadHighScore(HIGH_SCORE_FILE);
      setState(STATE_GAME_OVER);
    } break;

    case (EVENT_CLOSE):
    {
      mScoreSystem.saveHighScore(TEXT("highscore.txt"));

      mEntitySystem.clear();
      mParticleSystem.clear();
      mRockSystem.clear();
      mProximitySystem.clear();
      mScoreSystem.clear();
    } break;
  }

  return (handled);
}

void AppStateRunning::render()
{
  //mApplication->renderBorder();

  float w = (float)mApplication->mWindowWidth;
  float h = (float)mApplication->mWindowHeight;
  
  if (mGameState == STATE_RUNNING)
  {
    mRockSystem.render();
    mEntitySystem.render();
    mParticleSystem.render();

    mProximitySystem.render();
    mScoreSystem.render();
  }
  else
  {
    mApplication->renderBorder();

    glRasterPos2f(w / 4.0f, h / 2.0f);
    mApplication->printText("Press 'Enter' to begin...");
    
    mScoreSystem.render();
  }
}

void AppStateRunning::update(double _deltaTime)
{
  if (mGameState == STATE_RUNNING)
  {
    static const float increment = 5.0f;

    if (mEntitySystem.mSystem.has(mPlayerId))
    {
      Entity& player = mEntitySystem.mSystem.lookup(mPlayerId);

      if (mApplication->isKeyPressed('W'))      player.thrust = 1.0f;
      else if (mApplication->isKeyPressed('S')) player.thrust = -1.0f;
      else                                      player.thrust = 0.0f;

      if (mApplication->isKeyPressed('A'))      player.turn = 1.0f;
      else if (mApplication->isKeyPressed('D')) player.turn = -1.0f;
      else                                      player.turn = 0.0f;

      if (mApplication->isKeyPressed(VK_SPACE))
      {
        static const float playerShotSpread = 30.0f;
        if ((Mgl::getTickTime() - mPlayerWeaponLastFired) > mPlayerWeaponCooldown)
        {
          mPlayerWeaponLastFired = Mgl::getTickTime();

          Vector3 v = makeVector(player.angle + Mgl::randomf(-playerShotSpread, playerShotSpread), 50.0f);

          mParticleSystem.add(player.position, v, player.id);
        }
      } 
    }

    mRockSystem.update(_deltaTime);
    mParticleSystem.update(_deltaTime);
    mEntitySystem.update(_deltaTime);

    mProximitySystem.update(_deltaTime);
    mScoreSystem.update(_deltaTime);
  }
  else if (mGameState == STATE_GAME_OVER)
  {
    if (mApplication->isKeyPressed('\r'))
    {
      setState(STATE_RUNNING);
    }
  }

  checkEvents();
}

void AppStateRunning::checkEvents()
{
  if (mProximitySystem.getEvents(mEvents))
  {
    for (unsigned i = 0; i < array::size(mEvents); i++)
    {
      switch (mEvents[i]->type)
      {
        case ProximitySystem::EVENT_COLLISION:
        {
          ProximityEvent& proximityEvent = *((ProximityEvent *)mEvents[i]->data);

          if (proximityEvent.type == ProximitySystem::TYPE_PARTICLE_TO_ROCK)
          {
            ID particleId = proximityEvent.ids[0];
            ID rockId     = proximityEvent.ids[1];

            if (mParticleSystem.mSystem.has(particleId) && mRockSystem.mSystem.has(rockId))
            {
              Rock& rock = mRockSystem.mSystem.lookup(rockId);

              mParticleSystem.remove(particleId);
              
              destroyRock(rock);
              // TODO: Add explosion here
              //mEmitterSystem.add(EMITTER_TYPE_EXPLOSION, proximityEvent.where, 1.0);
            }
          }
          else if (proximityEvent.type == ProximitySystem::TYPE_ENTITY_TO_ROCK)
          {
            ID entityId = proximityEvent.ids[0];
            ID rockId   = proximityEvent.ids[1];

            if (mEntitySystem.mSystem.has(entityId) && mRockSystem.mSystem.has(rockId))
            {
              Entity& entity = mEntitySystem.mSystem.lookup(entityId);
              Rock&   rock   = mRockSystem.mSystem.lookup(rockId);
              
              if (entity.invincibleDuration <= 0.0)
              {
                destroyRock(rock);

                entity.life -= 1.0f;

                if (entity.life <= 0.0f)
                {
                  setState(STATE_GAME_OVER);
                }
              }

              // TODO: Add explosion here
              //mEmitterSystem.add(EMITTER_TYPE_EXPLOSION, proximityEvent.where, 1.0);
            }
          }
        } break;
      }
    }
    mProximitySystem.flushEvents();
  }
}

void AppStateRunning::destroyRock(Rock& _rock)
{
  static const int asteroidReward = 100;
  static const int smallAsteroidReward = 150;

  if (_rock.entity.size > 10.0f)
  {
    mScoreSystem.mScore += asteroidReward;

    float speed = (5.0f - (_rock.entity.size / 15.0f)) * Mgl::randomf(5.0f, 15.0f);
    Vector3 v;
    
    v = makeVector(Mgl::randomf(0.0f, 360.0f), speed);
    mRockSystem.add(_rock.entity.position, v, _rock.entity.size / 2.0f);

    v = makeVector(Mgl::randomf(0.0f, 360.0f), speed);
    mRockSystem.add(_rock.entity.position, v, _rock.entity.size / 2.0f);
  }
  else
  {
    mScoreSystem.mScore += smallAsteroidReward;
  }
  
  mRockSystem.remove(_rock.id);
}

void AppStateRunning::reset()
{
  mScoreSystem.loadHighScore(HIGH_SCORE_FILE);

  Vector3 p, v;
  float w = (float)mApplication->mWindowWidth;
  float h = (float)mApplication->mWindowHeight;

  mEntitySystem.clear();
  mParticleSystem.clear();
  mRockSystem.clear();
  mProximitySystem.clear();
  mScoreSystem.clear();

  mProximitySystem.init(&mEntitySystem, &mRockSystem, &mParticleSystem);

  // Initialize the player
  p.x = Mgl::randomf(0.0f, w);
  p.y = Mgl::randomf(0.0f, h);
  p.z = 0.0f;
  mPlayerId = mEntitySystem.add(p);
  mPlayerWeaponCooldown = 0.05;
  mPlayerWeaponLastFired = Mgl::getTickTime();
  Entity& player = mEntitySystem.mSystem.lookup(mPlayerId);
  player.life = player.lifeMax = 15.0f;
  player.invincibleDuration = 2.0;

  // Add some rocks in
  unsigned count = Mgl::random(35, 50);
  //unsigned count = 0;

  for (unsigned i = 0; i < count; i++)
  {
    p.x = Mgl::randomf(0.0f, w);
    p.y = Mgl::randomf(0.0f, h);
    p.z = 0.0f;

    v = makeVector(Mgl::randomf(0.0f, 360.0f), Mgl::randomf(0.0f, 15.0f));

    mRockSystem.add(p, v, Mgl::randomf(8.0f, 32.0f));
  }
}

void AppStateRunning::setState(int _gameState)
{
  if (_gameState >= 0 && _gameState < STATE_COUNT)
  {
    if (mGameState != _gameState)
    {
      switch (_gameState)
      {
        case STATE_RUNNING:
        {
          reset();
        } break;

        case STATE_GAME_OVER:
        {
          if (mScoreSystem.mScore > mScoreSystem.mHighScore)
          {
            mScoreSystem.mHighScore = mScoreSystem.mScore;
            mScoreSystem.saveHighScore(HIGH_SCORE_FILE);
          }
        } break;
      }

      mGameState = _gameState;
    }
  }
}
