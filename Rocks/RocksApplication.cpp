#include "RocksApplication.h"

#include "AppStateRunning.h"

using namespace std;
using namespace foundation;
using namespace Mgl;

RocksApplication::RocksApplication() : MglApplication()
{
}

RocksApplication::~RocksApplication()
{
}

bool RocksApplication::appInit()
{
  bool success = false;

  addAppState(new AppStateRunning);

  success = true;

  return (success);
}

void RocksApplication::appClose()
{
  MglApplication::close();
}