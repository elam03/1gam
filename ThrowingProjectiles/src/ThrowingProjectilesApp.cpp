#include "cinder/app/App.h"
#include "cinder/app/AppNative.h"

#include "cinder/gl/gl.h"
#include "cinder/gl/Texture.h"
#include "cinder/ImageIo.h"

#include "cinder/CinderMath.h"

#include <vector>

using namespace ci;
using namespace ci::app;
using namespace ci::gl;
using namespace std;

struct Projectile
{
  Projectile(const Texture& _texture) : cannonId(0), ready(true), shown(false), position(), velocity(), size(), texture(_texture), lastUpdatedTime(0.0) {}
  virtual ~Projectile() {}

  int     cannonId;
  bool    ready;
  bool    shown;
  Vec2f   position;
  Vec2f   velocity;
  Vec2f   size;
  Texture texture;
  double  lastUpdatedTime;

  void start(const Vec2f& _position, const Vec2f& _dir)
  {
    position = _position;
    velocity = _dir;
    size.set(25.0f, 25.0f);
    shown = true;
    ready = false;
    lastUpdatedTime = getElapsedSeconds();
  }
};

struct Cannon
{
  Cannon(const Vec2f& _position) : id(0), position(_position), size(50.0f, 100.0f), angle(90.0f), charge(0.0f), cannonLength(15.0f), health(100.0f) { static int count = 0; id = count++; }
  virtual ~Cannon() {}

  inline Area getArea() const { return Area(Vec2f(position.x - size.x / 2.0f, position.y), Vec2f(position.x + size.x / 2.0f, position.y + size.y)); }

  int   id;
  Vec2f position;
  Vec2f size;
  float angle;
  float charge;
  float cannonLength;
  float health;

  void render();
};

void Cannon::render()
{
  glPushAttrib(GL_TEXTURE_2D);
    glDisable(GL_TEXTURE_2D);
    gl::color(1.0f, 1.0f, 1.0f, 1.0f);
    gl::drawSolidRect(getArea());

    Vec3f pos(position.x, position.y + size.y, 0.0f);
    Vec3f dir(cannonLength, 0.0f, 0.0f);
    dir.rotateZ(toRadians(angle));
  
    gl::lineWidth(3.0f);
    gl::color(0.85f, 0.85f, 0.85f, 1.0f);
    gl::drawVector(pos, pos + dir, 15.0f, 5.0f);

    if (charge > 0.0f)
    {
      gl::color(1.0f, 0.0f, 0.0f, 1.0f);
      gl::drawVector(pos, pos + (dir * (1.0f + charge)), 20.0f, 8.0f);
    }
  glPopAttrib();
}

class ThrowingProjectilesApp : public AppNative
{
public:
  void setup();
  void mouseDown(MouseEvent event); 
  void mouseDrag(MouseEvent event);
  void mouseUp(MouseEvent event);
  void keyDown(KeyEvent event);
	void keyUp(KeyEvent event);
  void update();
  void draw();

  /////////////////////////////////////////////////////////////////////////////
  Texture mTexture;

  /////////////////////////////////////////////////////////////////////////////
public:
  void renderProjectiles();
  void updateProjectiles();

  inline Projectile& getNextProjectile() { return (mProjectiles[mProjectilesCount++]); }
  inline void removeProjectile(Projectile& _projectile) { if (mProjectilesCount > 0) memcpy(&_projectile, &mProjectiles[(mProjectilesCount--) - 1], sizeof(Projectile)); }

  //Projectile mProjectiles[i];
  vector<Projectile> mProjectiles;
  int                mProjectilesCount;

  /////////////////////////////////////////////////////////////////////////////
public:
  void renderCannons();
  void updateCannons();

  vector<Cannon> mCannons;
  /////////////////////////////////////////////////////////////////////////////
public:
  void renderInterface();
  void updateInterface();

  void updateInput();
  inline bool isDown(int _keyCode) { return ((mKeyCodes.size() > 0 && mKeyCodes.find(_keyCode) != mKeyCodes.end()) ? (mKeyCodes[_keyCode]) : (false)); }
  inline void setDown(int _keyCode, bool _down) { mKeyCodes[_keyCode] = _down; }

  Vec2f          mMousePos;
  Vec2f          mAnchorPos;
  Colorf         mMouseColor;
  map<int, bool> mKeyCodes;
  double         mCurrElapsedTime;
  /////////////////////////////////////////////////////////////////////////////
  // Configurables
public:
  float mFriction;
  Vec2f mGravity;
  Rectf mReadySignalRect;
  float mCannonChargeRate;
  float mCannonTurnRate;
  float mCannonVelocityMultiplier;
  /////////////////////////////////////////////////////////////////////////////
};

void ThrowingProjectilesApp::setup()
{
  mTexture = loadImage(loadAsset("stone0.png"));

  mFriction = .98f;
  mGravity.set(0.0f, -100.0f);
  mReadySignalRect.set(50.0f, 50.0f, 100.0f, 100.0f);
  mCannonChargeRate = .05f;
  mCannonTurnRate = 5.0f;
  mCannonVelocityMultiplier = 150.0f;

  mMouseColor.set(1.0f, 1.0f, 1.0f);

  mCannons.clear();
  mCannons.push_back(Cannon(Vec2f(0.1f * getWindowWidth(), 0.1f * getWindowHeight())));
  mCannons.push_back(Cannon(Vec2f(0.9f * getWindowWidth(), 0.1f * getWindowHeight())));

  mProjectiles.clear();
  for (int i = 0; i < 25; i++)
    mProjectiles.push_back(Projectile(mTexture));
  //mProjectiles.push_back(Projectile(mTexture));
  mProjectilesCount = 0;

  gl::enableAlphaTest();
  //gl::enableDepthRead();
  gl::enable(GL_TEXTURE_2D);

  mCurrElapsedTime = getElapsedSeconds();
}

void ThrowingProjectilesApp::mouseDown(MouseEvent event)
{
  mMouseColor.set(1.0f, 0.0f, 0.0f);
}

void ThrowingProjectilesApp::mouseDrag(MouseEvent event)
{
  mMouseColor.set(0.0f, 1.0f, 0.0f);
}

void ThrowingProjectilesApp::mouseUp(MouseEvent event)
{
  mMouseColor.set(1.0f, 1.0f, 1.0f);
}
void ThrowingProjectilesApp::keyDown(KeyEvent event)
{
  setDown(event.getCode(), true);
}

void ThrowingProjectilesApp::keyUp(KeyEvent event)
{
  setDown(event.getCode(), false);

  if (event.getChar() == 'e')
  {
    Projectile& projectile = getNextProjectile();
    Cannon& cannon = mCannons[0];

    Vec2f pos = cannon.position + Vec2f(0.0f, cannon.size.y);
    Vec2f dir(Vec2f(mCannonVelocityMultiplier * cannon.charge, 0.0f)); dir.rotate(toRadians(cannon.angle));

    projectile.start(pos, dir);
    projectile.cannonId = cannon.id;
    cannon.charge = 0.0f;
  }

  if (event.getChar() == 'o')
  {
    Projectile& projectile = getNextProjectile();
    Cannon& cannon = mCannons[1];

    Vec2f pos = cannon.position + Vec2f(0.0f, cannon.size.y);
    Vec2f dir(Vec2f(mCannonVelocityMultiplier * cannon.charge, 0.0f)); dir.rotate(toRadians(cannon.angle));

    projectile.start(pos, dir);
    projectile.cannonId = cannon.id;
    cannon.charge = 0.0f;
  }

  if (event.getCode() == KeyEvent::KEY_ESCAPE)
  {
    quit();
  }
}

void ThrowingProjectilesApp::update()
{
  updateInput();

  updateProjectiles();
  updateCannons();
  updateInterface();
  
  mCurrElapsedTime = getElapsedSeconds();
}

void ThrowingProjectilesApp::draw()
{
  // clear out the window with black
  gl::clear(Color(0, 0, 0));

  gl::setMatricesWindow(getWindowSize(), false);
  glLoadIdentity();

  renderProjectiles();
  renderCannons();
  renderInterface();
}

void ThrowingProjectilesApp::renderProjectiles()
{
  for (int i = 0; i < mProjectilesCount; i++)
  {
    Projectile& projectile = mProjectiles[i];

    if (projectile.shown)
    {
      projectile.texture.enableAndBind();
        Rectf rect(projectile.position - projectile.size, projectile.position + projectile.size);
        gl::color(1.0f, 1.0f, 1.0f, 1.0f);
        gl::draw(projectile.texture, rect);
      projectile.texture.unbind();
    }
    else
    {
    }
  }
}

void ThrowingProjectilesApp::updateProjectiles()
{
  for (int i = 0; i < mProjectilesCount; i++)
  {
    Projectile& projectile = mProjectiles[i];

    if (projectile.shown)
    {
      double currTime = getElapsedSeconds();
      double timeDelta = currTime - projectile.lastUpdatedTime;

      projectile.position += projectile.velocity * timeDelta;
      //projectile.velocity *= mFriction;
      projectile.velocity += mGravity * timeDelta;
      
      projectile.lastUpdatedTime = currTime;

      Rectf bounds(0.0f, 0.0f, (float)getWindowWidth(), (float)(getWindowHeight() * 2.0f));
      
      if (!bounds.contains(projectile.position))
      {
        projectile.shown = false;
        projectile.ready = true;

        removeProjectile(projectile);
        i--;
        continue;
      }

      for (int j = 0; j < mCannons.size(); j++)
      {
        Cannon& cannon = mCannons[j];
        
        if (cannon.id != projectile.cannonId && cannon.getArea().contains(projectile.position))
        {
          removeProjectile(projectile);
          i--;
        }
      }
    }
  }
}

void ThrowingProjectilesApp::renderCannons()
{
  for (unsigned i = 0; i < mCannons.size(); i++)
  {
    Cannon& cannon = mCannons[i];

    cannon.render();
  }
}

void ThrowingProjectilesApp::updateCannons()
{
}

void ThrowingProjectilesApp::renderInterface()
{
  gl::color(mMouseColor);
  gl::drawLine(Vec2f(0.0f, 5.0f), Vec2f((float)getWindowWidth(), 5.0f));

  //gl::color(1.0f, 1.0f, 1.0f, 1.0f);
  //gl::drawLine(mMousePos + Vec2f(-5.0f, -5.0f), mMousePos + Vec2f( 5.0f, 5.0f));
  //gl::drawLine(mMousePos + Vec2f( 5.0f, -5.0f), mMousePos + Vec2f(-5.0f, 5.0f));

  //gl::drawLine(mAnchorPos + Vec2f(-5.0f, -5.0f), mAnchorPos + Vec2f( 5.0f, 5.0f));
  //gl::drawLine(mAnchorPos + Vec2f( 5.0f, -5.0f), mAnchorPos + Vec2f(-5.0f, 5.0f));

  //if (!mProjectiles[i].shown)
  //{
  //  gl::color(1.0f, 1.0f, 1.0f, 1.0f);
  //  //gl::drawLine(mMousePos, mAnchorPos);
  //  gl::drawVector(Vec3f(mMousePos), Vec3f(mAnchorPos), 15.0f, 10.0f);

  //  gl::color(0.0f, 1.0f, 0.0f, 1.0f);
  //  gl::drawSolidRect(mReadySignalRect);
  //}
  //else
  //{
  //  gl::color(1.0f, 0.0f, 0.0f, 1.0f);
  //  gl::drawSolidRect(mReadySignalRect);
  //}
}

void ThrowingProjectilesApp::updateInterface()
{
}

void ThrowingProjectilesApp::updateInput()
{
  if (isDown('a'))
    mCannons[0].angle += mCannonTurnRate;
  if (isDown('d'))
    mCannons[0].angle -= mCannonTurnRate;
  if (isDown('e'))
    mCannons[0].charge += mCannonChargeRate;

  mCannons[0].angle = max(0.0f, min(180.0f, mCannons[0].angle));
  mCannons[0].charge = min(mCannons[0].charge, 2.0f);

  if (isDown('j'))
    mCannons[1].angle += mCannonTurnRate;
  if (isDown('l'))
    mCannons[1].angle -= mCannonTurnRate;
  if (isDown('o'))
    mCannons[1].charge += mCannonChargeRate;
  
  mCannons[1].angle = max(0.0f, min(180.0f, mCannons[1].angle));
  mCannons[1].charge = min(mCannons[1].charge, 2.0f);
}

CINDER_APP_NATIVE(ThrowingProjectilesApp, RendererGl)
