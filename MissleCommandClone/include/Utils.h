#pragma once

#include "cinder/CinderMath.h"

template<typename T>
static inline T random(T _min, T _max) { return (_min + ((_max - _min) * ((float)(rand() % 1000) / 1000.0f))); }

template<typename T>
static inline T random(T _max) { return (random<T>(0, _max)); }

#define randf(a, b) random<float>(a, b)

#define randd(a, b) random<double>(a, b)

static float angleTo(const ci::Vec3f& _origin, const ci::Vec3f& _target)
{
  float dx =  (float)(_target.x - _origin.x);
  float dz = -(float)(_target.z - _origin.z);

  float result = ci::toDegrees(atan2(dz, dx));

  if (result < 0.0f)
    result += 360.0f;

  return (result);
}