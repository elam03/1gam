#include "cinder/app/AppNative.h"
#include "cinder/gl/gl.h"
#include "cinder/ConcurrentCircularBuffer.h"

#include "Utils.h"

#include <vector>

using namespace ci;
using namespace ci::app;
using namespace std;

static const float TowerWidth = 32.0f;

struct SystemEvent
{
  int type;
  int id;
};

template<typename T>
struct System
{
  System(int _object_count = 100, int _event_count = 100) : 
    objects(_object_count),
    object_curr(0),
    events(_event_count),
    event_write_index(0),
    event_read_index(0)
  {
    for (int i = 0; i < objects.size(); i++)
      objects[i].id = i;
  }

  void render()
  {
    for (auto& object : objects)
      object.render();
  }

  void update(double _elapsed_time)
  {
    for (auto& object : objects)
    {
      int event_type = object.update(_elapsed_time);

      if (event_type)
      {
        SystemEvent& system_event = getNewEvent();

        system_event.id = object.id;
        system_event.type = event_type;
      }
    }
  }

  T& operator[](int _index) { return (objects[_index]); }
  const T& operator[](int _index) const { return (objects[_index]); }

  T& getNewObject()
  {
    T& new_object = objects[object_curr];
    if (++object_curr >= objects.size())
      object_curr = 0;
    
    return (new_object);
  }

  SystemEvent& getNewEvent()
  {
    SystemEvent& event = events[event_write_index];

    if (++event_write_index >= events.size())
      event_write_index = 0;

    return (event);
  }

  bool hasEvents() const { return (event_write_index != event_read_index); }

  SystemEvent& getNextEvent()
  {
    SystemEvent& event = events[event_read_index];

    if (++event_read_index >= events.size())
      event_read_index = 0;

    return (event);
  }

  vector<SystemEvent> events;
  int                 event_write_index;
  int                 event_read_index;

  vector<T> objects;
  int       object_curr;
};

struct Missle
{
  typedef enum EVENT
  {
    EVENT_NONE,
    EVENT_STARTED,
    EVENT_STOPPED,
  } EVENT_COUNT;

  Missle() : alive(false) {}
  virtual ~Missle() {}

  void launch(const Vec2f& _start_pos, const Vec2f& _end_pos, double _start_time, double _duration)
  {
    start_pos = _start_pos;
    end_pos   = _end_pos;
    curr_pos  = _start_pos;

    start_time = _start_time;
    end_time   = _start_time + _duration;

    t = 0.0f;
    alive = true;
  }

  void render()
  {
    if (!alive)
      return;

    glPushAttrib(GL_LINE_WIDTH);
      //glLineWidth(1.0f);
      //gl::color(0.25f, 0.0f, 0.0f);
      //gl::drawLine(start_pos, end_pos);
        
      glLineWidth(2.0f);
      gl::color(1.0f, 0.0f, 0.0f);
      gl::drawLine(start_pos, curr_pos);
    glPopAttrib();
  }

  int update(double _elapsed_time)
  {
    int event = EVENT_NONE;

    if (!alive)
      return (event);

    if (_elapsed_time >= start_time && _elapsed_time <= end_time)
    {
      t = static_cast<float>((_elapsed_time - start_time) / (end_time - start_time));
      curr_pos = start_pos + (end_pos - start_pos) * t;
    }
    else
    {
      alive = false;
      event = EVENT_STOPPED;
    }

    return (event);
  }

  int id;

  Vec2f  start_pos;
  Vec2f  end_pos;
  Vec2f  curr_pos;

  double start_time;
  double end_time;
  float  t;
  bool   alive;
};

struct Explosion
{
  typedef enum EVENT
  {
    EVENT_NONE,
    EVENT_STARTED,
    EVENT_STOPPED,
  } EVENT_COUNT;

  Explosion() {}
  virtual ~Explosion() {}

  void start(const Vec2f& _position, float _size, double _start_time, double _duration)
  {
    position = _position;

    size_curr = 0.0f;
    size_max = _size;

    start_time = _start_time;
    end_time = start_time + _duration;

    t = 0.0f;
    alive = true;
  }

  void render()
  {
    if (!alive)
      return;

    gl::color(0.85f, 0.0f, 0.0f);
    gl::drawSolidCircle(position, size_curr);
  }

  int update(double _elapsed_time)
  {
    int event = EVENT_NONE;

    if (!alive)
      return (event);

    if (_elapsed_time >= start_time && _elapsed_time <= end_time)
    {
      t = static_cast<float>((_elapsed_time - start_time) / (end_time - start_time));

      if (t >= 0.0f && t <= 0.5f)
        size_curr = (size_max) * (t * 2.0f);
      else
        size_curr = (size_max) * (1.0f - ((t - 0.5f) * 2.0f));
    }
    else
    {
      alive = false;
      event = EVENT_STOPPED;
    }

    return (event);
  }
  
  int id;

  Vec2f position;

  float size_curr;
  float size_max;

  double start_time;
  double end_time;

  float t;
  bool  alive;
};

struct Tower
{
  typedef enum EVENT
  {
    EVENT_NONE,
    EVENT_STARTED,
    EVENT_STOPPED,
  } EVENT_COUNT;


  void render()
  {
    if (!alive)
      return;

    gl::color(1.0f, 1.0f, 1.0f);
    gl::drawSolidRect(rect);
  }

  int update(double _elapsed_time)
  {
    int event = EVENT_NONE;
    
    if (!alive)
      return (event);

    return (event);
  }

  int id;
  
  Rectf rect;

  bool alive;
};

class MissleCommandCloneApp : public AppNative
{
public:
  MissleCommandCloneApp();
  virtual ~MissleCommandCloneApp();
	void setup();
	void resize();
	void mouseDown(MouseEvent _event);
	void mouseMove(MouseEvent _event);
	void keyDown(KeyEvent _event);
	void update();
	void draw();

public:
  System<Tower>     m_towers;
  System<Missle>    m_anti_missles;
  System<Missle>    m_missles;
  System<Explosion> m_explosions;

  Vec2f         m_mouse_pos;

  int    m_wave;
  int    m_missles_per_wave;
  int    m_missles_left;

  double m_missle_attack_time_delta;
  double m_next_missle_attack_time;

  double m_anti_missle_cooldown;
  double m_anti_missle_next_time;
};

MissleCommandCloneApp::MissleCommandCloneApp() : m_towers(100), m_anti_missles(100), m_missles(100), m_explosions(100)
{
}

MissleCommandCloneApp::~MissleCommandCloneApp()
{
}

void MissleCommandCloneApp::setup()
{
  gl::setMatricesWindow(getWindowSize(), false);

  int tower_count = (getWindowWidth() / TowerWidth) - 1;
  float x = 0.0f, y = 0.0f;

  for (int i = 0; i < tower_count; i++)
  {
    Tower tower;
    float height = randf(64.0f, 128.0f);

    tower.rect.set(x, y, x + TowerWidth, y + height);

    m_towers.getNewObject() = tower;

    x += TowerWidth;
    x += 5.0f;
  }

  m_anti_missle_cooldown = 0.5;
  m_anti_missle_next_time = getElapsedSeconds();

  m_wave = 1;
  m_missles_per_wave = 10;
  m_missles_left = 10;

  m_missle_attack_time_delta = 5.0;
  m_next_missle_attack_time = getElapsedSeconds();
}

void MissleCommandCloneApp::resize()
{
  gl::setMatricesWindow(getWindowSize(), false);
}

void MissleCommandCloneApp::mouseDown(MouseEvent _event)
{
  m_mouse_pos.set(static_cast<float>(_event.getX()), static_cast<float>(getWindowHeight() - _event.getY()));

  if (getElapsedSeconds() > m_anti_missle_next_time)
  {
    m_anti_missles.getNewObject().launch(Vec2f(static_cast<float>(getWindowWidth()) * randf(0.0f, 1.0f), 0.0f), m_mouse_pos, getElapsedSeconds(), 2.0);
    
    m_anti_missle_next_time = getElapsedSeconds() + m_anti_missle_cooldown;
  }
}

void MissleCommandCloneApp::mouseMove(MouseEvent _event)
{
  m_mouse_pos.set(static_cast<float>(_event.getX()), static_cast<float>(getWindowHeight() - _event.getY()));
}

void MissleCommandCloneApp::keyDown(KeyEvent _event)
{
  if (_event.getCode() == KeyEvent::KEY_ESCAPE)
  {
    quit();
  }
}

void MissleCommandCloneApp::update()
{
  double elapsed_time = getElapsedSeconds();

  m_towers.update(elapsed_time);
  m_anti_missles.update(elapsed_time);
  m_missles.update(elapsed_time);
  m_explosions.update(elapsed_time);

  /////////////////////////////////////////////////////////////////////////////
  while (m_anti_missles.hasEvents())
  {
    SystemEvent event = m_anti_missles.getNextEvent();

    if (event.type == Missle::EVENT_STOPPED)
    {
      Missle& missle = m_anti_missles[event.id];
      Explosion& explosion = m_explosions.getNewObject();

      explosion.start(missle.end_pos, 16.0f, getElapsedSeconds(), 3.0f);
    }
  }
  /////////////////////////////////////////////////////////////////////////////
  for (int i = 0; i < m_missles.objects.size(); i++)
  {
    Missle& missle = m_missles.objects[i];

    if (missle.alive)
    {
      for (int j = 0; j < m_towers.objects.size(); j++)
      {
        Tower& tower = m_towers.objects[j];

        if (tower.rect.contains(missle.curr_pos))
        {
          Explosion& explosion = m_explosions.getNewObject();
          explosion.start(missle.curr_pos, 32.0f, getElapsedSeconds(), 1.0f);

          missle.alive = false;
          tower.alive = false;
        }
      }
    }
  }

  for (int i = 0; i < m_explosions.objects.size(); i++)
  {
    Explosion& explosion = m_explosions.objects[i];

    if (explosion.alive)
    {
      for (int j = 0; j < m_missles.objects.size(); j++)
      {
        Missle& missle = m_missles.objects[j];
        
        if (missle.alive)
        {
          if (explosion.position.distanceSquared(missle.curr_pos) < (explosion.size_curr * explosion.size_curr))
          {
            missle.alive = false;
          }
        }
      }
    }
  }
  /////////////////////////////////////////////////////////////////////////////
  if (getElapsedSeconds() >= m_next_missle_attack_time)
  {
    Missle& missle = m_missles.getNewObject();
    
    missle.launch(Vec2f(randf(0.0, static_cast<float>(getWindowWidth())), static_cast<float>(getWindowHeight())),
                  Vec2f(randf(0.0, static_cast<float>(getWindowWidth())), 0.0f),
                  getElapsedSeconds(),
                  10.0);

    m_next_missle_attack_time += m_missle_attack_time_delta;

    if (m_missle_attack_time_delta > 1.0)
      m_missle_attack_time_delta -= 0.1;

    if (--m_missles_left <= 0)
    {
      m_wave++;

      m_missles_per_wave += (5 + rand() % 5);

      m_missles_left = m_missles_per_wave;
    }
  }
  /////////////////////////////////////////////////////////////////////////////
}

void MissleCommandCloneApp::draw()
{
  static Font font("arial", 32.0f);
	gl::clear(Color(0, 0, 0));

  m_towers.render();
  m_anti_missles.render();
  m_missles.render();
  m_explosions.render();

  gl::color(0.0f, 0.0f, 1.0f);
  gl::drawSolidCircle(m_mouse_pos, 8.0f);

  //{
  //  stringstream ss;
  //  
  //  ss << "wave: " << m_wave << " missles left: " << m_missles_left << endl;
  //  string s = ss.str();
  //  
  //  gl::drawString(s, Vec2f(5.0f, 5.0f), ColorA(1.0f, 1.0f, 1.0f, .25f), font);
  //}
}

CINDER_APP_NATIVE(MissleCommandCloneApp, RendererGl)
