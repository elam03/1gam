#pragma once

#include "MglSystem.h"
#include "RotatorTypes.h"

class ParticleSystem
{
public:
  ParticleSystem();
  virtual ~ParticleSystem();

  ID   add(float _angle, float _speed);
  void remove(ID _idToRemove);
  void clear();
  void render(const Mgl::CameraInfo& _cameraInfo);
  void update(double _deltaTime);

  System<Particle> mSystem;
};
