#include "EffectSystem.h"

#include "MglResourceManager.h"
#include "MglUtils.h"
#include "RotatorApplication.h"

#include "MglGl.h"
#include "MglUtils.h"

using namespace foundation;
using namespace std;
using namespace Mgl;

EffectSystem::EffectSystem() :
  mOutputEvents(),
  mSystem()
{
  mOutputEvents.init();
}

EffectSystem::~EffectSystem()
{
  mOutputEvents.close();
}

ID EffectSystem::add(double _duration, const foundation::Vector3& _position, const foundation::Vector3& _velocity)
{
  ID id = mSystem.add();

  if (mSystem.has(id))
  {
    Effect& effect = mSystem.lookup(id);
    memset(&effect, 0, sizeof(Effect));

    effect.id       = id;
    effect.timeLeft = _duration;
    effect.duration = effect.timeLeft;

    effect.position = _position;
    effect.velocity = _velocity;
  }

  return (id);
}

void EffectSystem::remove(ID _idToRemove)
{
  mSystem.remove(_idToRemove);
}

void EffectSystem::clear()
{
  mSystem.reset();
}

void EffectSystem::render(const CameraInfo& _cameraInfo)
{
  for (unsigned i = 0; i < mSystem.getNumObjects(); i++)
  {
    Effect& effect = mSystem._objects[i];

    if (!contains(_cameraInfo, makeVector2(effect.position)))
      continue;

    glPushMatrix();
      glTranslatef(effect.position);

      glColor3f(1.0f, 1.0f, 1.0f);
      glBegin(GL_LINES);
        glVertex2f(-3.0f,  0.0f);
        glVertex2f( 3.0f,  0.0f);
        glVertex2f( 0.0f, -3.0f);
        glVertex2f( 0.0f,  3.0f);
      glEnd();
    glPopMatrix();
  }
}

void EffectSystem::update(double _deltaTime)
{
  for (unsigned i = 0; i < mSystem.getNumObjects(); i++)
  {
    Effect& effect = mSystem._objects[i];

    effect.position = effect.position + effect.velocity * (float)_deltaTime;

    if (effect.timeLeft < 0.0)
    {
      EffectEvent e;
      e.effectId = effect.id;
      
      mOutputEvents.queueEvent(EVENT_EFFECT_FINISHED, &e, sizeof(EffectEvent));
    }
    else
    {
      effect.timeLeft -= _deltaTime;

      //switch (effect.type)
      //{
      //  case EFFECT_TYPE_EXPLOSION:
      //  {
      //    for (int i = 0; i < effect.unitCount; i++)
      //    {
      //      effect.units[i].position = effect.units[i].position + effect.units[i].velocity * _deltaTime;
      //      effect.units[i].timeLeft -= _deltaTime;
      //      //effect.units[i].color = effect.units[i].color * (effect.units[i].timeLeft / effect.units[i].duration);
      //    } 
      //  } break;

      //  case EFFECT_TYPE_FIELD:
      //  {
      //    effect.color.x -= _deltaTime;
      //    effect.color.y -= _deltaTime;
      //    effect.color.z -= _deltaTime;
      //  } break;
      //}
    }
  }
}

bool EffectSystem::getEvents(Array<EventHeader*>& _packets)
{
  return (mOutputEvents.getEvents(_packets));
}

bool EffectSystem::flushEvents()
{
  return (mOutputEvents.flushEvents());
}