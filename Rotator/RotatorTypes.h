#pragma once

#include "MglTypes.h"

struct Particle
{
  ID id;

  float angle;
  float distance;
  float speed;
};

struct Pig
{
  float size;
  
  float health;
  float healthMax;

  float angle;
  float rateOfTurn;

  bool  mouthOpen;
};

struct Effect
{
  ID id;

  foundation::Vector3 position;
  foundation::Vector3 velocity;

  double timeLeft;
  double duration;
};

struct EffectEvent
{
  ID effectId;
};