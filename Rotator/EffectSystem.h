#pragma once

#include "MglEventSystem.h"
#include "MglSystem.h"
#include "RotatorTypes.h"

#include <string>

class EffectSystem
{
public:
  EffectSystem();
  virtual ~EffectSystem();

  ID   add(double _duration, const foundation::Vector3& _position, const foundation::Vector3& _velocity);
  void remove(ID _idToRemove);
  void clear();
  void render(const Mgl::CameraInfo& _cameraInfo);
  void update(double _deltaTime);
  
  System<Effect> mSystem;

  /////////////////////////////////////////////////////////////////////////////
public:
  enum
  {
    EVENT_EFFECT_FINISHED,
    EVENT_COUNT
  };

  bool getEvents(foundation::Array<Mgl::EventHeader*>& _packets);
  bool flushEvents();

protected:
  EventSystem mOutputEvents;

  /////////////////////////////////////////////////////////////////////////////
};