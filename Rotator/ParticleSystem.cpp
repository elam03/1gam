#include "ParticleSystem.h"

#include "MglGl.h"
#include "MglResourceManager.h"
#include "MglUtils.h"

using namespace foundation;
using namespace Mgl;

ParticleSystem::ParticleSystem() :
  mSystem()
{
}

ParticleSystem::~ParticleSystem()
{
}

ID ParticleSystem::add(float _angle, float _speed)
{
  ID id = mSystem.add();

  if (mSystem.has(id))
  {
    Particle& particle = mSystem.lookup(id);
    memset(&particle, 0, sizeof(Particle));

    particle.id = id;
    particle.angle = _angle;
    particle.distance = 200.0f;
    particle.speed = _speed;
  }

  return (id);
}

void ParticleSystem::remove(ID _idToRemove)
{
  mSystem.remove(_idToRemove);
}

void ParticleSystem::clear()
{
  mSystem.reset();
}

void ParticleSystem::render(const CameraInfo& _cameraInfo)
{
  static const float size = 10.0f;
  
  Texture* texture = ResourceManager::Instance().getTexture(L"fireball");

  glEnable(GL_TEXTURE_2D);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

  texture->bind();

  for (unsigned i = 0; i < mSystem.getNumObjects(); i++)
  {
    Particle& particle = mSystem._objects[i];

    glPushMatrix();
      glRotatef(particle.angle, 0.0f, 0.0f, 1.0f);

      glTranslatef(particle.distance, 0.0f, 0.0f);
        
      glPushMatrix();
        glRotatef(45.0f, 0.0f, 0.0f, 1.0f);

        glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        glBegin(GL_QUADS);
          glTexCoord2f(0.0f, 0.0f); glVertex2f(-size, -size);
          glTexCoord2f(1.0f, 0.0f); glVertex2f( size, -size);
          glTexCoord2f(1.0f, 1.0f); glVertex2f( size,  size);
          glTexCoord2f(0.0f, 1.0f); glVertex2f(-size,  size);
        glEnd();
      glPopMatrix();
    glPopMatrix();
  }
      
  texture->unbind();
  glDisable(GL_TEXTURE_2D);
  glDisable(GL_BLEND);

  /////////////////////////////////////////////////////////////////////////////
  
  //for (unsigned i = 0; i < mSystem.getNumObjects(); i++)
  //{
  //  Particle& particle = mSystem._objects[i];
  //  
  //  glPushMatrix();
  //    glRotatef(particle.angle, 0.0f, 0.0f, 1.0f);
  //    glTranslatef(particle.distance, 0.0f, 0.0f);
  //    
  //    glColor3f(1.0f, 0.0f, 0.0f);
  //    glBegin(GL_LINES);
  //      glVertex2f(-5.0f, -5.0f);
  //      glVertex2f( 5.0f,  5.0f);

  //      glVertex2f(-5.0f,  5.0f);
  //      glVertex2f( 5.0f, -5.0f);
  //    glEnd();
  //  glPopMatrix();
  //}
}

void ParticleSystem::update(double _deltaTime)
{
}