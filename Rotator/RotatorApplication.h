#pragma once

#include "MglApplication.h"

#include "EffectSystem.h"
#include "ParticleSystem.h"

class AppState;
class ApplicationEvent;

class RotatorApplication : public MglApplication
{
public:
  RotatorApplication();
  virtual ~RotatorApplication();

protected:
  virtual bool appInit();
  virtual void appClose();

public:
  virtual bool defaultHandleEvent(int _event, void* _eventData = 0);
  virtual void defaultRender();
  virtual void defaultUpdate(double _deltaTime);

protected:
  enum
  {
    STATE_RUNNING,
    STATE_NEXT_WAVE,
    STATE_GAME_OVER,
    STATE_COUNT
  };

  Mgl::CameraInfo mCameraInfo;

  ParticleSystem mParticleSystem;

  void renderGrass();
  void renderPig();

  void checkFireballs(double _deltaTime);
  void checkFireBallCooldown(double _deltaTime);

  void renderRunning(const Mgl::CameraInfo& _cameraInfo);
  void updateRunning(double _deltaTime);

  void renderGameOver(const Mgl::CameraInfo& _cameraInfo);
  void updateGameOver(double _deltaTime);

  void renderNextWave(const Mgl::CameraInfo& _cameraInfo);
  void updateNextWave(double _deltaTime);

  void initGame();
  void setupNextWave();
  void startNextWave();

  int mGameState;
  int mCurrWave;
  int mCurrWaveFireBallsLeft;

  Pig mPig;

  double mFireBallCurrTime;
  double mFireBallCooldown;
  double mFireBallCooldownReduction;

  EffectSystem mEffectSystem;
};