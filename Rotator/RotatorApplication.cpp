#include "RotatorApplication.h"

#include "MglGl.h"
#include "MglResourceManager.h"
#include "MglUtils.h"

using namespace std;
using namespace foundation;
using namespace Mgl;

RotatorApplication::RotatorApplication() :
  MglApplication(),
  mParticleSystem()
{
}

RotatorApplication::~RotatorApplication()
{
}

bool RotatorApplication::appInit()
{
  bool success = false;

  mUpdatesPerSecond = 120;

  //ResourceManager::Instance().addTexture(L"particle1", L"..\\assets\\cloud_mutagenic_large1.png");
  //ResourceManager::Instance().addTexture(L"particle2", L"..\\assets\\cloud_mutagenic_large2.png");
  //ResourceManager::Instance().addTexture(L"particle3", L"..\\assets\\cloud_mutagenic_large3.png");
  //ResourceManager::Instance().addTexture(L"particle4", L"..\\assets\\cloud_mutagenic_large4.png");
  ResourceManager::Instance().addTexture(L"fireball", L"..\\assets\\fireball.png");
  ResourceManager::Instance().addTexture(L"grass", L"..\\assets\\grass0.png");
  
  ResourceManager::Instance().addTexture(L"pigMouthClosed", L"..\\assets\\pig_form.png");
  ResourceManager::Instance().addTexture(L"pigMouthOpen", L"..\\assets\\cloud_mutagenic_large1.png");
  
  mGameState = STATE_GAME_OVER;
  mCurrWave = 0;
  mCurrWaveFireBallsLeft = 0;

  memset(&mPig, 0, sizeof(Pig));

  mFireBallCurrTime          = 0.0;
  mFireBallCooldown          = 0.0;
  mFireBallCooldownReduction = 0.0;

  success = true;

  return (success);
}

void RotatorApplication::appClose()
{
  MglApplication::close();
}

bool RotatorApplication::defaultHandleEvent(int _event, void* _eventData)
{
  return (false);
}

void RotatorApplication::defaultRender()
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  //glClearColor(.1f, .3f, .1f, 0.0f);
  glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

  glLoadIdentity();

  switch (mGameState)
  {
    case STATE_RUNNING:   renderRunning(mCameraInfo); break;
    case STATE_NEXT_WAVE: renderNextWave(mCameraInfo); break;
    default:              renderGameOver(mCameraInfo); break;
  }
}

void RotatorApplication::defaultUpdate(double _deltaTime)
{
  switch (mGameState)
  {
    case STATE_RUNNING:   updateRunning(_deltaTime); break;
    case STATE_NEXT_WAVE: updateNextWave(_deltaTime); break;
    default:              updateGameOver(_deltaTime); break;
  }
}

void RotatorApplication::renderGrass()
{
  Texture* texture = ResourceManager::Instance().getTexture(L"grass");

  glEnable(GL_TEXTURE_2D);

  if (texture)
  {
    float w = (float)mWindowWidth;
    float h = (float)mWindowHeight;
    glPushMatrix();
      texture->bind();
      
      glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
      glBegin(GL_QUADS);
         glTexCoord2f(0.0f, 1.0f); glVertex2f(0.0f, 0.0f);   
         glTexCoord2f(1.0f, 1.0f); glVertex2f(w   , 0.0f);   
         glTexCoord2f(1.0f, 0.0f); glVertex2f(w   , h);
         glTexCoord2f(0.0f, 0.0f); glVertex2f(0.0f, h);
      glEnd();

      texture->unbind();
    glPopMatrix();
  }
}

void RotatorApplication::renderPig()
{
  Texture* texture = nullptr;

  if (mPig.mouthOpen) texture = ResourceManager::Instance().getTexture(L"pigMouthOpen");
  else                texture = ResourceManager::Instance().getTexture(L"pigMouthClosed");

  glEnable(GL_TEXTURE_2D);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  if (texture)
  {
    glPushMatrix();
      texture->bind();

      glTranslatef(mWindowWidth / 2.0f, mWindowHeight / 2.0f, 0.0f);
      glRotatef(mPig.angle, 0.0f, 0.0f, 1.0f);

      if (mPig.mouthOpen) glColor4f(1.0f, 0.0f, 0.0f, 1.0f);
      else                glColor4f(1.0f, 1.0f, 1.0f, 1.0f);

      glScalef(mPig.size, mPig.size, mPig.size);
      glBegin(GL_QUADS);
        glTexCoord2f(0.00f, 1.00f); glVertex2f(-1.0f, -1.0f);
        glTexCoord2f(1.00f, 1.00f); glVertex2f( 1.0f, -1.0f);
        glTexCoord2f(1.00f, 0.01f); glVertex2f( 1.0f,  1.0f);
        glTexCoord2f(0.00f, 0.01f); glVertex2f(-1.0f,  1.0f);
      glEnd();
      
      texture->unbind();
    glPopMatrix();
  }
  
  float w = mPig.size * 2.0f * (mPig.health / mPig.healthMax);
  float h = max(mPig.size / 4.0f, 3.0f);

  glPushMatrix();
    glTranslatef(mWindowWidth / 2.0f - mPig.size, mWindowHeight / 2.0f - mPig.size - h, 0.0f);
    glColor3f(0.0f, 1.0f, 0.0f);
    glQuad(0.0f, 0.0f, w, h);
  glPopMatrix();
}

void RotatorApplication::checkFireballs(double _deltaTime)
{
  static const float PigMouthSize = 30.0f;

  float pigAngle = mPig.angle + 180.0f;

  if (pigAngle >= 360.0f)   pigAngle -= 360.0f;
  else if (pigAngle < 0.0f) pigAngle += 360.0f;

  for (unsigned i = 0; i < mParticleSystem.mSystem.getNumObjects(); i++)
  {
    Particle& particle = mParticleSystem.mSystem._objects[i];

    if (particle.distance)
    {
      particle.distance -= particle.speed;

      if (particle.distance < mPig.size)
      {
        float angleDiff = fabs(particle.angle - pigAngle);

        if (mPig.mouthOpen && angleDiff < PigMouthSize)
        {
          debugPrint(0, "FIREBALL EATEN!");
        }
        else
        {
          debugPrint(0, "BACON CREATED! YOU WERE OFF BY %.2f (%.2f - %.2f(", angleDiff, particle.angle, pigAngle);

          if (mPig.health > 0.0f)
          {
            mPig.health -= 5.0f;
          }
          else
          {
            mPig.health = 0.0f;
            mGameState = STATE_GAME_OVER;
          }
        }

        mParticleSystem.remove(particle.id);

        if (mCurrWaveFireBallsLeft <= 0 && mParticleSystem.mSystem.getNumObjects() <= 0)
        {
          setupNextWave();
        }
      }
    }
  }
}

void RotatorApplication::checkFireBallCooldown(double _deltaTime)
{
  if (mCurrWaveFireBallsLeft > 0)
  {
    mFireBallCurrTime += _deltaTime;

    if (mFireBallCurrTime >= mFireBallCooldown)
    {
      mCurrWaveFireBallsLeft--;

      mParticleSystem.add(randomf(0.0f, 360.0f), randomf(0.25f, 1.0f));

      if (mFireBallCooldown > mFireBallCooldownReduction)
        mFireBallCooldown -= mFireBallCooldownReduction;

      mFireBallCurrTime -= mFireBallCooldown;
    }
  }
}
  
void RotatorApplication::renderRunning(const CameraInfo& _cameraInfo)
{
  renderGrass();
  renderPig();

  glPushMatrix();
    glTranslatef(mWindowWidth / 2.0f, mWindowHeight / 2.0f, 0.0f);
    mParticleSystem.render(mCameraInfo);
    mEffectSystem.render(_cameraInfo);
  glPopMatrix();
}

void RotatorApplication::updateRunning(double _deltaTime)
{
  static const float RateOfTurn = 180.0f;
  float deltaTime = (float)_deltaTime;

  if (isKeyPressed(VK_LEFT) || isKeyPressed('A'))       mPig.rateOfTurn = RateOfTurn;
  else if (isKeyPressed(VK_RIGHT) || isKeyPressed('D')) mPig.rateOfTurn = -RateOfTurn;
  else                                                  mPig.rateOfTurn = 0.0;

  if (isKeyPressed(VK_UP))
  {
  }
  if (isKeyPressed(VK_DOWN))
  {
  }

  if (isKeyPressed(VK_SPACE))
  {
    mPig.mouthOpen = true;
  }
  else
  {
    mPig.mouthOpen = false;
  }
  
  mPig.angle += (mPig.rateOfTurn) * deltaTime;

  if (mPig.angle > 360.0f)    mPig.angle -= 360.0f;
  else if (mPig.angle < 0.0f) mPig.angle += 360.0f;

  mParticleSystem.update(_deltaTime);

  checkFireBallCooldown(_deltaTime);

  checkFireballs(_deltaTime);
}

void RotatorApplication::renderGameOver(const CameraInfo& _cameraInfo)
{
  glColor3f(1.0f, 1.0f, 1.0f);
  glRasterPos2f(mWindowWidth / 4.0f, mWindowHeight / 2.0f);
  glText("Press the 'enter' key to start...");

  renderBorder();
}

void RotatorApplication::updateGameOver(double _deltaTime)
{
  if (isKeyPressed(VK_RETURN))
  {
    initGame();
  }
}

void RotatorApplication::renderNextWave(const Mgl::CameraInfo& _cameraInfo)
{
  char buf[256];
  sprintf_s(buf, 256, "Current Wave: %i", mCurrWave);
  
  glColor3f(1.0f, 1.0f, 1.0f);
  glRasterPos2f(mWindowWidth / 4.0f, mWindowHeight / 4.0f * 3.0f);
  glText(buf);

  glColor3f(1.0f, 1.0f, 1.0f);
  glRasterPos2f(mWindowWidth / 8.0f, mWindowHeight / 2.0f);
  glText("Press the 'enter' key to start next wave...");

  renderBorder();
}

void RotatorApplication::updateNextWave(double _deltaTime)
{
  if (isKeyPressed(VK_RETURN))
  {
    startNextWave();
  }
}

void RotatorApplication::initGame()
{
  if (mGameState != STATE_RUNNING)
  {
    glEnable(GL_TEXTURE_2D);

    mParticleSystem.clear();

    mCameraInfo.x = 0.0f;
    mCameraInfo.y = 0.0f;
    mCameraInfo.width  = (float)mWindowWidth;
    mCameraInfo.height = (float)mWindowHeight;

    mPig.size      = 10.0f;
    mPig.health    = 100.0f;
    mPig.healthMax = 100.0f;
    mPig.angle     = 0.0f;
    mPig.mouthOpen = false;

    setupNextWave();
    startNextWave();
  }
}


void RotatorApplication::setupNextWave()
{
  mFireBallCurrTime          = 0.0;
  mFireBallCooldown          = 5.0;
  mFireBallCooldownReduction = .5;

  mCurrWave++;

  mCurrWaveFireBallsLeft = mCurrWave * 2;

  mGameState = STATE_NEXT_WAVE;
}

void RotatorApplication::startNextWave()
{
  mGameState = STATE_RUNNING;
}

